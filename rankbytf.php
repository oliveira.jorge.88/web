<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter( "no");
$web->printHeader("Home", "index.php",
"Rank genes by TF","javascript:history.back()",
"Result", "");
$web->printBarMenu("Genes ranked by TF");

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

$localSpecies = $service->aSpecies();
$homoSpecies = (isset($_POST) && isset($_POST['species']) && $_POST['species'] != "0")? $_POST['species']: false;
$synteny = (isset($_POST) && isset($_POST['synteny']))? $_POST['synteny'] : 0;
$type = (isset($_POST) && isset($_POST['type']) && $_POST['type'] == 'doc')? 'documented': 'potential';
$dbTFs = (isset($_POST) && isset($_POST['alltfs']));
$regulators = (isset($_POST) && isset($_POST['regulators']))? $_POST['regulators']: false;

list($regs, $eviCode, $assocType, $ecgroup) = $web->getQueryParams(); // $regs is useless as potential regulations have been disabled

list($aORFGenes, $unknownORFs) = $service->user2Info($_POST['regulated'],$localSpecies); 
if (empty($aORFGenes)) $web->printErr("The inserted genes are not valid."); 

$unknownTFs = array();
if ($dbTFs) { $aTFs = $service->getTFs(); } 
else { list($aTFs,$unknownTFs) = $service->user2Info($regulators);  }
if (empty($aTFs)) $web->printErr("The inserted transcription factors are not valid."); 

?>
<table border="0" class="center" width="70%" summary="main content">
<tr><td class="align">
<?php
if ($regs['doc'])	print "<input type=\"hidden\" name=\"type\" value=\"doc\" />\n";
if ($eviCode['dir'])	print "<input type=\"hidden\" name=\"evidence\" value=\"dir\" />\n";
if ($eviCode['indir'])  print "<input type=\"hidden\" name=\"evidence\" value=\"indir\" />\n";
if ($eviCode['plus'])  print "<input type=\"hidden\" name=\"evidence\" value=\"plus\" />\n";
if ($eviCode['and'])   print "<input type=\"hidden\" name=\"evidence\" value=\"and\" />\n";
if ($assocType['pos']) print "<input type=\"hidden\" name=\"t_pos\" value=\"true\" />\n";
if ($assocType['neg']) print "<input type=\"hidden\" name=\"t_neg\" value=\"true\" />\n";
if ($ecgroup['big'])   print "<input type=\"hidden\" name=\"biggroup\" value=\"{$ecgroup['big']}\" />\n";
if ($ecgroup['sub'])   print "<input type=\"hidden\" name=\"subgroup\" value=\"{$ecgroup['sub']}\" />\n";
$nonRegORFs = array();
if ($homoSpecies) {

	$homoORFGenes = $service->getHomoORFInfo(array_keys($aORFGenes),$homoSpecies,$synteny);
	$orfidlist = [];
	foreach (array_keys($homoORFGenes) as $homoOrf) {
		foreach (array_keys($homoORFGenes[$homoOrf]['homo']['orfid']) as $ind) {
			$orfidlist[] = $homoORFGenes[$homoOrf]['homo']['orfid'][$ind];
		}
	}

	$homoTFs = array();
	$tfidlist = [];
	$homoTFs = $service->getHomoORFInfo(array_keys($aTFs),$homoSpecies,$synteny);
	foreach (array_keys($homoTFs) as $homoTF) {
		foreach (array_keys($homoTFs[$homoTF]['homo']['orfid']) as $jnd){
			$tfidlist[] = $homoTFs[$homoTF]['homo']['orfid'][$jnd];
		}
	}
	
	$homoRegs = $service->getRegulations($tfidlist, $orfidlist, $ecgroup, $eviCode, $assocType, true, true);
	list($retObj,$mapping) = $service->mapRegulations($homoRegs,$homoORFGenes,$homoTFs);

} else {
	if (count($aORFGenes)>0){
		list($retObj,$aTFids,$aTGids) = $service->getRegulations(array_keys($aTFs), array_keys($aORFGenes), $ecgroup, $eviCode, $assocType, false, true);
		foreach(array_keys($aORFGenes) as $orfid) { if (!in_array($orfid,$aTGids)) $nonRegORFs[$aORFGenes[$orfid]['orf']] = $aORFGenes[$orfid]['gene']; }
		$regStats = $service->regulations2stats($retObj,count($aORFGenes)); # INPUT: regulationsIDs, hypern
		$regCount = $service->callRhyper($regStats,$service->getTotalNumDBGenes(),count($aORFGenes)); 
	}
	else {
		$retObj     = [];
		$regCount   = [];
	}
}

include_once('inc/head.inc.php');
Head::printNonReg($aORFGenes, $nonRegORFs);
Head::printUnknown($unknownTFs, Head::TYPE_tf); // TFs
Head::printUnknown($unknownORFs);

if ($homoSpecies) {
	$web->printHomologyWarning($homoSpecies, $type);
	$web->printSyntenyWarn($synteny);

} else if ($regs['doc']) {
	$evidencetxt = false;
	if ($eviCode['dir']) {
		$evidencetxt .= "DNA binding evidence";
	} elseif ($eviCode['indir']) {
		$evidencetxt .= "Expression evidence";
	} elseif ($eviCode['and']) {
		$evidencetxt .= "Simultaneous DNA binding and expression evidence";
	} elseif ($eviCode['plus']) {
		$evidencetxt .= "DNA binding plus expression evidence";
	}
	print "<p>Documented regulations are filtered by:<br/>&nbsp;- $evidencetxt";
	if ($assocType['pos'] || $assocType['neg']) {
		print ";<br/>&nbsp;- TF acting as " . ($assocType['pos']?"activator":"");
		print ($assocType['neg']?($assocType['pos']?" or ":"") . "inhibitor":"");
	}
	print ".</p>";
	if ($ecgroup['big']) {
		print "<p>Environmental conditions filtered by {$ecgroup['big']}";
		if ($ecgroup['sub']) print ", in particular {$ecgroup['sub']}";
		print ".</p>";
	}
}

##########################
# Normal ranking algorithm

if ($homoSpecies && empty($retObj) || !$homoSpecies && empty($regCount)) {
	print "<p><span class=\"error\">No regulatory associations found!!</span></p>";
} else {
	$aTargetG = array();
	
	$tfs = '';
	$targets = '';
	foreach (array_keys($retObj) as $tfid) {
		$tf = $aTFs[$tfid]['prot'];
        	$tfs .= $tf." ";
		foreach ($retObj[$tfid] as $k => $orfid) {
			$o = $aORFGenes[$orfid]['orf'];
			$g = $aORFGenes[$orfid]['gene'];
			$targets .= $web->Gene($g,$o);
		}
	}

?>

<table border="1" summary="TFs" id="ranktfs" class="tablesorter">
<thead>
	<tr>
		<th>Transcription&nbsp;Factor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php if ($homoSpecies) { print "<br/>(&rarr;&nbsp;Homologous&nbsp;gene)"; } ?></th>
		<th title="Percentage of TF target genes to the user set">% in user set&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
<?php if (!$homoSpecies) { ?>
		<th title="Percentage of TF target genes relative to the whole genome">% in <?=$web->_dbname?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
		<th title="p-value (Hypergeometric distribution w/ Bonferroni correction): TF target genes in the user set relative to the <?=$web->_dbname?> described">p-value&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
<?php } ?>
		<th>Target ORF/Genes
<?php if ($homoSpecies) { print "<br/>(&rarr; Homologous gene)"; } ?></th>
</th>
	</tr>
</thead>
<tbody>
<?php
$csv = "";
$i = 0;

if ($homoSpecies) {
	foreach (array_keys($retObj) as $tfid) {
		$tf = $aTFs[$tfid]['prot'];
?>
	<tr>
		<td><input type="checkbox" name="tf[]" value="<?=$tf?>" checked="checked"/>&nbsp;
			<a href="view.php?existing=protein&amp;proteinname=<?=$tf?>"><?=$tf?></a>
<?php
			if (isset($mapping[$tfid])) {
				print " (&rarr; ".$web->linkGene($mapping[$tfid]['gene'],$mapping[$tfid]['orf'],false,$homoSpecies).")";
			}
?>
</td>
		<td align="right">
<?php
		printf("%.2f", count($retObj[$tfid]) * 100 / count($aORFGenes));
?>
		%</td><td align="right">
<?php
		$csv .= $tf;

		// Javascript submenus
		$aTGsForm = array();
		foreach ($retObj[$tfid] as $k => $orfid) {
                        $o = $aORFGenes[$orfid]['orf'];
                        $g = $aORFGenes[$orfid]['gene'];
			$aTGsForm[] = $g;
		}
		$items[0] = array("ranktf",  "Rank by TF", "formrankbytf.php", 1, 1);
		$items[1] = array("tfs",     "Search for TFs", "formfindregulators.php", 1, 1);
		$items[2] = array("tgs",     "Search for Genes", "formfindregulated.php", 1, 1);
		$items[3] = array("regs",    "Search for Associations", "formregassociations.php", 1, 1);
		$items[4] = array("orfgene", "ORF List &hArr; Gene List", "formorftogene.php", 0, 1);
		$items[5] = array("regviz",  "Interactive Network Image", "regnetviz.php", 1, 1);
		$web->jsmenuDraw("doc".$tf, $items, array($tf), $aTGsForm,$regs, $eviCode, $ecgroup, $assocType);

		$toPrint = "<br/>";
		foreach ($retObj[$tfid] as $k => $orfid) {
		    $o = $aORFGenes[$orfid]['orf'];
		    $g = $aORFGenes[$orfid]['gene'];
		    $aTargetG[$o] = $web->Gene($g,$o);
		    $toPrint .= $web->linkGene($g,$o)."&nbsp;(&rarr; ".$web->linkGene($mapping[$orfid]['gene'],$mapping[$orfid]['orf'],false,$homoSpecies).")<br/>";
		    $csv .= "\t".$web->Gene($g,$o);
	    	}
	    	print $toPrint."\t\t</td>\n\t</tr>";
	    	$i++;
	    	$csv .= "\n";
	}

} else { // not homoSpecies

	#Bonferroni correction
	$pv1 = 0.01 / count($regCount);
	$pv2 = 0.05 / count($regCount);
	foreach (array_keys($regCount) as $tfid) {
		$tf = $aTFs[$tfid]['prot'];
?>
	<tr>
		<td><input type="checkbox" name="tf[]" value="<?=$tf?>" checked="checked"/>&nbsp;
			<a href="view.php?existing=protein&amp;proteinname=<?=$tf?>"><?=$tf?></a></td>
		<td align="right">
<?php
		printf("%.2f", $regCount[$tfid]['setPer'] * 100);
		print "%</td><td align=\"right\">";
		printf("%.2f", $regCount[$tfid]['dbPer'] * 100);
		print "%</td><td align=\"right\"";
		$pvalue = $regCount[$tfid]['pvalue'];
		if ($pvalue <= $pv1) {
			print " class=\"pvalue1\">";
		} elseif ($pvalue <= $pv2) {
			print " class=\"pvalue2\">";
		} else {
			print " class=\"pvalue3\">";
		}
		printf("%.15f", $regCount[$tfid]['pvalue']);
		print "</td><td>";
		$csv .= $tf;

		// Javascript submenus
		$aTGsForm = array();
		foreach ($retObj[$tfid] as $k => $orfid) {
			$o = $aORFGenes[$orfid]['orf'];
                   	$g = $aORFGenes[$orfid]['gene'];
			$aTGsForm[] = $g;
		}
		$items[0] = array("ranktf",  "Rank by TF", "formrankbytf.php", 1, 1);
		$items[1] = array("tfs",     "Search for TFs", "formfindregulators.php", 1, 1);
		$items[2] = array("tgs",     "Search for Genes", "formfindregulated.php", 1, 1);
		$items[3] = array("regs",    "Search for Associations", "formregassociations.php", 1, 1);
		$items[4] = array("orfgene", "ORF List &hArr; Gene List", "formorftogene.php", 0, 1);
		$items[5] = array("regviz",  "Interactive Network Image", "regnetviz.php", 1, 1);
		$web->jsmenuDraw("doc".$tf, $items, array($tf), $aTGsForm,$regs, $eviCode, $ecgroup, $assocType);

		$toPrint = "";
		$link = "\t\t<a href=\"view.php?existing=locus&amp;orfname=";
		foreach ($retObj[$tfid] as $k => $orfid) {
			$o = $aORFGenes[$orfid]['orf'];
			$g = $aORFGenes[$orfid]['gene'];
			if (isset($g)) {
				$aTargetG[$g] = $g;
				$toPrint .= $link.$o."\">$g</a>\n";
				$csv .= "\t$g";
			} else {
				$aTargetG[$o] = $o;
				$toPrint .= $link.$o."\">$o</a>\n";
				$csv .= "\t$o";
			}
		}
		print $toPrint."\t\t</td>\n\t</tr>";
		$i++;
		$csv .= "\n";
	}
}

$pathtmp = $web->getProp("path.tmp.local");
$fname = "groupbytf_".rand() . ".csv";
file_put_contents($pathtmp . $fname, $csv);
$lcsv = $web->getProp("path.tmp.url") . $fname;
?>
	</tbody>
</table>

<form method="post" id="formtf" action="regnetviz.php">
<input type="hidden" name="origin" value="rankbytf" />

<?php
if ($regs['doc'])        print "<input type=\"hidden\" name=\"type\" value=\"doc\" />\n";
if ($eviCode['dir'])     print "<input type=\"hidden\" name=\"evidence\" value=\"dir\" />\n";
if ($eviCode['indir'])   print "<input type=\"hidden\" name=\"evidence\" value=\"indir\" />\n";
if ($eviCode['plus'])    print "<input type=\"hidden\" name=\"evidence\" value=\"plus\" />\n";
if ($eviCode['and'])     print "<input type=\"hidden\" name=\"evidence\" value=\"and\" />\n";
if ($assocType['pos'])   print "<input type=\"hidden\" name=\"t_pos\" value=\"true\" />\n";
if ($assocType['neg'])   print "<input type=\"hidden\" name=\"t_neg\" value=\"true\" />\n";
if ($ecgroup['big'])     print "<input type=\"hidden\" name=\"biggroup\" value=\"{$ecgroup['big']}\" />\n";
if ($ecgroup['sub'])     print "<input type=\"hidden\" name=\"subgroup\" value=\"{$ecgroup['sub']}\" />\n";
print "<input type=\"hidden\" name=\"synteny\" value=\"$synteny\" />\n";

?>
<p>
Select
	<a href="javascript:void(0);" onclick="javascript:selectAll()">All</a> /
  <a href="javascript:void(0);" onclick="javascript:selectNone()">None</a>
of the TFs to be considered in image:<br/>
<input type="radio" name="imagetype" value="static" />Static image extension:&nbsp;<select name="ext">
   <option value="png">PNG</option>
   <option value="svg">SVG</option>
   <option value="jpg">JPG</option>
   <option value="gif">GIF</option>
</select><br/>
<input type="radio" name="imagetype" value="d3jscirc" checked />Interactive image: circular layout<br/>
<input type="radio" name="imagetype" value="d3jsedge" checked />Interactive image: force-directed layout
&nbsp;&nbsp; <img src="images/new.gif"/><br/>
<input type="submit" name="submit" value="Visualize Network" onclick="return validate();" />
</p>
<?php
# THIS SEEMS TO BE USELESS
#	foreach ($aORFGenes as $o => $g) { //FIX HERE
#		print "<input type=\"hidden\" name=\"inserted_$g\" />\n";
#	}
	foreach ($aTargetG as $g) {
		print "<input type=\"hidden\" name=\"target_$g\" />\n";
	}
	if ($homoSpecies) {
		print "<input type=\"hidden\" name=\"homology\" value=\"$homoSpecies\" />\n";
	}
} ?>
</td></tr>
<tr><td class="align">
<input type="reset" value="Go Back" onclick="javascript:history.back()" />
<?php

if ($homoSpecies && !empty($retObj) || !$homoSpecies && !empty($regCount)) {
	print "<small>Download as <a href=\"$lcsv\">csv</a> file.</small>";
}
?>
</td></tr></table>
</form>
<?php
$web->printFooter();
?>
<script type="text/javascript" src="jquery/jquery-latest.js"></script>
<script type="text/javascript" src="jquery/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
$(document).ready(function() 
{ 
	$("#ranktfs").tablesorter( {
<?php if ($homoSpecies) { ?>
		sortList: [[1,1]]
<?php } else { ?>
		sortList: [[3,0]]
<?php } ?>
//		headers: {
//			1: {sorter: "percent"},
//			2: {sorter: "percent"},
//			3: {sorter: "floating"}
//		}
	} ); 
} 
); 
</script>
<script type="text/javascript" src="inc/menu.js"></script>
<script type="text/javascript">
function selectAll() {
  var elems = document.getElementsByName('tf[]');
  for (var i=0; i < elems.length; i++) {
    if (elems[i].type=="checkbox") {
      elems[i].checked = true;
    }
  }
}
function selectNone() {
  var elems = document.getElementsByName('tf[]');
  for (var i=0; i < elems.length; i++) {
    if (elems[i].type=="checkbox") {
      elems[i].checked = false; 
    }
  }
} 
function validate() {
  var ok = false;
  var formtf = document.getElementById('formtf');
  var elems = document.getElementsByName('tf[]');
  for (var i=0; i < elems.length; i++) {
    if (elems[i].type == "checkbox" && elems[i].checked) {
        ok = true;
        var newinput = document.createElement('input');
        newinput.type  = 'hidden';
        newinput.name  = 'tf_' + elems[i].value;
        newinput.value = 'tf_' + elems[i].value;
        formtf.appendChild(newinput);
      }
  }
	if (!ok) alert('Please select at least one TF to be considered in the image.');
	return ok;
}
</script>
<?php
$web->closePage();
?>
