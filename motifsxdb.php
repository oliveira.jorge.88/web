<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter("no");
$web->printHeader("Home", "index.php",
"Search by DNA motif", "javascript:history.back()",
"Result", "");
$web->printBarMenu("Search described TF binding sites by a given DNA Motif");

$motif = (isset($_POST) && isset($_POST['motif']))? trim($_POST['motif']): false;
$subst = (isset($_POST) && isset($_POST['subst']))? $_POST['subst']: 0;

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

list($matchesInDB1, $matchesInDB2, $motifsDB) =	$service->searchMotifOnDB($motif, $subst);

// function to be passed as an argument to arraymap 
function inc($input) { return ($input + 1); }
?>
<table border="0" class="center" width="70%" summary="main content">
<tr><td class="align">

Considering <span class="error"><?=$subst?></span> substitutions.<br/>

<table border="1" summary="motifs">
<tr><th colspan="4" class="center">Motif '<?=$motif?>' inside <?=$web->_dbname?> binding sites</th></tr>
<?php
if (count($matchesInDB1)>0) {
?>
	<tr><th>Binding Site</th><th>TF</th><th>Strand</th><th>Position</th></tr>
<?php
	foreach (array_keys($matchesInDB1) as $m) {
		$prot = "";
		foreach ($motifsDB[$m][4] as $p) {
			$prot .= "<a href=\"view.php?existing=protein&amp;proteinname=";
			$prot .= "$p\">$p</a>&nbsp;";
		}
		if (isset($matchesInDB1[$m]['fwr'])) {
			print "<tr><td>$m</td><td>$prot</td><td>Fwr</td><td>Pos:&nbsp;". join(",&nbsp;", array_map("inc", $matchesInDB1[$m]['fwr'])) . "</td></tr>";
		}
		if (isset($matchesInDB1[$m]['rev'])) {
			print "<tr><td>".$motifsDB[$m][3] ."</td><td>$prot</td><td>Rev</td><td>Pos:&nbsp;". join(",&nbsp;", array_map("inc", $matchesInDB1[$m]['rev'])) . "</td></tr>";
		}
	}
} else {
    print "<tr><td colspan=\"4\">No matches found!</td></tr>";
}
?>
	<tr><td colspan="4">&nbsp;</td></tr>
	<tr><th colspan="4" class="center"><?=$web->_dbname?> binding sites inside inserted motif '<?=$motif?>'</th></tr>
<?php
if (count($matchesInDB2)>0) {
?>
	<tr><th>Binding Site</th><th>TF</th><th>Strand</th><th>Position</th></tr>
<?php
	foreach (array_keys($matchesInDB2) as $m) {
		$prot = "";
		foreach ($motifsDB[$m][4] as $p) {
			$prot .= "<a href=\"view.php?existing=protein&amp;proteinname=";
			$prot .= "$p\">$p</a>&nbsp;";
		}
		if (isset($matchesInDB2[$m]['fwr'])) {
			print "<tr><td>$m</td><td>$prot</td><td>Fwr</td><td>Pos:&nbsp;". join(",&nbsp;", array_map("inc", $matchesInDB2[$m]['fwr'])) . "</td></tr>";
		}
		if (isset($matchesInDB2[$m]['rev'])) {
			print "<tr><td>".$motifsDB[$m][3] ."</td><td>$prot</td><td>Rev</td><td>Pos:&nbsp;". join(",&nbsp;", array_map("inc", $matchesInDB2[$m]['rev'])) . "</td></tr>";
		}
	}
} else {
    print "<tr><td colspan=\"4\">No matches found!</td></tr>";
}
?>
</table>
</td></tr>
<tr><td class="align">
<input type="reset" value="Go Back" onclick="javascript:history.back()"/>
</td></tr>
</table>
<?php
$web->printFooter();
$web->closePage();
?>
