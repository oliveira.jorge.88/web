<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Promoter Analysis", "");
$web->printBarMenu("Cross species comparison: Venn Diagram");

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

$homologSpecies = $service->getHomologSpecies();
$currSpecies = $service->aSpecies();

?>

<form method="post" action="venngenes.php">
	<table class="center" border="0" summary="main content">
	  <tr>
		<td>
		<table border="1" summary="main form">
		  <tr>
			<th>Calculate all combinations using species:</th>
		  </tr>
		  <tr>
			<td style="background-color: #DDDDDD" class="align" colspan="2">
			<br>
<?php
foreach ($currSpecies as $sp) {
	# type hidden is needed since disabled items don't pass to the form
	print "<input type=\"checkbox\" name=\"currspecies[]\" value=\"$sp\" checked=\"checked\"/>$sp</option><br/>\n";
}
foreach ($homologSpecies as $sp) {
  print "<input type=\"checkbox\" name=\"homospecies[]\"  value=\"$sp\" >$sp</option><br/>\n";
}
?>
<br/>
<p> NOTE: Select a maximum of 5 species</p>
<p> At least one of the current database species must be selected</p>
<br/>
	    </td>
		  </tr>
        <tr>
                <td colspan="2">
<?php
$web->printSyntenyForm();
?>
                </td>
        </tr>
		</table>
	  </td>
	</tr>
	<tr>
		<td align="left">
			<input type="submit" name="submit" value="Search" onclick="return validate();"/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" name="clear" value="Clear"/>
		</td>
		<td align="right">
			<a href="help_promanalysis.php" title="Help"><img style="border:0" src="../images/information.gif" alt="help icon" /></a>
		</td>
	</tr>
	    </table>
    </form>

<?php
$web->printFooter();
?>
<script type="text/javascript">
function validate() {
	var counter=0;
	for (var i=0;i<document.getElementsByName('homospecies[]').length;i++){
		if (document.getElementsByName('homospecies[]')[i].checked){
			counter++;
			if (counter>4){
				alert('Please select a maximum of 5 species');
				return false;
			}
		}
        }
        for (var i=0;i<document.getElementsByName('currspecies[]').length;i++){
                if (document.getElementsByName('currspecies[]')[i].checked){
                        return true;
                }
        }
        alert('Please enter at least one species belonging to the current database');
        return false;
}
</script>
<?php
$web->closePage();
?>
