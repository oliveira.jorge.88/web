<?php
include_once('../inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Contact", "");
?>

<div class="body">
  <table class="body">
    <tr>
      <td width="20%">&nbsp;</td>
      <td class="justify">

<p> 
  <div class="title">Contacts</div>
</p>

<p>&nbsp;</p>

<center>

<h3>Platform</h3>

Any question regarding the CommunityYeastract platform contact <?php $web->printJSEmail("contact", "yeastract.com"); ?>.

<br/><br/><br/>

<h3>CommunityYeastract was developed by:</h3>
</center>

<table class="center" border="0" width="85%" summary="contributors list">
	<tr>
		<td class="center">
		<b>Pedro T. Monteiro, PhD</b><br/>
		Associate Professor IST<br/>
		<a onclick="window.open(this.href); return false;" href="http://pedromonteiro.org">Homepage</a>
		</td>
		<td class="center">
		<b>Jorge Oliveira, PhD</b><br/>
		Post-doc researcher<br/>
		<a onclick="window.open(this.href); return false;" href="https://web.tecnico.ulisboa.pt/jorge.oliveira/">Homepage</a>
		</td>
	</tr>
</table>

</p>

      <td width="20%">&nbsp;</td>
    </tr>
  </table>
</div>


<div class="body">
  <table class="body">
    <tr><td class="content>&nbsp;
<?php
$web->printFooter();
$web->closePage();
?>
