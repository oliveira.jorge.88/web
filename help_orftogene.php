<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
	"Help", "help_index.php",
	"ORF list &hArr; Gene list", "");
$web->printBarMenu("Help - ORF name list &hArr; Gene name list");
?>

<table border="0" class="center" style="width:75%" summary="main content">
<tr><td>

<p>
This feature is meant specifically to convert a list of ORF (Open Reading Frame) names into a list of updated gene names or vice-versa. When an ORF has no attributed gene name, the ORF name appears in the gene name list.
</p>

<p>
Requires as input a list of gene and/or ORF names.
</p>

<p>
Returns the list of corresponding gene and/or ORF names.
</p>

</td></tr>
</table>

<?php
$web->printFooter();
$web->closePage();
?>
