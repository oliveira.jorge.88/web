<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
	"Help", "help_index.php",
	"Search for Genes", "");
$web->printBarMenu("Help - Search for Genes");
?>

<table border="0" class="center" style="width:75%" summary="main content">
<tr><td>

<p>
This query allows the user to search for the genes that are regulated by the given Transcription Factors (TFs).
</p>

<p>
A list of transcription factors (TFs) is required as input.
</p>

<p>
The user may either search for all the genes documented as being regulated by the input TFs, or for potentially regulated genes (based on the existence of a TF binding site in their promoter region).
The combined results of the two aforementioned searches can be obtained by selecting the option '<i>Both</i>'.
</p>

<p>
Furthermore, the user may restrict its search to the regulatory associations identified based on direct or indirect evidences.
Direct Evidence was considered to be provided through experiments such as Chromatine ImmunoPrecipitation (ChIP), ChIP-on-chip and Electrophoretic Mobility Shift Assay (EMSA), that prove the direct binding of the TF to the target gene's promoter region, or such as the analysis of the effect on target-gene expression of the site-directed mutation of the TF binding site in its promoter region, which strongly suggests that the TF interacts with that specific target promoter.
The classification Indirect Evidence was attributed to experiments such as the comparative analysis of gene expression changes occurring in response to the deletion, mutation or over-expression of a given TF.
</p>

<p>
The outcome of this search is a table containing the list of TFs and the genes regulated by each of them.
</p>

</td></tr>
</table>

<?php
$web->printFooter();
$web->closePage();
?>
