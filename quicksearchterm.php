<?php
if (!isset($_GET)) header("Location: index.php");

$type = false;
$term = false;

if (isset($_GET['orfname'])) {
    $type = "orfname";
    $term = $_GET['orfname'];
} elseif (isset($_GET['genename'])) {
    $type = "genename";
    $term = $_GET['genename'];
} elseif (isset($_GET['protein'])) {
    $type = "protein";
    $term = $_GET['protein'];
} elseif (isset($_GET['alias'])) {
    $type = "alias";
    $term = $_GET['alias'];
} elseif (isset($_GET['description'])) {
    $type = "description";
    $term = $_GET['description'];
} elseif (isset($_GET['pubmedid'])) {
	$type = "pubmedid";
	$term = $_GET['pubmedid'];
} elseif (isset($_GET['authors'])) {
    $type = "authors";
    $term = $_GET['authors'];
} elseif (isset($_GET['process'])) {
    $type = "process";
    $term = $_GET['process'];
} elseif (isset($_GET['function'])) {
    $type = "function";
    $term = $_GET['function'];
} elseif (isset($_GET['component'])) {
    $type = "component";
    $term = $_GET['component'];
}

if (!$type) {
    header("Location: index.php");
}

include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

$retObj = $service->quickSearchTerm($type, $term);

$web->printHeader(# css, Page Title, (Barname, Barlink)....
"Home", "index.php",
"Quick Search Result", "");
$web->printBarMenu("Quick Search Result");

?>
<table summary="main content">
<tr><td class="align">
<table border="1" summary="data">
<?php

if ($type == "orfname") {
?>
	<tr><th>ORF/Gene Name</th><th>Description</th></tr>
<?php
	$sgd = ""; //link 2 sgd
	$link = "<a href=\"view.php?existing=locus&amp;orfname=";
    foreach ($retObj as $elem) {
        print "<tr><td>$link{$elem['orfname']}\">" . $elem['orfname'] .
        	"/" . $elem['genename'] . "</a></td><td>{$elem['description']}</td></tr>\n";
    }

} elseif ($type == "genename") {
?>
	<tr><th>ORF/Gene Name</th><th>Description</th></tr>
<?php
	$sgd = ""; //link 2 sgd
	$link = "<a href=\"view.php?existing=locus&amp;orfname=";
    foreach ($retObj as $elem) {
        print "<tr><td>$link{$elem['orfname']}\">" . $elem['orfname'] .
        	"/" . $elem['genename'] . "</a></td><td>{$elem['description']}</td></tr>\n";
    }

} elseif ($type == "protein") {
?>
	<tr><th>Protein Name</th><th>Description</th></tr>
<?php
	$sgd = ""; //link 2 sgd
	$link = "<a href=\"view.php?existing=protein&amp;proteinname=";
    foreach ($retObj as $elem) {
        print "<tr><td>$link{$elem['protein']}\">" . $elem['protein'] .
        	"</a></td><td>{$elem['description']}</td></tr>\n";
    }

} elseif ($type == "alias") {
?>
	<tr><th>ORF/Gene Name</th><th>Alias</th><th>Description</th></tr>
<?php
	$sgd = ""; //link 2 sgd
	$link = "<a href=\"view.php?existing=locus&amp;orfname=";
    foreach ($retObj as $elem) {
        $alias = join(", ", $elem['alias']);
        print "<tr><td>$link{$elem['orfname']}\">" . $elem['orfname'] .
        	"/" . $elem['genename'] . "</a></td><td>$alias</td><td>{$elem['description']}</td></tr>\n";
    }
        
} elseif ($type == "description") {
?>
	<tr><th>ORF/Gene Name</th><th>Description</th></tr>
<?php
	$sgd = ""; //link 2 sgd
	$link = "<a href=\"view.php?existing=locus&amp;orfname=";
    foreach ($retObj as $elem) {
        print "<tr><td>$link{$elem['orfname']}\">" . $elem['orfname'] .
        	"/" . $elem['genename'] . "</a></td><td>{$elem['description']}</td></tr>\n";
    }
        
} elseif ($type == "authors" || $type == "pubmedid") {
	include_once('service/View.php');
?>
	<tr><th>Reference</th><th>PubmedID</th><th>Link</th></tr>
<?php
	$link = "https://www.ncbi.nlm.nih.gov/pubmed/";
	$img = "&nbsp;<img src=\"../images/external.png\" alt=\"external link\" />";
	foreach ($retObj as $pubmed => $refArray) {
    	$abbrev = View::printReference($retObj[$pubmed]);
	    print "<tr><td>$abbrev</td><td>$pubmed</td><td><a href=\"$link$pubmed\">Pubmed$img</a></td></tr>\n";
	}

} elseif ($type == "process" || $type == "function" || $type == "component") {
    if ($type == "process") $title = "Biological Process";
    elseif ($type == "function") $title = "Molecular Function";
    elseif ($type == "component") $title = "Cellular Component";
?>
	<tr><th><?=$title?> - GO terms</th></tr>
		<tr><td>
<?php
		include_once('service/View.php');
		print View::printGOTerms($retObj, false);
}
?>
		</td></tr>
</table>
</td></tr>
<tr><td class="align">
<input type="reset" value="Go Back" onclick="javascript:history.back()" />
</td></tr></table>

<?php
$web->printFooter();
$web->closePage();
?>
