<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter("no");
$web->printHeader("Home", "index.php",
"Retrieve Upstream Sequence", "javascript:history.back()",
"Result", "");
$web->printBarMenu("Upstream Sequence");

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

$pos_to = (isset($_POST) && isset($_POST['to']))? $_POST['to']: -1;
$pos_fr = (isset($_POST) && isset($_POST['from']))? $_POST['from']: -1000;
$genes = (isset($_POST['genes']))?$_POST['genes']:'';

$localSpecies = $service->aSpecies();

list($aOrfGenes,$unknown) = $service->user2Info($genes,$localSpecies);

if (empty($aOrfGenes)){ $web->printErr("The inserted genes are not valid."); $seqlist = [];}
else { $aPromoters = $service->upstreamSeq(array_keys($aOrfGenes), NULL, $pos_fr, $pos_to); }

// PRETTYFY
$seqlist = [];
$size = ($pos_to - $pos_fr)+1;
$top  = "   upstream sequence, from $pos_fr to $pos_to, size $size";
foreach($aPromoters as $orfid=>$seq){	
	$lines = array();
	for ($i=0; strlen($seq)>0; $i++) {
		$lines[$i] = substr($seq, 0, 60);
		$seq = substr($seq, 60);
	}
	$g   = $aOrfGenes[$orfid]['gene'];
        $orf = $aOrfGenes[$orfid]['orf'];
	if ($g == "Uncharacterized") $g = $orf;
	array_unshift($lines, ">$g $top");
	$seqlist[$orf] = join("\n", $lines);
}

?>
<table border="0" class="center" width="70%" summary="main content">
	<tr><td class="align">
<?php
include_once('inc/head.inc.php');
Head::printUnknown($unknown);

if (count($seqlist) > 0) {
	if (isset($_POST['vehicle']) && $_POST['vehicle'] == "file") {

		$seqlisttxt = "";
		foreach ($seqlist as $seq) {
			$seqlisttxt .= trim($seq)."\n";
		}
		$path = $web->getProp("path.tmp.local");
		$fname = "seq_".rand() . ".fasta";
		file_put_contents($path . $fname, $seqlisttxt);
?>
	The requested sequences can be obtained in the following file:
  <a href="<?=$web->getProp("path.tmp.url") . $fname?>"><?=$fname?></a>
	<br/>
	<br/>
	<br/>
	Thank you for using <?=$web->getProp("db.$web->_dbname.short")?> Upstream Sequence Retrieval Functionality
	<br/>
<?php
    } else {
?>
<pre>
<?php
		foreach ($seqlist as $seq) {
		    $s = trim($seq);
		    if (strlen($s)>0) {
		    	print "$s\n";
		    }
		}
?>
</pre>
<?php
    }
} else {
    print "<p><span class=\"error\">No promoter sequences found!</span></p>";
}
?>
	</td></tr>
	<tr><td class="align">
	<input type="reset" value="Go Back" onclick="javascript:history.back()" />
	</td></tr>
</table>
<?php
$web->printFooter();
$web->closePage();
?>
