<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
	"Help", "help_index.php",
	"Rank by TF", "");
$web->printBarMenu("Help - Rank by TF");
?>

<table border="0" class="center" style="width:75%" summary="main content">
<tr><td>

<p>
This query allows the user to group a given list of genes (e.g., a set of co-activated genes from a microarray experiment) according to the TFs which are their documented or potential regulators, ranking those TFs according to two possible criteria: % of genes associated to each TF; or enrichment on the TF targets in the dataset, evaluated based on a p-value.
</p>

<p>
This query requires a list of genes and, optionally, a list of transcription factors as input.
</p>

<p>
The TFs considered in this query can be either those in the input list, or all the TFs in the <?=$web->_dbname?> database (by selecting the option <i>Check for all TFs</i>).
Either documented or potential regulations can be considered (by selecting <i>Documented Regulations</i> or <i>Potential Regulations</i>, respectively).
Furthermore, for each established regulon, the percentage value representing the proportion of genes regulated by each TF can be calculated relative to:

<ol>
	<li>The total number of genes in the input list; this may identify the TFs involved in the regulation of the given gene list;</li>
	<li>The number of genes, in the whole genome, documented as being regulated by the same TF; this may identify the TF networks predominantly involved in the regulation of the referred gene list.</li>
	<li>A p-value the represents the enrichment of the TF regulon in our dataset, when compared to the genome.</li>
</ol>
</p>

<p>
Furthermore, the user may restrict its search to the regulatory associations identified based on direct or indirect evidences.
Direct Evidence was considered to be provided through experiments such as Chromatine ImmunoPrecipitation (ChIP), ChIP-on-chip and Electrophoretic Mobility Shift Assay (EMSA), that prove the direct binding of the TF to the target gene's promoter region, or such as the analysis of the effect on target-gene expression of the site-directed mutation of the TF binding site in its promoter region, which strongly suggests that the TF interacts with that specific target promoter.
The classification Indirect Evidence was attributed to experiments such as the comparative analysis of gene expression changes occurring in response to the deletion, mutation or over-expression of a given TF.
</p>

<p>
Depending on the options selected, the output is a table containing the input genes, grouped by TF and ordered by the percentage of genes regulated by the respective TF or a downloadable image of the regulatory network.
</p>

</td></tr>
</table>

<?php
$web->printFooter();
$web->closePage();
?>
