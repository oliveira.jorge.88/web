<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Cross Strain Comparison", "");
$web->printBarMenu("Cross Strain Comparison");

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

$homologSpecies = $service->getHomologSpecies();
$currSpecies = $service->aSpecies();
?>

<h4 class="center">by target genes</h4>
<form method="post" action="proms.php">
	<table class="center" border="0" summary="main content">
	  <tr>
		<td colspan="3">
		<table border="1" summary="main form">
		  <tr>
			<th colspan="2"></th>
			<th>Transcription Factors</th>
			<th>Regulated Genes</th>
		  </tr>
		  <tr>
			<td style="background-color: #DDDDDD" class="align" colspan="2">
			<br>
			<strong>Compare genes with orthology in strains:</strong>
			<br/>
<?php
foreach ($currSpecies as $sp) {
# print "<input type=\"checkbox\" value=\"$sp\" name=\"fakecurrspecies[]\" checked=\"checked\" disabled>$sp</option><br/>\n";
	# type hidden is needed since disabled items don't pass to the form
	print "<input type=\"checkbox\" name=\"currspecies[]\" value=\"$sp\" checked=\"checked\"/>$sp</option><br/>\n";
}
foreach ($homologSpecies as $sp) {
  print "<input type=\"checkbox\" name=\"homospecies[]\"  value=\"$sp\" checked=\"checked\">$sp</option><br/>\n";
}
?>
	    </td>
		<td style="background-color: #DDDDDD">
<br/>
		                        <strong>Consider TF binding sites from strain:</strong><br/>
                        <select name="tf_species">
<?php
foreach ($currSpecies as $sp) {
        print "<option value=\"$sp\">$sp</option>\n";
        }
foreach ($homologSpecies as $sp) {
  print "<option value=\"$sp\">$sp</option>\n";
}
?>
      </select>
                        <br/>
			<br/>
                        <textarea rows="11" cols="30" name="regulators" disabled=true><?=$web->post2Form("formtfs")?></textarea><br/>
                                <input type="checkbox" name="alltfs" checked="checked" onclick="javascript:toggleAllTfs()"/>Check for all TFs
                        </td>
			<br/>
			<td style="background-color: #DDDDDD">
			<textarea rows="15" cols="15" name="genes"><?=$web->post2Form("formtgs")?></textarea>
			</td>
		  </tr>
		</table>
	  </td>
	</tr>
	<tr>
		<td align="left">
			<input type="submit" name="submit" value="Search" onclick="return validate();"/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" name="clear" value="Clear"/>
		</td>
		<td align="right">
<?php $web->strainSampleIcons(array('genes' => 'tgs')); ?>
			<a href="help_crossstrain.php" title="Help"><img style="border:0" src="../images/information.gif" alt="help icon" /></a>
		</td>
	</tr>
	    </table>
    </form>

<?php
$web->printFooter();
?>
<script type="text/javascript">
function validate() {
	if (document.getElementsByName('genes')[0].value.replace(/^\s*$/g,"") == '') {
		alert('Please enter at least one ORF/gene name.');
		return false;
	}
        for (var i=0;i<document.getElementsByName('currspecies[]').length;i++){
		if (document.getElementsByName('currspecies[]')[i].checked){
			return alertEmptyTFsGenes() && checkTF();
		}
	}
        alert('Please enter at least one species belonging to the current database');
        return false;
}
function toggleAllTfs() {
        if (document.getElementsByName('alltfs')[0].checked) {
                document.getElementsByName('regulators')[0].disabled = true;
        } else {
                document.getElementsByName('regulators')[0].disabled = false;
        }
}
function checkTF() {
        if ((document.getElementsByName('evidence')[1].checked ||
                         document.getElementsByName('evidence')[2].checked) &&
                  (!document.getElementsByName('t_pos')[0].checked &&
                         !document.getElementsByName('t_neg')[0].checked)) {
                alert('Since you selected expression evidence, you must select at least one role for TFs.');
                return false;
        } else {
                return true;
        }
}
function alertEmptyTFsGenes() {
        if ((document.getElementsByName('regulators')[0].value.replace(/^\s*$/g,"") === "" && !document.getElementsByName('alltfs')[0].checked) ||
            (document.getElementsByName('regulated')[0].value.replace(/^\s*$/g,"") === "")) {
                alert('Please enter transcription factors and/or genes to group genes by TF.');
                return false;
        }
        return true;
}

</script>
<?php
$web->closePage();
?>
