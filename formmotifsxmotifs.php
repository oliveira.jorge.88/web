<?php
# LEGACY - to delete ?
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Search motifs on motifs", "");
$web->printBarMenu("Search a list of DNA motifs on another list of DNA motifs");
?>
<form method="post" action="motifsxmotifs.php"> 
<table class="center" border="0" summary="main content">
	<tr><td valign="bottom">
		<table border="1" summary="form list1">
			<tr><th>DNA Motif List 1</th></tr>
			<tr><td style="background-color: #DDDDDD" valign="top">
				<textarea rows="6" cols="18" name="list1"></textarea></td>
			</tr>
		</table>
		</td>
		<td align="right">
		<table border="1" summary="form list2">
			<tr><th>DNA Motif List 2</th></tr>
			<tr><td style="background-color: #DDDDDD" valign="top">
				<textarea rows="6" cols = "18" name="list2"></textarea>
			</td></tr>
		</table>
	</td></tr>
	<tr><td colspan="3">
		<table border="1" width="100%" summary="form options">
			<tr><td class="align">
			<input type="radio" value="1on2" name="type" checked="checked" />Search motifs from list 1 inside list 2<br/>
			<input type="radio" value="2on1" name="type" />Search motifs from list 2 inside list 1<br/>
			</td></tr>
			<tr><td class="align">
				Substitutions:
				&nbsp;&nbsp;&nbsp;
				<select name="subst">
					<option value="0" selected="selected">0</option>
					<option value="1">1</option>
					<option value="2">2</option>
				</select>
			</td></tr>
		</table>
	</td></tr>
	<tr>
		<td align="left">
			<input type="submit" name="submit" value="Search" onclick="return alertUser();" />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" name="clear" value="Clear" />
		</td>
		<td align="right">
			<a href="javascript:inputSample();" title="Sample Data"><img src="images/sampledata.gif" alt="sample data" /></a>
			<a href="help/motifsxmotifs.php" title="Help"><img src="images/information.gif" alt="help icon" /></a>
		</td>
	</tr>
</table>
</form>
<?php
$web->printFooter();
?>
<script type="text/javascript">
function inputSample() {
	document.getElementsByName('list1')[0].value = 'TATATAAG\nTATAWAAM\nTATA[GC]AA[AT]';
	document.getElementsByName('list2')[0].value = 'GTTATATAAGGT\nGTTATAWAAMGT\nGTTATA[GC]AA[AT]GT';
}
function alertUser(){
	if ((document.getElementsByName('list1')[0].value.replace(/^\s*$/g,"") == '')) {
		alert("Please enter two lists of motifs");
		return false;
	}
	if ((document.getElementsByName('list2')[0].value.replace(/^\s*$/g,"") == '')) {
		alert("Please enter two lists of motifs");
		return false;
	}
}
</script>
<?php
$web->closePage();
?>
