<?php
if (!isset($_GET) || !isset($_GET['existing'])) {
    header("Location: index.php");
}

$title = "";
switch (@$_GET['existing']) {
case "regulation": $title = "Regulation Reference(s)"; break;
case "protein":    $title = "Protein Information"; break;
case "locus":      $title = "Locus Information"; break;
case "go":         $title = "Gene Ontology Information"; break;
case "goterm":     $title = "Gene Ontology Term Information"; break;
case "consensus":  $title = "Transcription Factor Binding Site Reference(s)"; break;
case "orthologs":      $title = "Orthologs Information"; break;
}
if ($title == "")
	exit(header("Location: index.php"));

include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Go Back", "javascript:history.back()",
"View", "");
$web->printBarMenu($title);

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

?>
<table border="0" width="100%" cellspacing="1" summary="main content">
<tr><td class="align">
<?php
include_once('service/View.php');
try {
	switch($_GET['existing']) {
	case "regulation":
		$reg = $service->viewRegulation($_GET['proteinname'], $_GET['orfname']);
		View::regulation($reg, $web->getProp("site.name"));
		break;
	case "protein":
		$proteinname = (isset($_GET['proteinname']))? $_GET['proteinname']: false;
		if (!$proteinname) throw new Exception('Requested an empty Protein!');
		list($aPInfo, $aPUnknown) = $service->user2Info($proteinname,$service->aSpecies());
		if (!empty($aPUnknown)) { $web->printErr("Unknown protein inserted: $proteinname"); } 
		else {
		$retobj = $service->viewProtein($aPInfo);
		View::viewMenu("protein", $retobj['orfname'], $retobj['proteinname']);
		View::protein($retobj,
			$web->getProp("db.$web->_dbname.ext.name"),
			$web->getProp("db.$web->_dbname.ext.url"));
		}
		break;
	case "go":
		$orfname = (isset($_GET['orfname']))? $_GET['orfname']: false;
		if (!$orfname) throw new Exception('Requested an empty ORF!');
		$retobj = $service->viewGO($_GET['orfname']);
		View::viewMenu("go", $retobj['orf'], $retobj['protein']);
		View::GO($retobj,
			$web->getProp("db.$web->_dbname.ext.name"),
			$web->getProp("db.$web->_dbname.ext.url"));
		break;
	case "goterm":
		$retobj = $service->viewGOterm($_GET['goid']);
		if (!$retobj) throw new Exception('Requested an invalid or empty GOID!');
		View::GOterm($retobj,
			$web->getProp("db.$web->_dbname.ext.name"),
			$web->getProp("db.$web->_dbname.ext.url")
		);
		break;
	case "locus":
		$showSeq     = (isset($_GET['withsequence']) && $_GET['withsequence']=="true");
		$orfname     = (isset($_GET['orfname']))? $_GET['orfname']: false;
		if (!$orfname) throw new Exception('Requested an empty ORF!');
		$retobj = $service->viewOrfgene($_GET['orfname']);
		View::viewMenu("locus", $retobj['orfname'], $retobj['proteinname']);
		View::orfgene($retobj, $web, $showSeq);
		break;
	case "orthologs":
                $regulator = (isset($_GET['proteinname']))? $_GET['proteinname']: false;
                $showSeq = (isset($_GET['withsequence']) && $_GET['withsequence']=="true");
                $orfname = (isset($_GET['orfname']))? $_GET['orfname']: false;
                if (!$orfname) throw new Exception('Requested an empty ORF!');
                $retobj = $service->viewOrthologs($_GET['orfname']);
                View::viewMenu("orthologs",$retobj['orfname'],$retobj['proteinname']);
                View::orthologs($retobj, $web);
                break;
	case "consensus":
		$reg = $service->viewConsensus($_GET['proteinname'], $_GET['consensus']);
		View::consensus($reg, $web->getProp("site.name"));
		break;
	}

} catch (Exception $e) {
?>
	<span class="error"><?=$e->getMessage() ?></span>
<?php
}
?>
</td></tr>
<tr><td class="align">
<input type="reset" value="Go Back" onclick="javascript:history.back()" />
</td></tr></table>
<?php
$web->printFooter();
$web->closePage();
?>
