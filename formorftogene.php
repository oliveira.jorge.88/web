<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"ORF List &hArr; Gene List", "");
$web->printBarMenu("Transform ORF name list &hArr; Gene name list");

$formTGs = $web->post2Form("formtgs");
$orfs = $web->post2Form("formtfs");
if (strlen($formTGs) > strlen($orfs)) $orfs = $formTGs;
?>

<form method="post" action="orftogene.php"> 
	<table class="center" border="0" summary="main content">
		<tr>
			<td colspan="2">
				<table border="1" summary="main form">
                    <tr>
                      <th class="center">ORF/gene list</th>
                    </tr>
					<tr>
						<td style="background-color: #DDDDDD">
							<textarea rows="15" cols="25" name="orfs"><?=$orfs?></textarea>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		  <tr>
		    <td align="left">
				<input type="submit" name="submit" value="Transform" onclick = "return alertUser();"/>
				&nbsp;&nbsp;&nbsp;
				<input type="reset" name="clear" value="Clear"/>
		    </td>
		    <td align="right">
<?php $web->strainSampleIcons(array('orfs' => 'tgs')); ?>
                <a href="help_orftogene.php" title="Help"><img src="../images/information.gif" alt="help icon" /></a>
		    </td>
		  </tr>
	</table>
</form>
<?php
$web->printFooter();
?>
<script type="text/javascript">
function alertUser(){
	if (document.getElementsByName('orfs')[0].value.replace(/^\s*$/g,"") == ''){
	alert("Please enter gene and/or ORF list.");
	return false;
	}
}
</script>
<?php
$web->closePage();
?>
