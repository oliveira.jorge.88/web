<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home","");
$web->printBarMenu("Welcome to " . $web->getProp("site.name"));
?>

	<div class="title"><i>- <?=$web->getProp("db.$web->_dbname.long")?> -</i></div>

<table border="0" class="center" style="width:60%; line-height:150%;" summary="main content">
<tr><td>

<p>
<?=$web->getProp("site.name")?>/<i><?=$web->getProp("db.$web->_dbname.short")?></i> currently includes information about each gene of yeast <i>Rhodosporidium toruloides NP11</i>, including gene and promoter sequences, amino acid sequence and gene annotation, extracted from GenBank, Genome Assembly <a href="https://www.ncbi.nlm.nih.gov/assembly/GCF_000320785.1/">GCF_000320785.1</a>. 
<?=$web->getProp("site.name")?>/<i><?=$web->getProp("db.$web->_dbname.short")?></i> currently includes no regulatory data, but enables access to regulatory prediction based on known or potential regulatory associations in closely related yeast species.
</p>

<p>
Use <?=$web->getProp("site.name")?>'s set of queries to predict transcription regulation at the gene and genomic levels, a key feature to the functional analysis of a gene and to the full exploitation of transcriptomics data.
Additionally, try our new interspecies comparison tool to find more about the differences in transcription network organization among related yeast species.
</p>

</td></tr>
</table>

<?php
$web->printFooter();
$web->closePage();
?>
