<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Search by DNA motif", "formsearchbydnamotif.php",
"Result", "javascript:history.back()",
"View in Promoter", "");
$web->printBarMenu("View Consensus in Promoter");
?>

<table border="0" width="100%" summary="main content">
<tr><td class="align">

<?php
$motifs = (isset($_GET) && isset($_GET['consensus']))? 
	preg_split("/[\s|]+/", trim($_GET['consensus'])): false;
$orfname = (isset($_GET) && isset($_GET['orfname']))? $_GET['orfname']: false;
$subst = (isset($_GET) && isset($_GET['subst']))? $_GET['subst']: 0;

if ($motifs && $orfname) {

	include_once('service/Logic.php');
	$service = new Logic(
	  $web->getProp("db.$web->_dbname.dbspecies"),
	  $web->getProp("db.$web->_dbname.dbstrains"));

	$currSp = $service->aSpecies();
	list($aORFinfo, $tmp) = $service->user2Info($orfname,$currSp); 	

	$promoter = $service->upstreamSeq(array_keys($aORFinfo));

	$inputmotif = [];
        $badformat = array();
        $compressed = array();
	foreach ($motifs as $c) {
        	$c = trim($c);
		if (empty($c)) continue;
		list($motif,$good) = $service->motifIUPACCompress(strtoupper($c));
		if (!$good) {
			array_push($badformat, $motif);
			continue;
		}
		$compressed[$c] = strlen($motif);
		$inputmotif[$c][$c][$c] = $c;
	}

	list($matches,$motifs2tf) = $service->tfBindingSites($promoter, $inputmotif, $subst);

	if (!empty($matches)) $matches = current($matches); # removes the first layer of keys, because its unique for this orf
	$promoter = current($promoter);

	$orfid = array_keys($aORFinfo)[0];
	$gene = ($aORFinfo[$orfid]['gene']=='Uncharacterized')?$aORFinfo[$orfid]['orf']:$aORFinfo[$orfid]['gene'];

	$promoter = join("\n",str_split($promoter, 60));
	$top  = "   upstream sequence, from -1000 to -1, size 1000";
	$promoter = ">$gene $top\n$promoter";

	$fwrpos = array();
	$revpos = array();
	$fwrprint = "";
	$revprint = "";

	foreach (array_keys($matches) as $m) {
		if (isset($matches[$m]['fwr'])){
			if (count($matches[$m]['fwr'])>0) {
				$fwrprint .= "<br/>$m: ";
			}
			# forward strand
			foreach (array_keys($matches[$m]['fwr']) as $p) {
				$fwrprint .= ($p - 1000) . " ";
				$fwrpos[$p] = $compressed[$m];
			}
		}
		if (isset($matches[$m]['rev'])){
			if (count($matches[$m]['rev'])>0) {
				$revprint .= "<br/>$m: ";
			}
			# reverse strand
			foreach (array_keys($matches[$m]['rev']) as $pos) {
				$l = $compressed[$m];
				$revprint .= ($pos * -1) . " ";
				$revpos[(1000-($pos+$l))] = $l;
			}
		}
	}
	
	if (strlen($fwrprint)>0) {
	    $fwrprint = "<br/>Consensus<small>$fwrprint</small>";
	} else {
	    $fwrprint = "<br/><small>No match found!</small>";
	}
	if (strlen($revprint)>0) {
	    $revprint = "<br/>Consensus<small>$revprint</small>";
	} else {
		$revprint = "<br/><small>No match found!</small>";
	}
	
	ksort($fwrpos);
	ksort($revpos);

?>
<span class="error">Warning! Overlapping consensus sequences found in either strands may not be highlighted correctly!</span>

<table border="1" summary="orf">
	<tr><th align="left">Standard Name</th>
		<td class="align"><?=$orfname?></td></tr>
	<tr><th class="align">Promoter Sequence<br/><?=$fwrprint?></th>
		<td class="align">
<?php
	include_once('service/View.php');
	print View::printSeqWithPos($promoter, $fwrpos);
?>
	</td></tr>
	<tr><th class="align">Promoter Sequence<br/><small>Complementary Strand</small><br/><?=$revprint?></th>
		<td class="align">
<?php
print View::printSeqWithPos($promoter, $revpos, "R");
?>
	</td></tr>
</table>
<?php

} else {
?>
<span class="error">No orfname or consensus supplied for the search!</span>
<?php
}

?>
</td></tr>
<tr><td class="align">
<input type="reset" value="Go Back" onclick="javascript:history.back()"/>
</td></tr>
</table>

<?php
$web->printFooter();
$web->closePage();
?>
