<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home","");
$web->printBarMenu("Welcome to " . $web->getProp("site.name"));
?>

	<div class="title"><i>- <?=$web->getProp("db.$web->_dbname.long")?> -</i></div>

<table border="0" class="center" style="width:60%; line-height:150%;" summary="main content">
<tr><td>

<p>
<?=$web->getProp("site.name")?>/<i><?=$web->getProp("db.$web->_dbname.short")?></i> currently includes information about each gene of yeast <i>Saccharomyces cerevisiae S288c</i>, including gene and promoter sequences, amino acid sequence and gene annotation, extracted from <a href="https://yeastgenome.org/">SGD</a>, and regulatory association data curated by the YEASTRACT team.
<?=$web->getProp("site.name")?>/<i><?=$web->getProp("db.$web->_dbname.short")?></i> currently includes a snapshot of <a href="http://yeastract.com">YEASTRACT</a> regulatory and TF binding site data. For more recent data please contact YEASTRACT.
</p>

<p>
Use <?=$web->getProp("site.name")?>'s set of queries to predict transcription regulation at the gene and genomic levels, a key feature to the functional analysis of a gene and to the full exploitation of transcriptomics data.
Additionally, try our new interspecies comparison tool to find more about the differences in transcription network organization among related yeast species.
</p>

</td></tr>
</table>

<?php
$web->printFooter();
$web->closePage();
?>
