<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter("no");
$web->printHeader("Home", "index.php",
"Search Regulatory Associations","javascript:history.back()",
"Result", "");
$web->printBarMenu("Regulatory Associations");

include_once('service/Logic.php');
$service = new Logic(
	$web->getProp("db.$web->_dbname.dbspecies"),
	$web->getProp("db.$web->_dbname.dbstrains")
);

$allTFs   = (isset($_POST) && isset($_POST['type']) && $_POST['type']=="dbtfs");
$allGenes = (isset($_POST) && isset($_POST['type']) && $_POST['type']=="dbgenes");
$allToOne = (isset($_POST) && isset($_POST['association']) && $_POST['association']=="allToOne");
$localSpecies = $service->aSpecies();

list($regs, $eviCode, $assocType, $ecgroup) = $web->getQueryParams();

list($aTFs, $aPUnknown) = $service->user2Info((($allTFs)?'':$_POST['regulators']),$localSpecies);
list($aORFGenes, $aGUnknown) = $service->user2Info((($allGenes)?'':$_POST['regulated']),$localSpecies);

if (!empty($aTFs) && !empty($aORFGenes)) list ($regdoc, $regpot,$aMotifs) = $service->getRegAssociations($aTFs,$aORFGenes,$allToOne,$ecgroup,$eviCode,$assocType);

?>
<table border="0" class="center" width="90%" summary="main content">
<tr><td class="align">
<?php

include_once('inc/head.inc.php');
Head::printUnknown($aPUnknown, Head::TYPE_tf); // TFs
Head::printUnknown($aGUnknown);

$evidencetxt = false;
if ($eviCode['dir']) {
	$evidencetxt .= "DNA binding evidence";
} elseif ($eviCode['indir']) {
	$evidencetxt .= "Expression evidence";
} elseif ($eviCode['and']) {
	$evidencetxt .= "Simultaneous DNA binding and expression evidence";  
} elseif ($eviCode['plus']) {
	$evidencetxt .= "DNA binding plus expression evidence";
}
print "<p>Documented regulations are filtered by:<br/>&nbsp;- $evidencetxt";
if ($assocType['pos'] || $assocType['neg']) {
	print ";<br/>&nbsp;- TF acting as " . ($assocType['pos']?"activator":"");
	print ($assocType['neg']?($assocType['pos']?" or ":"") . "inhibitor":"");
}
print ".</p>";
if ($ecgroup['big']) {
	print "<p>Environmental conditions filtered by {$ecgroup['big']}";
	if ($ecgroup['sub']) print ", in particular {$ecgroup['sub']}";
	print ".</p>";
}

if (empty($retObj) && empty($regdoc)) {
        print "<p><span class=\"error\">No regulatory associations found!!</span></p>";
?>
	<input type="reset" value="Go Back" onclick="javascript:history.back()" />
<?php
}
else {
?>
<table border="1">
	<tr>
		<th rowspan="2">Transcription Factors</th>
		<th>Documented&nbsp;Regulated</th>
		<th>Potential&nbsp;Regulated</th>
	</tr>
	<tr>
		<th>Genes&nbsp;-&nbsp;Reference</th>
		<th>Genes&nbsp;-&nbsp;Promoter</th>
	</tr>
<?php

function allToOne($aTFs, $aORFGenes, $regdoc, $regpot, $web) {
	
	$tf_size = count($aTFs);
	$tflist = "";
	foreach (array_keys($aTFs) as $tfid){ $tflist .= $aTFs[$tfid]['prot'].";";}
        $tflist = rtrim($tflist,";");

        $doc = "";
        foreach (array_keys($regdoc) as $g) {
          if ($regdoc[$g]['tfcount'] == $tf_size) {
            $doc .= "<a href=\"view.php?existing=locus&amp;orfname=".$regdoc[$g]['orf']."\">";
            $doc .= $g . "</a>";
            $doc .= " - <a href=\"view.php?existing=regulation&proteinname=$tflist&orfname=".$regdoc[$g]['orf']."\">Reference</a><br/>";
          }
        }
    	$pot = "";
	foreach (array_keys($regpot) as $g) {
		$orf = $aORFGenes[$regpot[$g]['orf']]['orf'];
                if ($regpot[$g]['tfcount'] == $tf_size) {
                        $pot .= "<a href=\"view.php?existing=locus&amp;orfname=".$orf."\">";
                        $pot .= $g . "</a><br/>";
        }
    }
    if (strlen($doc)>0 || strlen($pot)>0) {
?>      <tr>
                <td class="align"> <?php
	    foreach (array_keys($aTFs) as $tfid) {
		    $tf = $aTFs[$tfid]['prot'];
?>                      <a href="view.php?existing=protein&proteinname=<?=$tf?>"><?=$tf?></a><br/><?php
                }
?>              </td>
                <td class="align"> <?php
                if ($doc) print $doc;
                else print "<span class=\"error\">Not found!</span>";
?>              </td>
                <td class="align"> <?php
                if ($pot) print $pot;
                else print "<span class=\"error\">Not found!</span>";
?>              </td>
        </tr> <?php
        } else {
?>  <tr>
                <td class="align" colspan="99">
                        <span class="error">No rows were found!</span>
                </td>
        </tr> <?php
        }
}


if ($allToOne) print allToOne($aTFs, $aORFGenes, $regdoc, $regpot, $web);
else { // oneToOne
	foreach (array_keys($aTFs) as $orfid) {
		$tf = $aTFs[$orfid]['prot'];
    if (!isset($regdoc[$tf]) && !isset($regpot[$tf])) continue;
?>
	<tr>
		<td class="align">
			<a name="<?=$tf?>" href="view.php?existing=protein&amp;proteinname=<?=$tf?>">
			<?=$tf?></a>
		</td>
		<td class="align">
<?php
	if (isset($regdoc[$tf])) {
		// Javascript submenus
		$aTGsForm = array();
		foreach ($regdoc[$tf] as $o => $g) {
			$aTGsForm[] = $web->Gene($g,$o);
		}
		$items[0] = array("ranktf",  "Rank by TF", "formrankbytf.php", 1, 1);
		$items[1] = array("tfs",     "Search for TFs", "formfindregulators.php", 1, 1);
		$items[2] = array("tgs",     "Search for Genes", "formfindregulated.php", 1, 1);
		$items[3] = array("regs",    "Search for Associations", "formregassociations.php", 1, 1);
		$items[4] = array("orfgene", "ORF List &hArr; Gene List", "formorftogene.php", 0, 1);
		$items[5] = array("regviz",  "Interactive Network Image", "regnetviz.php", 1, 1);
		$web->jsmenuDraw("doc".$tf, $items, array($tf), $aTGsForm,
			$regs, $eviCode, $ecgroup, $assocType);

		$dregs = $regdoc[$tf];
		asort($dregs);
		foreach ($dregs as $o => $g) {
			$web->printGene($g,$o);
			print " - ";
			$web->printRef($tf,$o);			
			print "<br/>";
		}
	} else print "<span class=\"error\">Not found!</span>";
?>
		</td><td class="align">
<?php
    if (isset($regpot[$tf])) {
	    $pregs = array();
	    foreach ($regpot[$tf] as $o => $id) {
	        $g = $aORFGenes[$o]['gene'];
	        $pregs[$g]['id'] = $id;
	        $pregs[$g]['orf'] = $o;
	    }
	    ksort($pregs);
	    $motifs = join("|",array_values($aMotifs[$orfid][$tf]));
	    foreach (array_keys($pregs) as $g) {
		    $orfname = $aORFGenes[$pregs[$g]['orf']]['orf'];
?>
    <a href="view.php?existing=locus&amp;orfname=<?=$orfname?>"><?=$g?></a>
			-
			<a href="viewconsensusinpromoter.php?orfname=<?=$orfname?>&amp;consensus=<?=$motifs?>&amp;subst=0">Promoter</a>
			<br/>
<?php	}
	} else print "<span class=\"error\">Not found!</span>";
?>		
		</td>
	</tr>
<?php
	}
} // oneToOne
?>
</table>
</td>
<td class="align">
	<table border="0" style="float: right">
	<tr><th>Jump&nbsp;to&nbsp;<img src="images/s_desc.png" alt="gene" /></th></tr>
		<tr><td class="smallfunc">
<?php
foreach (array_keys($aTFs) as $tfid) {
	$tf = $aTFs[$tfid]['prot'];
    print "<a href=\"#$tf\">$tf</a><br/>";
}
?>
		</td></tr>
    </table>
</td></tr>
<tr><td class="align">
<input type="reset" value="Go Back" onclick="javascript:history.back()" />
</td></tr></table>

<?php
}
$web->printFooter();
?>
<script type="text/javascript" src="inc/menu.js?v20170630"></script>
<?php
$web->closePage();
?>
