<?php
class HierarchicalEdgeBundling {
	function __construct() {
	}
	function __destruct() {
	}

	function draw($url, $name) {
?>
<script src="jquery/jquery-latest.js"></script>
		<style type="text/css">

			.node:hover {
				fill: #1f77b4;
			}

			.link {
				pointer-events: none;
			}

			.link.source, .link.target {
				stroke-opacity: 1;
				stroke-width: 2px;
			}

			.node.target {
				fill: #d62728 !important;
			}

			.link.source {
				stroke: #d62728;
			}

			.node.source {
				fill: #2ca02c;
			}

			.link.target {
				stroke: #2ca02c;
			}

		</style>
		<div style="position:absolute;">
			<b>Legend:</b>
			<br/><font color="red">Red arc</font> - Selected regulates
			<br/><font color="green">Green arc</font> - Selected is regulated by
			<br/><font color="red">Red node</font> - Regulated
			<br/><font color="green">Green node</font> - Regulator
		</div>
		<div style="z-index:0;" id="viz"></div>
		<div>
			<b>Link tension:</b>
			<input style="vertical-align:middle;" type="range" min="0" max="100" value="50"/>
		</div>
		<br/><b>Download:</b>
			<button id="show_pdf">PDF</button> 
		  <button id="show_svg">SVG</button>
			<button id="show_png">PNG</button> 
		<!-- <br/><pre id="svg_code"></pre> -->

		<!-- Hidden <FORM> to submit the SVG data to the server, which will 
		     convert it to SVG/PDF/PNG downloadable file.
		     The form is populated and submitted by the JavaScript below. -->
		<form id="svg_form" method="post" action="../../d3/image.download.php">
			<input type="hidden" id="output_format" name="output_format" value="">
			<input type="hidden" id="data" name="data" value="">
		</form>

    <script type="text/javascript" src="d3/d3.js"></script>
    <script type="text/javascript" src="d3/d3.layout.js"></script>
    <script type="text/javascript" src="d3/packages.js"></script>
    <script type="text/javascript">

		var w = 700,
			  h = 700,
			rx = w / 2,
			ry = h / 2,
			m0,
			rotate = 0;

		var splines = [];

		var cluster = d3.layout.cluster()
		.size([360, ry - 110])
		.sort(function(a, b) { return d3.ascending(a.key, b.key); });

		var bundle = d3.layout.bundle();

		var line = d3.svg.line.radial()
		.interpolate("bundle")
		.tension(.50)
		.radius(function(d) { return d.y; })
		.angle(function(d) { return d.x / 180 * Math.PI; });

		// Chrome 15 bug: <http://code.google.com/p/chromium/issues/detail?id=98951>
		var div = d3.select("#viz") //"body").insert("div", "h2")
		.style("top", "0px")
		.style("left", "80px")
		.style("width", w + "px")
		.style("height", w + "px")
		.style("position", "relative");

		var svg = div.append("svg:svg")
		.attr("width", w)
		.attr("height", w)
		.append("svg:g")
		.attr("transform", "translate(" + rx + "," + ry + ")");

		svg.append("svg:path")
		.attr("class", "arc")
		.attr("fill", "#ffffff")
		.attr("cursor", "move")
		.attr("d", d3.svg.arc().outerRadius(ry - 120).innerRadius(0).startAngle(0).endAngle(2 * Math.PI))
		.on("mousedown", mousedown);

		d3.json("<?=$url.$name?>", function(classes) {
					var nodes = cluster.nodes(packages.root(classes)),
					links = packages.imports(nodes),
					splines = bundle(links);

					var path = svg.selectAll("path.link")
					.data(links)
					.enter().append("svg:path")
					.attr("class", function(d) { return "link source-" + d.source.key + " target-" + d.target.key; })
					.attr("stroke", "#1f77b4")
					.attr("stroke-opacity", ".4")
					.attr("fill", "none")
					.attr("d", function(d, i) { return line(splines[i]); });

					svg.selectAll("g.node")
					.data(nodes.filter(function(n) { return !n.children; }))
					.enter().append("svg:g")
					.attr("class", "node")
					.attr("font-size", "14px")
					.attr("font-family", "Arial")
					.attr("id", function(d) { return "node-" + d.key; })
					.attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; })
					.append("svg:text")
					.attr("dx", function(d) { return d.x < 180 ? 8 : -8; })
					.attr("dy", ".31em")
					.attr("text-anchor", function(d) { return d.x < 180 ? "start" : "end"; })
					.attr("transform", function(d) { return d.x < 180 ? null : "rotate(180)"; })
					.text(function(d) { return d.key; })
					.on("mouseover", mouseover)
					.on("mouseout", mouseout);

		d3.select("input[type=range]").on("change", function() {
					line.tension(this.value / 100);
					path.attr("d", function(d, i) { return line(splines[i]); });
					});
		});

d3.select(window)
		.on("mousemove", mousemove)
		.on("mouseup", mouseup);

function mouse(e) {
	return [e.pageX - rx, e.pageY - ry];
}

function mousedown() {
	m0 = mouse(d3.event);
	d3.event.preventDefault();
}

function mousemove() {
	if (m0) {
		var m1 = mouse(d3.event),
				dm = Math.atan2(cross(m0, m1), dot(m0, m1)) * 180 / Math.PI;
		div.style("-webkit-transform", "translate3d(0," + (ry - rx) + "px,0)rotate3d(0,0,0," + dm + "deg)translate3d(0," + (rx - ry) + "px,0)");
	}
}

function mouseup() {
	if (m0) {
		var m1 = mouse(d3.event),
				dm = Math.atan2(cross(m0, m1), dot(m0, m1)) * 180 / Math.PI;

		rotate += dm;
		if (rotate > 360) rotate -= 360;
		else if (rotate < 0) rotate += 360;
		m0 = null;

		div.style("-webkit-transform", "rotate3d(0,0,0,0deg)");

		svg
		 .attr("transform", "translate(" + rx + "," + ry + ")rotate(" + rotate + ")")
		 .selectAll("g.node text")
		 .attr("dx", function(d) { return (d.x + rotate) % 360 < 180 ? 8 : -8; })
		 .attr("text-anchor", function(d) { return (d.x + rotate) % 360 < 180 ? "start" : "end"; })
		 .attr("transform", function(d) { return (d.x + rotate) % 360 < 180 ? null : "rotate(180)"; });
	}
}

function mouseover(d) {
	svg.selectAll("path.link.target-" + d.key)
	 .classed("target", true)
	 .each(updateNodes("source", true));

	svg.selectAll("path.link.source-" + d.key)
	 .classed("source", true)
	 .each(updateNodes("target", true));
}

function mouseout(d) {
	svg.selectAll("path.link.source-" + d.key)
	 .classed("source", false)
	 .each(updateNodes("target", false));

	svg.selectAll("path.link.target-" + d.key)
	 .classed("target", false)
	 .each(updateNodes("source", false));
}

function updateNodes(name, value) {
	return function(d) {
		if (value) this.parentNode.appendChild(this);
		svg.select("#node-" + d[name].key).classed(name, value);
	};
}

function cross(a, b) {
	return a[0] * b[1] - a[1] * b[0];
}

function dot(a, b) {
	return a[0] * b[0] + a[1] * b[1];
}

function downloadFormat(format) {
	// Get the d3js SVG element
	var tmp = document.getElementById("viz");
	var svg = tmp.getElementsByTagName("svg")[0];

	// Extract the data as SVG text string
	var svg_xml = (new XMLSerializer).serializeToString(svg);

	//Optional: prettify the XML with proper indentations
	//svg_xml = vkbeautify.xml(svg_xml);
	// Set the content of the <pre> element with the XML
	//$("#svg_code").text(svg_xml);

	var form = document.getElementById("svg_form");
	form['output_format'].value = format;
	form['data'].value = svg_xml;
	form.submit();
}

d3.select("#show_svg").on("click", function() { downloadFormat("svg"); });
d3.select("#show_png").on("click", function() { downloadFormat("png"); });
d3.select("#show_pdf").on("click", function() { downloadFormat("pdf"); });

</script>
<?php
	}
}
?>
