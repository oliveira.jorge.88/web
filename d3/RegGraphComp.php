<?php
class RegGraphComp {
	private $_ecgroup;
	private $_eviCode;
	private $_assocType;
	function __construct($ecgroup, $eviCode, $assocType) {
		$this->_ecgroup = $ecgroup;
		$this->_eviCode = $eviCode;
		$this->_assocType = $assocType;
	}
	function __destruct() {
	}

	function draw($yeastract, $url, $name, $aTFs, $homologSpecies) {
		$content = file_get_contents($url.$name);
		$currentSpecies = $yeastract->aSpecies()[0]; // FIX TO BOULARDII
		list($biggroup, $envCondGroups) = $yeastract->getEnvironCondsGroupsFromDB();

?>

<script src="d3/d3.v3.min.js"></script>
<style type="text/css">
	circle    { stroke: #333; stroke-width: 1.5px; }
	circle:hover { fill: fuchsia; }

	text { font: 900 9px sans-serif; pointer-events: none; fill: black; }
	text.shadow {
		stroke: #fff;
		stroke-width: 3px;
		stroke-opacity: .8;
	}
</style>

<script>
	function subgroups() {
  		var subgroup = document.getElementById('subgroup');
  		subgroup.options.length = 0;
  		subgroup.options[0] = new Option('----','----');
		<?php
			foreach ($biggroup as $key) {
				print "\tif (document.getElementsByName('biggroup')[0].value == '$key') {\n";
	  			foreach ($envCondGroups[$key] as $s) {
					print "\t\tsubgroup.options[subgroup.options.length] = new Option('$s','$s');\n";
					if ($this->_ecgroup['sub'] == $s) {
						print "\t\tsubgroup.options[subgroup.options.length-1].selected = true;\n";
					}
	  			}
				print "\t}\n";
			}
		?>
	updateGraph();
}
</script>

<table border="1" width="100%">
	<tr>
     <td style="vertical-align:top;">
       <b>Show documented regulations in:</b>
<?php
		/// RADIO BUTTONS WITH OPTIONS UNIQUE FOR 1, 2 OR COMMON
?>
	<br/>
	<select  name="compare" onchange="javascript=buildGraph();">
		<option value="all<?=$currentSpecies?>">All <?=$currentSpecies?></option>
		<option value="unq<?=$currentSpecies?>">Unique to <?=$currentSpecies?></option>
		<option value="common" selected>Common (Homolog gene in parentheses)</option>
		<option value="unq<?=$homologSpecies?>">Unique to <?=$homologSpecies?></option>
		<option value="all<?=$homologSpecies?>">All <?=$homologSpecies?></option>
	</select>
		</td>
    <td colspan="2">
      <input type="checkbox" name="join" value="N" onchange="javascript=updateGraph();"/>
      Show additional regulatory associations between TFs
    </td>
	</tr>
	<tr>
		<td style="vertical-align:top;">
		<b>Environmental condition:</b>
<?php
		$options = " onchange=\"javascript=subgroups();\" ";
?>
                <br/>Group:
                <select name="biggroup" <?=$options?>>
                        <option value="----" >----</option>
<?php
          foreach ($biggroup as $g) {
                        $sel = ($g == $this->_ecgroup['big'])?" selected":"";
                        print "<option value=\"$g\"".$sel.">$g</option>";
          }
?>
		</select>
<?php
		if ($this->_ecgroup['big'] == 0 
			|| $this->_ecgroup['sub'] == 0 
			|| !in_array($this->_ecgroup['sub'], $envCondGroups[$this->_ecgroup['big']])
		) {
?>
			<br/>Subgroup: <select id="subgroup" name="subgroup" onchange="javascript=updateGraph();">
			<option value="----" selected>----</option>
			<script>subgroups();</script>
			</select>
<?php
		} else {
?>
			<br/>Subgroup: <select id="subgroup" name="subgroup" >
				<option value="<?=$this->_ecgroup['sub']?>" selected><?=$this->_ecgroup['sub']?></option>
      </select>
<?php
}
?>
	</td>
		<td style="vertical-align:top;"><b>Supporting evidence:</b>
			<br/><input type="checkbox" name="evdbindonly" value="evdbindonly "
<?php
		if ($this->_eviCode['dir'] || $this->_eviCode['plus']) {
			print " checked";
		}
		if ($this->_eviCode['plus']) {
			print " onchange=\"javascript=updateGraph();\"";
		} else print " unchecked";
?>
		/>
				<svg width="40" height="10">
					<defs>
						<marker id="legend_unspec" viewBox="0 -5 10 10" refX="4" refY="0" markerUnits="strokeWidth" orient="auto" markerWidth="5" markerHeight="5">
							<path d="M0,-5L9,0L0,5L0,3L5,0L0,-3" stroke="black"/>
						</marker>
						<marker id="legend_neg" viewBox="0 -5 10 10" refX="4" refY="0" markerUnits="strokeWidth" orient="auto" markerWidth="5" markerHeight="5">
							<path d="M3,-5L6,-5L6,5L3,5" fill="red"/>
						</marker>
						<marker id="legend_pos" viewBox="0 -5 10 10" refX="4" refY="0" markerUnits="strokeWidth" orient="auto" markerWidth="5" markerHeight="5">
							<path d="M0,-5L9,0L0,5L0,3L5,0L0,-3" fill="green"/>
						</marker>
						<marker id="legend_dual" viewBox="-4 -5 15 10" refX="4" refY="0" markerUnits="strokeWidth" orient="auto" markerWidth="5" markerHeight="5">
							<path d="M0,-5L10,0L0,5L0,0L-2,0L-2,5L-5,5L-5,-5L-2,-5L-2,0" fill="sienna"/>
						</marker>
						<marker id="legend_com" viewBox="0 -5 10 10" refX="4" refY="0" markerUnits="strokeWidth" orient="auto" markerWidth="5" markerHeight="5">
              <path d="M0,-5L9,0L0,5L0,3L5,0L0,-3" fill="blue"/>
            </marker>
					</defs>
					<line x1="0" y1="5" x2="30" y2="5" stroke="black" stroke-width="2px" marker-end="url(#legend_unspec)"/>
				</svg>DNA Binding only
			<br/><input type="checkbox" name="evdbindexpr" value="evdbindexpr"
<?php
		if ($this->_eviCode['plus'] || $this->_eviCode['and']) {
			print " checked ";
		}
		if ($this->_eviCode['plus']) {
			print " ";
		} else print " unchecked ";
?>
				onchange="javascript=updateGraph();"/>
				<svg width="40" height="10">
					<line x1="0" y1="5" x2="30" y2="5" stroke="black" stroke-width="2px" marker-end="url(#legend_unspec)"/>
				</svg>DNA Binding + Expression
			<br/><input type="checkbox" name="evdexpronly" value="evdexpronly"
<?php
		if ($this->_eviCode['indir'] || $this->_eviCode['plus']) {
			print " checked ";
		}
		if ($this->_eviCode['plus']) {
			print " ";
		} else print " unchecked ";
?>
				onchange="javascript=updateGraph();"/>
				<svg width="40" height="10">
					<line x1="0" y1="5" x2="30" y2="5" stroke="black" stroke-dasharray="6,2" stroke-width="2px" marker-end="url(#legend_unspec)"/>
				</svg>Expression only
		<!--<script>updateGraph();</script>-->
		</td>	
		<td style="vertical-align:top;"><b>Type:</b>
			<br/><input type="checkbox" name="typepos" value="typepos"
<?php
		if ($this->_assocType['pos']) {
			print " checked ";
		} else {
			print " unchecked ";
		}
?>
				onchange="javascript=updateGraph();" />
				<svg width="40" height="10">
					<line x1="0" y1="5" x2="30" y2="5" stroke="green" stroke-width="2px" marker-end="url(#legend_pos)"/>
				</svg>Positive
			<br/><input type="checkbox" name="typeneg" value="typeneg"
<?php
		if ($this->_assocType['neg']) {
			print " checked ";
		} else {
			print " unchecked ";
		}
?>
				onchange="javascript=updateGraph();" />
				<svg width="40" height="10">
					<line x1="0" y1="5" x2="30" y2="5" stroke="red" stroke-width="2px" marker-end="url(#legend_neg)"/>
				</svg>Negative
			<br/><input type="checkbox" name="typeunspec" value="typeunspec"
				checked onchange="javascript=updateGraph();"/>
				<svg width="40" height="10">
					<line x1="0" y1="5" x2="29" y2="5" stroke="black" stroke-width="2px" marker-end="url(#legend_unspec)"/>
				</svg>Unspecified
			<br/><span style="display:block; height: 5px;"></span>
				<span style="display:inline-block; width: 20px;"></span>
				<svg width="40" height="10">
                                        <line x1="0" y1="5" x2="29" y2="5" stroke="blue" stroke-width="2px" marker-end="url(#legend_com)"/>
                                </svg>Common
		</td>
	</tr>
</table>
		</td>
	</tr>
	<tr>
		<td>
			<div style="z-index:0; border-style:solid;" id="viz"></div>
		</td>
	</tr>
	<tr>
		<td class="align">
			<b>Download:</b>
			<button id="show_pdf" onClick="downloadFormat('pdf');">PDF</button>
			<button id="show_svg" onClick="downloadFormat('svg');">SVG</button>
			<button id="show_png" onClick="downloadFormat('png');">PNG</button>
			<!-- Hidden <FORM> to submit the SVG data to the server, which will
					 convert it to SVG/PDF/PNG downloadable file.
					 The form is populated and submitted by the JavaScript below. -->
				 <form id="svg_form" method="post" action="d3/image.download.php">
				<input type="hidden" id="output_format" name="output_format" value="">
				<input type="hidden" id="data" name="data" value="">
			</form>


<script type="text/javascript">

		function filterLinkSetByGUI() {

			if (isCommon()) {
                                document.getElementsByName('evdbindonly')[0].checked = true;
                                document.getElementsByName('evdbindexpr')[0].checked = true;
                                document.getElementsByName('evdexpronly')[0].checked = true;
                                document.getElementsByName('typepos')[0].checked = true;
                                document.getElementsByName('typeneg')[0].checked = true;
                                document.getElementsByName('typeunspec')[0].checked = true;
                                document.getElementsByName('evdbindonly')[0].disabled = true;
                                document.getElementsByName('evdbindexpr')[0].disabled = true;
                                document.getElementsByName('evdexpronly')[0].disabled = true;
                                document.getElementsByName('typepos')[0].disabled = true;
                                document.getElementsByName('typeneg')[0].disabled = true;
                                document.getElementsByName('typeunspec')[0].disabled = true;
                        } else {
                                document.getElementsByName('evdbindonly')[0].disabled = false;
                                document.getElementsByName('evdbindexpr')[0].disabled = false;
                                document.getElementsByName('evdexpronly')[0].disabled = false;
                                document.getElementsByName('typepos')[0].disabled = false;
                                document.getElementsByName('typeneg')[0].disabled = false;
                                document.getElementsByName('typeunspec')[0].disabled = false;
                        }

	var bigG  = document.getElementsByName('biggroup')[0].value;
	var subG  = document.getElementsByName('subgroup')[0].value;
	var evdB  = document.getElementsByName('evdbindonly')[0].checked;
	var evdBE = document.getElementsByName('evdbindexpr')[0].checked;
	var evdE  = document.getElementsByName('evdexpronly')[0].checked;
	var typeP = document.getElementsByName('typepos')[0].checked;
	var typeN = document.getElementsByName('typeneg')[0].checked;
	var typeU = document.getElementsByName('typeunspec')[0].checked;
	var Comp  = document.getElementsByName('compare')[0].value;
	var join  = document.getElementsByName('join')[0].checked;

  links.splice(0, links.length);
  nodes.splice(0, nodes.length);

	sNodesSp = {};// aux structure to avoid repeated nodes
  linksSp.forEach(function(l){
    if (!(
      (bigG != "----" && l['biggroup'].indexOf(bigG) == -1) ||
      (subG != "----" && l['subgroup'].indexOf(subG) == -1) ||

      (l['species'].indexOf(Comp) == -1) ||

			(!join && l['join']=='Y') ||

      (!evdB  && l['evd']=='bindonly') ||
      (!evdBE && l['evd']=='bindexpr') ||
      (!evdE  && l['evd']=='expronly') ||

      ((l['evd']=='bindexpr' || l['evd']=='expronly') &&
      ((!typeP && l['type']=='pos') ||
      (!typeN && l['type']=='neg') ||
      (!typeP && !typeN && l['type']=='dual') ||
      (!typeU && l['type']=='unspec'))))) {

      links.push(l);

      if (!(l.source.name in sNodesSp)) {
				if (!('x' in l.source)) {
					l.source.x = w/2 + Math.floor(Math.random()*100);
					l.source.y = h/2 + Math.floor(Math.random()*100);
				}
        sNodesSp[l.source.name] = l.source;
        nodes.push(l.source);
      } else {
        l.source = sNodesSp[l.source.name];
      }
      if (!(l.target.name in sNodesSp)) {
				if (!('x' in l.target)) {
			    l.target.x = w/2 + Math.floor(Math.random()*100);
					l.target.y = h/2 + Math.floor(Math.random()*100);
				}
        sNodesSp[l.target.name] = l.target;
        nodes.push(l.target);
      } else {
        l.target = sNodesSp[l.target.name];
      }
    }
  });
}

// Use elliptical arc path segments to doubly-encode directionality.
function tick() {
	
	path.attr("d", function(d) {
   	var dx = d.target.x - d.source.x,
        dy = d.target.y - d.source.y,
        dr = 2 * Math.sqrt(dx * dx + dy * dy);
		if (dx != 0 || dy != 0) {
			return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;
		}
		return "M" + d.source.x + "," + d.source.y +
					 "C" + (d.source.x-30) + "," + (d.source.y-30) +
					 " " + (d.target.x+30) + "," + (d.target.y-30) +
					 " " + (d.target.x) + "," + d.target.y;
  });

  circle.attr("transform", function(d) {
    return "translate(" + d.x + "," + d.y + ")";
  });

  text.attr("transform", function(d) {
    return "translate(" + d.x + "," + d.y + ")";
  });
}

// Get all styles from CSS: https://spin.atomicobject.com/2014/01/21/convert-svg-to-png/
function generateStyleDefs(svgDomElement) {
  var styleDefs = "";
  var sheets = document.styleSheets;
  for (var i = 0; i < sheets.length; i++) {
    var rules = sheets[i].cssRules;
    for (var j = 0; j < rules.length; j++) {
      var rule = rules[j];
      if (rule.style) {
        var selectorText = rule.selectorText;
        var elems = svgDomElement.querySelectorAll(selectorText);

        if (elems.length) {
          styleDefs += selectorText + " { " + rule.style.cssText + " }\n";
        }
      }
    }
	}
	var s = document.createElement('style');
  s.setAttribute('type', 'text/css');
	s.innerHTML = "\n" + styleDefs + "\n";
	var defs = document.createElement('defs');
  defs.appendChild(s);
	return defs;
}
// Download
function downloadFormat(format) {
  // Get the d3js SVG element
  var tmp = document.getElementById("viz");
  var svgElem = tmp.getElementsByTagName("svg")[0];
  svgElem.insertBefore(generateStyleDefs(svgElem), svgElem.firstChild);

  // Extract the data as SVG text string
  var svg_xml = (new XMLSerializer).serializeToString(svgElem);

  // Optional: prettify the XML with proper indentations
  //svg_xml = vkbeautify.xml(svg_xml);
  // Set the content of the <pre> element with the XML
  //$("#svg_code").text(svg_xml);

  var form = document.getElementById("svg_form");
  form['output_format'].value = format;
  form['data'].value = svg_xml;
  form.submit();
}


function isCommon() {
	return document.getElementsByName('compare')[0].value === "common";
}

function buildGraph() {
	var Comp  = document.getElementsByName('compare')[0].value;

	linksSp.splice(0, linksSp.length);
  linksAll.forEach(function(l) {
    if (l.species==Comp) {
      linksSp.push(l);
    }
  });

	return updateGraph();
}

function updateGraph() {
	var Comp  = document.getElementsByName('compare')[0].value;

	filterLinkSetByGUI();

	// Effectively deletes the SVG content
  if (!d3.select("#viz").selectAll("g").empty()) {
    d3.select("#viz").selectAll("g").remove();
	}

	path = svg.append("svg:g").selectAll("path").data(links);
	path.enter().append("svg:path")
		.attr("id", function(d) { return d.source.index + "_" + d.target.index; })
		.attr("stroke-width", "2px")
		.attr("fill", "none")
		.attr("stroke", function(d) {
			if (isCommon()) return "blue";
			if (d.type == "pos")  return "green";
			if (d.type == "neg")  return "red";
			if (d.type == "dual") return "sienna";
			return "black";
		})
		.attr("stroke-dasharray", function(d) {
			if (isCommon())    return "none";
			if (d.evd == "expronly") return "6,2";
			if (d.evd == "unspec")   return "2,2";
			return "none";
		})
		.attr("marker-end", function(d) { 
			if (isCommon()) return "url(#marker_com)";
			return "url(#marker_" + d.type + ")"; 
		});

	var Homo = currentSpecies;
	if (!isCommon()) {
		Homo = Comp.slice(3,Comp.length);
	}
	var tfnode= [];
	circle = svg.append("svg:g").selectAll("circle").data(nodes);
	circle.enter().append("svg:circle")
		.attr("r", 5)
		.attr("fill", function(d) {
			tfnode[d.name] = false;
			tf = d.name.replace(/ *\([^)]*\) */g,"");
			if (tf.split(", ").length > 1){
				tf = tf.split(", ")[0];
			}
			if (aTFs[Homo].indexOf(tf) > -1) {
				tfnode[d.name] = true;
				return "#0bf";
			}
			return "#ee0";
		})
		.call(force.drag);

	text = svg.append("svg:g").selectAll("g")
		.data(nodes).enter().append("svg:g");
	text.append("svg:text")
		.attr("x", 8)
		.attr("y", ".31em")
		.attr("class", "shadow")
		.text(function(d) { return d.name; });
	text.append("svg:text")
		.attr("x", 8)
		.attr("y", ".31em")
		.text(function(d) { return d.name; });

	d3.selectAll('text')
		.filter(function(d) { return tfnode[d.name]; })
		.style("fill", "blue");

	force.start();
	return [path, circle, text];
}

function redraw(){
	// Extract the width and height that was computed by CSS.
	var width = document.getElementById('viz').clientWidth;
	var height = document.getElementById('viz').clientHeight;

	// Use the extracted size to set the size of an SVG element.
	svg.attr("width", width)
		.attr("height", height);
}



// ----------------------------- BEGIN ---------------------------------
var aTFs = <?php echo json_encode($aTFs);?>;

var currentSpecies = <?php echo json_encode($currentSpecies); ?>;

var w = document.getElementById('viz').offsetWidth;
var h = w*0.8;

var linksAll = <?=$content?>;

// Per-type markers, as they don't inherit styles.
var markerData = [
	{ id:0, name:'unspec', viewbox:'0 -5 10 10',  fill:'black',  path:'M0,-5L9,0L0,5L0,3L5,0L0,-3' },
 	{ id:1, name:'pos',    viewbox:'0 -5 10 10',  fill:'green',  path:'M0,-5L9,0L0,5L0,3L5,0L0,-3' },
	{ id:2, name:'neg',    viewbox:'0 -5 10 10',  fill:'red',    path:'M6,-5L9,-5L9,5L6,5' },
	{ id:3, name:'dual',   viewbox:'-4 -5 15 10', fill:'sienna', path:'M0,-5L10,0L0,5L0,0L-2,0L-2,5L-5,5L-5,-5L-2,-5L-2,0' },
	{ id:4, name:'com',    viewbox:'0 -5 10 10',  fill:'blue',   path:'M0,-5L9,0L0,5L0,3L5,0L0,-3' }
];

var linksSp = [];
var links = [];
var nodes = [];

var force = d3.layout.force()
	.nodes(nodes)
	.links(links)
	.size([w, h])
	.linkDistance(150)
	.charge(-300)
	.on("tick", tick)
	.start();

var svg = d3.select("#viz").append("svg:svg")
	.attr("width", w)
	.attr("height", h);

svg.append("svg:defs").selectAll("marker")
	.data(markerData).enter().append("svg:marker")
	.attr("id", function(d){ return 'marker_' + d.name})
	.attr("viewBox", function(d){ return d.viewbox })
	.attr("fill", function(d){ return d.fill })
	.attr("refX", 15).attr("refY", -1.5)
	.attr("markerWidth", 5).attr("markerHeight", 5)
	.attr('markerUnits', 'strokeWidth')
	.attr("orient", "auto")
	.append("svg:path")
	.attr("d", function(d){ return d.path });

var result = buildGraph();
var path   = result[0];
var circle = result[1];
var text   = result[2];

// Redraw based on the new size whenever the browser window is resized.
window.addEventListener("resize", redraw);

</script>
<?php
	}
}
?>
