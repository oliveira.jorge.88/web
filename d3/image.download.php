<?php
##
## Input validation
##
if (!isset($_POST) || !isset($_POST['output_format'])) {
	print "Missing 'output_format' parameter";
	exit;
}
$output_format = $_POST['output_format'];
if ($output_format != "svg" && $output_format != "png" && $output_format != "pdf") {
	print "Invalid output_format value";
	exit;
}
if (!isset($_POST['data'])) {
	print "Missing 'data' parameter";
	exit;
}
$data = $_POST['data'];
#die "Invalid data value"
#	unless $data =~ /^[\x20-\x7E\t\n\r ]+$/;

# Write the SVG data to a temporary file
$tmpsvg = tempnam("/tmp", "SVGin");
$fhi = fopen($tmpsvg, "w");
fwrite($fhi, $data);
fclose($fhi);

	## SVG output
if ($output_format == "svg") {
	## If both input & output are SVG, simply return the submitted SVG
	## data to the user.
	## The only reason to use a server side script is to be able to offer
	## the user a friendly way to "download" a file as an attachment.
	header("Content-type: image/svg+xml");
	header('Content-Disposition: attachment; filename="yeastract-tf.'.$output_format.'"');
	$sz = filesize($tmpsvg);
	header('Content-Length: '.$sz);

	readfile($tmpsvg);

	unlink($tmpsvg);

	## PDF/PNG output
} elseif ($output_format == "pdf" || $output_format == "png") {
	include('../conf/Properties.php');
	$props = new Properties("../");
	$dir = $props->get("path.tmp.local");
	chdir($dir);

	# Run "rsvg-convert", first create a PDF file
	$outfile = tempnam("./", "PDFout");
	chmod($outfile, 0666);

	$last_line = system("rsvg-convert -o '$outfile' -z 1 -f pdf '$tmpsvg'");

	$outcrop = tempnam("./", "PDFcrop"); # userid: 48 / User: apache
	chmod($outcrop, 0666);

	$last_line = system("pdfcrop --pdftex --pdftexcmd /usr/bin/pdftex $outfile $outcrop 2>&1 > /dev/null");

	if ($output_format == "png") {
		rename($outfile, "$outfile.png");
		$outfile = "$outfile.png";
		system("convert $outcrop $outfile");
	} else {
		$outfile = $outcrop;
	}
	$sz = filesize($outfile);

	## All is fine, send the data back to the user
	$mime_type = ($output_format == "pdf")?"application/pdf":"image/png";
	header("Content-Type: $mime_type");
	header('Content-Disposition: attachment; filename="yeastract-tf.'.$output_format.'"');
	header('Content-Length: '.$sz);

	# Read the binary output (PDF/PNG) file
	readfile($outfile);

	unlink($tmpsvg);
	if ($output_format == "png") { unlink($outcrop); }
	unlink($outfile);
}
exit;
?>
