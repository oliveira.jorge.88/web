<?php
class VennRegulon {
	function __construct() {
	}
	function __destruct() {
	}

	private function sets($set, $desc) {
		if (!empty($set))
			print "$desc targets: ".sizeof($set)."<br/>";
	}

	function draw($yeastract, $url, $name, $tf, $aTGs, $B, $BE, $E, $U, $P, $potcounts, $PB, $PE, $PBE, $PU) {
		$content = file_get_contents($url.$name);
?>

	<script src="d3/d3.v4.min.js"></script>

<!-- Credits to https://github.com/benfred/venn.js for -->
<!-- Area proportional Venn and Euler diagrams in JavaScript -->
	<script src="d3/venn.js"></script>

<div id="venn"></div>

<style type="text/css">
.venntooltip {
  position: absolute;
  text-align: center;
  font-size: 85%;
  height: 15px;
  background: #333;
  color: #ddd;
  padding: 2px;
  border: 0px;
  border-radius: 8px;
  opacity: 0;
}
</style>
<script>
var sets = <?=$content?>;

var chart = venn.VennDiagram()
	.width(650)
	.height(500);

var div = d3.select("#venn")
div.datum(sets).call(chart);

var tooltip = d3.select("body").append("div")
	.attr("class", "venntooltip");

div.selectAll("path")
	.style("stroke-opacity", 0)
	.style("stroke", "#fff")
	.style("stroke-width", 3);

div.selectAll("text")
	.style("font-size", "80%");

div.selectAll("g")
    .on("mouseover", function(d, i) {
        // sort all the areas relative to the current item
        venn.sortAreas(div, d);

        // Display a tooltip with the current size
        tooltip.transition().duration(400).style("opacity", .9);
        tooltip.text("Click to show/hide table");

        // highlight the current path
        var selection = d3.select(this).transition("tooltip").duration(400);
        selection.select("path")
            .style("fill-opacity", d.sets.length == 1 ? .4 : .1)
            .style("stroke-opacity", 1);
    })

		.on("mousedown", function(d, i) {
			var bindonly = document.getElementById('tablebindonly');
			var bindexpr = document.getElementById('tablebindexpr');
			var expronly = document.getElementById('tableexpronly');
			var unspec   = document.getElementById('tableunspec');
			var potregs  = document.getElementById('tablepotregs');
			var potbind  = document.getElementById('tablepotbind');
			var potexpr  = document.getElementById('tablepotexpr');
			var potbindexpr = document.getElementById('tablepotbindexpr');
			if (d.sets == "0") {
				if (bindonly.style.display === 'none') {
					bindonly.style.display = 'block';
				} else {
					bindonly.style.display = 'none';
				}
				bindexpr.style.display = 'none';
				expronly.style.display = 'none';
				unspec.style.display = 'none';
				potregs.style.display = 'none';
				potbind.style.display = 'none';
				potexpr.style.display = 'none';
				potbindexpr.style.display = 'none';
			} else if (d.sets == "1") {
				bindonly.style.display = 'none';
				if (expronly.style.display === 'none') {
					expronly.style.display = 'block';
				} else {
					expronly.style.display = 'none';
				}
				bindexpr.style.display = 'none';
				unspec.style.display = 'none';
				potregs.style.display = 'none';
				potbind.style.display = 'none';
				potexpr.style.display = 'none';
				potbindexpr.style.display = 'none';
			} else if (d.sets == "0,1") {
				bindonly.style.display = 'none';
				expronly.style.display = 'none';
				if (bindexpr.style.display === 'none') {
					bindexpr.style.display = 'block';
				} else {
					bindexpr.style.display = 'none';
				}
				unspec.style.display = 'none';
				potregs.style.display = 'none';
				potbind.style.display = 'none';
				potexpr.style.display = 'none';
				potbindexpr.style.display = 'none';
			} else if (d.sets == "2") {
				bindonly.style.display = 'none';
				expronly.style.display = 'none';
				bindexpr.style.display = 'none';
				if (unspec.style.display === 'none') {
					unspec.style.display = 'block';
				} else {
					unspec.style.display = 'none';
				}
				potregs.style.display = 'none';
				potbind.style.display = 'none';
				potexpr.style.display = 'none';
				potbindexpr.style.display = 'none';
			} else if (d.sets == "3") {
				bindonly.style.display = 'none';
				expronly.style.display = 'none';
				bindexpr.style.display = 'none';
				unspec.style.display = 'none';
				if (potregs.style.display === 'none') {
					potregs.style.display = 'block';
				} else {
					potregs.style.display = 'none';
				}
				potbind.style.display = 'none';
				potexpr.style.display = 'none';
				potbindexpr.style.display = 'none';
			} else if (d.sets == "0,3") {
				bindonly.style.display = 'none';
				expronly.style.display = 'none';
				bindexpr.style.display = 'none';
				unspec.style.display = 'none';
				potregs.style.display = 'none';
				if (potbind.style.display === 'none') {
					potbind.style.display = 'block';
				} else {
					potbind.style.display = 'none';
				}
				potexpr.style.display = 'none';
				potbindexpr.style.display = 'none';
			} else if (d.sets == "1,3") {
				bindonly.style.display = 'none';
				expronly.style.display = 'none';
				bindexpr.style.display = 'none';
				unspec.style.display = 'none';
				potregs.style.display = 'none';
				potbind.style.display = 'none';
				if (potexpr.style.display === 'none') {
					potexpr.style.display = 'block';
				} else {
					potexpr.style.display = 'none';
				}
				potbindexpr.style.display = 'none';
			} else if (d.sets == "0,1,3") {
				bindonly.style.display = 'none';
				expronly.style.display = 'none';
				bindexpr.style.display = 'none';
				unspec.style.display = 'none';
				potregs.style.display = 'none';
				potbind.style.display = 'none';
				potexpr.style.display = 'none';
				if (potbindexpr.style.display === 'none') {
					potbindexpr.style.display = 'block';
				} else {
					potbindexpr.style.display = 'none';
				}
			}
		})

    .on("mousemove", function() {
        tooltip.style("left", (d3.event.pageX) + "px")
               .style("top", (d3.event.pageY - 28) + "px");
    })

    .on("mouseout", function(d, i) {
        tooltip.transition().duration(400).style("opacity", 0);
        var selection = d3.select(this).transition("tooltip").duration(400);
        selection.select("path")
            .style("fill-opacity", d.sets.length == 1 ? .25 : .0)
            .style("stroke-opacity", 0);
    });

// Download
function downloadFormat(format) {
  // Get the d3js SVG element
  var tmp = document.getElementById("venn");
  var svg = tmp.getElementsByTagName("svg")[0];

  // Extract the data as SVG text string
  var svg_xml = (new XMLSerializer).serializeToString(svg);

  // Optional: prettify the XML with proper indentations
  //svg_xml = vkbeautify.xml(svg_xml);
  // Set the content of the <pre> element with the XML
  //$("#svg_code").text(svg_xml);

  var form = document.getElementById("svg_form");
  form['output_format'].value = format;
  form['data'].value = svg_xml;
  form.submit();
}
</script>
</td></tr>
<tr><td><pre>
<?php
	$this->sets($B,   "DNA binding");
	$this->sets($E,   "Expression evidence");
	$this->sets($BE,  "&nbsp;DNA binding &amp; Expression");
	$this->sets($P,   "Potential");
	$this->sets($PB,  "&nbsp;Potential &amp; DNA binding");
	$this->sets($PE,  "&nbsp;Potential &amp; Expression");
	$this->sets($PBE, "&nbsp;&nbsp;Potential &amp; DNA binding &amp; Expression");
	$this->sets($PU,  "&nbsp;Potential &amp; Undefined");
?>
</pre></td></tr>
<tr><td class="align">
	<b>Download:</b>
	<button id="show_pdf">PDF</button>
	<button id="show_svg">SVG</button>
	<button id="show_png">PNG</button>
	<form id="svg_form" method="post" action="d3/image.download.php">
		<input type="hidden" id="output_format" name="output_format" value="">
		<input type="hidden" id="data" name="data" value="">
	</form>
</td></tr>

<tr><td>

<!-- DNA Binding evidence -->
<div id="tablebindonly" style="display:none;">
<?php
		$this->printtable($tf, $aTGs, $B, "DNA Binding evidence (only)");
?>
</div>

<!-- DNA Binding & Expression evidence -->
<div id="tablebindexpr" style="display:none;">
<?php
		$this->printtable($tf, $aTGs, $BE, "DNA Binding &amp; Expression evidence");
?>
</div>

<!-- Expression evidence -->
<div id="tableexpronly" style="display:none;">
<?php
		$this->printtable($tf, $aTGs, $E, "Expression evidence (only)");
?>
</div>

<!-- Unspecified -->
<div id="tableunspec" style="display:none;">
<?php
		$this->printtable($tf, $aTGs, $U, "Unspecified");
?>
</div>

<!-- Potential & DNA Binding evidence -->
<div id="tablepotbind" style="display:none;">
<?php
		$this->printtable($tf, $aTGs, $PB, "Potential &amp; DNA Binding evidence");
?>
</div>

<!-- Potential & Expression evidence -->
<div id="tablepotexpr" style="display:none;">
<?php
		$this->printtable($tf, $aTGs, $PE, "Potential &amp; Expression evidence");
?>
</div>

<!-- Potential & DNA Binding & Expression evidence -->
<div id="tablepotbindexpr" style="display:none;">
<?php
		$this->printtable($tf, $aTGs, $PBE, "Potential &amp; DNA binding &amp; Expression evidence");
?>
</div>

<!-- Potential -->
<div id="tablepotregs" style="display:none;">
	<table border="1">
		<tr><td colspan="3" class="center"><b>Potential regulations</b></td></tr>
		<tr>
			<th>Transcription factor</th>
			<th>Consensus</th>
			<th># of targets</th>
		</tr>
<?php
	foreach ($potcounts as $c => $n) {
		print "<tr><td><a href=\"view.php?existing=protein&proteinname=";
		print "$tf\">$tf</td><td><a href=\"";
		print "view.php?existing=consensus&proteinname=$tf&consensus=$c\">";
		print "$c</td><td style=\"text-align:right;\">$n</td></tr>";
	}
?>
	</table>
</div>
<script>
d3.select("#show_pdf").on("click", function() { downloadFormat("pdf"); });
d3.select("#show_svg").on("click", function() { downloadFormat("svg"); });
d3.select("#show_png").on("click", function() { downloadFormat("png"); });
</script>

<?php
	}

	private function printtable($tf, $aTGs, $set, $title) {
?>
	<table border="1">
		<tr><td colspan="3" class="center"><b><?=$title?></b></td></tr>
		<tr>
			<th>Gene name</th>
			<th>ORF name</th>
			<th>References</th>
		</tr>
<?php
		foreach ($set as $orf) {
			$g = ( (!isset($aTGs[$orf])) or ($aTGs[$orf] == "Uncharacterized") )?
				$orf: $aTGs[$orf];
?>
		<tr>
			<td>
				<a href="view.php?existing=locus&amp;orfname=<?=$orf?>"><?=$g?></a>
			</td>
			<td>
				<a href="view.php?existing=locus&amp;orfname=<?=$orf?>"><?=$orf?></a>
			</td>
			<td>
				<a href="view.php?existing=regulation&amp;proteinname=<?=$tf?>&amp;orfname=<?=$orf?>">Reference</a>
			</td>
		</tr>
<?php
		}
?>
	</table>
<?php
	}
}
?>
