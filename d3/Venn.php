<?php
class Venn {
	private $_mapping;
	function __construct($mapping) {
		$this->_mapping = $mapping;
	}
	function __destruct() {
	}
	
	private function sets($set, $desc, $index, $allCombs){

		$species="";
		foreach ($allCombs[$index] as $i=>$homo){ $species .= "$homo,";	}
		$sp= rtrim($species,",");

		print $desc.": <strong>".$set."</strong> genes ";
		print "<a href=\"javascript:showTable('".addslashes($sp)."');\">(Show Orthology Table)</a>";
		print "<br/><hr/>";
	}

	function draw($yeastract, $url, $name, $allCombs, $aLabels, $aCounts, $aURLs, $aSets,$currSpecies,$synteny) {
		$content = file_get_contents($url.$name);
		

?>

<script src="d3/d3.v4.min.js"></script>
<!-- Credits to https://github.com/benfred/venn.js for -->
<!-- Area proportional Venn and Euler diagrams in JavaScript -->
<script src="d3/venn.js"></script>

<div id="venn"></div>

<style type="text/css">
.venntooltip {
  position: absolute;
  text-align: center;
  font-size: 85%;
  height: 15px;
  background: #333;
  color: #ddd;
  padding: 2px;
  border: 0px;
  border-radius: 8px;
  opacity: 0;
}
</style>
<script>
var sets = <?=$content?>;

var labs = <?php echo json_encode($aLabels); ?>;

var combi = <?php echo json_encode($aSets); ?>;

var names = <?php echo json_encode($allCombs); ?>;

var chart = venn.VennDiagram()
	.width(1080)
	.height(640);

var div = d3.select("#venn")
div.datum(sets).call(chart);

var tooltip = d3.select("body").append("div")
	.attr("class", "venntooltip");

div.selectAll("path")
	.style("stroke-opacity", 0)
	.style("stroke", "#fff")
	.style("stroke-width", 3);

div.selectAll("text")
	.style("font-size", "80%");

div.selectAll("g")
	.on("mouseover", function(d, i) {

        // sort all the areas relative to the current item
        venn.sortAreas(div, d);

        // Display a tooltip with the current size
	tooltip.transition().duration(400).style("opacity", .9);
	if (names[i].length>1) {
		tooltip.text(d.label+"\n\n Click to go to orthology table");
	}
	else {
		tooltip.text(d.label);
	}

        // highlight the current path
        var selection = d3.select(this).transition("tooltip").duration(400);
        selection.select("path")
            .style("fill-opacity", d.sets.length == 1 ? .4 : .1)
            .style("stroke-opacity", 1);
    })

    .on("mousedown", function(d, i) {
	    if (d.size>0 && names[i].length>1) {
		    var table_form = document.getElementById("table_form");
		    var org = names[i];
			table_form['set'].value = org;
			var jsArr = {};
<?php
        foreach ($this->_mapping as $sp => $url) {
                print "jsArr[\"$sp\"] = \"$url\";";
        }
?>
        table_form.action = jsArr[org[0]] + 'orthologs.php';
	table_form.submit();
		}
	})

    .on("mousemove", function() {
        tooltip.style("left", (d3.event.pageX) + "px")
               .style("top", (d3.event.pageY - 28) + "px");
    })

    .on("mouseout", function(d, i) {
        tooltip.transition().duration(400).style("opacity", 0);
        var selection = d3.select(this).transition("tooltip").duration(400);
        selection.select("path")
            .style("fill-opacity", d.sets.length == 1 ? .25 : .0)
            .style("stroke-opacity", 0);
    });

// Download
function downloadFormat(format) {
  // Get the d3js SVG element
  var tmp = document.getElementById("venn");
  var svg = tmp.getElementsByTagName("svg")[0];

  // Extract the data as SVG text string
  var svg_xml = (new XMLSerializer).serializeToString(svg);

  // Optional: prettify the XML with proper indentations
  //svg_xml = vkbeautify.xml(svg_xml);
  // Set the content of the <pre> element with the XML
  //$("#svg_code").text(svg_xml);
  

  var form = document.getElementById("svg_form");
  form['output_format'].value = format;
  form['data'].value = svg_xml;
  form.submit();
}
function showTable(orgs){
	var table_form = document.getElementById("table_form");
	table_form['set'].value = orgs;
	var jsArr = {};
<?php
	foreach ($this->_mapping as $sp => $url) {
		print "jsArr[\"$sp\"] = \"$url\";";
	}
?>
	table_form.action = jsArr[orgs.split(",")[0]] + 'orthologs.php';
        table_form.submit();
}

</script>
</td></tr>
<tr><td><pre>
<?php
		foreach (array_keys($allCombs) as $comb){
			if (!empty($aCounts[$comb]) && (count($allCombs[$comb])>1) ) {
	                        #foreach($allCombs[$comb] as $i=>$s) {
					#if (in_array($s,$currSpecies)) {
						$this->sets($aCounts[$comb],$aLabels[$comb], $comb, $allCombs);
						continue;
					#}
				#}
			}
		}
?>
</pre></td></tr>
<tr><td class="align">
	<form id="table_form" method="post" action="orthologs.php">
	<input type="hidden" name="synteny" value="<?=$synteny?>" />
	<input type="hidden" id="set" name="set" value="">	
	</form>

	<b>Download:</b>
	<button id="show_pdf">PDF</button>
	<button id="show_svg">SVG</button>
	<button id="show_png">PNG</button>
	<form id="svg_form" method="post" action="d3/image.download.php">
		<input type="hidden" id="output_format" name="output_format" value="">
		<input type="hidden" id="data" name="data" value="">
	</form>
</td></tr>

<tr><td>
</table>
</div>
<script>
d3.select("#show_pdf").on("click", function() { downloadFormat("pdf"); });
d3.select("#show_svg").on("click", function() { downloadFormat("svg"); });
d3.select("#show_png").on("click", function() { downloadFormat("png"); });
</script>

<?php
	}
}
?>
