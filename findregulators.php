<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter("no");
$web->printHeader("Home", "index.php", "Search Transcription Factors", "formfindregulators.php", "Result", "");
$web->printBarMenu("Search Transcription Factors");

$synteny = (isset($_GET['synteny']))?$_GET['synteny']:0;

?>

<table class="center" border="0" width="80%">
<?php

list($regs, $eviCode, $assocType, $ecgroup) = $web->getQueryParams(true);

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

$localSpecies = $service->aSpecies();
$filterDoc = false;

if ( (isset($_POST) && isset($_POST['orfname'])) or (isset($_GET) && isset($_GET['tf_species'])) ){ // if it comes from REDONE or CROSS STRAIN COMPARE
	$regs['pot']  = true;
	if (isset($_GET) && !empty($_GET)) { $DBmotifs = true; $filterDoc = false; }
	$homoSpecies = (isset($_GET['tf_species']))? $_GET['tf_species']:$_POST['species'];
	$web->printSyntenyWarn($synteny);
	$orf = (isset($_GET['orfname']))? $_GET['orfname']: $_POST['orfname'];
	list($aORFids, $aUnknown) = $service->user2Info($orf,$localSpecies); # in the future use only this

	$homologSpecies = isset($_POST['homo'])?explode("|",$_POST['homo']):$service->getHomologSpecies();
	$currSpecies = $localSpecies;

	// TEMPORARILY DELETE BOULARDI
	foreach ($homologSpecies as $index => $sp) {
		if (strpos($sp, 'boulardii') !== false) { unset($homologSpecies[$index]);}
	}

	$homoORFs = [];
	$aPromoterIDs = [];
       	foreach ($homologSpecies as $toSpecies){
		$homoORFs[$toSpecies] = $service->getHomoORFInfo(array_keys($aORFids),$toSpecies,$synteny);
		foreach (array_keys($homoORFs[$toSpecies]) as $horf){ 
			foreach($homoORFs[$toSpecies][$horf]['homo']['orfid'] as $k=>$id) { $aPromoterIDs[]=$id; }
		}
	}
	if (!isset($aPromoters)) $aPromoters = [];

	foreach(array_keys($aORFids) as $id) { $aPromoterIDs[] = $id; }

	$aPromoters = $aPromoters + $service->upstreamSeq($aPromoterIDs);
	$aORFids = $aORFids + $service->orfIDs2Info($aPromoterIDs); # give aORFids all the info about all the promoters, this information is used in the image

	$fasta = "";
	foreach (array_keys($aPromoters) as $id=>$seq) {
		$fasta .= ">".$id."\n".$seq."\n";
	}
}

// end of if View or if REdone

$type = (isset($_POST['type']))?$_POST['type']:false;
$image = (!$regs['both'] && isset($_POST) && isset($_POST['image']) && $_POST['image']=="Y")? true: false;
if ( isset($_POST) && isset($_POST['doc-species']) && $_POST['doc-species'] != "0") {	$homoSpecies = $_POST['doc-species'];} 
elseif (isset($_POST) && isset($_POST['pot-species']) && $_POST['pot-species'] != "0") { $homoSpecies = $_POST['pot-species'];} 
elseif (isset($_POST) && isset($_POST['species'])) { $homoSpecies = $_POST['species'];}
elseif (!isset($homoSpecies)) { $homoSpecies = false; }

// CASE FIND REGULATORS - POTENTIAL
if ($regs['pot'] ){

	if (isset($_POST['genes'])) list($aORFids,$unknown) = $service->user2Info($_POST['genes'], $localSpecies);

	if (!isset($fasta) || $fasta==''){
		if ($homoSpecies) {
			$homoORFInfo = $service->getHomoORFInfo(array_keys($aORFids), $homoSpecies, $synteny);
			$homoORFids = [];
			foreach(array_keys($homoORFInfo) as $homoORF){ if (!empty($homoORFInfo[$homoORF]['homo']))
				foreach($homoORFInfo[$homoORF]['homo']['orfid'] as $in=>$val){ 
					$HOMOaORFids[$val]['user']    = $aORFids[$homoORFInfo[$homoORF]['orfid']]['user'];
					$HOMOaORFids[$val]['species'] = $homoSpecies;
					$HOMOaORFids[$val]['gene']    = $homoORFInfo[$homoORF]['homo']['gene'][$in];
					$HOMOaORFids[$val]['orf']     = $homoORFInfo[$homoORF]['homo']['orf'][$in];
				}
			}
			$aORFids = $HOMOaORFids;
		}

		if (!empty($aORFids)) $aPromoters = $service->upstreamSeq(array_keys($aORFids));
	
		if (isset($_POST['sequence'])) { $fasta = $_POST['sequence']; }
		else {	
			$fasta = ""; 
			foreach($aPromoters as $orfid=>$seq){ $fasta .= ">".$orfid."\n".$seq."\n";}
		}
	}

	$sequences = isset($aPromoters)?$aPromoters:$service->splitFastASequence($fasta);

	if (isset($_POST['redone'])) {
	        $filterDoc = (isset($_POST) && isset($_POST['filterdoc']));
		$docs = $filterDoc ? $service->getDocumentedFromDB() : [];
		$DBmotifs = ($_POST['inserted']==='dbmotifs') ;
		$insMotifs = ($DBmotifs) ? 'dbmotifs' : $_POST['inserted'] ;
		$userMotifs = ($DBmotifs)?$service->getMotifsFromDB($homoSpecies):$insMotifs;
	}	
	else {
		$DBmotifs = (isset($_POST) && isset($_POST['allmotifs']) || (isset($_GET) && !empty($_GET)));
		$userMotifs = (isset($_POST) && isset($_POST['motifs']) && ($_POST['motifs']!==''))? $_POST['motifs']: $service->getMotifsFromDB($homoSpecies) ; // FIX HERE
		$docs = [];
		$insMotifs = ($DBmotifs)?'dbmotifs':$userMotifs;
	}

	if (empty($userMotifs)) print "<p><span class=\"error\">No binding sites found for selected species!!</span></p>"; 

	if (($DBmotifs) || isset($_POST['genes'])) {
		$motifs = [];
		foreach (array_keys($userMotifs) as $tfid) {
			foreach(array_keys($userMotifs[$tfid]) as $tf) {
				if ($filterDoc && !in_array($tf, $docs)) continue;
				foreach ($userMotifs[$tfid][$tf] as $ref => $m) {
					// TODO remove this when shiftand supports > 1 box
					if ($service->motifIsComplex($m)){
						$aM = $service->expandRegExp($m);
						foreach ($aM as $mo){	$motifs[$m][$mo][] = $tf;	}
					}
					else {	$motifs[$m][$m][] = $tf;	}
				}
			}
		}
	}
	else { $motifs = $service->identifyMotifs($userMotifs,$docs,$homoSpecies);	}

	list($matches,$motif2tf) = $service->tfBindingSites($sequences, $motifs); // USES SHIFT_AND

	$tf2motif  = [];
	$commonTFs = [];
	if (!empty($matches)) {
		foreach (array_keys($matches) as $name) { // FOREACH PROMOTER ID 
			$promTFs = [];
			foreach ($matches[$name] as $m => $strands) { // FOREACH MOTIF (IN REGEX FORMAT)
				foreach($motif2tf[$m] as $k=>$tf){ // FOREACH TF OF THE MOTIF
					if (!isset($promTFs[$tf])) $promTFs[$tf] = $tf;
					if (isset($tf2motif[$tf]) && !in_array($m,$tf2motif[$tf])) {
						$tf2motif[$tf][] = $m;
					} else {
						$tf2motif[$tf] = array();
						$tf2motif[$tf][] = $m;
					}
				}
			}
			if (empty($commonTFs)) {
				$commonTFs = $promTFs;
			} else {
				$commonTFs = array_intersect($commonTFs, $promTFs);
			}
		}

		include_once('service/Image.php');
		$img = new Image($service);
		$local = $web->getProp("path.tmp.local");
		$url   = $web->getProp("path.tmp.url");
		if (!isset($aORFids)) $aORFids = null;
		foreach ($sequences as $name => $seq) { $seqSize[$name] = strlen($seq); }
		list($im,$aColors) = $img->bindingSites($matches, $motifs, $seqSize, $local, $tf2motif, $docs, $aORFids);

		$num_tfs = array_keys($aColors);
		$cols = 7;
		$div = (int)(count($num_tfs)/($cols-1));
?>
<img src="<?=$url . $im?>" alt="Generated Image with TFs binding to the inserted promoters" style="height:60%;" />
</br></br>
<form method="post" id="formtf" action="findregulators.php">
<table border="1" summary="TFs" >
<tr><td class="align">
<input type="hidden" name="sequence" value="<?=$fasta?>" />
<input type="hidden" name="synteny" value="<?=$synteny?>" />
<?php
		if ($homoSpecies) print "<input type=\"hidden\" name=\"species\" value=\"$homoSpecies\" />";
		if (isset($_GET['orfname']) or isset($_POST['orfname']) ) print "<input type=\"hidden\" name=\"orfname\" value=\"$orf\" />";
		if (isset($_GET['genes']) or isset($_POST['genes']) ) print "<input type=\"hidden\" name=\"genes\" value=\"{$_POST['genes']}\" />";
		if (isset($_GET['homo']) or isset($_POST['homo']) ) print "<input type=\"hidden\" name=\"homo\" value=\"{$_POST['homo']}\" />";
		if ($image) print "<input type=\"hidden\" name=\"type\"  value=\"pot\" /><input type=\"hidden\" name=\"image\" value=\"Y\" />";
		if (($DBmotifs) || isset($_POST['genes'])) { 
			print "<input type=\"hidden\" name=\"inserted\" value=\"dbmotifs\" />";
		}
		else {
			print "<input type=\"hidden\" name=\"inserted\" value=\"$insMotifs\" />";
		}
?>
	<input type="hidden" name="redone" value="Y" />

<?php
		$j = 0;
		foreach (array_keys($aColors) as $tf) {
			if (($j > 0) && ($div > 0) && (($j%$div) == 0))
				print "</td><td class=\"align\">";

			$f = $url . $aColors[$tf]['file'] ;
			$chk = ($aColors[$tf]['isset'])? "checked=\"checked\"": "";
			print "<input type=\"checkbox\" name=\"$tf\" $chk />";
			print "<img src=\"$f\" alt=\"Color\"/><br>";
			$j++;
		}
?>
	</td></tr>
	<tr><td colspan="<?=$cols?>"><small>Select:
    <a href="javascript:void(0);" onclick="javascript:selectAll()">All</a>
		<a href="javascript:void(0);" onclick="javascript:selectNone()">None</a>
		<a href="javascript:void(0);" onclick="javascript:selectCommon()"
			data-toggle="tooltip" data-placement="auto" title="Common TFs to all promoters">Common</a>
<?php
		if (!$homoSpecies) print "<input type=\"checkbox\" name=\"filterdoc\" /> Consider only documented TFs &nbsp;";
?>
</small>
<input type="submit" value="Redisplay" />
        </td></tr>
</table>
</form>
<br/>
<table border="0" class="center" width="100%" summary="main content">
	<tr>
		<td class="align">

			<table border="1" summary="TFs">
				<tr>
					<th><?=($DBmotifs?"Transcription Factor":"User inserted")?></th>
					<th>Consensus</th>
					<th>Position</th>
					<th>Strand</th>
				</tr>
<?php
		$currSp = array_shift($localSpecies);
		foreach (array_keys($matches) as $id) {
			$name = isset($aORFids[$id]['user'])?$aORFids[$id]['user']:$id;
		       $hname="";	
			if ($homoSpecies) $hname = ($aORFids[$id]['gene']!='Uncharacterized')?" &rarr; homolog ".$aORFids[$id]['gene']:" &rarr; homolog ".$aORFids[$id]['orf'];
			$name .= $hname;
?>
				<tr>
					<td colspan="4" align="center">
						<b>Target Sequence: <a name="<?=$name?>"><?=$name?></a></b>
						(size <?=$seqSize[$id]?>)<br/>
						<small><a href="#TOP">Back to top <img src="../images/s_asc.png" alt="top"/></a></small>
					</td>
				</tr>
<?php
		foreach ($matches[$id] as $m => $strands) {
			$linkP = "";
			foreach ($motif2tf[$m] as $tf) {
				$linkP .= "<a href=\"".$web->getDBurl((($homoSpecies)&&($homoSpecies!=''))?$homoSpecies:$currSp);
				$linkP .= "view.php?existing=protein&amp;proteinname=$tf\">$tf</a>&nbsp;";
			}
			$linkC  = "<a href=\"".$web->getDBurl((($homoSpecies)&&($homoSpecies!=''))?$homoSpecies:$currSp);
			$linkC .= "view.php?existing=consensus&amp;proteinname=".$tf."&amp;consensus=";
			if (isset($strands['fwr'])) {
				foreach ($matches[$id][$m]['fwr'] as $pos) {
					$p = $pos - $seqSize[$id];
?>
<tr>
		<td><?=$linkP?></td>
		<td><?=$linkC.$m."\">$m"?></a></td>
    <td><?=$p?></td>
    <td>F</td>
</tr>
<?php
				}
			} // fwr
			if (isset($strands['rev'])) {
				foreach ($matches[$id][$m]['rev'] as $pos) {
					$p = -1 * $pos;
?>
<tr>
		<td><?=$linkP?></td>
		<td><?=$linkC.$m."\">$m"?></a></td>
    <td><?=$p?></td>
    <td>R</td>
</tr>
<?php
				}
			} // rev
		} // $m 
	} // name
?>
</table>
</td>
<td class="align">
        <table border="0" class="right" summary="quick jump">
                <tr><th>Jump&nbsp;to&nbsp;<img src="../images/s_desc.png" alt="gene" /></th></tr>
                <tr><td class="smallfunc">
<?php
		foreach (array_keys($matches) as $id) {
			$n = isset($aORFids[$id])?(isset($aORFids[$id]['user'])?$aORFids[$id]['user']:(($aORFids[$id]['gene']=='Uncharacterized')?$aORFids[$id]['orf']:$aORFids[$id]['gene'])):$id;
			$hn= "";
			if ($homoSpecies) $hn = ($aORFids[$id]['gene']!='Uncharacterized')?" &rarr; homolog ".$aORFids[$id]['gene']:" &rarr; homolog ".$aORFids[$id]['orf'];
			$n .= $hn;
    print "<a href=\"#$n\">$n</a><br/>";
        }
?>
                </td></tr>
    </table>
<?php
}  
else {
?>
<table border="0" class="center" width="90%" summary="main content">
<tr><td class="align">No matches found!!!
<?php
}
?>
</td>
</tr>
<tr><td class="align">
<br/><input type="reset" value="Go Back" onclick="javascript:history.back()"/>
</td></tr>
</table>

<?php

}  // CASE FIND TF BINDING SITE ENDS HERE!!! 
else {
	// SEARCH FOR DOCUMENTED!!! TFS STARTS HERE

$web->setPOSTdefaultVal('genes','');

list($aORFids, $aUnknown)  = $service->user2Info($_POST['genes'],$localSpecies); 

if ($homoSpecies) {
	$homoORFs = $service->getHomoORFInfo(array_keys($aORFids),$homoSpecies,$synteny);
	list($docRegs, $mapping) = $service->findHomoRegulators($homoORFs,$homoSpecies,$synteny);
} else {
	$docRegs = $service->getRegulations([], array_keys($aORFids), $ecgroup, $eviCode, $assocType, true, true, false);
}
	// reorganize array by tgid 
	$docReg = [];
	$aDocTFids = [];
	foreach (array_keys($docRegs) as $tfid){
		$aDocTFids[$tfid] = $tfid;
		foreach($docRegs[$tfid] as $tgid){
			$docReg[$tgid][$tfid] = $tfid;
		}
	}
	$aTFinfo = $service->orfIDs2Info(array_keys($aDocTFids));
?>
<table border="0" class="center" width="90%" summary="main content">
<tr><td class="align">
<?php
include_once('inc/head.inc.php');
Head::printUnknown($aUnknown);

if ($homoSpecies) {
	$web->printHomologyWarning($homoSpecies, $type);
	$web->printSyntenyWarn($synteny);
}

	$evidencetxt = false;
	if ($eviCode['dir']) {
		$evidencetxt .= "DNA binding evidence";
	} elseif ($eviCode['indir']) {
		$evidencetxt .= "Expression evidence";
	} elseif ($eviCode['and']) {
		$evidencetxt .= "Simultaneous DNA binding and expression evidence";
	} elseif ($eviCode['plus']) {
		$evidencetxt .= "DNA binding plus expression evidence";
	}
	print "<p>Documented regulations are filtered by:<br/>&nbsp;- $evidencetxt";
	if ($assocType['pos'] || $assocType['neg']) {
		print ";<br/>&nbsp;- TF acting as " . ($assocType['pos']?"activator":"");
		print ($assocType['neg']?($assocType['pos']?" or ":"") . "inhibitor":"");
	}
	print ".</p>";
	if ($ecgroup['big']) {
		print "<p>Environmental conditions filtered by {$ecgroup['big']}";
		if ($ecgroup['sub']) print ", in particular {$ecgroup['sub']}";
		print ".</p>";
	}


#if ( isset($docReg) && count($docReg)>0 ) {

	$csv = "";
?>

<table border="1" summary="main">
	<tr>
		<th rowspan="2">ORF/Gene Name
<?php if ($homoSpecies) { print "<br/>(&rarr; Homologous gene)"; } ?></th>
<?php if ($regs['doc'] ) { ?>
		<th>Documented</th>
<?php } ?>
	</tr>
	<tr>
<?php
	if ($homoSpecies) {
		print "<th>Transcription&nbsp;Factor&nbsp;(&rarr; Homologous gene)</th>";
	} else {
		print "<th>Transcription&nbsp;Factor&nbsp;-&nbsp;Reference</th>";
	}
	print "</tr>";
	foreach (array_keys($aORFids) as $orfid) {
	        $orf = $aORFids[$orfid]['orf'];
		$gene = $aORFids[$orfid]['gene'];
		$prot = $aORFids[$orfid]['prot'];
		$jsmenu = "menu$orf";
?>

	<tr>
		<td class="align" id="<?=$orf?>">
		    <a name="<?=$orf?>" href="view.php?existing=locus&amp;orfname=<?=$orf?>">
<?=$orf.(($orf != $gene)?"/".$gene:"")?></a>
<?php
	    if ($homoSpecies){
		    if (isset($mapping[$orf])) {
			    foreach($mapping[$orf]['gene'] as $mapkey=>$mapgene){
				    $linkP = "";
				    if ($mapgene=='Uncharacterized') $mapgene = $mapping[$orf]['orf'][$mapkey];
				$linkP .= "<a href=\"".$web->getDBurl($homoSpecies);
	                        $linkP .= "view.php?existing=locus&amp;orfname={$mapping[$orf]['orf'][$mapkey]}\">$mapgene</a>&nbsp;";
			    	   print "<br/>(&rarr; ".$linkP.")";
	                   }
		    }
		    else { print "<br/>(&rarr; No homologs )"; }
	    }
?>
		</td>
		<td class="align">
<?php
		if (isset($docReg[$orfid])) {
			// Javascript submenus
			$aTFsForm = array();
			foreach ($docReg[$orfid] as $tfid) {
				$p = $aTFinfo[$tfid]['prot'];
				$aTFsForm[] = $p;
				$csv .= "$orf\t$gene\t$p\tDoc\n";
			}
			$items[0] = array("ranktf",  "Rank by TF", "formrankbytf.php", 1, 1);
			$items[1] = array("tfs",     "Search for TFs", "formfindregulators.php", 1, 1);
			$items[2] = array("tgs",     "Search for Genes", "formfindregulated.php", 1, 1);
			$items[3] = array("regs",    "Search for Associations", "formregassociations.php", 1, 1);
			$items[4] = array("orfgene", "ORF List &hArr; Gene List", "formorftogene.php", 1, 0);
			$items[5] = array("regviz",  "Interactive Network Image", "regnetviz.php", 1, 1);
			$web->jsmenuDraw("doc".$orf, $items, $aTFsForm, array($gene),
				$regs, $eviCode, $ecgroup, $assocType);
			
			if ($homoSpecies) {
				foreach ($docReg[$orfid] as $tfid) {
					$p = $aTFinfo[$tfid]['prot'];
					$homoP = $mapping[$tfid]['gene'];
?>
			<a href="view.php?existing=protein&amp;proteinname=<?=$p?>"><?=$p?></a>
				(&rarr; <a href="<?=$web->getDBurl($homoSpecies);?>view.php?existing=protein&amp;proteinname=<?=$homoP?>"><?=$homoP?></a>) <br/>
<?php
				}
			} else {
				foreach ($docReg[$orfid] as $tfid) {
					$p = $aTFinfo[$tfid]['prot'];
?>
		    <a href="view.php?existing=protein&amp;proteinname=<?=$p?>"><?=$p?></a>
				-
		<?php
$web->printRef($p,$orf);
print "<br/>";
				}
			}
		} else print "Not found!"; ?>
		</td>

	</tr>

<?php } ?>
</table>
<?php

$tmppath = $web->getProp("path.tmp.local");
$fname = "findregulators_".rand() . ".csv";
file_put_contents($tmppath . $fname, $csv);
$lcsv = $web->getProp("path.tmp.url") . $fname;
/*} else { # if no results // THIS WAY THE USER EXPLICITLY KNOWS IF NO HOMOLOUGS WERE FOUND OR IF THERE IS NO TFS
 * ?>
<span class="error">No transcription factors found!</span>
<?php
}*/
?>
</td>
<td class="align">
	<table border="0" class="right" summary="quick jump">
	<tr><th>Jump&nbsp;to&nbsp;<img src="images/s_desc.png" alt="gene" /></th></tr>
		<tr><td class="smallfunc">
<?php
foreach (array_keys($aORFids) as $orfid) {
	$orf = $aORFids[$orfid]['orf'];
	$gene = $aORFids[$orfid]['gene'];
    print "<a href=\"#$orf\">$orf";
	if (($orf != $gene)&&($gene!='Uncharacterized')) {
	    print "&nbsp;-&nbsp;" . $gene;
	}
	print "</a><br/>";
}
?>
		</td></tr>
    </table>
</td></tr>
<tr><td>
<input type="reset" value="Go Back" onclick="javascript:history.back()" />
<?php
if (count($docReg)>0 ) {
?>
<small>Download as <a href="<?=$lcsv?>">csv</a> file.</small>
<?php 
}
?>
</td></tr></table>
</table>

<?php

}


$web->printFooter();
?>
<script type="text/javascript" src="inc/menu.js"></script>
<script type="text/javascript">
<!--
function selectAll() {
    for (var i=0; i < document.forms['formtf'].elements.length; i++) {
		if (document.forms['formtf'].elements[i].type=="checkbox") {
        	document.forms['formtf'].elements[i].checked = true;
        }
    }
    document.getElementsByName('filterdoc')[0].checked = false;
}   
function selectNone() {
    for (var i=0; i < document.forms['formtf'].elements.length; i++) {
		if (document.forms['formtf'].elements[i].type=="checkbox") {
        	document.forms['formtf'].elements[i].checked = false; 
        }
    }
}
function selectCommon() {
	selectNone();
<?php
if ($regs['pot']){
foreach ($commonTFs as $tf) {
	print "  document.getElementsByName('$tf')[0].checked = true;\n";
}
}
?>
}
-->
</script>
<?php
$web->closePage();
?>
