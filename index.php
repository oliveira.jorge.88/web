<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home","");
?>

<div class="body">
	<table class="body" style="line-height:120%;">
		<tr>
			<td width="20%">&nbsp;</td>
			<td class="justify">

<p>
	<div class="title">Welcome to <?=$web->getProp("site.name")?></div>
</p>

<p><?=$web->getProp("site.name")?></b> is a framework for the semi-automatic generation of biologic databases from GenBank files.</p>
<p> The framework supports multi-species deposit of known regulatory associations between transcription factors (TF) and target genes, based on bibliographic references.</p>

<p>
Facilities are also provided to enable the exploitation of the gathered data when solving a number of biological questions, as exemplified in the Tutorial.
The framework allows the identification of documented or potential transcription regulators of a given gene and of documented or potential regulons for each transcription factor.
It also renders possible the comparison between DNA motifs and the transcription factor binding sites described in the literature.
Finally, the system provides a useful tool for grouping a list of genes (for instance a set of genes with similar expression profiles as revealed by microarray or RNA-seq analysis) based on their regulatory associations with known transcription factors.
All analysis can be carried out based on the existing data for the species the user is working on or based on transcription associations occurring among orthologous genes and TFs in the remaining species.
</p>

<table width="100%">
	<tr>
		<td width="10%">&nbsp;</td>
		<td width="30%">
<form method="post" action="scerevisiae/index.php"><button>
Enter<br/>
<i>S. cerevisiae</i> <img height="16" src="images/logo_scerevisiae.png" alt="species icon"/>
</button></form>
		</td>
		<td width="10%">&nbsp;</td>
		<td width="30%">
<form method="post" action="rtoruloides/index.php"><button>
Enter<br/>
<i>R. toruloides</i> <img height="16" src="images/logo_rtoruloides.png" alt="species icon"/>
</button></form>
		</td>
		<td width="10%">&nbsp;</td>
	</tr>

	<tr><td colspan="5"><p>&nbsp;</p></td></tr>

	<tr>
		<td width="10%">&nbsp;</td>
		<td width="30%">
<form method="post" action="anotherspecies/index.php"><button>
Enter<br/>
<i>Another species</i> <img height="16" src="images/logo_another.png" alt="species icon"/>
</button></form>
		</td>
	</tr>
</table>
	
			</td><td width="20%">&nbsp;
<?php
$web->printFooter();
$web->closePage();
?>
