<?php
$tf = $_GET['proteinname'];

include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Regulon Venn Diagram","javascript:history.back()",
"Result", "");
$web->printBarMenu((empty($regs)?"":$tf) . " Regulon Venn Diagram");

//----------------

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

list($regs, $regData, $aTGs, $potcounts, $pottargets) =
	$service->getRegulon($tf);

//----------------

$B  = array();
$E  = array();
$BE = array();
$U  = array();

$P = $pottargets;
$PB = array();
$PE = array();
$PBE = array();
$PU = array();

if (!empty($regs)) {
	foreach (array_keys($regs) as $tgid) {
		$tg = $aTGs[$tgid]['orf'];
		if ($regData[$regs[$tgid]]['evd'] == "bindonly") {
			$B[]  = $tg;
			if (isset($pottargets[$tg])) $PB[] = $tg;
		} elseif ($regData[$regs[$tgid]]['evd'] == "bindexpr") {
			$B[]  = $tg;
			$BE[] = $tg;
			$E[]  = $tg;
			if (isset($pottargets[$tg])) $PBE[] = $tg;
		} elseif ($regData[$regs[$tgid]]['evd'] == "expronly") {
			$E[]  = $tg;
			if (isset($pottargets[$tg])) $PE[] = $tg;
		} else {
			$U[]  = $tg;
			if (isset($pottargets[$tg])) $PU[] = $tg;
		}
	}

	// Build JSON file
	$json = "";
	if (!empty($B)) {
		$json .= "{sets: [0], label: \"DNA Binding evidence (".sizeof($B);
		$json .= ")\", size: ".sizeof($B)."},\n";
	}
	if (!empty($E)) {
		$json .= "{sets: [1], label: \"Expression evidence (".sizeof($E);
		$json .= ")\", size: ".sizeof($E)."},\n";
	}
	if (!empty($BE)) {
		$json .= "{sets: [0, 1], label: \"DNA Binding & Expression evidence (";
		$json .= sizeof($BE).")\", size: ".sizeof($BE)."},\n";
	}
	if (!empty($U)) {
		$json .= "{sets: [2], label: \"Unspecified (".sizeof($U);
		$json .= ")\", size: ".sizeof($U)."},\n";
	}
	if (!empty($P)) {
		$json .= "{sets: [3], label: \"Potential (".sizeof($P);
		$json .= ")\", size: ".sizeof($P)."},\n";
		if (!empty($PB)) {
			$json .= "{sets: [0, 3], label: \"DNA Bind & Potential (".sizeof($PB);
			$json .= ")\", size: ".sizeof($PB)."},\n";
		}
		if (!empty($PE)) {
			$json .= "{sets: [1, 3], label: \"Expression & Potential (".sizeof($PE);
			$json .= ")\", size: ".sizeof($PE)."},\n";
		}
		if (!empty($PU)) {
			$json .= "{sets: [2, 3], label: \"Unspecified & Potential (".sizeof($PU);
			$json .= ")\", size: ".sizeof($PU)."},\n";
		}
		if (!empty($PBE)) {
			$json .= "{sets: [0, 1, 3], label: \"DNA Bind & Expression & Potential (".sizeof($PBE);
			$json .= ")\", size: ".sizeof($PBE)."},\n";
		}

	}
	$json = "[\n$json]";
	list($url,$name) = $service->saveTmpFile($json, "VennRegulon", "json");
}
?>

<table border="0" class="center" summary="main content">
	<tr>
		<td class="align">
<?php
if (!empty($regs)) {
	include_once('d3/VennRegulon.php');
	$d3 = new VennRegulon();
	$d3->draw($service, $url, $name, $tf, $aTGs, $B, $BE, $E, $U, $P, $potcounts, $PB, $PE, $PBE, $PU);
} else {
?>
	<span class="error">No documented regulations were found for '<?=$tf?>'!</span>
<?php
}
?>
		</td>
	</tr>
	<tr>
		<td class="align">
		<input type="reset" value="Go Back" onclick="javascript:history.back()" />
		</td>
	</tr>
</table>

<?php
$web->printFooter();
$web->closePage();
?>
