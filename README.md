# README #

Repository of the web PHP code for CommunityYeastract

## Requirements

CommunityYeastract will run on any system with a webserver (e.g. [Apache](https://httpd.apache.org/), a relational database (e.g. [MariaDB](https://mariadb.org/), [PHP](https://www.php.net/), and the appropriate libraries to connect them.

    [user@server ~]$ apt install apache2 mariadb-server php libapache2-mod-php php-mysql

Some functionalities may only work if additional software/libraries are also installed:
+ For static image processing and generation, ensure that [php-gd] is installed.
+ For the generation of regulatory graphs, `dot` command is used, which is part of [Graphviz].
+ [R] is necessary to calculate p-value using hypergeometric test. Only the `r-base` package is needed, which provides the command line interface.
+ [ESPRESSO] is necessary for the IUPAC code generation tool.

To install the above dependencies, you should do the following (or similar, depending on your OS/distribution):

    [user@server ~]$ apt install php-gd graphviz r-base

## Database

Instructions on how to create the database are available in the `db_load/mysql.README.md` file.

## Webserver

Once the webserver is installed you should clone this project on you local server `public_html`:

    [user@server ~]$ cd public_html
    [user@server public_html]$ git clone git@gitlab.com:oliveira.jorge.88/web.git

The `tmp/` directory must be declared in the `conf/path.properties`, since many of the functionalities may not work unable to write temporary files.
Also, the user must change the directory permissions in order to be writable by the webserver.

    [user@server public_html]$ cd web/
    [user@server public_html/web]$ chmod og+w tmp

Once installed, the local path of each of these additional software/libraries mentioned above must be configured in `conf/path.properties`.
This file contains key/value pairs defining the directory location and the name of the binary to be used by the webserver.

Example:

    prog.R.path = /usr/bin/
    prog.R.bin = Rscript

### Test the webserver

Once the webserver and the database are installed and configured, you can test your:
- local webserver installation by checking the URL `http://localhost` on your browser;
- local CommunityYeastract by checking the URL `http://localhost/~user/web/`.

This assumes that you have a `public_html` directory containing the CommunityYeastract `web/` directory, and that your webserver configuration has the [`UserDir`](https://httpd.apache.org/docs/2.4/howto/public_html.html) directive activated. Also, replace the `user` in the `path.properties` file accordingly.

### Who do I talk to? ###

Any problems and/or questions please contact [Jorge Oliveira](https://web.tecnico.ulisboa.pt/jorge.oliveira/) and/or [Pedro T. Monteiro](http://pedromonteiro.org).

[Graphviz]: https://graphviz.org/
[R]: https://www.r-project.org/
[Espresso]: https://ptolemy.berkeley.edu/projects/embedded/pubs/downloads/espresso/index.htm
[php-gd]: https://www.php.net/manual/en/image.installation.php
