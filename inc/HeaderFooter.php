<?php
class HeaderFooter {
	private $_indexRobots;
	private $_webPath;
	private $_localPath;
	private $_relPath;
	private $_css;
	private $_props;
	public $_site;
	public $_dbname;

	// From URL gets the Site/DB name.
	// Everything should read from these attributes!
	
	public function __construct($index = "") {
		$this->_relPath = "";
		$this->_indexRobots = $index;
		if (file_exists('robots.txt')) {
			$this->_localPath = "";
			$this->_webPath = "";
		} else {
			$this->_localPath = "../";
			$this->_webPath = "../";
		}
		include($this->_localPath . 'conf/Properties.php');
		$this->_props = new Properties($this->_localPath);
		list($this->_site, $this->_dbname) = $this->_props->getSiteDB();
	}
	public function __destruct() {
	}

	public function setRelativePath($path) {
		$this->_relPath = $path;
	}

	public function getProp($k) {
		return $this->_props->get($k);
	}

	private function GET2POST() {
		if (isset($_GET)) {
			foreach ($_GET as $k => $v) {
				$_POST[$k] = $v;
			}
		}
	}

	public function setPOSTdefaultVal($field, $val) {
		if (!isset($_POST) || !isset($_POST[$field])) {
			$_POST[$field] = $val;
		}
	}

	public function getQueryParams($getParams = false) {
		if ($getParams) {
			$this->GET2POST();
		}

		// Old type="docpot" -> $doc = true & $pot = true
		$regs = array(
			"doc"  => (isset($_POST) && isset($_POST['type']) && $_POST['type']=="doc" )? true: false,
			"pot"  => (isset($_POST) && isset($_POST['type']) && $_POST['type']=="pot" )? true: false,
			"both" => (isset($_POST) && isset($_POST['type']) && $_POST['type']=="both")? true: false,
		);
		$eviCode = array(
			"dir"   => (isset($_POST) && isset($_POST['evidence']) && $_POST['evidence']=="dir"  )? true: false,
			"indir" => (isset($_POST) && isset($_POST['evidence']) && $_POST['evidence']=="indir")? true: false,
			"plus"  => (isset($_POST) && isset($_POST['evidence']) && $_POST['evidence']=="plus" )? true: false,
			"and"   => (isset($_POST) && isset($_POST['evidence']) && $_POST['evidence']=="and"  )? true: false,
		);
		$assocType = array(
			"pos" => (isset($_POST) && isset($_POST['t_pos']) && $_POST['t_pos']=="true")? true: false,
			"neg" => (isset($_POST) && isset($_POST['t_neg']) && $_POST['t_neg']=="true")? true: false,
			"NA"  => (isset($_POST) && isset($_POST['use_na']) && $_POST['use_na']=="true")? true: false,
		);
		$ecgroup = array(
			"big" => (isset($_POST) && isset($_POST['biggroup']) && $_POST['biggroup']!="----")? $_POST['biggroup']: false,
			"sub" => (isset($_POST) && isset($_POST['subgroup']) && $_POST['subgroup']!="----")? $_POST['subgroup']: false,
		);

		// Add default values whenever GET queries don't specify everything
		if ($getParams) {
			if (!$regs["doc"] && !$regs["pot"] && !$regs["both"]) $regs["doc"] = true;
			if (!$eviCode["dir"] && !$eviCode["indir"] && !$eviCode["plus"] && !$eviCode["and"]) $eviCode["plus"] = true;
			if (!$assocType["pos"] && !$assocType["neg"]) { $assocType["pos"] = true; $assocType["neg"] = true; $assocType["NA"] = true; }
		}
		return array($regs, $eviCode, $assocType, $ecgroup);
	}

	function printHeader() {
		// Favicon: common pages vs specific ones
		$favicon = empty($this->_dbname)? "nospecies" : $this->_dbname;
		$title = empty($this->_dbname)? "":
			$this->_props->get("db.$this->_dbname.short") . " - " .
			$this->_props->get("site.name");
?>
<!DOCTYPE html 
	PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="robots" content="<?=$this->_indexRobots?>index, follow"/>
		<title><?=$title?></title>
		<link href="<?=$this->_localPath?>css/default.css" rel="stylesheet" type="text/css"/>
		<link href="<?=$this->_localPath?>images/logo_<?=$favicon?>.png" rel="icon" type="image/png"/>
		<script type="text/javascript" src="inc/site.js"></script>
	</head>
	<body>

<div class="header">
	<table class="Hlogos">
		<tr>
			<td class="headerLeft">
				<table border="0"><tr><td>
<?php
			$path = $this->_webPath;
			if (!empty($this->_dbname)) {
?>
				<a class="main" href="<?=$path?>index.php">
					<img class="logo" src="<?=$path?>images/logo_<?=$favicon?>.png" alt="Logo" height="45" />
				</a>
<?php } ?>
				</td><td>
<?php if (!empty($this->_site)) {
?>
				<small><a class="main" href="<?=$this->_webPath?>index.php">
					<?=$this->_props->get("site.name")?></a>
				<br/>
				<small><a class="main" href="index.php">
					<i><?=$this->_props->get("db.$this->_dbname.short")?></i></a></small></small>
<?php } ?>
				</td></tr></table>
			</td>
			<td class="headerCenter">
				<a href="<?=$this->_localPath?>contact.php">
				<big><a class="main" href="<?=$this->_webPath?>">
					<?=$this->_props->get("site.name")?></a></big>
				</a>
			</td>
			<td class="headerRight">
					<a href="<?=$this->_relPath?>contact.php">
					<img src="<?=$this->_relPath?>images/logo_institute1.png" height="45" alt="Institute 1" />
					<img src="<?=$this->_relPath?>images/logo_institute2.png" height="45" alt="Institute 2" />
				</a>
			</td>
		</td>
	</table>
	<table class="Htopbar">
		<tr>
			<td class="headerLeft">
<?php
		if (!empty($this->_site)) {
			$arg_list = func_get_args();
			$num_args = count($arg_list);
			for ($i = 0; $i < $num_args; $i+=2) {
				if (!isset($arg_list[$i+1]) || $arg_list[$i+1] == "") {
					print $arg_list[$i];
				} else {
					print "<a href=\"".$arg_list[$i+1]."\">".$arg_list[$i]."</a>";
				}
				if (($i+2) < $num_args)
					print "&nbsp;>&nbsp;";
			}
?>
			</td>
			<td class="headerCenter">
<?php
		// Empty space for something...
?>
			</td>
			<td class="headerRight">
				<a href="contact.php">Contact/Credits</a>
				&nbsp;&nbsp;&nbsp;
<?php
		if (empty($this->_dbname)) {
?>
				<a href="cite.php">How to cite</a>
				&nbsp;&nbsp;&nbsp;
				<a href="acknowledgments.php">Acknowledgments</a>
<?php	} else { ?>
				<a href="help_index.php">Help</a>
<?php
			}
		}
?>
			</td>
		</tr>
	</table>
</div>
<?php
	}


	function printBarMenu($title) {
		$this->_props = new Properties();
?>
<div class="body">
	<table class="body">
		<tr>
			<td class="leftBar" rowspan="2">
<ul class="bar">
	<li >
		<form method="get" id="quicksearch" action="quicksearch.php">
			<input type="text" size="15" name="q" placeholder="Search..." required />
		</form>
	</li>
	<li class="barTitle">
		Cross species
	</li>
	<li class="bar">
	  <a title="Promoter analysis"
		href="<?=$this->_relPath?>formpromanalysis.php">Promoter&nbsp;analysis</a>
	</li>
	<li class="bar">
	  <a title="Homologous network analysis"
		href="<?=$this->_relPath?>formregnetviz.php">Homologous&nbsp;network</a>
	</li>
	<li class="bar">
	  <a title="Network comparison analysis"
		href="<?=$this->_relPath?>formregnetcomp.php">Network&nbsp;comparison</a>
	</li>
	<li class="barTitle">
	Rank genes
	</li>
	<li class="bar">
	  <a title="Rank genes by given TFs"
		href="<?=$this->_relPath?>formrankbytf.php">Rank by TF</a>
	</li>
	<li class="bar">
	  <a title="Rank genes by associated GO terms"
		href="<?=$this->_relPath?>formrankbygo.php">Rank by GO</a>
	</li>
	<li class="barTitle">
	  Regulatory Associations
	</li>
	<li class="bar">
	  <a title="For gene(s), find its documented/potential TFs."
		href="<?=$this->_relPath?>formfindregulators.php">Search for TFs</a>
	</li>
	<li class="bar">
	  <a title="For TF(s), find its documented/potential regulated genes"
		href="<?=$this->_relPath?>formfindregulated.php">Search for Genes</a>
	</li>
	<li class="bar">
	  <a title="Search regulatory associations between given genes and TFs"
		href="<?=$this->_relPath?>formregassociations.php">Search for Associations</a>
	</li>
	<li class="barTitle">
	  Pattern Matching
	</li>
	<li class="bar">
	  <a title="Search genes or TF having given DNA motif in their promoter or binding site respectively"
		href="<?=$this->_relPath?>formsearchbydnamotif.php">Search by DNA Motif</a>
	</li>
	<li class="bar">
	  <a title="Find TF binding sites in promoter sequence"
		href="<?=$this->_relPath?>formtfsbindingsites.php">Find TF Binding Site(s)</a>
	</li>
	<li class="barTitle">
	  Utilities
	</li>
	<li class="bar">
	  <a title="Search for Orthologs"
		href="<?=$this->_relPath?>formorthologs.php">Search&nbsp;for&nbsp;Orthologs</a>
	</li>
	<li class="bar">
		<a title="Generate IUPAC representation"
		href="<?=$this->_relPath?>iupacgeneration.php">IUPAC Code Generation</a>
	</li>
	</li>
	<li class="bar">
	  <a title="Find ORF name for Gene name and vice-versa"
		href="<?=$this->_relPath?>formorftogene.php">ORF List &hArr; Gene List</a>
	</li>
	<li class="barTitle">
	  Retrieve
	</li>
	<li class="bar">
	  <a title="Transcription Factor &hArr; Consensus association list"
		href="<?=$this->_relPath?>consensuslist.php">TF-Consensus List</a>
	</li>
	<li class="bar">
	  <a title="Regulation Matrix"
		href="<?=$this->_relPath?>formregmatrix.php">Regulation&nbsp;Matrix</a>
	</li>
	<li class="bar">
	  <a title="Retrieve upstream sequence for given ORF/Gene(s)"
		href="<?=$this->_relPath?>formseqretrieval.php">Upstream Sequence</a>
	</li>
	<li class="barTitle">
	About
	</li>
	<li class="bar">
		<a title="Send a suggestion or question to curators"
		href="<?=$this->_localPath?>contact.php">Contact/Credits</a>
	</li>
	<li class="bar">
		<a title="How to Cite"
		href="<?=$this->_localPath?>cite.php">How to cite</a>
	</li>
	<li class="bar">
		<a title="Acknowledgments"
		href="<?=$this->_localPath?>acknowledgments.php">Acknowledgments</a>
	</li>
</ul>

				<p>&nbsp;</p>
				<div class="backTop">
					<a href="#TOP">Back to top <img src="<?=$this->_localPath?>images/s_asc.png" alt="top" /></a>
				</div>
			</td>
			<td class="content" colspan="2">
				<p>
				<div class="title"><?=$title?></div>
				</p>
<?php
	}


	function printFooter() {
?>
			</td> <!-- content -->
		</tr>
		<tr>
			<td class="footerLeft">
			</td>
			<td class="footerRight">
				<a href="http://validator.w3.org/check?uri=referer">
				<img src="<?=$this->_localPath?>images/icon_xhtml.png" alt="w3c xhtml validator" /></a>
				<a href="http://jigsaw.w3.org/css-validator/check?uri=referer"><img src="<?=$this->_localPath?>images/icon_css.png" alt="w3c css validator" /></a>
			</td>
		</tr>
	</table>
</div> <!-- body -->
<?php
	}

	function closePage() {
		$gProp = empty($this->_dbname)?
			$this->_props->get("site.$this->_site.ganalytics"):
			$this->_props->get("db.$this->_dbname.ganalytics");
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', '<?=$gProp?>', 'auto');
  ga('send', 'pageview');
</script>
	</body>
</html>
<?php
	}

	function strainSampleIcons($fieldType, $callBack = "") {
		foreach (explode("\t", $this->_props->get("db.$this->_dbname.dbstrains")) as $strain) {
			$f = $callBack;
			foreach ($fieldType as $field => $type) {
				$data = $this->_props->get("db.$this->_dbname.$strain.$type");
				$f .= "fieldData('$field','$data');";
			}
?>
	<a href="javascript:<?=$f?>" title="Sample strain <?=$strain?>"><img src="../images/sampledata.gif" alt="Sample strain <?=$strain?>" /></a>
<?php
		}
	}
	

	function printJSEmail($user, $server) {
		$pos = round(strlen($user) / 2, 0, PHP_ROUND_HALF_DOWN);
		$u1  = substr($user, 0, $pos);
		$u2  = substr($user, $pos);
		$pos = round(strlen($server) / 2, 0, PHP_ROUND_HALF_DOWN);
		$p1  = substr($server, 0, $pos);
		$p2  = substr($server, $pos);
?><script type="text/javascript">
<!--
u1 = "<?=$u1?>";
u2 = "<?=$u2?>";
p1 = "<?=$p1?>";
p2 = "<?=$p2?>";
document.write('<' + 'a ' + 'href=\"mailto:' + u1 + u2 + '@' + p1 + p2 + '\">');
document.write(u1 + u2 + '@' + p1 + p2 + '<' + '/a>');
-->
</script><?php
	}


	   /*
     * get2Form
     */
    function get2Form($name) {
        return (isset($_GET) && isset($_GET[$name]))? str_replace(array("|"," ","+"), "\n", $_GET[$name]): false;
    }
 	
		function post2Form($name) {
			return (isset($_POST) && isset($_POST[$name]))? str_replace(array("|"," ","+"), "\n", $_POST[$name]): false;
		}

    /*
     * jsmenuDraw
     */
        function jsShowList($name,$aTGs,$species=false) {
                $lk = "lookup$name";
                $sm = "drop$name";
                $fn = "form$name";
?>
        <a id="<?=$lk?>"
                onclick="javascript:showActionMenu('<?=$sm?>');"
                onmouseover="javascript:showActionMenu('<?=$sm?>');"
		onmouseout="javascript:hideActionMenu('<?=$sm?>');" />
			Go to ORF &rarr;
                <div id="<?=$sm?>" style=" display:none;">
                        <table summary="javascript list of genes" class="jsmenu" 
                onmouseover="javascript:showActionMenu('<?=$sm?>');" 
                onmouseout="javascript:hideActionMenu('<?=$sm?>');">
                                <tr id="title">
				</tr>
<?php
		foreach($aTGs as $orf) {
			$id = $name.$orf;
			$url  = $this->hrefORF($orf,$species);

?>
            <tr id="<?=$id?>">
                <td class="jsmenu"
                        onmouseover="javascript:highlightCell('<?=$id?>');"
                        onmouseout="javascript:unHighlightCell('<?=$id?>');"
			onclick="location.href='<?=$url?>';">
			<?=$orf?>
                </td>
            </tr>
<?php
                }
?>
        </table>
        </div>
<?php
        }


    /*
     * jsmenuDraw
     */
	function jsmenuDraw($name, $items, $aTFs, $aTGs,
		$regtype, $eviCode, $ecgroup, $assocType, $species=false) {
		$lk = "lookup$name";
		$sm = "drop$name";
		$fn = "form$name";
		$sTFs = implode("\n",$aTFs);
		$sTGs = implode("\n",$aTGs);
?>
	<img src="<?=$this->_localPath?>images/menu.png" height="13px" alt="send data to" id="<?=$lk?>"
		style="float:right" 
		onclick="javascript:showActionMenu('<?=$sm?>');"
		onmouseover="javascript:showActionMenu('<?=$sm?>');"
		onmouseout="javascript:hideActionMenu('<?=$sm?>');" />
		<form name="<?=$fn?>" id="<?=$fn?>" method="post">
			<input type="hidden" id="formtfs" name="formtfs"
				value="<?=$sTFs?>" />
			<input type="hidden" id="formtgs" name="formtgs"
				value="<?=$sTGs?>" />
			<input type="hidden" id="type" name="type"
				value="<?=$regtype['doc']?"doc":($regtype['pot']?"pot":"both")?>" />
			<input type="hidden" id="evidence" name="evidence"
				value="<?=$eviCode['dir']?"dir":($eviCode['indir']?"indir":($eviCode['plus']?"plus":"and"))?>" />
			<input type="hidden" id="t_pos" name="t_pos"
				value="<?=$assocType['pos']?"true":""?>" />
			<input type="hidden" id="t_neg" name="t_neg"
				value="<?=$assocType['neg']?"true":""?>" />
			<input type="hidden" id="biggroup" name="biggroup"
				value="<?=$ecgroup['big']?>" />
			<input type="hidden" id="subgroup" name="subgroup"
				value="<?=$ecgroup['sub']?>" />
		</form>
		<div id="<?=$sm?>" style="float:right; display:none;">
			<table summary="javascript menu table to copy results of one query to another" class="jsmenu" 
        	onmouseover="javascript:showActionMenu('<?=$sm?>');" 
		onmouseout="javascript:hideActionMenu('<?=$sm?>');">
				<?php $id = "orflist"; ?>
				<tr id="title"><th>Send ORF/Gene list to:</th></tr>
				<tr id="<?=$id?>">
					<td class="jsmenu"
				      		onmouseover="javascript:highlightCell('<?=$id?>');"
						onmouseout="javascript:unHighlightCell('<?=$id?>');">
					<?php
						$this->jsShowList($name."list",$aTGs,$species);
?>
				        </td>
            			</tr>
<?php
		for ($i=0; $i<count($items); $i++) {
		    $id   = $items[$i][0] . $name;
		    $desc = $items[$i][1];
		    $url  = $items[$i][2];
?>
            <tr id="<?=$id?>">
                <td class="jsmenu"
                	onmouseover="javascript:highlightCell('<?=$id?>');"
                	onmouseout="javascript:unHighlightCell('<?=$id?>');"
							onclick="javascript:sendTo('<?=$fn?>', '<?=$url?>', <?=$items[$i][3]?>, <?=$items[$i][4]?>);">
                    <?=$desc?>
                </td>
            </tr>
<?php	    
		}
?>
        </table>
	</div>
<?php
	}

	function printHomologyWarning($species, $type) {
?>
	<p>
		Search of regulatory associations by homology with <?=$type?> regulations in <i><?=$species?></i>.
		<br/>
		<span class="error">
			Please notice that the results obtained with this query are merely indicative, given that:<br/>
			1) the orthologs from different species are predicted, at CGD, based on sequence homology;<br/>
			2) the conservation of regulatory associations among different species is not complete.
		</span>
	</p>
	<p>
		All regulation filters have been deliberately ignored!
	</p>
<?php
	}

	function getDBurl($sDB) {
		return $this->_props->getDBurl($sDB);
	}

	function printPotential($orf,$consensus,$species=false){

                if ($species) {
                        $url = $this->getDBurl($species);
                        $link = "<a href=$url"."viewconsensusinpromoter.php?orfname=$orf&amp;consensus=$consensus>$orf</a> " ;
                }
                else { $link = "<a href=viewconsensusinpromoter.php?orfname=$orf&amp;consensus=$consensus>$orf</a> "; }

                print $link;
        }

	function printConsensus($protein,$consensus,$species=false){

		if ($species) { 
			$url = $this->getDBurl($species); 
			$link = "<a href=$url"."view.php?existing=consensus&amp;proteinname=$protein&amp;consensus=$consensus>$consensus</a> " ; 
		}
		else { 	$link = "<a href=view.php?existing=consensus&amp;proteinname=$protein&amp;consensus=$consensus>$consensus</a> "; }

                print $link;
        }

	function printGene($gene,$orf,$showorf=false,$species=false){

		if ($species) { 
			$url = $this->getDBurl($species);
			$link = "<a href=$url"."view.php?existing=locus&amp;orfname=$orf> " ; 
		}
		else { $link = "<a href=view.php?existing=locus&amp;orfname=$orf> "; }

		if ($gene=='Uncharacterized'){ $link .= $orf;}
		else { 
			$link .= $gene;
			if ($showorf && $gene!=$orf) $link .= "/$orf";
		}

		$link .= "</a>";

		print $link;
	}

	function linkGene($gene,$orf,$showorf=false,$species=false){
		
		if ($species) {
			$url = $this->getDBurl($species);
		 	$link = "<a href=\"$url"."view.php?existing=locus&amp;orfname=$orf\">" ;
		} else { $link = "<a href=\"view.php?existing=locus&amp;orfname=$orf\">"; }

		if ($gene=='Uncharacterized'){ $link .= $orf;}
		else {
			$link .= $gene;
			if ($showorf && $gene!=$orf) $link .= "/$orf";
		}

		$link .= "</a>";

		return $link;
	}

	function Gene($gene,$orf,$showorf=false){

		if ($gene=='Uncharacterized'){ $link = $orf;}
		else {
			$link = $gene;
			if ($showorf && $gene!=$orf) $link .= "/$orf";
		}

		return $link;
	} 

	function printRef($tf,$o){
		$link = "<a href=view.php?existing=regulation&amp;proteinname=$tf&amp;orfname=$o>Reference</a>";
		print $link;
	}

	function printArr($arr) {

		print "<pre>";
		print_r($arr);
		print "</pre>";
	}

	function printSyntenyWarn($synteny) {
		if ($synteny=='0') { print "<p><strong>Considering homology only (No synteny)</strong></p>"; }
		else { print "<p><strong>Considering homologs with at least $synteny neighbor(s) in common</strong></p>"; }
	}

	function printSyntenyForm() {
		print " <b>Synteny Level:</b>
			<select id=\"doc-synteny\"  name=\"synteny\">
				<option value=\"0\">BLAST Best-Score (Only Homology)</option>
				<option value=\"1\">BLAST Best-Score + at least 1 neighbor</option>
				<option value=\"2\">BLAST Best-Score + at least 2 neighbors</option>
				<option value=\"3\">BLAST Best-Score + at least 3 neighbors</option>
			</select>";
	}

	function printErr($message) {
		print "<p><span class=\"error\">$message</span></p>";
        }

	function hrefORF($orf,$species=false){

                if ($species) {
                        $url = $this->getDBurl($species);
                        $link = $url."view.php?existing=locus&amp;orfname=".$orf ;
                } else { $link = "view.php?existing=locus&amp;orfname=".$orf; }

                return $link;
        }


}
