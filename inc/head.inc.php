<?php
class Head {
	const BR = "<br/>";
	const Finish = ".";

	const UNKNOWN_gene  = "Unknown gene/ORF name(s)";
	const UNKNOWN_tf    = "Unknown Transcription Factor name(s)";
	const UNKNOWN_motif = "Badly formed motif(s)";
	const TYPE_gene  = 0;
	const TYPE_tf    = 1;
	const TYPE_motif = 2;

	const NONReg_genes = "% of user inserted ORF/gene are not regulated by any TF";

	public static function printUnknown($aUnknown, $type = 0) {
		if (isset($aUnknown) && is_array($aUnknown) && count($aUnknown)>0) {

			switch ($type) {
			case self::TYPE_gene:
				print self::BR . self::UNKNOWN_gene;break;
			case self::TYPE_tf:
				print self::BR . self::UNKNOWN_tf;break;
			case self::TYPE_motif:
				print self::BR . self::UNKNOWN_motif;break;
			}

			foreach ($aUnknown as $o) {
				print ", '<b>$o</b>'";
			}
			print self::Finish;
		}
	}

	public static function printNonReg($aOrfGenes, $nonRegORFs) {
		if (!empty($nonRegORFs)) {
			$percent = 100 * count($nonRegORFs) / count($aOrfGenes);
			printf("%.0f", $percent);
			$tmp = self::NONReg_genes;
			foreach ($nonRegORFs as $orf => $g) {
				$tmp .= ", <b>$g</b>";
			}
			print $tmp . self::Finish;
		}
	}
}
?>
