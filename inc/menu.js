function highlightCell(rowId) {    //rowId is element id of the row to highlight
	document.getElementById(rowId).style.background = "lightgray";    //change 'ghostwhite' to the color you wish to be the highlight color
}

function unHighlightCell(cellId) {    //rowId is element id of the row to highlight
	document.getElementById(cellId).style.background = "ghostwhite";    //should match original tr.a backgound color
}

function showActionMenu(menuId) {     //anchorId is the id of the element that the menu should be anchored to, menuId is the id of the menu id
	document.getElementById(menuId).style.display = "inline"; //set the menu to visible
}

function hideActionMenu(menuId) {     //anchorId is the id of the element that the menu should be anchored to, menuId is the id of the menu id
	document.getElementById(menuId).style.display = "none"; //hide the menu
}
function sendTo(formID, url, boolTFs, boolTGs) {
	if (! boolTFs) {
		document.querySelector('form[name='+formID+'] input[name=formtfs').remove();
	}
	if (! boolTGs) {
		document.querySelector('form[name='+formID+'] input[name=formtgs').remove();
	}
	var formElem = document.getElementById(formID);
	formElem.action = url;
	formElem.submit();
}
