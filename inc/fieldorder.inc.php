<?php
class FieldOrder {
    private $_fields;
    
    function __construct() {
        $this->_fields = array();
        $this->initFields();
    }
    function __destruct() {
        unset($this->_fields);
    }
    
    private function initFields() {
        if (!isset($_GET)) return;
        
        foreach (array_keys($_GET) as $key) {
            if (strpos($key, "order") !== false) {
	            $this->_fields{substr($key, strlen("order"))} = $_GET[$key];
            }
        }
    }
    
    public function getField($key) {
        if (!isset($this->_fields{$key})) return false;
        return $this->_fields{$key};
    }
}

$fieldOrder = new FieldOrder();

function printField($fieldname) {
    global $fieldOrder;
    
    $order = "Descending";
    $tag = "desc";
	
    if ($fieldOrder->getField($fieldname) == "desc") {
	    $order = "Ascending";
	    $tag = "asc";
	}
?>
    <a href="<?=$_SERVER['PHP_SELF']?>?order<?=$fieldname?>=<?=$tag?>"
    title="<?=$order?>">
    <?=$fieldname?>&nbsp;<img border="0" class="icon" src="images/s_<?=$tag?>.png"
    alt="<?=$order?>" height="9" width="11"></a>
<?php
}
?>