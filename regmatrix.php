<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter("no");
$web->printHeader("Home", "index.php",
"Regulation matrix","javascript:history.back()",
"Result", "");
$web->printBarMenu("Regulation matrix");

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

$allTFs   = (isset($_POST) && isset($_POST['tfs']) && $_POST['tfs']!="inserted" && $_POST['tfs']!="dbtfs_except");
$exceptTF = (isset($_POST) && isset($_POST['tfs']) && $_POST['tfs']=="dbtfs_except");
$allGenes = (isset($_POST) && isset($_POST['allgenes']))? true: false;
$localSpecies = $service->aSpecies();

$regs = array(
	"doc"  => (isset($_POST) && isset($_POST['type']) && $_POST['type']=="doc" ),
	"pot"  => (isset($_POST) && isset($_POST['type']) && $_POST['type']=="pot" ),
	"plus" => (isset($_POST) && isset($_POST['type']) && $_POST['type']=="plus"),
	"and"  => (isset($_POST) && isset($_POST['type']) && $_POST['type']=="and" ),
);
$eviCode = array(
	"dir"   => (isset($_POST) && isset($_POST['evidence']) && $_POST['evidence']=="dir"  ),
	"indir" => (isset($_POST) && isset($_POST['evidence']) && $_POST['evidence']=="indir"),
	"plus"  => (isset($_POST) && isset($_POST['evidence']) && $_POST['evidence']=="plus" ),
	"and"   => (isset($_POST) && isset($_POST['evidence']) && $_POST['evidence']=="and"  ),
);
$assocType = array(
	"pos" => (isset($_POST) && isset($_POST['t_pos']) && $_POST['t_pos']=="true")? true: false,
	"neg" => (isset($_POST) && isset($_POST['t_neg']) && $_POST['t_neg']=="true")? true: false,
	"NA"  => (isset($_POST) && isset($_POST['use_na']) && $_POST['use_na']=="true")? true: false,
);
$ecgroup = array(
	"big" => (isset($_POST) && isset($_POST['biggroup']) && $_POST['biggroup']!="----")? $_POST['biggroup']: false,
	"sub" => (isset($_POST) && isset($_POST['subgroup']) && $_POST['subgroup']!="----")? $_POST['subgroup']: false,
);

$bind = (isset($_POST) && isset($_POST['bind']) && is_numeric($_POST['bind']))? $_POST['bind'] : "1";

$regulators = (isset($_POST) && isset($_POST['regulators']))? $_POST['regulators']: "";
$regulated  = (isset($_POST) && isset($_POST['regulated']))?  $_POST['regulated']:  "";
$ext = (isset($_POST) && isset($_POST['ext']))? $_POST['ext']: "svg";

?>
<table border="0" width="100%" summary="main content">
<tr><td class="align">
<?php
// all x all constraint
//if (!($allTFs && $allGenes && $exceptTF==false)) {

if (!$regs['pot']) {
	$evidencetxt = false;
	if ($eviCode['dir']) {
		$evidencetxt .= "DNA binding evidence";
	} elseif ($eviCode['indir']) {
		$evidencetxt .= "Expression evidence";
	} elseif ($eviCode['and']) {
		$evidencetxt .= "Simultaneous DNA binding and expression evidence";
	} elseif ($eviCode['plus']) {
		$evidencetxt .= "DNA binding plus expression evidence";
	}
	print "<p>Documented regulations are filtered by:<br/>&nbsp;- $evidencetxt";
	if ($assocType['pos'] || $assocType['neg']) {
		print ";<br/>&nbsp;- TF acting as " . ($assocType['pos']?"activator":"");
		print ($assocType['neg']?($assocType['pos']?" or ":"") . "inhibitor":"");
	}
	print ".</p>";
	if ($ecgroup['big']) {
		print "<p>Environmental conditions filtered by {$ecgroup['big']}";
		if ($ecgroup['sub']) print ", in particular {$ecgroup['sub']}";
		print ".</p>";
	}
}

print "<br/><br/>Processing regulations...";

ob_flush();
flush();

	list($aTFs, $tUnknown) = $service->user2Info(((($allTFs)||($exceptTF))?'':$regulators),$localSpecies);
	list($aORFGenes, $gUnknown) = $service->user2Info((($allGenes)?'':$regulated),$localSpecies);

	if ($exceptTF){
		list($badTFs, $tUnknown) = $service->user2Info($regulators,$localSpecies);
		$aTFs = array_diff_assoc($aTFs,$badTFs);
	}

	$reg = $service->getRegMatrix($aTFs, $aORFGenes, $regs, $bind, $ecgroup, $eviCode, $assocType);

include_once('inc/head.inc.php');
Head::printUnknown($tUnknown, Head::TYPE_tf); // For TFs
Head::printUnknown($gUnknown);

if (empty($reg)) {
        print "<p><span class=\"error\">No regulatory associations found!!</span></p>";
}
else {

// two-column table
	$table = "";
	$simple_reg = [];
foreach (array_keys($reg) as $tfid) {
	foreach (array_values($reg[$tfid]) as $orfid) {
		$simple_reg[$tfid][$orfid] = 1;
	        $table .= $aTFs[$tfid]['gene'].";".$aORFGenes[$orfid]['gene']."\n";  #"$tf\t$orf\n";
    }
}

// csv matrix
$csv = count($aORFGenes);
foreach (array_keys($aORFGenes) as $orfid) {
	$gene = $aORFGenes[$orfid]['gene'];
	$csv .= ";$gene";
}
foreach (array_keys($aTFs) as $tfid) {
	$prot = $aTFs[$tfid]['prot'];
	$csv .= "\n$prot";
	foreach (array_keys($aORFGenes) as $orfid) {
		if (isset($simple_reg[$tfid][$orfid])) { $csv .= ";1"; } 
		else {   $csv .= ";0";   }
  	}
}

try {
	$type = $regs['doc']?"Documented":"Potential";
	list($tablepath, $tablename) = $service->saveTmpFile($table, "RegulationTwoColumnTable_$type", "tsv");
	list($csvpath, $csvname)     = $service->saveTmpFile($csv, "RegulationMatrix_$type", "csv");    
?>
    Done!<br/>
    <table border="1"><tr>
    	<th align="center">Regulation matrix <small>(Semicolon Separated Values (CSV) file)</small></th>
    	<th align="center">Two-column table <small>(Tab Separated Values (TSV) file)</small></th>
	</tr><tr>
    	<td><a href="<?=$csvpath.$csvname?>"><?=$csvname?></a></td>
    	<td><a href="<?=$tablepath.$tablename?>"><?=$tablename?></a></td>
	</tr></table>
<?php
} catch (Exception $e) {
   	print $e->getMessage();
}


/* *************** DOT ******************* */
?>
<br/>
Generating Transcriptional Regulatory Network static image...
<?php
flush();

include_once('service/Image.php');
$image = new Image();

list($url, $name, $wimage) = $image->generateDotGraph($reg, $web, $ext, $aTFs,$aORFGenes);

if ($wimage) {
?>
Done!
<br/>
See <i><a href="<?=$url.$name?>">Image</a></i>
<?php
} else {
?>
<br/>
<span class="error">Resulting graph too big! Image not generated.</span>
<br/>
Download the generated dot file:
<i><a href="<?=$url.$name?>">dot file</a></i>
<?php
}
}
// $allTFs && $allGenes
//} else {
//? >
//You have asked for the regulation matrix of <u>All TFs against All Genes</u>.<br/>
//We apologize for the inconvenient, but due to processing constraints<br/>
//we advise you not to choose All against All.
//< ?php
//}
?>

<br/>
</td></tr>
<tr><td class="align">
<input type="reset" value="Go Back" onclick="javascript:history.back()" />
</td></tr></table>
<?php
$web->printFooter();
$web->closePage();
?>
