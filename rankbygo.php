<?php
if (@$_POST['ontology'] == 'process')
	$goTerm = "Biological Process";
elseif (@$_POST['ontology'] == 'function')
	$goTerm = "Molecular Function";
else $goTerm = "Cellular Component";

include_once('inc/HeaderFooter.php');
$web = new HeaderFooter("no");
$web->printHeader("Home", "index.php",
"Group Genes by GO","javascript:history.back()",
"Result", "");
$web->printBarMenu("GO terms ($goTerm) ranked according to the user gene set statistical relevance");

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

list($aORFGenes,$unknown) = $service->user2Info($_POST['genes'],$service->aSpecies());

$ontoDB = (isset($_POST) && isset($_POST['ontology']))? $_POST['ontology'] : "process";

$hyperN = $service->getTotalNumDBGenes();
list($assocs, $terms) = $service->getGO($aORFGenes, $ontoDB,$hyperN);
$terms = $service->callRhyper($terms,$hyperN,count($aORFGenes));

?>
<table border="0" class="center" width="70%" summary="main content">
<tr><td class="align">
<?php

include_once('inc/head.inc.php');
Head::printUnknown($unknown);

?>
<table border="1" summary="go terms" id="rankgo" class="tablesorter">
<thead>
	<tr>
	  <th>GO ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
		<th>GO term&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
		<th title="Depth level of the GO term in the respective ontology graph">Depth level&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
		
	  <th title="Percentage of GO associated genes in the user set">%&nbsp;in&nbsp;user&nbsp;set&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
		<th title="Percentage of GO associated genes relative to the whole genome">%&nbsp;in&nbsp;<?=$web->getProp("db.$web->_dbname.short")?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
		<th title="p-value (Hypergeometric distribution w/ Bonferroni correction): GO associated genes in the user set relative to the <?=$web->getProp("db.$web->_dbname.short")?> described">p-value&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
	  <th>Genes/ORF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
	</tr>
</thead>
<tbody>
<?php
$csv = "";
#Bonferroni correction
$pv1 = 0.01 / count($terms);
$pv2 = 0.05 / count($terms);
foreach (array_keys($terms) as $id) {
	print "<tr><td><a href=\"http://amigo.geneontology.org/amigo/term/$id\" target=\"_blank\">";
	print "$id</a></td><td align=\"left\">";
	print $terms[$id]['term']."</td><td align=\"center\">";
	print $terms[$id]['depth']."</td><td align=\"right\">";
	printf("%.2f", $terms[$id]['setPer'] * 100);
	print "%</td><td align=\"right\">";
	printf("%.2f", $terms[$id]['dbPer'] * 100);
	print "%</td><td align=\"right\"";
	$pvalue = $terms[$id]['pvalue'];
	if ($pvalue <= $pv1) {
		print " class=\"pvalue1\">";
	} elseif ($pvalue <= $pv2) {
		print " class=\"pvalue2\">";
	} else {
		print " class=\"pvalue3\">";
	}
	printf("%.15f", $terms[$id]['pvalue']);
	print "</td><td>";

	// Javascript submenus
	$args = [];
	foreach ($assocs[$id] as $k=>$orfid) { $args[] = $aORFGenes[$orfid]['orf'];}

	$items[0] = array("orfgene", "ORF List &hArr; Gene List", "formorftogene.php", 0, 1);
	// "doc".$orf - variable ID to discriminate different menus in the same page
	$web->jsmenuDraw($id, $items, [], $args,
		['doc' => 'doc'], ['dir'=>'dir'], ['big'=>'big','sub'=>'sub'], ['pos'=>'pos', 'neg'=>'neg']);
	
	$aGs = [];
	$toPrint = "";
	foreach ($assocs[$id] as $k => $orfid) {
		$o = $aORFGenes[$orfid]['orf'];
		$g = $aORFGenes[$orfid]['gene'];
		$toPrint .= "<a href=\"view.php?existing=locus&amp;orfname=$o\">$g</a> ";
		$aGs[] = $g;
	}
	print $toPrint."</td></tr>";
	$csv .= "$id\t{$terms[$id]['term']}\t{$terms[$id]['depth']}\t";
	$csv .= "{$terms[$id]['setPer']}\t{$terms[$id]['dbPer']}\t$pvalue\t";
	$csv .= join(",", $aGs) . "\n";
}
$tmppath = $web->getProp("path.tmp.local");
$fname = "rankbygo_".rand() . ".csv";
file_put_contents($tmppath . $fname, $csv);
$lcsv = $web->getProp("path.tmp.url") . $fname;
?>
</tbody>
</table>
</td></tr>
<tr><td class="align">
<input type="reset" value="Go Back" onclick="javascript:history.back()" />
<small>Download as <a href="<?=$lcsv?>">csv</a> file.</small>
</td></tr></table>
<?php
$web->printFooter();
?>
<script type="text/javascript" src="jquery/jquery-latest.js"></script>
<script type="text/javascript" src="jquery/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
$(document).ready(function() 
{ 
	$("#rankgo").tablesorter( {
		sortList: [[5,0]]
//		headers: {
//			1: {sorter: "percent"},
//			2: {sorter: "percent"},
//			3: {sorter: "floating"}
//		}
	} ); 
} 
); 
</script>
<script type="text/javascript" src="inc/menu.js"></script>
<?php
$web->closePage();
?>
