<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Homologous network analysis", "");
$web->printBarMenu("Cross species: Homologous network analysis");

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

list($biggroup, $retObj) = $service->getEnvironCondsGroupsFromDB();
$homologSpecies = $service->getHomologSpecies();

?>
<form method="post" action="regnetviz.php">
<input type="hidden" name="origin" value="homo-net" />
	<table class="center" border="0" summary="main content">
	  <tr><td colspan="2">
		<table border="1" summary="main form">
		  <tr>
			<th>Regulations Filter</th>
			<th>Transcription factors</th>
			<th>Target ORF/Genes</th>
		  </tr>
		  <tr>
			<td style="background-color: #DDDDDD" class="vcenter">
				<input type="radio" value="doc" name="type" onchange="javascript:toggleVisible();" checked /><b>Documented</b>
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="evidence" value="dir" onchange="javascript:toggleVisible();" /><b>Only</b> DNA binding evidence
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="evidence" value="indir" onchange="javascript:toggleVisible();" /><b>Only</b> Expression evidence
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="t_pos" value="true" onchange="javascript:toggleVisible();" checked />TF acting as activator
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="t_neg" value="true" onchange="javascript:toggleVisible();" checked />TF acting as inhibitor
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="evidence" value="plus" checked onchange="javascript:toggleVisible();" />DNA binding <b>plus</b> expression evidence
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="evidence" value="and" onchange="javascript:toggleVisible();" />DNA binding <b>and</b> expression evidence
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="use_na" value="true" onchange="javascript:toggleVisible();" checked />Consider also regulations without
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;associated information

				</td>
			<td rowspan="2" style="background-color: #DDDDDD">
                        <textarea rows="10" cols="18" name="regulators"><?=$web->post2Form("formtfs")?></textarea><br/>
                                <input type="checkbox" name="alltfs" onclick="javascript:toggleAllTfs()"/>Check for all TFs
                        </td>
                        <td rowspan="2" style="background-color: #DDDDDD">
                        <textarea rows="11" cols="15" name="regulated"><?=$web->post2Form("formtgs")?></textarea>
                        </td>

							</tr>
							<tr><td>
								<?php $web->printSyntenyForm(); ?>
							</td></tr>
							</br>
		<!--	<tr>
				<td class="align" colspan="3">

					<b>Filter Documented Regulations by environmental condition:</b>
					<br/>Group: <select name="biggroup" onchange="javascript=subgroups();">
						<option value="0" selected>----</option>
<?php/*
foreach ($biggroup as $g) {
	print "<option value=\"$g\">$g</option>";
}
 */?>
					</select>
					<br/>Subgroup: <select id="subgroup" name="subgroup">
						<option value="0" selected>----</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="align" colspan="3">
					<b>Search for Homologous Regulations in:</b>
					<br/>Species: <select name="homology">
						<option value="0" selected>----</option>
<?php/*
foreach ($homologSpecies as $species) {
	print "<option value=\"$species\">$species</option>";
}
 */?>
					</select>
					<br/>Considering only documented regulations (all evidence codes)
       	</td>
			</tr>-->
		</table>
		</td></tr>
		<tr>
			<td align="left">
    	<input type="submit" name="submit" value="Search" onclick="return validate();"/>
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    	<input type="reset" name="clear" value="Clear"/>
			</td>
			<td align="right">
<?php $web->strainSampleIcons(array('regulators' => 'tfs', 'regulated' => 'tgs'), "inputSample();"); ?>
			<a href="help_rankbytf.php" title="Help"><img src="images/information.gif" alt="help icon" /></a>
		  </td>
		</tr>
	</table>
</form>
<?php
$web->printFooter();
?>

<script type="text/javascript">
/*
function subgroups() {
	var subgroup = document.getElementById('subgroup');
	subgroup.options.length = 0;
	subgroup.options[0] = new Option('----','----');
<?php/*
foreach ($biggroup as $key) {
	print "\tif (document.getElementsByName('biggroup')[0].value == '$key') {\n";
	foreach ($retObj[$key] as $s) {
		print "\t\tsubgroup.options[subgroup.options.length] = new Option('$s','$s');\n";
	}
	print "\t}\n";
}
 ?>
}
 */
function toggleVisible() {
/*	if (document.getElementsByName('type')[1].checked) {
		// Disable all documented stuff!
		document.getElementsByName('evidence')[0].disabled = true;
		document.getElementsByName('evidence')[1].disabled = true;
		document.getElementsByName('evidence')[2].disabled = true;
		document.getElementsByName('evidence')[3].disabled = true;
		document.getElementsByName('biggroup')[0].disabled = true;
		document.getElementsByName('subgroup')[0].disabled = true;
	} else {
		// Enable all documented stuff
		document.getElementsByName('evidence')[0].disabled = false;
		document.getElementsByName('evidence')[1].disabled = false;
		document.getElementsByName('evidence')[2].disabled = false;
		document.getElementsByName('evidence')[3].disabled = false;
		document.getElementsByName('biggroup')[0].disabled = false;
		document.getElementsByName('subgroup')[0].disabled = false;
	}
 */
	toggleVisibleTF();
	toggleAllTfs();
}
function toggleVisibleTF() {
	if (document.getElementsByName('evidence')[0].disabled ||
		document.getElementsByName('evidence')[0].checked) {
		document.getElementsByName('t_pos')[0].disabled = true;
		document.getElementsByName('t_neg')[0].disabled = true;
	} else {
		document.getElementsByName('t_pos')[0].disabled = false;
		document.getElementsByName('t_neg')[0].disabled = false;
	}
}
function inputSample() {
	document.getElementsByName('type')[0].checked = true;
	document.getElementsByName('alltfs')[0].checked = false;
	document.getElementsByName('evidence')[2].checked = true;
	document.getElementsByName('t_pos')[0].checked = true;
  document.getElementsByName('t_neg')[0].checked = true;
	toggleVisible();
}
function toggleAllTfs() {
	if (document.getElementsByName('alltfs')[0].checked) {
		document.getElementsByName('regulators')[0].disabled = true;
	} else {
		document.getElementsByName('regulators')[0].disabled = false;
	}
}
function validate() {
	return alertEmptyTFsGenes() && checkTF();
}
function checkTF() {
	if ((document.getElementsByName('evidence')[1].checked ||
			 document.getElementsByName('evidence')[2].checked) &&
		  (!document.getElementsByName('t_pos')[0].checked &&
			 !document.getElementsByName('t_neg')[0].checked)) {
		alert('Since you selected expression evidence, you must select at least one role for TFs.');
		return false;
	} else {
		return true;
	}
}
function alertEmptyTFsGenes() {
	if ((document.getElementsByName('regulators')[0].value.replace(/^\s*$/g,"") === "" && !document.getElementsByName('alltfs')[0].checked) ||
	    (document.getElementsByName('regulated')[0].value.replace(/^\s*$/g,"") === "")) {
		alert('Please enter transcription factors and/or genes to group genes by TF.');
		return false;
	}
	return true;
}
-->
</script>
<?php
$web->closePage();
?>
