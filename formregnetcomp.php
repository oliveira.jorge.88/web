<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Network comparison", "");
$web->printBarMenu("Cross species: Network comparison");

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

list($biggroup, $retObj) = $service->getEnvironCondsGroupsFromDB();
$homologSpecies = $service->getHomologSpecies();

?>
<form method="post" action="regnetcomp.php">
	<table class="center" border="0" summary="main content">
	  <tr><td colspan="2">
		<table border="1" summary="main form">
		  <tr>
			<th>Regulations Filter</th>
			<th>Transcription factors</th>
			<th>Target ORF/Genes</th>
		  </tr>
		  <tr>
			<td style="background-color: #DDDDDD" class="vcenter">
				<b>Documented</b>
				<br/>&nbsp;&nbsp;
				<input type="radio" name="evidence" value="dir" /><b>Only</b> DNA binding evidence
				<br/>&nbsp;&nbsp;
				<input type="radio" name="evidence" value="indir" /><b>Only</b> Expression evidence
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="t_pos" value="true" checked />TF acting as activator
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="t_neg" value="true" checked />TF acting as inhibitor
				<br/>&nbsp;&nbsp;
				<input type="radio" name="evidence" value="plus" checked />DNA binding <b>plus</b> expression evidence
				<br/>&nbsp;&nbsp;
				<input type="radio" name="evidence" value="and" />DNA binding <b>and</b> expression evidence
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="use_na" value="true" onchange="javascript:toggleVisible();" checked />Consider also regulations without
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;associated information
			</td>
			<td style="background-color: #DDDDDD">
			<textarea rows="12" cols="15" name="regulators"><?=$web->post2Form("formtfs")?></textarea><br/>
			</td>
			<td style="background-color: #DDDDDD">
			<textarea rows="12" cols="15" name="regulated"><?=$web->post2Form("formtgs")?></textarea>
			</td>
			</tr>
			<tr>
				<td class="align" colspan="3">
					<b>Filter Documented Regulations by environmental condition:</b>
					<br/>Group: <select name="biggroup" onchange="javascript=subgroups();">
						<option value="0" selected>----</option>
<?php
foreach ($biggroup as $g) {
	print "<option value=\"$g\">$g</option>";
}
?>
					</select>
					<br/>Subgroup: <select id="subgroup" name="subgroup">
						<option value="0" selected>----</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="align" colspan="3">
					<b>Search for Common/Unique Regulations in:</b>
					<br/>Species: <select name="species">
					<!--	<option value="0" selected>----</option>-->
<?php
foreach ($homologSpecies as $species) {
	print "<option value=\"$species\">$species</option>";
}
?>
					</select>
					<br/>Considering only documented regulations (all evidence codes)
       	</td>
			</tr>
<tr>
                                        <tr>
						<td class="align" colspan="3">
						<?php $web->printSyntenyForm(); ?> 						
</td>
                                        </tr>
		</table>
		</td></tr>
		<tr>
			<td align="left">
    	<input type="submit" name="submit" value="Search" onclick="return validate();"/>
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    	<input type="reset" name="clear" value="Clear"/>
			</td>
			<td align="right">
<?php $web->strainSampleIcons(array('regulators' => 'tfs', 'regulated' => 'tgs'), "inputSample();"); ?>
			<a href="help_rankbytf.php" title="Help"><img src="images/information.gif" alt="help icon" /></a>
		  </td>
		</tr>
	</table>
</form>
<?php
$web->printFooter();
?>
<script type="text/javascript">
<!--
function subgroups() {
	var subgroup = document.getElementById('subgroup');
	subgroup.options.length = 0;
	subgroup.options[0] = new Option('----','----');
<?php
foreach ($biggroup as $key) {
	print "\tif (document.getElementsByName('biggroup')[0].value == '$key') {\n";
	foreach ($retObj[$key] as $s) {
		print "\t\tsubgroup.options[subgroup.options.length] = new Option('$s','$s');\n";
	}
	print "\t}\n";
}
?>
}
function inputSample() {
	document.getElementsByName('evidence')[2].checked = true;
	document.getElementsByName('t_pos')[0].checked = true;
  document.getElementsByName('t_neg')[0].checked = true;
}
function validate() {
	return alertEmptyTFsGenes() && checkTF();
}
function checkTF() {
	if ((document.getElementsByName('evidence')[1].checked ||
			 document.getElementsByName('evidence')[2].checked) &&
		  (!document.getElementsByName('t_pos')[0].checked &&
			 !document.getElementsByName('t_neg')[0].checked)) {
		alert('Since you selected expression evidence, you must select at least one role for TFs.');
		return false;
	} else {
		return true;
	}
}
function alertEmptyTFsGenes() {
	if ((document.getElementsByName('regulators')[0].value.replace(/^\s*$/g,"") === "") ||
	    (document.getElementsByName('regulated')[0].value.replace(/^\s*$/g,"") === "")) {
		alert('Please enter transcription factors and/or genes to group genes by TF.');
		return false;
	}
	return true;
}
-->
</script>
<?php
$web->closePage();
?>
