<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
	"Help", "help_index.php",
	"Orthology", "");
$web->printBarMenu("Help - Orthology");
?>

<table border="0" class="center" style="width:75%" summary="main content">
<tr><td>

<p>
The orthology information in YEASTRACT+ was obtained using the Reciprocal Best Score (RBS) BLAST approach. The detailed methodology is presented below. 
</p>

<h3>
Methodology
</h3>

<ol>
<li>The proteome of each species was used as the input for a BLASTp, using a p-value of 1e-5 and performed in a reciprocal way between all the species of the DB. </li>
<li>The BLAST hit with the highest score for each protein sequence is considered as the BEST hit. Additionally, a tolerance of 10% is applied, meaning that alignments with a score almost identical to the best-score are not lost.</li>
<li>At this stage the homology is obtained.</li>
<li>This information is crossed with the synteny by comparing the genes adjacent to each homolog locus. 15 neigbors are considered in each direction.</li>
<li>3 levels of synteny "strength" were created, by requiring a minimum of 1, 2 or 3 neighbor genes in common.</li>
</ol>


</td></tr>
</table>

<?php
$web->printFooter();
$web->closePage();
?>
