<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
if (isset($_GET) && isset($_GET['type']) && isset($_GET['id'])) {
	include_once('service/Logic.php');
	$service = new Logic(
	  $web->getProp("db.$web->_dbname.dbspecies"),
		$web->getProp("db.$web->_dbname.dbstrains"));

	$url = $service->sequenceDB2File($_GET['type'], $_GET['id']);	
	if ($url != false) header("Location: $url");
}
$web->printHeader("Home", "index.php",
"Flat files", "");
$web->printBarMenu("Retrieve flat files");
?>

<br/> 

<table class="center" border="0" width="70%" summary="main content">
    <tr><td class="align">
    
		If you are searching for specific data, please <a href="../contact.php">Contact Us</a>.
</p>

</td></tr>
</table>

<?php
$web->printFooter();
$web->closePage();
?>
