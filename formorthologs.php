<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"ORF/Gene Orthologs List", "");
$web->printBarMenu("Obtain the ORF/Gene Orthologs List");

# FormOrthologs is agnostic to the information sent
$orfs = trim($web->post2Form("formtfs") . "\n" . $web->post2Form("formtgs"));
?>

<form method="post" action="orthologs.php"> 
	<table class="center" border="0" summary="main content">
		<tr>
			<td colspan="2">
				<table border="1" summary="main form">
                    <tr>
                      <th class="center">ORF/Gene list</th>
                    </tr>
					<tr>
						<td style="background-color: #DDDDDD">
							<textarea rows="15" cols="40" name="orfs"><?=$orfs?></textarea>
						</td>
					</tr>
<tr>
                                        </tr>
                                        <tr>
						<td class="center">
							<?php $web->printSyntenyForm();?>
                                                </td>
                                        </tr>

				</table>
			</td>
		</tr>
		  <tr>
		    <td align="left">
				<input type="submit" name="submit" value="Run" onclick = "return alertUser();"/>
				&nbsp;&nbsp;&nbsp;
				<input type="reset" name="clear" value="Clear"/>
		    </td>
		    <td align="right">
<?php $web->strainSampleIcons(array('orfs' => 'tgs')); ?>
<a href="help_orthology.php" title="Help">
        <img style="border:0" src="images/information.gif" alt="help icon">
      </a>
		    </td>
		  </tr>
	</table>
</form>
<?php
$web->printFooter();
?>
<script type="text/javascript">
function alertUser(){
	if (document.getElementsByName('orfs')[0].value.replace(/^\s*$/g,"") == ''){
	alert("Please enter gene and/or ORF list.");
	return false;
	}
}
</script>
<?php
$web->closePage();
?>
