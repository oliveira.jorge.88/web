<?php
include_once("inc/HeaderFooter.php");
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Tutorial", "");
$web->printBarMenu("Tutorial");
?>

<p><strong>Create your tutorial were. A sugestion of tutorial is shown below.</strong>
</p>

<p>
This platform provides assistance in two major issues:
prediction of gene transcriptional regulation and global expression analysis
according to Candida transcription networks described in the literature. This
tutorial presents two case-studies, exemplifying the use of different query
options and utilities. Various other ways to exploit available options and
utilities are possible.
</p>

<p style="color:#1F497D">
- <a href="#example1">Example 1: Identification of the documented and potential
regulatory associations for an ORF/Gene</a>
</p>

<p style="color:#1F497D">
- <a href="#example2">Example 2: Gene expression analysis based on regulatory
associations</a>
</p>


<p>
Throughout the database and this tutorial, the
regulatory associations are denominated &quot;Documented&quot; or
&quot;Potential&quot;:
</p>

<ul>
 <li>a <b>documented association</b> between a Transcription Factor
     (TF) and a target gene is supported by published data showing at least one
     of the following experimental evidences: i) Change in the expression of
     the target gene due to a deletion (or mutation) in the gene encoding
     transcription factor; these evidences may come from detailed gene by gene
     analysis or genome-wide expression analysis; ii) Binding of the transcription
     factor to the promoter region of the target gene, as supported by
     band-shift, foot-printing or Chromatine ImmunoPrecipitation (ChIP) assays.
     Therefore, the user is urged to check the literature references provided
     in the database to fully understand the nature of the evidences underlying
		 the identified regulatory associations.
	</li>
	<li>a <b>potential association</b> between a TF and a target gene
     is based on the occurrence of the TF binding site in the promoter region
     of the target gene. The binding sites associated to each TF in this
     database are supported by published experimental evidence for the binding
     of the TF to the specific nucleotide sequence (data coming from
     foot-printing or ChIP assays). Again, the user is urged to check the
		 literature references provided in the database.
	</li>
</ul>
<br/><br/><hr/>

<p style="color:#1F497D">
<big><b><a name="#example1">Example 1: Identification of the documented and potential
regulatory associations for an ORF/Gene</a></b></big>
</p>

<p>
The functional analysis of an ORF or gene can be guided through
the identification of its documented and potential transcription factors (TF).
This example describes one of the possible ways to explore the regulatory
associations for the  ORF.
</p>

<p>
<b>1.1 - Search for Documented Transcription Factors (TFs)</b>
<br/>
The use of &quot;Search for Transcription
Factors&quot; query allows the identification of TFs which, are Documented
and/or Potential transcriptional regulators of a given gene. 
The associated bibliographic references may be checked by
the user to know the experimental basis for these regulatory associations.
</p>

<p>
<b>1.2 - Search for Potential Transcription Factors (TFs)</b>
<br/>
The use of the &quot;Search for
Transcription Factors&quot; query may also identify the potential regulators. By default, all of the searched potential
transcription factors will be displayed in tabular form. The <i>Promoter</i>
link can be followed to see the binding
sites for each TF in the promoter sequence. The distribution of TF binding sites in the promoter region can be viewed by checking the option <i>image</i> while searching.
</p>

<p>
The display of potential TFs on the image can be controlled by un-checking their respective box in the color pallet below the image and pressing the <i>Redisplay</i> button.
The color pallet displays the color for only those TFs for which binding sites are found in the promoter region of the given gene(s).
</p>

<p>
<b>1.3 - Search for Genes</b>
<br/>
If the ORF/gene under study is predicted to encode a TF, it would
be convenient to use the query, &quot;Search Regulated Genes&quot;, options
<i>Documented</i> or <i>Potential</i>, to retrieve all
documented and potential targets for the TF, respectively.
</p>

<br/><br/><hr/>


<p style="color:#1F497D">
<big><b><a name="example2">Example 2: Gene expression analysis based on regulatory associations</a></b></big>
</p>

<p>
The platform provides tools for the classification and grouping
of large lists of genes of interest, such as those found up- or down-regulated
under a specific environmental cue or genetic mutation, as suggested by
genome-wide expression data inspection. These analyses are based on known or
algorithmically identified potential regulatory associations, deposited in the
 database, or on shared Gene Ontology (GO) terms.
</p>

<p>
<b>2.2 - Rank by Gene Ontology (GO)</b>
<br/>
The grouping of genes based on the GO terms they share is a
feature common to a number of gene expression analysis software and is also
implemented in the Database. 
</p>

<p>
<b>2.3 - Rank by Transcription Factor</b>
<br/>
The query &quot;Rank by TF&quot; enables automatic selection and ranking of
transcription factors potentially involved in the regulation of the genes in a
list of interest. The TFs and their direct targets are presented in a table in
decreasing order of a relevance score calculated for each TF, based on either
regulations or regulatory paths targeting the genes in the list of interest and
deposited in the database. Different filters can be used in order to
steer the search to a particular type of regulatory activity. 
</p>

<p>
TFs predicted to regulate this transcriptional response can be
ranked by the % of genes in the list associated to them. 
When ranking by statistical significance of regulations, the TF
score is given by a p-value denoting the overrepresentation of regulations of
the given TF targeting genes in the list of interest relative to the
regulations of that TF targeting genes in the whole database. The
p-value further denotes the probability that the TF regulates at least the
number of genes found to be regulated in the list of interest if we were to
sample a set of genes of the same size as the list of interest from all the
genes in the database. This probability is modeled by a
hypergeometric distribution and the p-value is finally subject to a Bonferroni
correction for multiple testing.
</p>

<p>
In this case the enrichment-based ranking of transcription factors
reports basically the same two TF as the highest ranking TFs.
</p>

<script type="text/javascript" src="../inc/menu.js"></script>
<?php
$web->printFooter();
$web->closePage();
?>
