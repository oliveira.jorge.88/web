<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Generate Regulation Matrix", "");
$web->printBarMenu("Generate Regulation Matrix");

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

list($biggroup, $retObj) = $service->getEnvironCondsGroupsFromDB();

?>
<form method="post" action="regmatrix.php">
	<table class="center" border="0" summary="main content">
	  <tr><td colspan="2">
		<table border="1" summary="main form">
		  <tr>
			<th>Regulations Filter</th>
			<th>Transcription factors</th>
			<th>Target ORF/Genes</th>
		  </tr>
		  <tr>
			<td style="background-color: #DDDDDD" class="vcenter">
				<input type="radio" value="doc" name="type" onchange="javascript:toggleVisible();" checked />Documented
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="evidence" value="dir" onchange="javascript:toggleVisible();" /><b>Only</b> DNA binding evidence
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="evidence" value="indir" onchange="javascript:toggleVisible();" /><b>Only</b> Expression evidence
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="t_pos" value="true" onchange="javascript:toggleVisible();" checked />TF acting as activator
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="t_neg" value="true" onchange="javascript:toggleVisible();" checked />TF acting as inhibitor
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="evidence" value="plus" onchange="javascript:toggleVisible();" checked />DNA binding <b>plus</b> expression evidence
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="evidence" value="and" onchange="javascript:toggleVisible();" />DNA binding <b>and</b> expression evidence
				<br/>
				<input type="radio" value="pot" name="type" onchange="javascript:toggleVisible();" />Potential
				<!--<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				Search TFBSs having &ge;
					<select name="bind" disabled="disabled">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					</select> Binding Sites
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					in the promoters of their target genes
-->
				<br/>
				<input type="radio" value="plus" name="type" onchange="javascript:toggleVisible();" />Documented <b>plus</b> Potential
				<br/>
				<input type="radio" value="and" name="type" onchange="javascript:toggleVisible();" />Documented <b>and</b> Potential
			</td>
			<td style="background-color: #DDDDDD">
				<textarea rows="16" cols="18" name="regulators"></textarea>
			</td>
			<td style="background-color: #DDDDDD">
				<textarea rows="15" cols="15" name="regulated"></textarea><br/>
				<input type="checkbox" name="allgenes" onclick="javascript:toggleAllGenes()"/>All ORF/Genes
			</td>
			</tr>
			<tr>
				<td class="align" colspan="3">
					<b>Filter Documented Regulations by environmental condition:</b>
					<br/>Group: <select name="biggroup" onchange="javascript=subgroups();">
						<option value="0" selected>----</option>
<?php
foreach ($biggroup as $g) {
	print "<option value=\"$g\">$g</option>";
}
?>
					</select>
					<br/>Subgroup: <select id="subgroup" name="subgroup">
						<option value="0" selected>----</option>
					</select>
				</td>
			</tr>
			<tr>
			<td class="align" colspan="2">
				<input type="radio" name="tfs" value="inserted" checked="checked" onclick="javascript:toggleVisible()"/>Consider user inserted TF list<br/>
				<input type="radio" name="tfs" value="dbtfs" onclick="javascript:toggleVisible()"/>Consider <?=$web->getProp("db.$web->_dbname.short")?> TF list<br/>
				<input type="radio" name="tfs" value="dbtfs_except" onclick="javascript:toggleVisible()"/>Consider <?=$web->getProp("db.$web->_dbname.short")?> TF list excluding user inserted
			</td>
			<td class="align">
					Output&nbsp;image:<br/><select name="ext">
					<option value="png">PNG</option>
					<option value="svg">SVG</option>
					<option value="jpg">JPG</option>
					<option value="gif">GIF</option>
					</select>
			</td></tr>
		</table>	    
		</td></tr>
		<tr>
			<td align="left">
			<input type="submit" name="submit" value="Generate" onclick = "return validate();"/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" name="clear" value="Clear"/>
			</td>
			<td align="right">
<?php $web->strainSampleIcons(array('regulators' => 'tfs', 'regulated' => 'tgs'), "inputSample();"); ?>
			<a href="help_regmatrix.php" title="Help"><img src="../images/information.gif" alt="help icon" /></a>
		  </td>
		</tr>
	</table>
</form>

<?php
$web->printFooter();
?>
<script type="text/javascript">
<!--
function subgroups() {
	var subgroup = document.getElementById('subgroup');
	subgroup.options.length = 0;
	subgroup.options[0] = new Option('----','----');
<?php
foreach ($biggroup as $key) {
	print "\tif (document.getElementsByName('biggroup')[0].value == '$key') {\n";
	foreach ($retObj[$key] as $s) {
		print "\t\tsubgroup.options[subgroup.options.length] = new Option('$s','$s');\n";
	}
	print "\t}\n";
}
?>
}
function toggleVisible() {
	if (document.getElementsByName('type')[1].checked) {
		// Disable all documented stuff!
		document.getElementsByName('evidence')[0].disabled = true;
		document.getElementsByName('evidence')[1].disabled = true;
		document.getElementsByName('evidence')[2].disabled = true;
		document.getElementsByName('evidence')[3].disabled = true;
		document.getElementsByName('biggroup')[0].disabled = true;
		document.getElementsByName('subgroup')[0].disabled = true;
		// Enable all potential stuff!
		//document.getElementsByName('bind')[0].disabled = false;
	} else if (document.getElementsByName('type')[0].checked) {
		// Enable all documented stuff
		document.getElementsByName('evidence')[0].disabled = false;
		document.getElementsByName('evidence')[1].disabled = false;
		document.getElementsByName('evidence')[2].disabled = false;
		document.getElementsByName('evidence')[3].disabled = false;
		document.getElementsByName('biggroup')[0].disabled = false;
		document.getElementsByName('subgroup')[0].disabled = false;
		// Disable all potential stuff!
		//document.getElementsByName('bind')[0].disabled = true;
	} else {
		// Enable all documented stuff
		document.getElementsByName('evidence')[0].disabled = false;
		document.getElementsByName('evidence')[1].disabled = false;
		document.getElementsByName('evidence')[2].disabled = false;
		document.getElementsByName('evidence')[3].disabled = false;
		document.getElementsByName('biggroup')[0].disabled = false;
		document.getElementsByName('subgroup')[0].disabled = false;
		// Enable all potential stuff!
		//document.getElementsByName('bind')[0].disabled = false;
	}
	if (document.getElementsByName('tfs')[1].checked) {
		document.getElementsByName('regulators')[0].disabled = true;
	} else {
		document.getElementsByName('regulators')[0].disabled = false;
	}
	toggleVisibleTF();
}
function toggleVisibleTF() {
	if (document.getElementsByName('evidence')[0].disabled ||
		document.getElementsByName('evidence')[0].checked) {
		document.getElementsByName('t_pos')[0].disabled = true;
		document.getElementsByName('t_neg')[0].disabled = true;
	} else {
		document.getElementsByName('t_pos')[0].disabled = false;
		document.getElementsByName('t_neg')[0].disabled = false;
	}
}
function toggleAllGenes() {
	if (document.getElementsByName('allgenes')[0].checked) {
		document.getElementsByName('regulated')[0].disabled = true;
	} else {
		document.getElementsByName('regulated')[0].disabled = false;
	}
}
function inputSample() {
	document.getElementsByName('type')[0].checked = true;
	document.getElementsByName('evidence')[2].checked = true;
	document.getElementsByName('t_pos')[0].checked = true;
	document.getElementsByName('t_neg')[0].checked = true;
	document.getElementsByName('allgenes')[0].checked = false;
	document.getElementsByName('tfs')[0].checked = true;
	//document.getElementsByName('bind')[0].selectedIndex = 0;
	toggleVisible();
	toggleAllGenes();
}
function validate() {
	return alertEmptyTFs() && alertEmptyGenes() && checkTF() && alertGreedyCombo();
}
function alertGreedyCombo() {
        if (!document.getElementsByName('type')[0].checked && document.getElementsByName('tfs')[1].checked && document.getElementsByName('allgenes')[0].checked) {
                alert("The combination of the 'Consider C.glabrata TF list' and 'All ORF/Genes' options is not allowed for Potential Regulations due to its processing demand. Please specify either a TF or Gene list.");
                return false;
	}
	if (!document.getElementsByName('type')[0].checked && document.getElementsByName('tfs')[2].checked && document.getElementsByName('allgenes')[0].checked) {
                alert("The combination of the 'Consider S.cerevisiae TF list excluding user inserted' and 'All ORF/Genes' options is not allowed for Potential Regulations due to its processing demand. Please specify either a TF or Gene list.");
                return false;
        }
	if (!document.getElementsByName('type')[0].checked && document.getElementsByName('tfs')[1].checked) { alert("Warning: Selecting All the TFs in the database for potential regulation may take a lot of time or even fail to compute.");}
	if (!document.getElementsByName('type')[0].checked && document.getElementsByName('allgenes')[0].checked) { alert("Warning: Selecting All the Target ORFs in the database for potential regulation may take a lot of time or even fail to compute.");}
        return true;
}
function checkTF() {
	if ((document.getElementsByName('evidence')[1].checked ||
	     document.getElementsByName('evidence')[2].checked ||
			 document.getElementsByName('evidence')[3].checked) &&
		  (!document.getElementsByName('t_pos')[0].checked &&
			 !document.getElementsByName('t_neg')[0].checked)) {
		alert('Since you selected expression evidence, you must select at least one role for TFs.');
		return false;
	} else {
		return true;
	}
}
function alertEmptyTFs() {
	if (!document.getElementsByName('tfs')[1].checked && 
		document.getElementsByName('regulators')[0].value.replace(/\s*/g,"") == '') {
		alert("You have not specified a list of TFs");
		return false;
		}
	return true;
}
function alertEmptyGenes() {
	if (!document.getElementsByName('allgenes')[0].checked &&
		document.getElementsByName('regulated')[0].value.replace(/\s*/g,"") == '') {
		alert("You have not specified a list of genes");
		return false;
		}
	return true;
}
-->
</script>
<?php
$web->closePage();
?>
