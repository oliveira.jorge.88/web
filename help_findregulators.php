<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
	"Help", "help_index.php",
	"Search for TFs", "");
$web->printBarMenu("Help - Search for TFs");
?>

<table border="0" class="center" style="width:75%" summary="main content">
<tr><td>

<p>
This query allows the user to search for the Transcription Factors (TFs), included in the <?=$web->_dbname?> database, involved in the regulation of a given list of genes.
</p>

<p>
The input required is a list of <i>C. glabrata</i> or <i>C. albicans</i> genes or ORF names.
</p>

<p>
The user may select to search for TFs that have been experimentally shown to regulate the expression of the genes (i.e., <i>documented</i> associations) or for TFs whose consensus binding site matches a subsequence of the promoter region of the genes (i.e., <i>potential</i> associations).
The combined result of the two aforementioned search modes can be obtained using the search option '<i>Both</i>'.
<br/>
Furthermore, the user may restrict its search to the regulatory associations identified based on direct or indirect evidences. Direct Evidence was considered to be provided through experiments such as Chromatine ImmunoPrecipitation (ChIP), ChIP-on-chip and Electrophoretic Mobility Shift Assay (EMSA), that prove the direct binding of the TF to the target gene's promoter region, or such as the analysis of the effect on target-gene expression of the site-directed mutation of the TF binding site in its promoter region, which strongly suggests that the TF interacts with that specific target promoter. The classification Indirect Evidence was attributed to experiments such as the comparative analysis of gene expression changes occurring in response to the deletion, mutation or over-expression of a given TF.
</p>

<p>
When the search is performed for documented associations, the output is a table containing, for each gene, the list of transcription factors that regulate that gene, links to their entries in the <?=$web->_dbname?> database and links to the publications where the relationship between the gene and the TF was identified.
</p>

<p>
A search for potential associations between genes and TFs returns a similar output as the search for documented associations, including in addition the TF binding site that was matched in the promoter of the gene.
There is an additional option in this case, of the generation of an image (by selecting the box labeled <i>Image</i>). This image depicts the promoter region of each gene and displays the subsequences of the gene that match the binding site consensi of the transcription factors in the database.
</p>

</td></tr>
</table>

<?php
$web->printFooter();
$web->closePage();
?>
