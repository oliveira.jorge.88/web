<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

$fileWritten = false;
if (isset($_GET) && isset($_GET['type']) && isset($_GET['tf']) && 
	isset($_GET['consensus']) && ($_GET['type'] == "transfac" || $_GET['type'] == "meme")) {

		$content = $service->consensus2matrix($_GET['type'], $_GET['tf'], $_GET['consensus']);
		if ($content) {
			$filename  = $_GET['type'] . "_matrix_" . rand() . ".txt";
			$localpath = $web->getProp("path.tmp.local");
			if ($handle = fopen($localpath . $filename, 'w')) {
				if (fwrite($handle, $content) !== FALSE) {
					$fileWritten = true;
				}
			}
			fclose($handle);

			if ($fileWritten) {
				$webpath = $web->getProp("path.tmp.url");
				header("Location: $webpath$filename");
			}
		}

	} else if (isset($_GET) && isset($_GET['type']) && isset($_GET['all']) &&
		$_GET['type'] == "meme") {

			$tfconslist = $service->getMotifsFromDB();

			$justConsensus = array();
			foreach (array_keys($tfconslist) as $tfid){
			       foreach(array_keys($tfconslist[$tfid]) as $cons_desc) {
					foreach ($tfconslist[$tfid][$cons_desc] as $desc => $c) {
						$justConsensus[$c] = $c;
					}
			       }
			}

			$content = "";
			foreach ($justConsensus as $cons) {
				$content .= $service->consensus2matrix($_GET['type'], false, $cons);
			}
			if ($content) {
				$filename  = $_GET['type'] . "_matrix_" . rand() . ".txt";
				$localpath = $web->getProp("path.tmp.local");
				if ($handle = fopen($localpath . $filename, 'w')) {
					if (fwrite($handle, $content) !== FALSE) {
						$fileWritten = true;
					}
				}
				fclose($handle);

				if ($fileWritten) {
					$webpath = $web->getProp("path.tmp.url");
					header("Location: $webpath$filename");
				}
			}

		}
if (!$fileWritten) header("Location: consensuslist.php");
?>
