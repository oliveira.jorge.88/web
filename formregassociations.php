<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Search Regulatory Associations", "");
$web->printBarMenu("Search Regulatory Associations");

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

list($biggroup, $retObj) = $service->getEnvironCondsGroupsFromDB();

?>
<form method="post" action="regassociations.php"> 
	<table class="center" border="0" summary="main content">
	  <tr><td colspan="2">
		<table border="1" summary="main form">
		  <tr>
			<th>Filter Documented Regulations by</th>
			<th>Transcription factors</th>
			<th>Target ORF/Genes</th>
		  </tr>
		  <tr>
			<td style="background-color: #DDDDDD" class="vcenter">
				<br/>&nbsp;&nbsp;
				<input type="radio" name="evidence" value="dir" onchange="javascript:toggleVisible();"/><b>Only</b> DNA binding evidence
				<br/>&nbsp;&nbsp;
				<input type="radio" name="evidence" value="indir" onchange="javascript:toggleVisible();" /><b>Only</b> Expression evidence
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="t_pos" value="true" onchange="javascript:toggleVisible();" checked />TF acting as activator
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="t_neg" value="true" onchange="javascript:toggleVisible();" checked />TF acting as inhibitor
				<br/>&nbsp;&nbsp;
				<input type="radio" name="evidence" value="plus" onchange="javascript:toggleVisible();" checked />DNA binding <b>plus</b> expression evidence
				<br/>&nbsp;&nbsp;
				<input type="radio" name="evidence" value="and" onchange="javascript:toggleVisible();" />DNA binding <b>and</b> expression evidence
				<br/>&nbsp;&nbsp;
				<input type="checkbox" name="use_na" value="true" onchange="javascript:toggleVisible();" checked />Consider also regulations without
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;associated information
			</td>
			<td style="background-color: #DDDDDD">
			<textarea rows="11" cols="17" name="regulators"><?=$web->post2Form("formtfs")?></textarea>
			</td>
			<td style="background-color: #DDDDDD">
			<textarea rows="11" cols="17" name="regulated"><?=$web->post2Form("formtgs")?></textarea>
			</td>
			</tr>
			<tr>
				<td class="align" colspan="3">
					<b>Filter Documented Regulations by environmental condition:</b>
					<br/>Group: <select name="biggroup" onchange="javascript=subgroups();">
						<option value="0" selected>----</option>
<?php
foreach ($biggroup as $g) {
	print "<option value=\"$g\">$g</option>";
}
?>
					</select>
					<br/>Subgroup: <select id="subgroup" name="subgroup">
						<option value="0" selected>----</option>
					</select>
				</td>
			</tr>
			<tr><td class="align" colspan="3">
			<b>While searching for regulations, consider:</b><br/>
			<input type="radio" value="input"   name="type" checked onclick="javascript:toggleVisible();"/>User TF list against User target gene list<br/>
			<input type="radio" value="dbtfs"   name="type" onclick="javascript:toggleVisible();"/><?=$web->getProp("db.$web->_dbname.short")?> TF list against User target gene list<br/>
			<input type="radio" value="dbgenes" name="type" onclick="javascript:toggleVisible();"/>User TF list against <?=$web->getProp("db.$web->_dbname.short")?> target gene list<br/>
			</td></tr>
			<tr><td class="align" colspan="3">
			<b>Search regulations between:</b><br/>
			<input type="radio" value="oneToOne" name="association" checked="checked" />Each Transcription Factor to Any&nbsp;Gene (one-to-one)<br/>
			<input type="radio" value="allToOne" name="association"/>Every Transcription Factor to Any Gene (all-to-one)
			</td></tr>
			</table>
		</td></tr>
		<tr>
			<td align="left">
				<input type="submit" name="submit" value="Search" onclick = "return validate();"/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="reset" name="clear" value="Clear"/>
			</td>
			<td align="right">
<?php $web->strainSampleIcons(array('regulators' => 'tfs', 'regulated' => 'tgs'), "inputSample();"); ?>
			<a href="help_regassociations.php" title="Help"><img src="images/information.gif" alt="help icon" /></a>
			</td>
		</tr>
	</table>
</form>
<?php
$web->printFooter();
?>
<script type="text/javascript">
<!--
function subgroups() {
	var subgroup = document.getElementById('subgroup');
	subgroup.options.length = 0;
	subgroup.options[0] = new Option('----','----');
<?php
foreach ($biggroup as $key) {
	print "\tif (document.getElementsByName('biggroup')[0].value == '$key') {\n";
	foreach ($retObj[$key] as $s) {
		print "\t\tsubgroup.options[subgroup.options.length] = new Option('$s','$s');\n";
	}
	print "\t}\n";
}
?>
}
function toggleVisible() {
	document.getElementsByName('regulators')[0].disabled = false;
	document.getElementsByName('regulated')[0].disabled = false;
	if (document.getElementsByName('type')[1].checked) {
		document.getElementsByName('regulators')[0].disabled = true;
	} else if (document.getElementsByName('type')[2].checked) {
		document.getElementsByName('regulated')[0].disabled = true;
	}
	toggleVisibleTF();
}
function toggleVisibleTF() {
	if (document.getElementsByName('evidence')[0].disabled ||
		document.getElementsByName('evidence')[0].checked) {
		document.getElementsByName('t_pos')[0].disabled = true;
		document.getElementsByName('t_neg')[0].disabled = true;
	} else {
		document.getElementsByName('t_pos')[0].disabled = false;
		document.getElementsByName('t_neg')[0].disabled = false;
	}
}
function inputSample() {
	document.getElementsByName('regulators')[0].disabled = false;
	document.getElementsByName('regulated')[0].disabled = false;
	document.getElementsByName('type')[0].checked = true;
	document.getElementsByName('association')[0].checked = true;
	document.getElementsByName('evidence')[2].checked = true;
	document.getElementsByName('t_pos')[0].checked = true;
	document.getElementsByName('t_neg')[0].checked = true;
	toggleVisibleTF();
}
function validate() {
	if (alertAllTFsEveryTF() && alertEmptyTFsGenes()) {
		if (document.getElementsByName('type')[1].checked ||
			document.getElementsByName('type')[2].checked ||
			(document.getElementsByName('regulators')[0].value.split('\n').length * document.getElementsByName('regulated')[0].value.split('\n').length > 100)) {
				alert('This search may take longer, due to many TFs and/or genes.\nPlease be patient !');
			}
	} else return false;
}
function alertAllTFsEveryTF() {
	if (document.getElementsByName('type')[1].checked && document.getElementsByName('association')[1].checked) {
		alert('It is unreasonable to expect that all TFs in the database\nwould be simultaneous regulators of the same genes');
		return false;
	}
	return true;
}
function alertEmptyTFsGenes() {
	if ((document.getElementsByName('regulators')[0].value.replace(/^\s*$/g,"") === "" && !document.getElementsByName('type')[1].checked) ||
	    (document.getElementsByName('regulated')[0].value.replace(/^\s*$/g,"") === "" && !document.getElementsByName('type')[2].checked)) {
		alert('Please enter transcription factors and/or genes to search for regulatory associations.');
		return false;
	    }
	return true;
}
-->
</script>
<?php
$web->closePage();

