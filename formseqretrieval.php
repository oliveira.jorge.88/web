<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Retrieve Upstream Sequence", "");
$web->printBarMenu("Retrieve Upstream Sequence");
?>
  <form method="post" action="seqretrieval.php">
      <table class="center" border="0" summary="main content">
	    <tr>
		  <td colspan="2">
      <table border="1" summary="main content">
        <tr><th colspan="2" >
				All <i><?=$web->getProp("db.$web->_dbname.short")?></i> genes
		  <input type="checkbox" name="all" value="yes" onchange="Javascript:toggleVisible()"/>
        </th></tr>
        <tr><td valign="top">
            <b>or ORF/Gene list</b></td>
            <td><textarea rows="15" cols="17" name="genes"><?=@$_POST['genes']?></textarea></td>
        </tr>
		<tr><td colspan="2" class="align">
      Output:<br/>
      <input type="radio" name="vehicle" value="display" checked="checked" />Display<br/>
      <input type="radio" name="vehicle" value="file" />File download
	  </td></tr>
      <tr><td colspan="2" class="align">
        From: <input type="text" size="5" name="from" value="-1000"/>
        To:  <input type="text" size="5" name="to" value="-1"/>
        </td></tr>
	  </table>
	  </td>
	  </tr>
	  <tr>
    	  <td align="left">
        	  <input type="submit" name="submit" value="Retrieve" onclick = "return alertUser();"/>
        	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        	  <input type="reset" name="clear" value="Clear"/>
    	  </td>
    	  <td align="right">
<?php $web->strainSampleIcons(array('genes' => 'tgs')); ?>
        	  <a href="help_seqretrieval.php" title="Help"><img src="images/information.gif" alt="help icon" /></a>
    	  </td>
	  </tr>
	  </table>

    </form>
<?php
$web->printFooter();
?>
<script type="text/javascript">
<!--
function toggleVisible() {
	if (document.getElementsByName('all')[0].checked) {
		document.getElementsByName('genes')[0].disabled = true;
		document.getElementsByName('vehicle')[1].checked = true;
		document.getElementsByName('vehicle')[0].disabled = true;				
	} else {
		document.getElementsByName('genes')[0].disabled = false;
		document.getElementsByName('vehicle')[1].checked = false;
		document.getElementsByName('vehicle')[0].disabled = false;
		document.getElementsByName('vehicle')[0].checked = true;
	}
}
function alertUser(){
	if (!(document.getElementsByName('all')[0].checked) && 
	     (document.getElementsByName('genes')[0].value.replace(/^\s*$/g,"") == '')) {
		alert('Please enter gene(s) to retrieve upstream sequence.');
		return false;
	}		
	if (document.getElementsByName('from')[0].value < -1000) {
		alert('Field -From- must be greater or equal than -1000');
		return false;
	}
	if (document.getElementsByName('to')[0].value > -1) {
		alert('Field -To- must be less or equal than -1');
		return false;
	}
	var from = -1 * document.getElementsByName('from')[0].value;
	var to   = -1 * document.getElementsByName('to')[0].value;
	if (from < to) {
		alert('Field -From- must be less or equal than field -To-');
		return false;
	}
}
-->
</script>
<?php
$web->closePage();
?>
