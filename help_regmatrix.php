<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
	"Help", "help_index.php",
	"Generate Regulation Matrix", "");
$web->printBarMenu("Help - Generate Regulation Matrix");
?>

<table border="0" class="center" style="width:75%" summary="main content">
<tr><td>

<p>
This utility generates a simple binary regulation matrix for input transcription factors (TFs) and regulated genes (RGs).
For the input lists of TFs and RGs, the matrix summarizes which TFs regulate which genes.
For each TF/Gene pair, the Regulatory Association (RA) is represented by 1 or 0, representing an existing or non-existing association, respectively.
</p>

<p>
The utility requires a list of transcription factors and genes.
Alternatively, the user can choose to construct the matrix with all the transcription factors or all the genes in the database.
</p>

<p>
By default, the matrix is generated for documented regulations.
However, user may choose to generate a regulation matrix representing potential RAs.
The dimensions of the potential regulation matrix can be controlled by specifying the number of occurrences of each TF binding site in the promoter region of each given gene (i.e., at least one, two or three binding sites). 
<br/>
Furthermore, the user may restrict its search to the regulatory associations identified based on direct or indirect evidences.
Direct Evidence was considered to be provided through experiments such as Chromatine ImmunoPrecipitation (ChIP), ChIP-on-chip and Electrophoretic Mobility Shift Assay (EMSA), that prove the direct binding of the TF to the target gene's promoter region, or such as the analysis of the effect on target-gene expression of the site-directed mutation of the TF binding site in its promoter region, which strongly suggests that the TF interacts with that specific target promoter.
The classification Indirect Evidence was attributed to experiments such as the comparative analysis of gene expression changes occurring in response to the deletion, mutation or over-expression of a given TF.
</p>

<p>
Two files are given as output.
The first, a <a href="https://en.wikipedia.org/wiki/Comma-separated_values">semicolon separated values (CSV)</a> file, contains the binary regulation matrix.
Depending on the size of the regulation matrix, the second file is either an image file depicting the regulatory associations, or a .dot file, which can be used to generate the image using the software available <a href="http://graphviz.org">here</a>.
</p>

</td></tr>
</table>

<?php
$web->printFooter();
$web->closePage();
?>
