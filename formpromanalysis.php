<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Promoter Analysis", "");
$web->printBarMenu("Cross species comparison: Promoter analysis");

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

$currSpecies = $service->aSpecies();
$homologSpecies = $service->getHomologSpecies();

?>

<h4 class="center">by target genes</h4>
<form method="post" action="promanalysis.php">
	<table class="center" border="0" summary="main content">
	  <tr>
		<td colspan="2">
		<table border="1" summary="main form">
		  <tr>
			<th></th>
			<th>Regulated Genes</th>
		  </tr>
		  <tr>
			<td style="background-color: #DDDDDD" class="align">
			<br>
			<strong>Consider TF binding sites from strain:</strong><br/>
			<select name="tf_species">
<?php
foreach ($currSpecies as $sp) {
	print "<option value=\"$sp\">$sp</option>\n";
}
foreach ($homologSpecies as $sp) {
  print "<option value=\"$sp\">$sp</option>\n";
}
?>
      </select>
			<br/>
			<br/>
			<strong>Compare genes with orthology in strains:</strong>
			<br/>
<?php
foreach ($currSpecies as $sp) {
  print "<input type=\"checkbox\" value=\"$sp\" name=\"fakecurrspecies[]\" checked=\"checked\" disabled>$sp</option><br/>\n";
	# type hidden is needed since disabled items don't pass to the form
	print "<input type=\"hidden\" name=\"currspecies[]\" value=\"$sp\"/>\n";
}
foreach ($homologSpecies as $sp) {
  print "<input type=\"checkbox\" name=\"homospecies[]\"  value=\"$sp\">$sp</option><br/>\n";
}
?>
			</td>
			<td style="background-color: #DDDDDD">
			<textarea rows="25" cols="15" name="genes"><?=$web->post2Form("formtgs")?></textarea>
			</td>
		  </tr>
	<tr>
		<td colspan="2">
<?php
$web->printSyntenyForm();
?>
		</td>
	</tr>
		</table>
	  </td>
	</tr>
	<tr>
		<td align="left">
			<input type="submit" name="submit" value="Search" onclick="return validate();"/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" name="clear" value="Clear"/>
		</td>
		<td align="right">
<?php $web->strainSampleIcons(array('genes' => 'tgs')); ?>
			<a href="help_promanalysis.php" title="Help"><img style="border:0" src="../images/information.gif" alt="help icon" /></a>
		</td>
	</tr>
	    </table>
    </form>

<?php
$web->printFooter();
?>
<script type="text/javascript">
function validate() {
	if (document.getElementsByName('genes')[0].value.replace(/^\s*$/g,"") == '') {
		alert('Please enter at least one ORF/gene name.');
		return false;
	}
	var counter=0;
	for (var i=0;i<document.getElementsByName('homospecies[]').length;i++){
		if (document.getElementsByName('homospecies[]')[i].checked) counter++;
	}
	if (counter == 0) {
		alert('Please select at least one species for orthology');
		return false;
	}
	return true;
}
</script>
<?php
$web->closePage();
?>
