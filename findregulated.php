<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter("no");
$web->printHeader("Home", "index.php",
"Search Regulated Genes", "javascript:history.back()",
"Result", "");
$web->printBarMenu("Search Regulated Genes");

list($regs, $eviCode, $assocType, $ecgroup) = $web->getQueryParams(true);
$web->setPOSTdefaultVal('tfs','');

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

$homoSpecies = (isset($_POST) && isset($_POST['species']) && $_POST['species'] != "0")? $_POST['species']: false;
$localSpecies = $service->aSpecies();
list($aTFs, $unknown) = $service->user2Info($_POST['tfs'],$localSpecies); 
if ($homoSpecies) {
	$synteny = (isset($_POST) && isset($_POST['synteny']))? $_POST['synteny']: 0;
	$homoORFInfo = $service->getHomoORFInfo(array_keys($aTFs), $homoSpecies, $synteny); 
	$mapping = array();
	$aHomoIDs = [];
	foreach (array_keys($homoORFInfo) as $orf) {
		$tfid = $homoORFInfo[$orf]['orfid'];
		foreach ($homoORFInfo[$orf]['homo']['orfid'] as $k=>$homoorfid) { 
			$aHomoIDs[$homoorfid]    = $homoorfid;
			// mapping of the TFs 
			$mapping[$tfid]['gene'][] = $homoORFInfo[$orf]['homo']['gene'][$k];
			$mapping[$tfid]['orf'][] = $homoORFInfo[$orf]['homo']['orf'][$k];
		}
	}
}
$aTGids = [];
$aTGs   = [];
if ( $regs['doc'] or $regs['both'] ){
	if ($homoSpecies && isset($homoORFInfo) && !empty($homoORFInfo)) { 
		list($docReg,$mapping2) = $service->findHomoRegulated($homoORFInfo,$homoSpecies,$synteny);
		// mapping now also includes TGs
		$mapping = $mapping + $mapping2;
	} else {
		$docReg = $service->getRegulations(array_keys($aTFs), [], $ecgroup, $eviCode, $assocType, true, true, false); 
	}

	if (!empty($docReg)){
		foreach(array_keys($docReg) as $tfid){
			foreach($docReg[$tfid] as $regid=>$tgid){
				if (!in_array($tgid,$aTGids)) $aTGids[] = $tgid;
			}
		}
		$aTGs = $service->orfIDs2Info($aTGids);
	}
}
if ($regs['pot'] or $regs['both']) { // potential
	if ($homoSpecies){
		if (isset($homoORFInfo) && !empty($homoORFInfo)){
			$aPromoters = $service->upstreamSeq('',$homoSpecies);
			$aMotifs = $service->getMotifsFromDB($homoSpecies,$aHomoIDs); // get only bsrg motifs, no mitomi
		}
	}
	else {
		$aPromoters = $service->upstreamSeq('',$localSpecies[0]);
        	$aMotifs = $service->getMotifsFromDB(false,$aTFs); // get only bsrg motifs, no mitomi
	}

	if (isset($aMotifs) && !empty($aMotifs)) {
	
		$inputmotifs = [];
		foreach(array_keys($aMotifs) as $tfid){ 
			foreach(array_keys($aMotifs[$tfid]) as $tfName){
				foreach($aMotifs[$tfid][$tfName] as $in=>$motif){
					$inputmotifs[$motif][$motif][$tfName]=$tfName;
				}
			}
		}
		list($matches, $motif2tf) = $service->tfBindingSites($aPromoters,$inputmotifs); 

		// GET THE INFO FOR ALL TGs
		foreach (array_keys($matches) as $tgid){ if (!in_array($tgid,$aTGids)) $aTGids[] = $tgid; }
		$aTGs = $aTGs + $service->orfIDs2Info($aTGids);

		// PROCESS THE MATCHES AND TRANSLATE THEM INTO NAMES
		$potReg = [];
		foreach (array_keys($matches) as $tgid) {
			if (!isset($aTGs[$tgid])) continue;
			$orf = $aTGs[$tgid]['orf'];
			foreach (array_keys($matches[$tgid]) as $motif){
				foreach($motif2tf[$motif] as $tf){
					if (isset($matches[$tgid][$motif]['fwd'])) { 
						foreach ($matches[$tgid][$motif]['fwd'] as $k=>$pos ) { 
							if (!isset($potReg[$tf][$motif][$orf])) { $potReg[$tf][$motif][$orf] = $pos;	}
							else { $potReg[$tf][$motif][$orf] .= ",".$pos; }
						}
					}
					if (isset($matches[$tgid][$motif]['rev'])) { 
                        		        foreach ($matches[$tgid][$motif]['rev'] as $k=>$pos ) {
                                		        if ( !isset($potReg[$tf][$motif][$orf])) { $potReg[$tf][$motif][$orf] = $pos;    }
                                        		else { $potReg[$tf][$motif][$orf] .= ",".$pos; }
		                                }
					}
				}
			}
		}
	}
}
?>
<table border="0" class="center" width="90%" summary="main content">
<tr><td class="align">
<?php

include_once('inc/head.inc.php');
Head::printUnknown($unknown);

if ($homoSpecies) {
	$web->printHomologyWarning($homoSpecies, $_POST['type']);
	if ($regs['doc'] or $regs['both']) $web->printSyntenyWarn($synteny);
} else if ($regs['doc'] || $regs['both']) {
	$evidencetxt = false;
	if ($eviCode['dir']) {
		$evidencetxt .= "DNA binding evidence";
	} elseif ($eviCode['indir'])  {
		$evidencetxt .= "Expression evidence";
	} elseif ($eviCode['and']) {
		$evidencetxt .= "Simultaneous DNA binding and expression evidence";
	} elseif ($eviCode['plus']) {
		$evidencetxt .= "DNA binding plus expression evidence";
	}
	print "<p>Documented regulations are filtered by:<br/>&nbsp;- $evidencetxt";
	if ($assocType['pos'] || $assocType['neg']) {
		print ";<br/>&nbsp;- TF acting as " . ($assocType['pos']?"activator":"");
		print ($assocType['neg']?($assocType['pos']?" or ":"") . "inhibitor":"");
	}
	print ".</p>";
	if ($ecgroup['big']) {
		print "<p>Environmental conditions filtered by {$ecgroup['big']}";
		if ($ecgroup['sub']) print ", in particular {$ecgroup['sub']}";
		print ".</p>";
	}
}

if ( (isset($docReg) && count($docReg)>0) || (isset($potReg) && count($potReg)>0) ){
?>
<table border="1" summary="TFs">
	<tr>
		<th rowspan="2">Transcription Factor
<?php if ($homoSpecies) { print "&nbsp;(&rarr; Homologous genes)"; } ?></th>
<?php if ($regs['doc'] || $regs['both']) { ?>
		<th align="center">Documented</th>
<?php } if ($regs['pot'] || $regs['both']) { ?>
		<th align="center" colspan="2">Potential</th>
<?php } ?>
	</tr>
	<tr>
<?php
	if ($regs['doc'] || $regs['both']) {
		if ($homoSpecies) {
			print "<th>Genes&nbsp;(&rarr; Homologous genes)</th>";
		} else {
			print "<th>Genes&nbsp;-&nbsp;Reference</th>";
		}
	}
	if ($regs['pot'] || $regs['both']) { ?>
		<th align="center">Binding&nbsp;Sites</th>
		<th align="center">Regulated&nbsp;Genes</th>
<?php } ?>
	</tr>
<?php
		# place user inserted here!
		foreach (array_keys($aTFs) as $tfid) {
			$tf=$aTFs[$tfid]['prot'];
?>
	<tr>
		<td class="align">
			<a name="<?=$tf?>" href="view.php?existing=protein&amp;proteinname=<?=$tf?>">
		<?=$tf?></a>
<?php
			if ($homoSpecies && isset($mapping[$tfid])) {
				print "&nbsp;(&rarr; ";
				$i=0;
				foreach($mapping[$tfid]['gene'] as $key=>$value){
					if ($i>0) print ",&nbsp;";
			$web->printGene($mapping[$tfid]['gene'][$key],$mapping[$tfid]['orf'][$key],false,$homoSpecies);
			$i++;
		}
				print ")";
	}
?>
		</td>
<?php if ($regs['doc'] || $regs['both']) { ?>
		<td class="align">
<?php
	if (isset($docReg[$tfid])) {
		$aTGsForm = array();
		foreach($docReg[$tfid] as $tgid){
			// Javascript submenus
			if ($homoSpecies) {
				$aTGsForm[] = $web->Gene($mapping[$tgid]['gene'],$mapping[$tgid]['orf']);
			} else {
				$g = $aTGs[$tgid]['gene'];
				$o = $aTGs[$tgid]['orf'];
	      			$aTGsForm[] = $web->Gene($g,$o);
			}
		}
		$items[0] = array("ranktf",  "Rank by TF", "formrankbytf.php", 1, 1);
		$items[1] = array("tfs",     "Search for TFs", "formfindregulators.php", 1, 1);
		$items[2] = array("tgs",     "Search for Genes", "formfindregulated.php", 1, 1);
		$items[3] = array("regs",    "Search for Associations", "formregassociations.php", 1, 1);
		$items[4] = array("orfgene", "ORF List &hArr; Gene List", "formorftogene.php", 0, 1);
		$items[5] = array("regviz",  "Interactive Network Image", "regnetviz.php", 1, 1);
		
		$web->jsmenuDraw("doc".$tf, $items, array($tf), $aTGsForm,$regs, $eviCode, $ecgroup, $assocType);

		foreach($docReg[$tfid] as $tgid){
			$g = $aTGs[$tgid]['gene'];
                        $o = $aTGs[$tgid]['orf'];
			if ($homoSpecies) {
				$hg = $mapping[$tgid]['gene'];
				$ho = $mapping[$tgid]['orf'];
				$web->printGene($g,$o,false,false);	
				print "&nbsp;(&rarr; ";
				$web->printGene($hg,$ho,false, $homoSpecies);
				print ")<hr>";
			} else {
				$web->printGene($g,$o,false,false);
				print "&nbsp;-&nbsp;";
				$web->printRef($tf,$o);
?>					<hr/>
<?php
			}
		}
	} else print "Not found!";
?>		</td> <?php
	}
	if ($regs['pot'] || $regs['both']) {
?> 		<td class="align" colspan="2"> <?php
		if (isset($potReg[$tf])) {
?>
		<table border="1" summary="TFs">
<?php
			foreach (array_keys($potReg[$tf]) as $c) {
				print "<tr><td class=\"align\">";
				$web->printConsensus($tf,$c,$homoSpecies);
				print "</td><td class=\"align\">";
			// Javascript submenus
			$aTGsForm = array();
      foreach ($potReg[$tf][$c] as $o => $id) {
          $aTGsForm[] = $o; 
      }   
      $items[0] = array("ranktf",  "Rank by TF", "formrankbytf.php", 1, 1);
      $items[1] = array("tfs",     "Search for TFs", "formfindregulators.php", 1, 1);
      $items[2] = array("tgs",     "Search for Genes", "formfindregulated.php", 1, 1);
      $items[3] = array("regs",    "Search for Associations", "formregassociations.php", 1, 1);
      $items[4] = array("orfgene", "ORF List &hArr; Gene List", "formorftogene.php", 0, 1);
      $items[5] = array("regviz",  "Interactive Network Image", "regnetviz.php", 1, 1);
      $web->jsmenuDraw("pot".$tf.$c, $items, array($tf), $aTGsForm,
        $regs, $eviCode, $ecgroup, $assocType);

?>
			Found <?=count($potReg[$tf][$c])?> potential regulated genes<br/>
<?php
      foreach ($potReg[$tf][$c] as $o => $id) {
	      $web->printPotential($o,$c,$homoSpecies);
	      print "&nbsp;";
      } ?>
		</td></tr>
<?php	} ?>
		</table>
<?php	} else print "Not found!"; ?>
		</td>
<?php } ?>
	</tr>
<?php } ?>
</table>
<?php
} else { # if no results
	if (!isset($homoORFInfo) || empty($homoORFInfo)) {
	        ?><span class="error">No homologous Transcription Factors found for <?=$_POST['tfs']?> in <?=$homoSpecies?>.</span><?php
        }
	elseif (!isset($aMotifs) || empty($aMotifs)) {
		?><span class="error">This species does not have any binding site identified for the selected TFs. </span><?php
	}
	elseif (!isset($matches) || empty($matches)){
		?><span class="error">No target genes found!</span><?php
	}
}
?>
</td>
<td class="align">
	<table border="0" class="right" summary="quick jump">
		<tr><th>Jump&nbsp;to&nbsp;<img src="images/s_desc.png" alt="gene" /></th></tr>
		<tr><td class="smallfunc">
<?php
foreach (array_keys($aTFs) as $tfid) {
	$tf=$aTFs[$tfid]['prot'];
    print "<a href=\"#$tf\">$tf</a><br/>";
}
?>
		</td></tr>
    </table>
</td></tr>
<tr><td>
<input type="reset" value="Go Back" onclick="javascript:history.back()" />
</td></tr></table>
<?php
$web->printFooter();
?>
<script type="text/javascript" src="inc/menu.js"></script>
<?php
$web->closePage();
?>
