<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Search by DNA motif", "");
$web->printBarMenu("Search by DNA motifs");
?>

<form method="post" action="searchbydnamotif.php"> 
<table class="center" border="0" summary="main content">
	<tr>
		<td colspan="2" class="center">
		<h4>Search for DNA motif(s) on <?=$web->getProp("db.$web->_dbname.short")?> promoter regions</h4>
		</td>
	</tr>
	<tr>
		<td valign="bottom" align="center" colspan="2">
			<table border="1" summary="form dna motif">
				<tr>
					<th>DNA Motif</th>
					<th>ORF/Gene</th>
				</tr>
				<tr>
					<td style="background-color: #DDDDDD" class="align">
						<textarea rows="8" cols="18" name="consensus"></textarea>
					</td>
					<td style="background-color: #DDDDDD" class="align" rowspan="2">
						<textarea rows="10" cols="18" name="genes"></textarea>
					</td>
				</tr>
				<tr>
					<td style="background-color: #DDDDDD" class="align">
						Substitutions:&nbsp;&nbsp;&nbsp;
						<select name="subst">
							<option value="0" selected="selected">0</option>
							<option value="1">1</option>
							<option value="2">2</option>
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="left">
			<input type="submit" name="submit" value="Search" onclick="return alertUser();" />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" name="clear" value="Clear" />
		</td>
		<td align="right">
<?php $web->strainSampleIcons(array('genes' => 'tgs'), "inputSample();"); ?>
<!--			<a href="help/searchbydnamotif.php" title="Help"><img src="../images/information.gif" alt="help icon" /></a>-->
		</td>
	</tr>
</table>
</form>

<form method="post" action="motifsxdb.php">
<table class="center" border="0" summary="main content">
	<tr>
		<td colspan="2" align="center">
			<h4>Search described TF binding sites by a given DNA Motif</h4>
		</td>
	</tr>
	<tr><td colspan="2">
			<table border="1" summary="form dna motif">
				<tr>
					<th>DNA Motif</th>
					<th>Substitutions</th>
				</tr>
				<tr>
					<td><input type="text" size="45" name="motif" /></td>
					<td class="align"><select name="subst">
						<option value="0" selected="selected">0</option>
						<option value="1">1</option>
						<option value="2">2</option>
					</select></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="left">
			<input type="submit" name="submit" value="Search" onclick="return alertUserInputConsensus();" />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" name="clear" value="Clear" />
		</td>		
		<td align="right">
			<a href="javascript:inputSampleBindingSite();" title="Sample Data"><img src="../images/sampledata.gif" alt="sample data" /></a>
<!--			<a href="help/searchdnamotifintfbs.php" title="Help"><img src="../images/information.gif" alt="help icon" /></a>-->
		</td>
	</tr>
</table>
</form>
<?php
$web->printFooter();
?>
<script type="text/javascript">
<!--
function inputSample() {
	document.getElementsByName('consensus')[0].value = 'TATATAAG\nTATAWAAM\nTATA[GC]AA[AT]';//TODO \nTAACGTN{1,2}TGAT';
	document.getElementsByName('subst')[0].value = '0';
}
function inputSampleBindingSite() {
	document.getElementsByName('motif')[0].value = 'CACCAGTCGGTGGCTGTGCGCTTGTTACGTAA';
}
function alertUser(){
	if (document.getElementsByName('genes')[0].value.replace(/\s*/g,"") == ''){
		alert("Please enter one or more gene names.");
		return false;
	}
	if (document.getElementsByName('consensus')[0].value.replace(/\s*/g,"") == '') {
		alert("Please enter DNA motif(s) to search.");
		return false;
	}

	var separator = '\n';
	var stringArray = document.getElementsByName('consensus')[0].value.split(separator);
	for (var i=0; i < stringArray.length; i++) {
		if (stringArray[i].replace(/\s*/g,"").length < 4) {
			alert("All DNA motifs need to have at least 4 bases.");
			return false;
		}
	}
	
}
function alertUserInputConsensus() {
	var motif = document.getElementsByName('motif')[0].value.replace(/\s*/g,"");
	if (motif.length < 4) {
		alert("Please insert a DNA motif with at least 4 bases.");
		return false;
	}
	
	var reg = /^([actgwsrymkdhvbnACTGWSRYMKDHVBN]*(\[[actgwsrymkdhvbnACTGWSRYMKDHVBN]+\])?)+$/i;
	if (reg.exec(motif)) {
		return true;
	}
	alert("The DNA motif can only be composed by the IUPAC alphabet\nor equivalent representation, e.g., W - [AT]");
	return false;
}
-->
</script>
<?php
$web->closePage();
?>
