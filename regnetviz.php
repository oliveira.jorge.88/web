<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter("no");
$web->printHeader("Home", "index.php",
"Homologous network analysis","javascript:history.back()","Result");
$web->printBarMenu("Transcriptional Regulatory Network Visualization");

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

if (isset($_POST) && isset($_POST['formtfs']) ) { // IF IT COMES FROM INTERACTIVE NETWORK (SEARCH BY...MENU)
	$regulators = (isset($_POST) && isset($_POST['formtfs']))? $_POST['formtfs']: false;
	$regulated = (isset($_POST) && isset($_POST['formtgs']))? $_POST['formtgs']: false;
	$imagetypestatic = false; 
	$imagetypecirc = false;
}
else if ( isset($_POST) && isset($_POST['origin']) && ($_POST['origin']=="rankbytf") ){ // IF IT COMES FROM THE RANK BY TF
	$regulators = (isset($_POST) && isset($_POST['regulators']))? $_POST['regulators']: false;
	$regulated = (isset($_POST) && isset($_POST['regulated']))? $_POST['regulated']: false; 
	$ext = (isset($_POST) && isset($_POST['ext']))? $_POST['ext']: "svg";
	$imagetypestatic = (isset($_POST) && isset($_POST['imagetype']) && $_POST['imagetype']=="static");
	$imagetypecirc = (isset($_POST) && isset($_POST['imagetype']) && $_POST['imagetype']=="d3jscirc");
}
else if ( isset($_POST) && isset($_POST['origin']) && ($_POST['origin']=="homo-net") ){ // IF IT COMES FROM HOMOLOGOUS NETWORK
	$alltfs = (isset($_POST) && isset($_POST['alltfs']))? $_POST['alltfs']: false;
	if ($alltfs) {
		$f = function($elem) { return $elem['prot']; };
		$regulators = join(" ",array_map($f, $service->getTFs(true)));
	} else { $regulators = (isset($_POST) && isset($_POST['regulators']))? $_POST['regulators']: false;}
	$regulated = (isset($_POST) && isset($_POST['regulated']))? $_POST['regulated']: false;
	$ext = (isset($_POST) && isset($_POST['ext']))? $_POST['ext']: "svg";
	$imagetypestatic = (isset($_POST) && isset($_POST['imagetype']) && $_POST['imagetype']=="static");
	$imagetypecirc = (isset($_POST) && isset($_POST['imagetype']) && $_POST['imagetype']=="d3jscirc");
}

$synteny = (isset($_POST) && isset($_POST['synteny']))? $_POST['synteny']: 0;
$homoSpecies = (isset($_POST) && isset($_POST['homology']))? $_POST['homology']: false;

list($regs, $eviCode, $assocType, $ecgroup) = $web->getQueryParams(); // $regs is not used nowadays, is always documented

$inserted_g = "";

// IF IT COMES FROM RANKBYTF
if (!$regulators){
	$regulators = "";
	$regulated = "";
	$swf_regulators = "";
	$swf_regulated = "";
	foreach (array_keys($_POST) as $key) {
		if (strpos($key, "target_") === 0 ) {
			$regulated .= substr($key, 7) . "\n";
			if (!empty($swf_regulated)) $swf_regulated .= ";";
			$swf_regulated .= substr($key, 7);
			continue;
		}
		if (strpos($key, "tf_") === 0 ) {
			$regulators .= substr($_POST[$key], 3) . "\n";
			if (!empty($swf_regulators)) $swf_regulators .= ";";
			$swf_regulators .= substr($key, 3);
			continue;
		}

		if (strpos($key, "inserted_") === 0) {
			if (!empty($inserted_g)) $inserted_g .= ";";
			$inserted_g .= substr($key, 9);
		}
	}
}

?>
<table border="0" class="center" width="90%" summary="main content">
<tr><td class="align">

<?php

if ($imagetypestatic || $imagetypecirc) { 
	include_once('service/Image.php');
	$image = new Image();
	list($aTGs, $unknownTGs) = $service->user2Info($regulated,$service->aSpecies());
	list($aTFs, $unknownTFs) = $service->user2Info($regulators,$service->aSpecies());
	$retObj = $service->getRegulations(array_keys($aTFs), array_keys($aTGs), $ecgroup, $eviCode, $assocType, true, true);
}

if ($imagetypestatic) {
	if (strlen($regulators)==0) {
?>
<br/>
<span class="error">No Transcription Factors were selected!</span>
<br/>
<?php
	} else {
		list($url, $name, $wimage) = $image->generateDotGraph($retObj,$web,$ext,$aTFs,$aTGs);
		if ($wimage) {
?>
<a href="<?=$url.$name?>">
<img style="border:0" alt="Click to see the regulatory network"
	src="<?=$url.$name?>" width="800" />
</a>
<?php
		} else {
?>
<br/>
<span class="error">Resulting graph too big! Image not generated.</span>
<br/>
Download the generated dot file:
<i><a href="<?=$url.$name?>">dot file</a></i>
<?php
		}
	}
}
else if ($imagetypecirc) { // Use D3.js
	$json = $image->generateD3jsGraph($retObj);
	list($url,$name) = $service->saveTmpFile($json, "RegulatoryNetwork", "json");

	include_once('d3/HierarchicalEdgeBundling.php');
	$d3 = new HierarchicalEdgeBundling();
	$d3->draw($url,$name);
}
else {  // THE NETWORK

	$currSp = $service->aSpecies();

	// FIND THE ORTHOLOG FOR EACH INSERTED TF
	list($aTFInfo,$unknownTFs) = $service->user2Info($regulators,$currSp);
	list($TFcurr2homo,$TFhomo2curr) = $service->ids2orthologs(array_keys($aTFInfo),$synteny,false);
	$aHomoTFids = array_keys($TFhomo2curr);

	// FIND THE ORTHOLOG FOR EACH INSERTED TARGET

	list($aTGInfo,$unknownTGs) = $service->user2Info($regulated,$currSp);
	list($TGcurr2homo,$TGhomo2curr) = $service->ids2orthologs(array_keys($aTGInfo),$synteny,false);
	$aHomoTGids = array_keys($TGhomo2curr);

        if (empty($aHomoTFids)) {
                ?>
                        <br/>
                        <span class="error">No homologs for the inserted transcription factors.</span>
                        <br/>
                <?php
        }
        elseif (empty($aHomoTGids)) {
                ?>
                        <br/>
                        <span class="error">No homologs for the inserted target genes.</span>
                        <br/>
                <?php
        }
        else {



	$ahomoTFInfo = $service->orfIDs2Info($aHomoTFids);
	$aTFInfo = $aTFInfo + $ahomoTFInfo;
	$ahomoTGInfo = $service->orfIDs2Info($aHomoTGids);
	$aTGInfo = $aTGInfo + $ahomoTGInfo;

	$aTFs = [];
	foreach (array_keys($aTFInfo) as $tfid){
        	if (!isset($aTFs[$aTFInfo[$tfid]['species']])) $aTFs[$aTFInfo[$tfid]['species']] = [];
	        $g = ($aTFInfo[$tfid]['gene']=='Uncharacterized')?$aTFInfo[$tfid]['orf']:$aTFInfo[$tfid]['gene'];
        	$aTFs[$aTFInfo[$tfid]['species']][] = $g;
	}

	include_once('inc/head.inc.php');
	Head::printUnknown($unknownTGs,0);
	Head::printUnknown($unknownTFs,1);

	$web->printSyntenyWarn($synteny);


	// NEW JASON BUILD FUNCTION
	list($url,$name,$aHomoSp) =
	       $service->buildJSONViz($aTFInfo,$aTGInfo,$TFcurr2homo+$TGcurr2homo,$TFhomo2curr+$TGhomo2curr,$currSp);

	include_once('d3/RegGraphAnnot.php');
	$d3 = new RegGraphAnnot($ecgroup, $eviCode, $assocType);
	$d3->draw($service, $url, $name, $aTFs, $aHomoSp);

	}
}
?>
</td></tr>
<tr><td class="align">
<input type="reset" value="Go Back" onclick="javascript:history.back()" />
</td></tr></table>
<?php

$web->printFooter();
$web->closePage();
?>
