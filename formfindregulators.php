<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Search for Transcription Factors", "");
$web->printBarMenu("Search for Transcription Factors");

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

list($biggroup, $retObj) = $service->getEnvironCondsGroupsFromDB();
$homologSpecies = $service->getHomologSpecies();

?>

<h4 class="center">by target genes</h4>
<form method="post" action="findregulators.php">
	<table class="center" border="0" summary="main content">
	  <tr>
		<td colspan="2">
		<table border="1" summary="main form">
		  <tr>
			<th>Regulations Filter</th>
			<th>Regulated Genes</th>
		  </tr>
		  <tr>
			<td style="background-color: #DDDDDD" class="vcenter">
				<input type="radio" value="doc" name="type" onchange="javascript:toggleVisible();" checked /><b>Documented</b>
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="evidence" value="dir" onchange="javascript:toggleVisible();" /><b>Only</b> DNA binding evidence
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="evidence" value="indir" onchange="javascript:toggleVisible();" /><b>Only</b> Expression evidence
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="t_pos" value="true" onchange="javascript:toggleVisible();" checked />TF acting as activator
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="t_neg" value="true" onchange="javascript:toggleVisible();" checked />TF acting as inhibitor
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="evidence" value="plus" onchange="javascript:toggleVisible();" checked />DNA binding <b>plus</b> expression evidence
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="evidence" value="and" onchange="javascript:toggleVisible();" />DNA binding <b>and</b> expression evidence
        <br/>&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="use_na" value="true" onchange="javascript:toggleVisible();" checked />Consider also regulations without
				<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;associated information
				<br/>
				<input type="radio" value="pot" name="type" onchange="javascript:toggleVisible();" /><b>Potential</b>
				<!--<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO IMAGE OPTION IS DEPRECATED                  -->
				<input type="hidden" name="image" value="Y" />
				<!--<br/> DEPRECATED
				<input type="radio" value="both" name="type" onchange="Javascript:toggleVisible();"/><b>Documented and Potential</b> --> 
			</td>
			<td style="background-color: #DDDDDD">
			<textarea rows="15" cols="15" name="genes"><?=$web->post2Form("formtgs")?></textarea>
			</td>
		  </tr>
<tr id="doc-filters" style="visibility:visible;">			
				<td class="align" colspan="2">
					<b>Filter Documented Regulations by environmental condition:</b>
					<br/>Group: <select name="biggroup" onchange="javascript=subgroups();">
						<option value="0" selected>----</option>
<?php
foreach ($biggroup as $g) {
	print "<option value=\"$g\">$g</option>";
}
?>
					</select>
					<br/>Subgroup: <select id="subgroup" name="subgroup">
						<option value="0" selected>----</option>
					</select>
				</td>
			</tr>
	
		<tr id="doc-homo" style="visibility:visible;">
				<td class="align" colspan="2">
					<b>Search for Homologous Regulations in:</b>
					<br/>Species: <select id="doc-species" name="doc-species" onchange="javascript=toggleVisible();">
						<option value="0" selected>----</option>
<?php
foreach ($homologSpecies as $species) {
	print "<option value=\"$species\">$species</option>";
}
?>
					</select>
					<br/>Considering only documented regulations (all evidence codes)
       	</td>
			</tr>
			<tr id="doc-syn" style="visibility:visible;">
				<td class="align" colspan="3">
				<?php $web->printSyntenyForm();?>				
</td>
                        </tr>

<tr id="pot-filters" style="display:none;" >
                                <td class="align" colspan="2">
                                        <b>Consider TF binding sites from strain:</b>
                                        <br/>Species: <select id="pot-species" name="pot-species">
                                                <option value="0" selected>----</option>
<?php
foreach ($homologSpecies as $species) {
        print "<option value=\"$species\">$species</option>";
}
?>
                                        </select>
                        </tr>
		</table>
	  </td>
	</tr>
	<tr>
		<td align="left">
			<input type="submit" name="submit" value="Search" onclick="return validate();"/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" name="clear" value="Clear"/>
		</td>
		<td align="right">
<?php $web->strainSampleIcons(array('genes' => 'tgs'), "inputSample();"); ?>
			<a href="help_findregulators.php" title="Help"><img style="border:0" src="../images/information.gif" alt="help icon" /></a>
		</td>
	</tr>
	    </table>
    </form>

<?php
$web->printFooter();
?>
<script type="text/javascript">
<!--
function subgroups() {
	var subgroup = document.getElementById('subgroup');
	subgroup.options.length = 0;
	subgroup.options[0] = new Option('----','----');
<?php
foreach ($biggroup as $key) {
	print "\tif (document.getElementsByName('biggroup')[0].value == '$key') {\n";
	foreach ($retObj[$key] as $s) {
		print "\t\tsubgroup.options[subgroup.options.length] = new Option('$s','$s');\n";
	}
	print "\t}\n";
}
?>
}
function toggleVisible() {
	if (document.getElementsByName('type')[1].checked) {
		// Disable all documented stuff!
		document.getElementsByName('evidence')[0].disabled = true;
		document.getElementsByName('evidence')[1].disabled = true;
		document.getElementsByName('evidence')[2].disabled = true;
		document.getElementsByName('evidence')[3].disabled = true;
		document.getElementsByName('biggroup')[0].disabled = true;
		document.getElementsByName('subgroup')[0].disabled = true;
		document.getElementById('doc-filters').style.display = "none";
		document.getElementById('doc-homo').style.display = "none";
		document.getElementById('doc-species').disabled = true;
		document.getElementById('doc-syn').style.display = "none";
  //              document.getElementById('doc-synteny').disabled = true;

	} else {
		// Enable all documented stuff
		//document.getElementsByName('image')[0].disabled = true;
		document.getElementsByName('evidence')[0].disabled = false;
		document.getElementsByName('evidence')[1].disabled = false;
		document.getElementsByName('evidence')[2].disabled = false;
		document.getElementsByName('evidence')[3].disabled = false;
		document.getElementsByName('biggroup')[0].disabled = false;
		document.getElementsByName('subgroup')[0].disabled = false;
		document.getElementById('doc-filters').style.display = "table-row";
		document.getElementById('doc-homo').style.display = "table-row";
		document.getElementById('doc-species').disabled = false;
                document.getElementById('doc-syn').style.display = "table-row";
    //            document.getElementById('doc-synteny').disabled = false;
	}
	if (document.getElementsByName('doc-species')[0].value!=0) {
                document.getElementsByName('synteny')[0].disabled = false;
        }
        else {
                document.getElementsByName('synteny')[0].disabled = true;
        }
	if (document.getElementsByName('type')[0].checked) {
		// Disable all potential stuff!
		//document.getElementsByName('image')[0].disabled = true;
		document.getElementById('pot-filters').style.display = "none";
		 document.getElementById('pot-species').disabled = true;
	} else {
		// Enable all potential stuff!
		//document.getElementsByName('image')[0].disabled = false;
		document.getElementById('pot-filters').style.display = "table-row";
		document.getElementById('pot-species').disabled = false;
	}
	toggleVisibleTF();
}
function toggleVisibleTF() {
	if (document.getElementsByName('evidence')[0].disabled ||
		document.getElementsByName('evidence')[0].checked) {
		document.getElementsByName('t_pos')[0].disabled = true;
		document.getElementsByName('t_neg')[0].disabled = true;
	} else {
		document.getElementsByName('t_pos')[0].disabled = false;
		document.getElementsByName('t_neg')[0].disabled = false;
	}
}
function inputSample() {
	document.getElementsByName('type')[0].checked = true;
	document.getElementsByName('evidence')[2].checked = true;
	document.getElementsByName('image')[0].checked = false;
	document.getElementsByName('t_pos')[0].checked = true;
	document.getElementsByName('t_neg')[0].checked = true;
	toggleVisible();
}
function validate() {
	return alertUserEmptyGene() && checkTF();
}
function checkTF() {
	if ((document.getElementsByName('evidence')[1].checked ||
	     document.getElementsByName('evidence')[2].checked ||
			 document.getElementsByName('evidence')[3].checked) &&
		  (!document.getElementsByName('t_pos')[0].checked &&
			 !document.getElementsByName('t_neg')[0].checked)) {
		alert('Since you selected expression evidence, you must select at least one role for TFs.');
		return false;
	} else {
		return true;
	}
}
function alertUserEmptyGene(){
	if (document.getElementsByName('genes')[0].value.replace(/^\s*$/g,"") == '') {
		alert('Please enter gene(s) name to search for transcription factors.');
		return false;
	}
	return true;
}
function showOrHide(eId) {
	e = document.getElementById(eId);
	if (e.style.visibility == "hidden") {
		e.style.visibility = "visible";
		document.getElementById(eId+"_img").src="../images/minus_icon.gif";
		document.getElementsByName("show").value="true";
	} else {
		e.style.visibility = "hidden";
		document.getElementById(eId+"_img").src="../images/plus_icon.gif";
		document.getElementsByName("show").value="false";
	}
}
-->
</script>
<?php
$web->closePage();
?>
