<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Help", "");
$web->printBarMenu("Index of Help Files");
?>

<table border="0" class="center" style="width:75%" summary="main content">
<tr><td>

<ol style="font-weight: bold; font-size: 11pt; line-height: 150%">
	<li>Regulatory Associations
		<ul>
			<li><a href="help_findregulators.php">Search for TFs</a></li>
			<li><a href="help_findregulated.php">Search for Genes</a></li>
			<li><a href="help_regassociations.php">Search for Associations</a></li>
		</ul>
	</li>

	<li>Rank Genes
		<ul>
			<li><a href="help_rankbytf.php">Rank by TF</a></li>
			<li><a href="help_rankbygo.php">Rank by GO</a></li>
		</ul>
	</ul>
	</li>

	<li>Utilities
		<ul>
			<li><a href="help_orftogene.php">ORF List <=> Gene List</a></li>
			<li><a href="help_regmatrix.php">Generate Regulation Matrix</a></li>
		</ul>
	</li>

	<li>Retrieve
		<ul>
			<li><a href="help_seqretrieval.php">Upstream Sequence</a></li>
		</ul>
	</li>
</ol>

</td></tr>
</table>

<?php
$web->printFooter();
$web->closePage();
?>
