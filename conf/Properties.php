<?php
class Properties {
	private static $_propsFile = 'path.properties';
	private static $_websiteFile = 'website.properties';
	private static $_map;

	private function loadFile($fPath) {
		$lines = file($fPath);
		foreach ($lines as $l) {
			if (empty(trim($l)))
				continue;
			$parts = explode(' = ', $l);
			if (count($parts)!=2) continue;
			Properties::$_map[trim($parts[0])] = trim($parts[1]);
		}
	}

	public function __construct($path = "") {
		if (isset(Properties::$_map) && !empty(Properties::$_map))
			return;
		Properties::$_map = [];
		Properties::loadFile($path . 'conf/' . Properties::$_propsFile);
		foreach (scandir($path . 'conf/') as $file) {
			if ($file === Properties::$_websiteFile) {
				Properties::loadFile($path . 'conf/' . $file);
			}
		}
	}

	public function __destruct()  { }

	public function get($p) {
		if (isset(Properties::$_map) && isset(Properties::$_map[$p])) {
			return Properties::$_map[$p];
		}
		return false;
	}

	public function getKey($value) {
		foreach (Properties::$_map as $k => $v) {
			if ($value==$v) {
				return $k;
			}
		}
		return null;
	}

	public function getAllDBs() {
		$aDBs = [];
		foreach (Properties::$_map as $k => $v) {
			if (strpos($k, "site.") !== false &&
				  strpos($k, ".dbs") !== false) {
					foreach (explode(" ", $v) as $db) {
						$aDBs[] = $db;
					}
			}
		}
		return $aDBs;
	}

	public function getDBurl($species) {
		$db = "";
		foreach (Properties::$_map as $k => $v) {
			if (strpos($k, "db.") !== false &&
				strpos($k, ".dbspecies") !== false &&
				strpos($species, $v) !== false) {
				$db = substr($k, 3, strrpos($k, ".")- 3);
				break;
			}
		}
		return $this->get("db.$db.url");
	}	

	public function getSiteDB() {
		$db = "";

		foreach (explode(" ", $this->get("site.dbs")) as $k) {
			if (strpos($_SERVER['PHP_SELF'], $k) === false)
				continue;
			$db = $k;
		}
		return array($this->get("site.name"), $db);
	}

	public function getDBspecies() {
		$retObj = [];
		$dbkeys = $this->get("site.dbs");
		foreach (explode(" ", $dbkeys) as $key) {
			$species = $this->get("db.$key.dbspecies");
			$strains = $this->get("db.$key.dbstrains");
			foreach (explode("\t", $strains) as $s) {
				$retObj[] = "$species $s";
			}
		}
		return $retObj;
	}
}
?>
