<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter("no");
$web->printHeader("Home", "index.php",
"Search by DNA motif", "javascript:history.back()",
"Result", "");
$web->printBarMenu("Search for DNA motif(s) on ".$web->getProp("db.$web->_dbname.short")." promoter regions");

$consensus = (isset($_POST) && isset($_POST['consensus']))? 
	preg_split("/[\s|]+/", trim($_POST['consensus'])): false;

$genes = (isset($_POST) && isset($_POST['genes']))? $_POST['genes']: false;
$subst = (isset($_POST) && isset($_POST['subst']))? $_POST['subst']: 0;
$allGenes = (isset($_POST) && isset($_POST['allgenes']))? true: false;

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

$currSp = $service->aSpecies();
list($aORFinfo, $unknown) = $service->user2Info($genes,$currSp);

$promoter = $service->upstreamSeq(array_keys($aORFinfo));

$inputmotif = [];
$badmotifs = array();

foreach ($consensus as $c) {
	$c = trim($c);
	if (empty($c)) continue;
	list($motif,$good) = $service->motifIUPACCompress(strtoupper($c));
	if (!$good) {
		array_push($badmotifs, $motif);
		continue;
	}
	$inputmotif[$motif][$motif][$motif] = $motif;
}

list($matches,$motifs2tf) = $service->tfBindingSites($promoter, $inputmotif, $subst);

?>
<table border="0" class="center" width="70%" summary="main content">
<tr><td class="align">

<?php
include_once('inc/head.inc.php');
Head::printUnknown($badmotifs, Head::TYPE_motif);
Head::printUnknown($unknown);

?>

Considering <span class="error"><?=$subst?></span> substitutions.<br/>

<table border="1" summary="ORF/genes">
	<tr>
		<th>ORF</th>
		<th>Gene</th>
		<th># Occurrences for DNA Motifs</th>
		<th>View</th>
	</tr>
<?php 
	if (count($matches)>0) {
		$oLink = "<a href=\"view.php?existing=locus&amp;orfname=";
		$pLink = "<a href=\"viewconsensusinpromoter.php?orfname=";
		foreach (array_keys($aORFinfo) as $orfid) {
			$num_m = 0;
			$mtfs = "";
			if (isset($matches[$orfid])){
				foreach (array_keys($matches[$orfid]) as $m) {
					if (isset($matches[$orfid][$m]['fwr'])) $num_m += count($matches[$orfid][$m]['fwr']);
					if (isset($matches[$orfid][$m]['rev'])) $num_m += count($matches[$orfid][$m]['rev']);
					$mtfs .= "|$m";
				}
			}
			$orf = $aORFinfo[$orfid]['orf'];
			$gene = $aORFinfo[$orfid]['gene'];
			$gene_txt = $oLink.$gene."\">$gene</a>";
			print "<tr><td>$oLink$orf\">$orf</a></td><td>$gene_txt</td><td>$num_m</td>";
			print "<td>$pLink$orf&amp;consensus=$mtfs&amp;subst=$subst\">Promoter</a></td></tr>";
		}
	} else print "<tr><td colspan=\"3\">No matches found!</td></tr>";
?>
</table>

</td></tr>
<tr><td class="align">
<input type="reset" value="Go Back" onclick="javascript:history.back()"/>
</td></tr>
</table>
<?php
$web->printFooter();
$web->closePage();
?>
