<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"IUPAC Code Generation", "");
$web->printBarMenu("IUPAC Code Generation for DNA Sequences");
?>
<form method="post" action="iupacgeneration.php" id="iupac">
  <table class="center" border="0" summary="main content">
    <tr>
      <td colspan="2">
        <table border="1" summary="main form">
          <tr>
              <th>Please paste your sequences<br/>
              <small>(all sequences must be of same length)</small></th>
              </tr>
          <tr><td align="left" style="background-color: #DDDDDD">
            <textarea rows="10" cols="40" name="sequences"><?=trim(@$_POST['sequences'])?></textarea>
          </td></tr>
        </table>
<?php
include_once('service/Logic.php');
$service = new Logic(
	$web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

$aSeq = $service->formString2Array(@$_POST['sequences']);

if (count($aSeq)>0) {

    if ($service->allSameLength($aSeq)) {
        $outSeq = $service->iupacGeneration($aSeq);
        
        $toPrint = "";
        foreach ($outSeq as $seq) $toPrint .= "$seq<br/>";
?>
        </td><td valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;&rarr;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td style="background-color:#EEDDEE">
        <pre><?=$toPrint?>&nbsp;</pre>
<?php
    } else {
?>
        </td><td><span class="error">At least one sequence has different length!</span>
<?php
	}
}

?>
      </td>
    </tr>
      <tr>
            <td align="left">
<input type="submit" name="submit" value="Generate IUPAC Code" onclick = "return alertUser();"/>
            </td>
            <td align="right">
								<a href="#" onclick="javascript:document.forms['iupac'].sequences.value='';" >Clear</a>&nbsp;&nbsp;
                <a href="javascript:inputSampleIupac();" title="Sample Data"><img src="../images/sampledata.gif" alt="sample data" /></a>
            </td>
    </tr>
  </table>

<br/><hr/>

<center><h4>IUPAC Code Expansion (max 1000)</h4></center>

 <form method="post" action="iupacgeneration.php" id="expansion">
   <table class="center" border="0" summary="main content">
     <tr><td colspan="2">
       <table border="1" summary="main form">
         <tr>
           <th>IUPAC sequence&nbsp;&nbsp;</th>
           <td style="background-color: #DDDDDD">
             <input style="float: left" type="text" size="20" name="inputExpansion"/>
           </td>
         </tr>
       </table>
     </td></tr>
     <tr><td align="left">
       <input type="submit" name="submitIUPAC" value="Expand" onclick = "return alertUserEmptyIUPAC();"/>
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       <input type="reset" name="clearIUPAC" value="Clear"/>
     </td>
     <td align="right">
       <a href="javascript:inputSampleExpansion();" title="Sample Data"><img style="border:0" src="../images/sampledata.gif" alt="sample data" /></a>
     </td></tr>
   </table>
 </form>

<?php
if (isset($_POST) && isset($_POST['inputExpansion']) && strlen(trim($_POST['inputExpansion']))>0) {
  $iupacseq = strtoupper($_POST['inputExpansion']);
  $len = strlen($iupacseq);

  $IUPACcount['A'] = 1; $v['A'][0] = 'A';
  $IUPACcount['C'] = 1; $v['C'][0] = 'C';
  $IUPACcount['T'] = 1; $v['T'][0] = 'T';
  $IUPACcount['G'] = 1; $v['G'][0] = 'G';

  $IUPACcount['W'] = 2; $v['W'][0] = 'A'; $v['W'][1] = 'T';
  $IUPACcount['S'] = 2; $v['S'][0] = 'C'; $v['S'][1] = 'G';
  $IUPACcount['R'] = 2; $v['R'][0] = 'A'; $v['R'][1] = 'G';
  $IUPACcount['Y'] = 2; $v['Y'][0] = 'T'; $v['Y'][1] = 'C';
  $IUPACcount['M'] = 2; $v['M'][0] = 'A'; $v['M'][1] = 'C';
  $IUPACcount['K'] = 2; $v['K'][0] = 'T'; $v['K'][1] = 'G';

  $IUPACcount['D'] = 3; $v['D'][0] = 'A'; $v['D'][1] = 'T'; $v['D'][2] = 'G';
  $IUPACcount['H'] = 3; $v['H'][0] = 'A'; $v['H'][1] = 'T'; $v['H'][2] = 'C';
  $IUPACcount['V'] = 3; $v['V'][0] = 'A'; $v['V'][1] = 'C'; $v['V'][2] = 'G';
  $IUPACcount['B'] = 3; $v['B'][0] = 'T'; $v['B'][1] = 'G'; $v['B'][2] = 'C';

  $IUPACcount['N'] = 4;
  $v['N'][0] = 'A'; $v['N'][1] = 'T'; $v['N'][2] = 'G'; $v['N'][3] = 'C';

  $numseq = 1;
  for ($i = 0; $i < $len; $i++) {
    $numseq *= $IUPACcount[$iupacseq{$i}];
  }
  if ($numseq <= 1000) {
    $unfolded = array();
		for ($i = 0; $i < $numseq; $i++) {
			$unfolded[$i] = "";
		}
    $folds = 1;

    for ($i = 0; $i < $len; $i++) {
      $c = $iupacseq{$i};
      $folds *= $IUPACcount[$c];
      $div = $numseq / $folds;
      $divpos = 0; $k = 0;

      for ($j = 0; $j < $numseq; $j++) {
        if ($k >= ($div)) {
          $k = 0; $divpos ++;
        }
        if ($divpos >= $IUPACcount[$c]) {
          $divpos = 0;
        }
        $k++;
        $unfolded[$j] .= $v[$c][$divpos];
      }
    }
    $str_sequences = join("<br/>",$unfolded);
?>
  <br/>
  <table class="center" border="1" summary="iupac expansion">
  <th><?=$numseq?> expanded sequences</th>
    <tr><td>
<?php 
  print $str_sequences;
?>
    </td></tr>
  </table>
<?php
  } else {
?>
    <center><b><span class="error">Cannot expand: <?=$iupacseq?><br/>It generates more than 1000 sequences!</span></b></center>
<?php
  }
}
?>

</form>
<?php
$web->printFooter();
?>
<script type="text/javascript">
function inputSampleIupac() {
    document.getElementsByName('sequences')[0].value = 'TCCGTGGG\nTCCGTGGA\nTCCATGGA\nTCCGTGGG\nTCCGCGGG';
    }

function alertUser(){
    if (document.getElementsByName('sequences')[0].value.replace(/^\s*$/g,"") == ''){
    alert("Please enter more than one sequences to generate IUPAC code.");
    return false;
    }
}

function alertUserEmptyIUPAC(){
    var sRegExp = /[ACTGWSRYMKDHVBN]+/i;
    if (document.getElementsByName('inputExpansion')[0].value.replace(/^\s*$/g,"") == ''	   
|| !sRegExp.test(document.getElementsByName('inputExpansion')[0].value)) {
      alert("Please enter a valid IUPAC sequence.");
      return false;
    }
}

function inputSampleExpansion() {
    document.getElementsByName('inputExpansion')[0].value = 'TCCGYGWGG';
}
</script>
<?php
$web->closePage();
?>
