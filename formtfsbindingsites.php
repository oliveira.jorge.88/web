<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Find TFs Binding Sites", "");
$web->printBarMenu("Find transcription factor binding sites on a sequence");
?>
<form method="post" action="findregulators.php">
<input type="hidden" name="type"  value="pot" />
<input type="hidden" name="image" value="Y" />
<table class="center" border="0" summary="main content">
<tr>
	<td colspan="2">
	<table border="1" summary="main form">
		<tr>
			<th align="left">DNA Motifs</th>
			<th align="left">Paste your sequence <small>(FastA format)</small></th>
		</tr>
		<tr>
			<td style="background-color: #DDDDDD" class="align">
				<textarea rows="13" cols="20" name="motifs" ></textarea>
				<br/>
				<input type="checkbox" name="allmotifs" onclick="javascript:toggleVisible()" />All <?=$web->_dbname?> consensus
			</td>
			<td style="background-color: #DDDDDD" class="align">
				<textarea rows="15" cols="62" name="sequence"></textarea>
			</td>
		</tr>
	</table>
	</td>
	</tr>
			<tr>
				<td align="left">
					<input type="submit" name="submit" value="Search" onclick="return alertUser();"/>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="reset" name="clear" value="Clear" onclick="document.getElementsByName('allmotifs')[0].checked=true;toggleVisible();"/>
				</td>
				<td align="right">
								<a href="javascript:inputSample();" title="Sample Data"><img src="../images/sampledata.gif" alt="sample data" /></a>
<!--								<a href="help/findtfbs.php" title="Help"><img src="../images/information.gif" alt="help icon" /></a>-->
				</td>
		  </tr>
	    </table>
    </form>
<?php
$web->printFooter();
?>
<script type="text/javascript">
function toggleVisible() {
	if (document.getElementsByName('allmotifs')[0].checked) {
		document.getElementsByName('motifs')[0].disabled = true;
	} else {
		document.getElementsByName('motifs')[0].disabled = false;
	}
}
function inputSample() {
    document.getElementsByName('sequence')[0].value = '> Sample sequence\nCTCAAGTACTTCTGAACGGCTTCCTTTTGGGCAGAGTTGTTCTTCGACATCAATGCACTC\nAAACCTGTCTTTTTACCAATATCATCACCATGTAACTCCCTAGTGAATTGGGCCTGTCTT\nTTTCTCAATTCTGTTTCACTCATCTTATGCTGCCTACTATATTATTATTTTATTCTTGTT\nTTTTTAAATTATGTAAAACTTAAATCGAAAGTAAAACAGATAAGGGAAACTTGAATGAAA\nATTAACAGCTTATAAATTACAAAGACGAATGGAAAGAAAAAGAGAGCAGCAGAGGATAGG\nAGAAAACACCCGAACTATATAAAAAGCAATGAAGAAGTTATCGGCAACAGCAAGTATTGG\n> Yet another sequence\nACCATTGAGACGAAGTGGAGCAGTTTACAGTGAATAATACATTGTCTGCTCCTCACTTAC\nTCTCGCTTCTCGAAAACTTTTTTGTATTTATCGGAAACGGCAGTAAACGACGCCAAAAAT\nTGAAAATAAGTACGGGAACGAGGTACCATATACGAAGATCGGGGCTTTGTTCGGCCCTAA\nATTTTGCTGATCTTACCCTGTTGCATCTTTTCAAAAATAGTAACCAGCCCTAGCTGTTTG\nGTTGATTTGACCTAGGTTACTCTTTTCTTTTTCTGGGTGCGGGTAACAATTTGGGCCCCG\nCCAAAGCGCCGTCTTTGTCATGGGAACCGGAAACCCTCCGATGAAGAGTAGGAGGGTGGC\nAACTGATGGATGCGTAAGGTCTTAAGAGATACATTTGCTTAATAGTCTTCCGTTTACCGA\nACCCAAAACGTTTAAAGAAGGAAAAGTTGTTTCTTAAACC';
		document.getElementsByName('allmotifs')[0].checked = true;
		toggleVisible();
}
function alertUser(){
    if (document.getElementsByName('sequence')[0].value.replace(/^\s*$/g,"") == '') {
	    alert("Please enter a sequence in FASTA format.");
    	return false;
    }
    var reg = /^(\s*>[^\n\r]+((\n|\r)+[actgwsrymkdhvbnACTGWSRYMKDHVBN]+)+)+\s*$/i;
	if (reg.exec(document.getElementsByName('sequence')[0].value)) {
		return true;
	}
	alert("Inserted sequence is not in FASTA format.\nPlease see an example by clicking in the Sample Data icon.");
	return false;
}
</script>
<?php
$web->closePage();
?>
