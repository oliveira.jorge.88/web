<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"Rank Genes by GO", "");
$web->printBarMenu("Rank Genes by GO");
?>

<form method="post" action="rankbygo.php"> 
	<table class="center" border="0" summary="main content">
	  <tr>
	  <td colspan="2">
	    <table border="1" summary="main form">
	      <tr>
			<td style="background-color: #DDDDDD">Gene List</td>
			<td style="background-color: #DDDDDD">
				<textarea rows="15" cols="20" name="genes"></textarea>
			</td>
	      </tr>
		  <tr>
			<td style="background-color: #DDDDDD">Select Ontology</td>
			<td class="align"><select name="ontology">
				<option value="process">Biological process</option>
				<option value="function">Molecular function</option>
				<option value="component">Cellular component</option>
			</select></td>
		  </tr>
	    </table>
	  </td>
	</tr>
	<tr>
		<td align="left">
			<input type="submit" name="submit" value="Submit" onclick ="return alertUser();"/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="reset" name="clear" value="Clear"/>
		</td>
		<td align="right">
<?php $web->strainSampleIcons(array('genes' => 'tgs')); ?>
			<a href="help_rankbygo.php" title="Help"><img src="../images/information.gif" alt="help icon" /></a>
		</td>
	</tr>
</table>
</form>
<?php
$web->printFooter();
?>
<script type="text/javascript">
function alertUser(){
   if (document.getElementsByName('genes')[0].value.replace(/^\s*$/g,"") == '') {
		alert('Please enter genes to rank by GO');
		return false;
	}
}
</script>
<?php
$web->closePage();
?>
