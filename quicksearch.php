<?php
if (!isset($_GET['q']) || strlen(trim($_GET['q']))==0) {
	header("Location: index.php");
}
$initterm = $_GET['q'];

include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();

include_once('service/Logic.php');
$service = new Logic(
	$web->getProp("db.$web->_dbname.dbspecies"),
	$web->getProp("db.$web->_dbname.dbstrains"));
$terms = $service->quickSearch($initterm);

if (isset($terms['orfname'])) {
    header("Location: view.php?existing=locus&orfname=" . $terms['orfname']);
		exit;
}

$web->printHeader(# css, Page Title, (Barname, Barlink)....
"Home", "index.php",
"Quick Search", "");
$web->printBarMenu("Quick Search");


?>
<table align="left">
<tr><td class="align">
<?php
foreach (array_keys($terms) as $desc) {
	foreach ($terms[$desc] as $table => $num)
		$s  = $num . " $desc were found to have the search term ";
	$s .= "'<span class=\"error\"><b>$initterm</b></span>'";
	print (!isset($num) || $num == 0)? "<p>$s</p>": "<p><a href=\"quicksearchterm.php?$table=$initterm\">$s</a></p>";
}
?>
</td></tr>
</table>
<?php
$web->printFooter();
$web->closePage();
?>
