<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
"TF &hArr; Consensus List", "");
$web->printBarMenu("Transcription Factor &hArr; Consensus List");

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

$tfconslist = $service->getMotifsFromDB();

if (empty($tfconslist)) { print "<p><span class=\"error\">This species does not have TF binding sites yet!</span></p>"; }
else {

ksort($tfconslist);
?>
<table border="0" width="100%">
<tr><td>
      <table border="1" class="center">
        <tr>
          <th>Transcription Factor</th>
          <th>Consensus</th>
          <th colspan="2">Position-specific Weight Matrix</th>
          <th>Reference</th>
				</tr>
<?php
foreach (array_keys($tfconslist) as $tfid) {
	foreach(array_keys($tfconslist[$tfid]) as $tf){
		foreach ($tfconslist[$tfid][$tf] as $in => $c) {
?>
        <tr><td class="align">
			<a href="view.php?existing=protein&amp;proteinname=<?=$tf?>"><?=$tf?></a></td>
			<td class="align"><?=$c?></td>
			<td class="align"><a href="matrix.php?type=transfac&amp;tf=<?=$tf?>&amp;consensus=<?=$c?>">TRANSFAC</a></td>
			<td class="align"><a href="matrix.php?type=meme&amp;tf=<?=$tf?>&amp;consensus=<?=$c?>">MEME</a></td>
			<td class="center"><a href="view.php?existing=consensus&amp;proteinname=<?=$tf?>&amp;consensus=<?=$c?>">View</a></td></tr>
<?php
		}
	}
}
?>
	</table>
</tr>
</table>
<?php
}
$web->printFooter();
$web->closePage();
?>
