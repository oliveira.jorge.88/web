<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
	"Help", "help_index.php",
	"Upstream sequence", "");
$web->printBarMenu("Help - Upstream sequence");
?>

<table border="0" class="center" style="width:75%" summary="main content">
<tr><td>

<p>
This query retrieves the upstream promoter sequences for a given list of genes.
</p>

<p>
The input required is a list of genes.
The user can choose to, in alternative, retrieve the sequences for all the genes in the database by selecting the option <i>All genes</i>.
</p>

<p>
By default, the output is displayed in the browser (<i>Display</i>), although the results can be sent to a given e-mail in alternative, by selecting the option <i>E-mail</i> and introducing the e-mail address.
In addition, the coordinates of the upstream sequences can be altered (between -1 and -1000).
</p>

<p>
The sequences are retrieved in <a href="https://blast.ncbi.nlm.nih.gov/blastcgihelp.shtml">Fasta format</a>, and are either displayed in the browser or sent to the given e-mail.
</p>

</td></tr>
</table>

<?php
$web->printFooter();
$web->closePage();
?>
