<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter("no");
$web->printHeader("Home", "index.php",
"Network comparison","javascript:history.back()","Result");
$web->printBarMenu("Cross species: Network comparison visualization");

$alltfs = (isset($_POST) && isset($_POST['alltfs']))? $_POST['alltfs']: false;
$homoSp = (isset($_POST) && isset($_POST['species']))? $_POST['species']: false;
$synteny = (isset($_POST) && isset($_POST['synteny']))? $_POST['synteny']: 0;
$regulators = (isset($_POST) && isset($_POST['regulators']))? $_POST['regulators']: false;
$regulated = (isset($_POST) && isset($_POST['regulated']))? $_POST['regulated']: false;;
$imagetypestatic = false; 
$imagetypecirc = false; 
$join=true;

list($regs, $eviCode, $assocType, $ecgroup) = $web->getQueryParams();

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

$currSp = $service->aSpecies();

if ($alltfs) {
	$f = function($elem) { return $elem['prot']; };
	$regulators = join(" ",array_map($f, $service->getTFs(true)));
}

$sAllORFs = $regulators." ".$regulated ;

// FIND THE ORTHOLOG FOR EACH INSERTED TF

list($aTFInfo,$unknownTFs) = $service->user2Info($regulators,$currSp);
$homoTFs = $service->getHomoORFInfo(array_keys($aTFInfo),$homoSp,$synteny);

$aHomoTFids = array_keys($aTFInfo); // INCLUDE THE TFS AND TGS OF THE SCOPE SPECIES IN THE ARRAYS
foreach (array_keys($homoTFs) as $homoORF){
	foreach ($homoTFs[$homoORF]['homo']['orfid'] as $k=>$homoTFid){
		$aHomoTFids[]=$homoTFid;
	}
}	

// FIND THE ORTHOLOG FOR EACH INSERTED TARGET

list($aTGInfo,$unknownTGs) = $service->user2Info($regulated,$currSp);
$homoTGs = $service->getHomoORFInfo(array_keys($aTGInfo),$homoSp,$synteny);

$aHomoTGids = array_keys($aTGInfo);
foreach (array_keys($homoTGs) as $homoTG){
        foreach ($homoTGs[$homoTG]['homo']['orfid'] as $k=>$homoTGid){
                $aHomoTGids[]=$homoTGid;
        }
}

$ahomoTFInfo = $service->orfIDs2Info($aHomoTFids);
$aTFInfo = $aTFInfo + $ahomoTFInfo;
$ahomoTGInfo = $service->orfIDs2Info($aHomoTGids);
$aTGInfo = $aTGInfo + $ahomoTGInfo;

$aTFs = [];
foreach (array_keys($aTFInfo) as $tfid){
	if (!isset($aTFs[$aTFInfo[$tfid]['species']])) $aTFs[$aTFInfo[$tfid]['species']] = [];
	$g = ($aTFInfo[$tfid]['gene']=='Uncharacterized')?$aTFInfo[$tfid]['orf']:$aTFInfo[$tfid]['gene'];
	$aTFs[$aTFInfo[$tfid]['species']][] = $g;
}

?>
<table border="0" class="center" width="90%" summary="main content">
<tr><td class="align">

<?php

include_once('inc/head.inc.php');
Head::printUnknown($unknownTGs,0);
Head::printUnknown($unknownTFs,1);

$web->printSyntenyWarn($synteny);

// NEW JASON BUILD FUNCTION

$aInfo = $aTGInfo + $aTFInfo;

list($url,$name) = $service->buildJSONComp($aHomoTFids,$aHomoTGids,$aInfo,$homoTFs,$homoTGs,$currSp,$homoSp);

include_once('d3/RegGraphComp.php');
$d3 = new RegGraphComp($ecgroup, $eviCode, $assocType);
$d3->draw($service, $url, $name, $aTFs, $homoSp);

?>
</td></tr>
<tr><td class="align">
<input type="reset" value="Go Back" onclick="javascript:history.back()" />
</td></tr></table>
<?php

$web->printFooter();
$web->closePage();
?>
