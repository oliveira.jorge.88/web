<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
	"Help", "help_index.php",
	"Search for Associations", "");
$web->printBarMenu("Help - Search for Regulatory Associations");
?>

<table border="0" class="center" style="width:75%" summary="main content">
<tr><td>

<p>
This query allows the user to identify documented as well as potential regulatory associations (RAs) between input TFs and Genes.
</p>

<p>
The query requires a list of TFs, a list of genes or both as input, depending on the search mode selected: <i>Input TF list against all genes in the database</i>, <i>All TFs in the database against input gene list</i> or <i>Input TF list against input gene list</i>, respectively.
</p>

<p>
There are three possible search modes: input TFs against input genes (the default mode, <i>Input TF list against input Gene list</i>), input genes against all TFs in the database (<i>All TFs in the database against input Gene list</i>) and input TFs against all genes in the database (<i>Input TF list against all Genes in the database</i>).
</p>

<p>
While the query's default is to search for the RAs between each of the TFs and all the genes in the input sets (<i>Each Transcription Factor to Any Gene</i>), an additional option is available to allow the search for genes that are regulated by all the transcription factors in the input set (<i>Every Transcription Factor to Any Gene</i>).
In the former search mode all the RAs will show up in the results, while in the latter one only the subset of RAs which involve genes whose expression is regulated by all the TFs in the input set will appear.
</p>

<p>
Furthermore, the user may restrict its search to the regulatory associations identified based on direct or indirect evidences. Direct Evidence was considered to be provided through experiments such as Chromatine ImmunoPrecipitation (ChIP), ChIP-on-chip and Electrophoretic Mobility Shift Assay (EMSA), that prove the direct binding of the TF to the target gene's promoter region, or such as the analysis of the effect on target-gene expression of the site-directed mutation of the TF binding site in its promoter region, which strongly suggests that the TF interacts with that specific target promoter. The classification Indirect Evidence was attributed to experiments such as the comparative analysis of gene expression changes occurring in response to the deletion, mutation or over-expression of a given TF.
</p>

<p>
The search results are displayed in a table that contains the documented and potential targets of each TF, from the set of genes in the input list.
This search rejects the TFs for which no documented or potential RAs with any of the input genes exists.
</p>

</td></tr>
</table>

<?php
$web->printFooter();
$web->closePage();
?>
