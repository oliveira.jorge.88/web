# Create a database for your local instalation of CommunityYeastract

This section of the installation tutorial concerns the configuration of the database and loading of the data.

## Requirements

It assumes that the user already installed the necessary packages (see [../README.md](README.md)), in particular a MySQL/MariaDB relational database.

The user must install `BLAST+` if it considers necessary to compute the homologs.
Download from [NCBI] and follow their instructions.

### Database configuration

Please configure the database and load the data following the instructions on [mysql.README.md](db_load/mysql.README.md).

### Files 

- The basis for the database should be a [GenBank Flat File]. Download the GBFF file for each species you wish to include and place the files in the `data/` directory 

- To compute the homology, the aminoacid sequences of the ORFs are needed. Place a file per organism in the `genomes/` folder.

## Instalation

Ensure that mysqld is up and running and load the basic structure by following the steps in mysql.README.md 

Then, run:

    [user@server db_load]$ php LoadGBFF.php <file.gbff> <species> <strain>

(example: `php LoadGBFF.php data/Rhodotorula_toruloides_NP11.gbff "Rhodotorula toruloides" "NP11"`)

### Homologs

To compute the homologs, run:

    [user@server db_load]$ sh computeHomologs.sh

A folder named `homology/` will me created and a file named `blast_orthologies.tab` will contain the results for all the species inserted in the `genomes/` folder. You need this file to compute the Synteny.

To compute the synteny, a little customization is needed.
Because the fasta files don't usually have the exact same organism name as in the database, you must insert that information with an `orgmap.tab` (tab-separated) file before synteny computation. The file is composed of:
- `Org Name in genome file` followed by a `tab` followed by `Org name in database`. We provide an example of how your file should look like (see `db_load/orgmap.tab`).

It is also possible to define the number of neighbors to consider in the synteny search, by changing the `$window` property in the `Synteny.php` script. The default and recomended value is 15.
 
When the customization is complete, just run:

    [user@server db_load]$ php Synteny.php

A `synteny/` folder will be created with the 

To effectively upload the synteny info into the database, type:

    [user@server db_load]$ php UploadSynteny.php

### Potential Regulations

The next step is to pre-compute the potential regulations between all the species.
This will permit to speed up queries regarding the potential binding of TFBS on gene promoters. First, you need to compute then with the following command:

    [user@server db_load]$ php computeHomoPotRegs.php

The results will be put in the `homo_pot_regs/` folder, which will be created with the necessary files.

To then effectively upload the Homologous Potential Regulations into the database, type:

    [user@server db_load]$ php UploadHomoPotRegs.php

### Documented Regulations

A spreadsheet titled `CommunityDataSubmissionForm.xlsx` is available in this `db_load/` folder. This spreadsheet contains four sheets, where three sheets can be filled in:
- Instructions on how to fill the form. Column names, description and examples.
- Regulatory data, a form specifying all the information supporting regulatory interactions;
- Binding site data, a form specifying all the information supporting a transcription factor binding site;
- Environmental groups data, a form where environmental conditions associated with regulatory data are grouped into groups/subgroups, which can then be used as filters in most queries.

To load any of these forms, you need to save each as a Tab-Separated-Values file.
You can then load regulatory data with the following command:

    [user@server db_load]$ php UploadRegulations.php <regulatory_data.tsv> <species binomial name>

and binding site data with the following command:

    [user@server db_load]$ php UploadBinding.php <binding_data.tsv> <species binomial name>


The `UploadRegulations.php` and `UploadBinding.php` files only need the Pubmed ID to associate the article supporting the associated information. To load the details (authors, journal, year, ...) of all articles in the database from Pubmed, you can run the following command:

    [user@server db_load]$ php UpdatePubmedInfo.php

Additionally, if the user needs to include more groups than the ones specified in the spreadsheet, associated with the Regulatory and/or Binding site data, a Tab-Separated-file can be specified with each line containing the `group` followed by a `tab` followed by the `subgroup`. This file can be loaded using the following command:

    [user@server db_load]$ php createExtraGroups.php <extra_groups.tsv>

### Gene Ontology

Download the latest [Gene Ontology](http://geneontology.org/docs/download-ontology/) in the OBO format (`go.obo` file) and run the following command:

    [user@server db_load]$ php GeneOntology.php go.obo

This will upload the three gene ontologies (molecular function, biological process, and cellular component) into the database.

Afterwards, a Tab-Separated-file containing the association pairs between GO Terms and ORF/Genes can be loaded into the database by running the following command:

    [user@server db_load]$ php insertGO.php <GO_term-ORF.tsv> <species> <strain>

In this tutorial, we do not cover how to obtain the information regarding these association pairs, since for new species, this information might not yet be known.

### Database Information

After the instalation, to obtain some statistics regarding the information (# of genes, # of TFs, # of regulations, ...) stored in the database, one can run the following command:

    [user@server db_load]$ php Stats.php


## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.


[GenBank Flat File]: https://www.ncbi.nlm.nih.gov/datasets/docs/about-ncbi-gbff/

[ncbi]: <ftp://ftp.ncbi.nlm.nih.gov/blast/executables/LATEST/>

