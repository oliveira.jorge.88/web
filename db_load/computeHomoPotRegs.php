<?php
class homoPotRegs {
        private $_dbAccess;
        private $_path;

        function __construct($path = "../conf/") {
                $this->_path = $path;
                include_once("IDBAccess.php");
                $this->_dbAccess = new IDBAccess($path);
                $this->_dbAccess->openConnection('r');
        }

	private function dbclean($str) {
                return addslashes(trim($str));
	}

	function expandRegExp($seq) { // to expand motifs to be used in sequence alignments
                $retObj = [];

                if (strpos($seq, ",") !== false) {
                        $expanded = [];
                        preg_match_all('/{(\d*),(\d*)}/', $seq, $coord);
                        $seqs = preg_split('/[N]{(\d*),(\d*)}/', $seq);
                        $num_seqs = ($coord[2][0]-$coord[1][0]+1);
                        $n = str_repeat("N",$coord[1][0]);
                        for ($j=0; $j<$num_seqs;$j++){
                                array_push($expanded,$seqs[0].$n.$seqs[1]);
                                $n .= "N";
                        }

                        foreach($expanded as $seq){
                                $retStr = "";
                                for ($i=0, $last=0; $i<strlen($seq); $i++) {
                                        if ($seq{$i} == '{') {
                                                $i++;
                                                $n = "";
                                                while ($seq{$i} != '}') {
                                                        $n = $n . $seq{$i};
                                                        $i++;
                                                }
                                                for ($j=1; $j < $n; $j++) {
                                                        $retStr .= $seq{$last};
                                                }
                                        }
                                        else {
                                                $retStr .= $seq{$i};
                                                $last = $i;
                                        }
                                }
                                array_push($retObj, $retStr);
                        }
                }
                else {

                        $retStr = "";

                        for ($i=0, $last=0; $i<strlen($seq); $i++) {
                                if ($seq{$i} == '{') {
                                        $i++;
                                        $n = "";
                                        while ($seq{$i} != '}') {
                                                $n = $n . $seq{$i};
                                                $i++;
                                        }
                                        for ($j=1; $j < $n; $j++) {
                                                $retStr .= $seq{$last};
                                        }
                                } else {
                                        $retStr .= $seq{$i};
                                        $last = $i;
                                }
                        }
                        array_push($retObj, $retStr);
                }
                return $retObj;
        }

	function compute($path){

	// get all species in database
	$q  = "select distinct species from orfgene";
	if ($this->_dbAccess->query("species", $q)) {
		while ($row = $this->_dbAccess->nextObject("species")) {
			$dbSpecies[] = $row['species'];
		}
	}

	foreach ($dbSpecies as $species){
		$homolog = [];
		print "\nProcessing $species";

		// get all homologs of current species
		$q  = "select distinct species from orfgene where orfid in (select ";
                $q .= "distinct orfiddest from orthologs where orfidsrc in (select ";
		$q .= "distinct orfid from orfgene where species='".addslashes($species)."'))";
                if ($this->_dbAccess->query("homologs", $q)) {
                        while ($row = $this->_dbAccess->nextObject("homologs")) {
                                if (strpos($species,addslashes($row['species']))!==false) continue;
                                $homolog[] = $row['species'];
                        }
                }
                $this->_dbAccess->freeResult("homologs");

		$synteny = 0;

		// get the motifs (transcription factors sequences) for the current species 
		
                $q  = "select distinct P.tfid, P.protein, C.IUPACseq from protein as P, ";
                $q .= "consensus as C, tfconsensus as TFC, orfgene as O where ";
                $q .= "C.consensusid=TFC.consensusid and TFC.tfid=P.tfid and ";
                $q .= "P.tfid=O.orfid and O.species='".addslashes($species)."'";

		$aMotifs = [];
		if ($this->_dbAccess->query("consensus", $q)) {
                        while ($row = $this->_dbAccess->nextObject("consensus")) {
                                $tfid = $row['tfid'];
                                $p = $row['protein'];
                                $c = $row['IUPACseq'];
                                $aMotifs[$tfid][$p][] = $c;
                        }
		}
		$this->_dbAccess->freeResult("consensus");
		if (empty($aMotifs)) {
			print "\nNo Motifs for $species, skipping...";
			continue;
		}

		$tfid2name = [];
		foreach (array_keys($aMotifs) as $tfid){
		        foreach(array_keys($aMotifs[$tfid]) as $tf){
                		$tfid2name[$tfid] = $tf;
		        }
		}

		// get info for all the ORFS of current species

		$q = "select orfid,orf,species from orfgene where species='".addslashes($species)."'";
		$aInfo = [];
                if ($this->_dbAccess->query("infoorfgene", $q)) {
	                while ($row = $this->_dbAccess->nextObject("infoorfgene")) {
				$aInfo[$row['orfid']]['orf']     = $row['orf'];
				$aInfo[$row['orfid']]['species'] = $row['species'];
			}
		}

		$aSpecies = array_merge($homolog, [$species]);

		// CREATE AN ARRAY CONTAINING ALL THE UPSTREAM SEQS 
		$homoORFs   = [];
		$aPromoters = [];
		$aHomoORFids = [];
		$homo2orf = [];
		$curr2homo = [];
		foreach ($aSpecies as $toSpecies){ 			

                	$orflist = implode(",",array_keys($aInfo));
			$homoORFs[$toSpecies] = [];
			$q  = "select O.orfid,O.orf,O.gene,P.protein, H.orfiddest, ";
			$q .= "Homo.orf as homoorf,Homo.gene as homogene from orfgene as O, ";
			$q .= "protein as P, orthologs as H left join orfgene as Homo on ";
			$q .= "H.orfiddest=Homo.orfid where O.orfid in ($orflist) ";
			$q .= "and P.tfid=O.orfid ";
			$q .= " and H.classif=$synteny ";
			$q .= "and O.orfid=H.orfidsrc and H.orfiddest in (select distinct ";
			$q .= "orfid from orfgene where species in ('".addslashes($toSpecies)."') )";
			if ($this->_dbAccess->query("homologous", $q)) {
				while ($row = $this->_dbAccess->nextObject("homologous")) {
                	                $homoORFs[$toSpecies][$row['orf']]['orfid']           = $row['orfid'];
                        	        $homoORFs[$toSpecies][$row['orf']]['homo']['orfid'][] = $row['orfiddest'];
        	                }
                	}
	                $this->_dbAccess->freeResult("homologous");

			foreach (array_keys($homoORFs[$toSpecies]) as $orf){ 
				foreach ($homoORFs[$toSpecies][$orf]['homo']['orfid'] as $k => $homoorfid) {
					$orfid = $homoORFs[$toSpecies][$orf]['orfid'];
					$aHomoORFids[] = $homoorfid;
					$curr2homo[$orfid][] = $homoorfid;
					$homo2orf[$homoorfid][] = $orfid;
				}
			}
		}

		$homoInfo = [];
                $q  = "select O.orfid,O.orf,O.gene,O.species,P.protein from orfgene as O, ";
                $q .= "protein as P where P.tfid=O.orfid and O.orfid in (";
                $q .= join(",", $aHomoORFids) . ")";
                if ($this->_dbAccess->query("ids2info", $q)) {
                        while ($row = $this->_dbAccess->nextObject("ids2info")) {
                                $homoInfo[$row['orfid']]['species'] = $row['species'];
                        }
                }
                $this->_dbAccess->freeResult("ids2info");

		$aORFids = array_merge($aHomoORFids,array_keys($aInfo));

                $q  = "select orfid,promoterseq from orfgene where orfid in (".join(",", $aORFids) . ")";
		if ($this->_dbAccess->query("prom", $q)) {
                        while ($row = $this->_dbAccess->nextObject("prom")) {
                                $lines = explode("\n", $row['promoterseq']);
                                array_shift($lines);
                                $seq = join($lines);
                                $seq = substr($seq, 0, 1000);
                                $aPromoters[$row['orfid']] = $seq;
                        }
                }

		$aInfo = $aInfo + $homoInfo;

		// CALCULATE THE POTENTIAL MATCHES 

		$nSubst	= 0; // number of allowed substitutions. we suggest to keep it 0 (zero), otherwise bad things might happen
		include_once('../service/ShiftAnd.php');
	        include_once('../service/IUPAC.php');
         	$fwr = array();
            	$rev = array();

            	foreach(array_keys($aMotifs) as $tfid){
                    foreach (array_keys($aMotifs[$tfid]) as $protein) {
                            foreach($aMotifs[$tfid][$protein] as $key=>$m){
                                $aMotif_exp = $this->expandRegExp($m);
                                    foreach ($aMotif_exp as $m_exp) {
                                            $m_exp = str_ireplace("X","N",$m_exp);
                                            $shiftAndFwr = new ShiftAnd($m_exp);
                                            $shiftAndRev = new ShiftAnd(strrev(IUPAC::complement($m_exp)));

                                            foreach ($aPromoters as $orf => $s) {

                                                // Forward Strand
                                                $pos = $shiftAndFwr->find($s, $nSubst);
                                                if (count($pos)>0) {
                                                        $fwr[$orf][$tfid] = $pos;
                                                }
                                                // Reverse Strand
                                                $pos = $shiftAndRev->find($s, $nSubst);
                                                if (count($pos)>0) {
                                                        $rev[$orf][$tfid] = $pos;
                                                }
                                             }
                                     }
                            }
                    }
            	}

		if (empty($fwd) && empty($rev)) continue;

        	$fname = $path."/".$species.".csv";
	        $fh = fopen($fname, "w") or die("\nUnable to open file  $fname !");
		$head = "#tfid\ttargetid\ttfspecies\ttargetspecies\n";
                fwrite($fh, $head);

		if (!empty($fwd)){
			foreach (array_keys($fwd) as $orfid){
				$homoSpecies = $aInfo[$orfid]['species'];
				foreach(array_keys($fwd[$orfid]) as $tfid){ 
					$csv = "$tfid\t$orfid\t$species\t$homoSpecies\n";
					fwrite($fh, $csv);
				}
			}
		}
		if (!empty($rev)){
			foreach(array_keys($rev) as $orfid){ 
				$homoSpecies = $aInfo[$orfid]['species'];
				foreach (array_keys($rev[$orfid]) as $tfid){
					$csv = "$tfid\t$orfid\t$species\t$homoSpecies\n";
					fwrite($fh, $csv);
				}
			}
		}
		fclose($fh);

	}

	return true;

	}

}

$path = "homo_pot_regs";
if (!file_exists($path)) {
    mkdir($path,0777, true);
}

$cod = new homoPotRegs();
$done = $cod->compute($path);

?>
