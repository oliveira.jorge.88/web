-- MySQL dump 10.17  Distrib 10.3.17-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: multiyeast
-- ------------------------------------------------------
-- Server version	10.3.17-MariaDB

--
-- Table structure for table `description`
--

DROP TABLE IF EXISTS `description`;
CREATE TABLE `description` (
  `descriptionid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(512) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`descriptionid`),
  UNIQUE KEY `description_uk_description` (`description`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `description`
--

LOCK TABLES `description` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `orfgene`
--

DROP TABLE IF EXISTS `orfgene`;
CREATE TABLE `orfgene` (
  `orfid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gdid` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `orf` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `gene` varchar(20) COLLATE latin1_general_ci DEFAULT 'Uncharacterized',
  `descriptionid` int(10) unsigned DEFAULT NULL,
  `species` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `geneseq` text COLLATE latin1_general_ci DEFAULT NULL,
  `promoterseq` text COLLATE latin1_general_ci DEFAULT NULL,
  `coords` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`orfid`),
  UNIQUE KEY `orfgene_uk_orf` (`orf`),
  UNIQUE KEY `orfgene_uk_gdid` (`gdid`),
  KEY `orfgene_fk_descriptionid` (`descriptionid`),
  KEY `species` (`species`),
  KEY `gene` (`gene`),
  CONSTRAINT `orfgene_fk_descriptionid` FOREIGN KEY (`descriptionid`) REFERENCES `description` (`descriptionid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `orfgene`
--

LOCK TABLES `orfgene` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `alias`
--

DROP TABLE IF EXISTS `alias`;
CREATE TABLE `alias` (
  `orfid` int(10) unsigned NOT NULL,
  `alias` varchar(32) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`orfid`,`alias`),
  CONSTRAINT `alias_fk_orfid` FOREIGN KEY (`orfid`) REFERENCES `orfgene` (`orfid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `alias`
--

LOCK TABLES `alias` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `consensus`
--

DROP TABLE IF EXISTS `consensus`;
CREATE TABLE `consensus` (
  `consensusid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IUPACseq` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`consensusid`),
  UNIQUE KEY `consensus_uk_IUPACseq` (`IUPACseq`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `consensus`
--

LOCK TABLES `consensus` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `envconditiongroups`
--

DROP TABLE IF EXISTS `envconditiongroups`;
CREATE TABLE `envconditiongroups` (
  `groupid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `biggroup` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `subgroup` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`groupid`),
  UNIQUE KEY `envconditiongroups_uk_subgroup` (`subgroup`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `envconditiongroups`
--

LOCK TABLES `envconditiongroups` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `envcondition`
--

DROP TABLE IF EXISTS `envcondition`;
CREATE TABLE `envcondition` (
  `envconditionid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `envconditiondesc` text COLLATE latin1_general_ci NOT NULL,
  `groupid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`envconditionid`,`groupid`),
  KEY `envcondition_fk_groupid` (`groupid`),
  CONSTRAINT `envcondition_fk_groupid` FOREIGN KEY (`groupid`) REFERENCES `envconditiongroups` (`groupid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `envcondition`
--

LOCK TABLES `envcondition` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `evidencecode`
--

DROP TABLE IF EXISTS `evidencecode`;
CREATE TABLE `evidencecode` (
  `evidencecodeid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` enum('Direct','Indirect','Prediction','N/A') COLLATE latin1_general_ci NOT NULL,
  `experiment` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`evidencecodeid`),
  UNIQUE KEY `evidencecode_uk_code_experiment` (`code`,`experiment`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `evidencecode`
--

LOCK TABLES `evidencecode` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `geneontology`
--

DROP TABLE IF EXISTS `geneontology`;
CREATE TABLE `geneontology` (
  `goid` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `term` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `onto` enum('process','function','component') COLLATE latin1_general_ci NOT NULL,
  `depth` int(11) DEFAULT 0,
  PRIMARY KEY (`goid`),
  UNIQUE KEY `geneontology_uk_term` (`term`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `geneontology`
--

LOCK TABLES `geneontology` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `goorflist`
--

DROP TABLE IF EXISTS `goorflist`;
CREATE TABLE `goorflist` (
  `goid` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `orfid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`goid`,`orfid`),
  KEY `goorflist_ik_orfid` (`orfid`),
  CONSTRAINT `goorflist_fk_goid` FOREIGN KEY (`goid`) REFERENCES `geneontology` (`goid`),
  CONSTRAINT `goorflist_fk_orfid` FOREIGN KEY (`orfid`) REFERENCES `orfgene` (`orfid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `goorflist`
--

LOCK TABLES `goorflist` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `goparents`
--

DROP TABLE IF EXISTS `goparents`;
CREATE TABLE `goparents` (
  `goid` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `goidson` varchar(15) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`goid`,`goidson`),
  KEY `goparents_ik_goidson` (`goidson`),
  CONSTRAINT `goparents_fk_goid` FOREIGN KEY (`goid`) REFERENCES `geneontology` (`goid`),
  CONSTRAINT `goparents_fk_goidson` FOREIGN KEY (`goidson`) REFERENCES `geneontology` (`goid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `goparents`
--

LOCK TABLES `goparents` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `orthologs`
--

DROP TABLE IF EXISTS `orthologs`;
CREATE TABLE `orthologs` (
  `orfidsrc` int(10) unsigned NOT NULL,
  `orfiddest` int(10) unsigned NOT NULL,
  `classif` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`orfidsrc`,`orfiddest`,`classif`),
  KEY `orthologs_fk_orfiddest` (`orfiddest`),
  CONSTRAINT `orthologs_fk_orfiddest` FOREIGN KEY (`orfiddest`) REFERENCES `orfgene` (`orfid`),
  CONSTRAINT `orthologs_fk_orfidsrc` FOREIGN KEY (`orfidsrc`) REFERENCES `orfgene` (`orfid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `orthologs`
--

LOCK TABLES `orthologs` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `protein`
--

DROP TABLE IF EXISTS `protein`;
CREATE TABLE `protein` (
  `tfid` int(10) unsigned NOT NULL,
  `protein` varchar(21) COLLATE latin1_general_ci NOT NULL,
  `aminoacidseq` text COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`tfid`,`protein`),
  CONSTRAINT `protein_fk_tfid` FOREIGN KEY (`tfid`) REFERENCES `orfgene` (`orfid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `protein`
--

LOCK TABLES `protein` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `homopotreg`
--

DROP TABLE IF EXISTS `homopotreg`;
CREATE TABLE `homopotreg` (
  `tfid` int(10) unsigned NOT NULL,
  `targetid` int(10) unsigned NOT NULL,
  `tfspecies` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `homospecies` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`tfid`,`targetid`),
  KEY `homopotreg_fk_targetid` (`targetid`),
  KEY `homopotreg_fk_tfspecies` (`tfspecies`),
  KEY `homopotreg_fk_homospecies` (`homospecies`),
  CONSTRAINT `homopotreg_fk_homospecies` FOREIGN KEY (`homospecies`) REFERENCES `orfgene` (`species`),
  CONSTRAINT `homopotreg_fk_targetid` FOREIGN KEY (`targetid`) REFERENCES `orfgene` (`orfid`),
  CONSTRAINT `homopotreg_fk_tfid` FOREIGN KEY (`tfid`) REFERENCES `protein` (`tfid`),
  CONSTRAINT `homopotreg_fk_tfspecies` FOREIGN KEY (`tfspecies`) REFERENCES `orfgene` (`species`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `homopotreg`
--

LOCK TABLES `homopotreg` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `pubmed`
--

DROP TABLE IF EXISTS `pubmed`;
CREATE TABLE `pubmed` (
  `pubmedid` int(10) unsigned NOT NULL,
  `journal` text COLLATE latin1_general_ci NOT NULL,
  `title` text COLLATE latin1_general_ci NOT NULL,
  `authors` text COLLATE latin1_general_ci NOT NULL,
  `date` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `pages` varchar(15) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `volume` int(11) DEFAULT NULL,
  `issue` int(11) DEFAULT NULL,
  PRIMARY KEY (`pubmedid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `pubmed`
--

LOCK TABLES `pubmed` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `regulation`
--

DROP TABLE IF EXISTS `regulation`;
CREATE TABLE `regulation` (
  `regulationid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tfid` int(10) unsigned NOT NULL,
  `targetid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`regulationid`),
  UNIQUE KEY `regulation_uk_tfid_targetid` (`tfid`,`targetid`),
  KEY `regulation_fk_targetid` (`targetid`),
  CONSTRAINT `regulation_fk_targetid` FOREIGN KEY (`targetid`) REFERENCES `orfgene` (`orfid`),
  CONSTRAINT `regulation_fk_tfid` FOREIGN KEY (`tfid`) REFERENCES `protein` (`tfid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `regulation`
--

LOCK TABLES `regulation` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `regulationdata`
--

DROP TABLE IF EXISTS `regulationdata`;
CREATE TABLE `regulationdata` (
  `regulationid` int(10) unsigned NOT NULL,
  `pubmedid` int(10) unsigned NOT NULL,
  `evidencecodeid` int(10) unsigned NOT NULL,
  `envconditionid` int(10) unsigned NOT NULL DEFAULT 0,
  `association` enum('Positive','Negative','Positive/Negative','N/A') COLLATE latin1_general_ci NOT NULL DEFAULT 'N/A',
  `strain` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`regulationid`,`pubmedid`,`evidencecodeid`,`envconditionid`),
  KEY `regulationdata_fk_pubmedid` (`pubmedid`),
  KEY `regulationdata_fk_evidencecodeid` (`evidencecodeid`),
  KEY `regulationdata_fk_envconditionid` (`envconditionid`),
  CONSTRAINT `regulationdata_fk_envconditionid` FOREIGN KEY (`envconditionid`) REFERENCES `envcondition` (`envconditionid`),
  CONSTRAINT `regulationdata_fk_evidencecodeid` FOREIGN KEY (`evidencecodeid`) REFERENCES `evidencecode` (`evidencecodeid`),
  CONSTRAINT `regulationdata_fk_pubmedid` FOREIGN KEY (`pubmedid`) REFERENCES `pubmed` (`pubmedid`),
  CONSTRAINT `regulationdata_fk_regulationid` FOREIGN KEY (`regulationid`) REFERENCES `regulation` (`regulationid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `regulationdata`
--

LOCK TABLES `regulationdata` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `tfconsensus`
--

DROP TABLE IF EXISTS `tfconsensus`;
CREATE TABLE `tfconsensus` (
  `tfconsensusid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tfid` int(10) unsigned NOT NULL,
  `consensusid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`tfconsensusid`),
  UNIQUE KEY `tfconsensus_uk_tfconsensusid` (`tfid`,`consensusid`),
  KEY `tfconsensus_fk_consensusid` (`consensusid`),
  CONSTRAINT `tfconsensus_fk_consensusid` FOREIGN KEY (`consensusid`) REFERENCES `consensus` (`consensusid`),
  CONSTRAINT `tfconsensus_fk_tfid` FOREIGN KEY (`tfid`) REFERENCES `protein` (`tfid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tfconsensus`
--

LOCK TABLES `tfconsensus` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `consensusdata`
--

DROP TABLE IF EXISTS `consensusdata`;
CREATE TABLE `consensusdata` (
  `tfconsensusid` int(10) unsigned NOT NULL,
  `pubmedid` int(10) unsigned NOT NULL,
  `evidencecodeid` int(10) unsigned NOT NULL,
  `envconditionid` int(10) unsigned DEFAULT NULL,
  `strain` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`tfconsensusid`,`pubmedid`,`evidencecodeid`),
  KEY `consensusdata_fk_pubmedid` (`pubmedid`),
  KEY `consensusdata_fk_evidencecodeid` (`evidencecodeid`),
  KEY `consensusdata_fk_envconditionid` (`envconditionid`),
  CONSTRAINT `consensusdata_fk_envconditionid` FOREIGN KEY (`envconditionid`) REFERENCES `envcondition` (`envconditionid`),
  CONSTRAINT `consensusdata_fk_evidencecodeid` FOREIGN KEY (`evidencecodeid`) REFERENCES `evidencecode` (`evidencecodeid`),
  CONSTRAINT `consensusdata_fk_pubmedid` FOREIGN KEY (`pubmedid`) REFERENCES `pubmed` (`pubmedid`),
  CONSTRAINT `consensusdata_fk_tfconsensusid` FOREIGN KEY (`tfconsensusid`) REFERENCES `tfconsensus` (`tfconsensusid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `consensusdata`
--

LOCK TABLES `consensusdata` WRITE;
UNLOCK TABLES;


-- Dump completed on 2019-11-19 14:10:25
