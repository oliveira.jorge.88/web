<?php
class LoadGBFF {
	private $_dbAccess;
	private $_path;
	private $_refSpecies;
	private $_refStrain;
	private $_species;
	private $_strain;
	private $_chr;

	function __construct($species, $strain, $path = "../conf/") {
		$this->_refSpecies = $species;
		$this->_refStrain = $strain;
		$this->_path = $path;
		include_once("IDBAccess.php");
		$this->_dbAccess = new IDBAccess($path);
		$this->_dbAccess->openConnection('w');
	}

	private function checkDBError() {
		if ($this->_dbAccess->hasError()) {
			echo $this->_dbAccess->error() . "\n";
		}
	}

	private function advance($fh, $str) {
	    while(!feof($fh)) {
        	$line = fgets($fh);
		if (strpos($line, $str) !== false) 	return $line;
        }
	}
	
	private function getValue($fh, $line, $sep) {
		$seq = trim(substr($line, strpos($line, '"')+1));
		while (strpos($seq, '"') === false) {
			$line = fgets($fh);
			$seq = $seq . $sep . trim($line);
		}
		return substr($seq, 0, strpos($seq, '"'));
	}

	private function insertDBInfo($orf, $gene, $coords, $gdid, $prot, $desc, $seq) {
		#print "\n[$orf]\n[$gene]\n[$coords]\n[$prot]\n[$desc]\n[$seq]\n";
		$orf = addslashes($orf);
		$gene = addslashes($gene);
		$coords = addslashes($coords);
		$gdid = addslashes($gdid);
		$prot = addslashes($prot);
		$desc = addslashes($desc);
		$seq = addslashes($seq);
		//
		$q  = "insert into description (description) values ('$desc')";
		$this->_dbAccess->insertUpdate($q);
		$q = "select descriptionid from description where description='$desc'";
		$descid = $this->_dbAccess->getObject("descriptionid", $q);
		// 
		$q  = "insert into orfgene (orf,gene,gdid,species,descriptionid,coords) values ('";
		$q .= "$orf','$gene','$gdid','$this->_species $this->_strain','$descid','$coords')";
		$this->_dbAccess->insertUpdate($q);
		//
		$q = "select orfid from orfgene where orf='$orf' and species='$this->_species $this->_strain'";
		$orfid = $this->_dbAccess->getObject("orfid", $q);
		$this->insertProtSeq($orfid, $orf, $prot, $seq);
	}
	private function insertProtSeq($orfid, $orf, $prot, $str) {
		$seq = ">$orf $prot\n" . implode("\n", str_split($str, 60));
		$q  = "insert into protein (tfid, protein, aminoacidseq) values ";
		$q .= "('$orfid','$prot','$seq')";
		$this->_dbAccess->insertUpdate($q);
	}
	private function insertSeqDB($o, $seqG, $seqP) {
		$seqG = ">$o	size ".strlen($seqG)."\n"
				. wordwrap(strtoupper($seqG), 60, "\n", true);
		$seqP = ">$o	upstream sequence, from -1000 to -1, size 1000\n"
				. wordwrap(strtoupper($seqP), 60, "\n", true);
		$q  = "update orfgene set geneseq='$seqG',promoterseq='$seqP' where ";
		$q .= "species='$this->_species $this->_strain' and orf='$o'";
		$this->_dbAccess->insertUpdate($q);
	}

	/**
	 * From the GBFF line with the DEFINITION keyword extracts the chromosome
	 */
	private function getChr($line) {
		$p = strpos($line, "chromosome");
		if ($p > 0) { $chr = substr($line, $p); }
		else {
			$p = strpos($line, "clone");
			if ($p > 0) { $chr = substr($line, $p); }
			else {
				$p = strpos($line, "scf");
				if ($p > 0) { $chr = substr($line, $p); }
				else {
					$p = strpos($line, "mitochondrial");
					if ($p > 0) { $chr = substr($line, $p); }
					else {
						$p = strpos($line, "scaffold");
        	                                if ($p > 0) { $chr = substr($line, $p); }
                	                        else {
							print "Could not extract chromosome information from:\n$line\nExiting...";
							exit;
						}
					}
				}
			}
		}
		$p = strpos($chr, "complete");
		if ($p > 0) { $chr = trim(substr($chr, 0, $p), " ,"); }
		$p = strpos($chr, "whole");
		if ($p > 0) { $chr = trim(substr($chr, 0, $p), " ,"); }
		$p = strpos($chr, "sequence");
		if ($p > 0) { $chr = trim(substr($chr, 0, $p), " ,"); }
		$p = strpos($chr, "genomic");
		if ($p > 0) { $chr = trim(substr($chr, 0, $p), " ,"); }
		return rtrim($chr, " .");
	}

	function parse($file, $species, $strain) {
		if (!file_exists($file)) {
			return false;
		}
		$this->_species = addslashes($species);
		$this->_strain = addslashes($strain);

		if ($fh = fopen($file, "r")) {
			while (!feof($fh)) {
				$orf2coords = [];
				$line = trim(substr($this->advance($fh, 'DEFINITION'), 12));
				if (empty($line)) break;
				if (substr($line, -1) != ".") { $line = trim($line)." ".trim(fgets($fh)); }
				$this->_chr = $this->getChr($line);

				print "[" . $this->_species . "]\t[" . $this->_strain . "]\t[" . $this->_chr. "]\n";
				$line = $this->advance($fh, 'CDS');
				while (!feof($fh)) {
					$line = trim($line);
					if (substr($line, 0,3) === "CDS") {
						// Clean previous record
						unset($coords);unset($orf);unset($gene);unset($gdid);unset($desc);unset($prot);unset($protseq);unset($note);
						$coords = "$this->_chr from " . trim(str_replace(array(" ","C","D","S"), "", $line));
						while (substr($coords, -1) == ",") { // multi-line
							$coords .= trim(fgets($fh));
						}
					} elseif (substr($line, 0, 10) === "/locus_tag") {
						$orf = explode('"', $line)[1];
						$orf2coords[$orf] = $coords;
					} elseif (substr($line, 0, 5) === "/note") {
						$note = $this->getValue($fh, $line, " ");
						#print "\n note is: $note";
					} 
					elseif (substr($line, 0, 8) === "/product") {
						$desc = $this->getValue($fh, $line, " ");
						if (isset($note)) {
							if ($desc==$orf) { $desc=$note; }
							elseif (strpos($note, $desc)>-1) { $desc = $note; }
							elseif (strpos($desc, $note)>-1){ // do nothing 
							}
							else { $desc = "$desc ($note)"; }
						}
					} 
					elseif (substr($line, 0, 11) === "/protein_id") {
                                                $gdid = explode('"', $line)[1];
                                                $prot = $gdid;
                                        }
					elseif (substr($line, 0, 12) === "/translation") {
						$seq = $this->getValue($fh, $line, "");
						// insert DB
						if (!isset($gene)) $gene = "Uncharacterized";
						$this->insertDBInfo($orf, $gene, $coords, $gdid, $prot, $desc, $seq);
					} elseif (substr($line, 0, 6) === "ORIGIN") {
						// 1st load the whole Chr sequence
						$chrSeq = "";
						$line = fgets($fh);
						while (!feof($fh) && $line != "//\n") {
							$blop = trim(str_replace(" ", "", substr($line, 10)));
							$chrSeq .= $blop;
							$line = fgets($fh);
						}
						#print "\nChr $this->_chr size: " . strlen($chrSeq);
						// 2nd parse it gene by gene
						foreach ($orf2coords as $o => $c) {
							$this->insertDBSeqs($chrSeq, $o, $c);
						}
						break;
					}
					$line = fgets($fh);
				}
			}
		}
		fclose($fh);
	}
	private function complement($seq) {
		for ($i = 0; $i < strlen($seq); $i++) {
			switch($seq{$i}) {
			case 'a': $seq{$i} = 't'; break;
			case 't': $seq{$i} = 'a'; break;
			case 'g': $seq{$i} = 'c'; break;
			case 'c': $seq{$i} = 'g'; break;
			# Stay the same :)
			#case 'w': $seq{$i} = 'w'; break;
			#case 's': $seq{$i} = 's'; break;
			case 'r': $seq{$i} = 'y'; break;
			case 'y': $seq{$i} = 'r'; break;
			case 'm': $seq{$i} = 'k'; break;
			case 'k': $seq{$i} = 'm'; break;
			case 'd': $seq{$i} = 'h'; break;
			case 'h': $seq{$i} = 'd'; break;
			case 'b': $seq{$i} = 'v'; break;
			}
		}
		$seq = strrev($seq);
		return $seq;
	}

	// Position
	private function getSeqGPos($chrSeq, $c) {
		$pos = explode("..", $c);
		if (count($pos) < 2) {
			# Case: https://www.ncbi.nlm.nih.gov/gene/2897329
			return "";
		}
		return substr($chrSeq, $pos[0]-1, $pos[1] - $pos[0]+1);
	}
	private function getSeqPPos($chrSeq, $c) {
		$pos = explode("..", $c);
		#if (!is_numeric($pos[0])) { print "getSeqPPos: $c\n"; }
		if ($pos[0] < 1001) {
			$i = 0;
			$l = $pos[0]-1;
		} else {
			$l = 1000;
			$i = $pos[0]-1001;
		}
		return substr($chrSeq, $i, $l);
	}
	// Join
	private function getSeqGJoin($chrSeq, $c) {
		$c = rtrim(substr($c, strpos($c, "(")+1), ")");
		$aPos = explode(",", $c);
		$seq = "";
		foreach ($aPos as $pos) {
			$seq .= $this->getSeqGPos($chrSeq, $pos);
		}
		return $seq;
	}
	private function getSeqPJoin($chrSeq, $c) {
		$c = substr(substr($c, strpos($c, "(")+1), 0, -1);
		$pos = preg_split("/(\.\.|,)/",$c);
		return $pos[0]."..".$pos[count($pos) - 1];
	}
	// Complement
	private function getSeqGComp($chrSeq, $c) {
		$c = substr(substr($c, strpos($c, "(")+1), 0, -1);
		if (strpos($c, "join") !== false) {
			$seq = $this->getSeqGJoin($chrSeq, $c);
		} else {
			$seq = $this->getSeqGPos($chrSeq, $c);
		}
		return $this->complement($seq);
	}
	private function getSeqPComp($chrSeq, $c) {
		$c = substr(substr($c, strpos($c, "(")+1), 0, -1);
		if (strpos($c, "join") !== false) {
			$c = $this->getSeqPJoin($chrSeq, $c);
		}
		$pos = explode("..", $c);
		return $this->complement(substr($chrSeq, $pos[1], 1000));
	}
	private function insertDBSeqs($chrSeq, $o, $c) {
		#print "\nOrig: $c\n";
		$c = substr($c, strpos($c, "from")+5);
		$c = str_replace(array("<",">"), "", $c);
		// Gene sequence
		$cG = $c;
		if (strpos($cG, "complement") !== false) {
			$seqG = $this->getSeqGComp($chrSeq, $cG);
		} elseif (strpos($cG, "join") !== false) {
			$seqG = $this->getSeqGJoin($chrSeq, $cG);
		} else {
			$seqG = $this->getSeqGPos($chrSeq, $cG);
		}
		#print "LenG: ".strlen($seqG)."\n";
		#print "SeqG: $seqG\n";
		// Promoter sequence
		$cP = $c;
		if (strpos($cP, "complement") !== false) {
			$seqP = $this->getSeqPComp($chrSeq, $cP);
		} elseif (strpos($cP, "join") !== false) {
			$seqP = $this->getSeqPPos($chrSeq, 
				$this->getSeqPJoin($chrSeq, $cP));
		} else {
			$seqP = $this->getSeqPPos($chrSeq, $cP);
		}
		#print "LenP: ".strlen($seqP)."\n";
		#print "SeqG: $seqP\n";
		$this->insertSeqDB($o,$seqG,$seqP);
	}
}

if (count($argv)!=4) { 
	die("\nUsage: php LoadGBFF.php <file.gbff> <species> <strain>\n\n");
}
else {
	$gbff = new LoadGBFF($argv[2], $argv[3]);

	print "Parsing genomic gbff file $argv[1] for $argv[2] $argv[3]  ...\n";

}

$gbff->parse($argv[1], $argv[2], $argv[3]);
print "\nDone!";

?>
