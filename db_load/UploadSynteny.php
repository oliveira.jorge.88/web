<?php
class Orthologs {
	private $_dbAccess;
	private $_path;

	function __construct($path = "../conf/") {
		$this->_path = $path;
		include_once("IDBAccess.php");
		$this->_dbAccess = new IDBAccess($path);
		$this->_dbAccess->openConnection('w');
	}

	private function dbclean($str) {
		return addslashes(trim($str));
	}

	private function getOrfID($orf) {
		$o = $this->dbclean($orf);
		$q = "select orfid from orfgene where orf='$o'";
		$orfid = $this->_dbAccess->getObject("orfid", $q);
		if (!$orfid) {
			$q  = "select O.orfid from orfgene as O, alias as A where A.alias='$o' ";
		  $q .= "and A.orfid=O.orfid";
			$orfid = $this->_dbAccess->getObject("orfid", $q);
		}
		return $orfid;
	}

	private function insertOrt($orfS1, $orfS2, $class) {
		$orfidS1 = $this->getOrfID($orfS1);
		if (!$orfidS1) {
			print "\n[orfS1]\t$orfS1\t$orfS2";
			return;
		}
		$orfidS2 = $this->getOrfID($orfS2);
		if (!$orfidS2) {
			print "\n[orfS2]\t$orfS1\t$orfS2";
			return;
		}
		// insert both senses
		$q = "insert into orthologs values ($orfidS1,$orfidS2,$class)";
		$this->_dbAccess->insertUpdate($q);
	}

	function insertPillars($folders) {

		foreach ($folders as $folder){

			$aux=explode("p",$folder);
			$class = $aux[1];	

			print "\nStarting processing folder $folder with class $class";

			$dh = opendir($folder."/") or die("Unable to open dir $folder!");

			$files = glob($folder.'/*-pillars.tab', GLOB_BRACE);

			$orgMap = [];

			foreach ($files as $file){
				$aux = explode("/",$file);
                                $shortname = explode("-pillars.tab",end($aux));
                                $org = $shortname[0];

				$fh = fopen($file, "r") or die("Unable to open file $file!");

				$linecount = 0;
				while (!feof($fh)){
					$line = trim(fgets($fh));
					if ($linecount==0){ #first line contains the species order in the pillars 
						$orgMap = explode("\t",$line); 
						$orgIndex = array_search($org,$orgMap);
						print "\nStarting processing file $file with organism $org and index $orgIndex";
						$linecount++;
						continue;
					}
					if ($line==='#') continue;
					$orfs=explode("\t",$line);
					$main = $orfs[$orgIndex];
					foreach($orfs as $orf){						
						if (($orf =='---') or ($orf==$main)) continue;
						$aHomo = explode(",",$orf);
						foreach ($aHomo as $homo){
							$this->insertOrt($main,$homo,$class);
						}
					}
					$linecount++;
				}
			}
		}
		return true;
	}
}

$cod = new Orthologs();

$dh = opendir("synteny/") or die("Unable to open synteny dir!");

$folders = glob('synteny/*', GLOB_BRACE);
closedir($dh);

if ($cod->insertPillars($folders)) {
	print "\nDone!\n";
} else {
	print "\nCould not process!\n";
}
 
?>
