<?php
class Stats {
  private $_dbAccess;
  private $_path;
	private $_species;

  function __construct($path = "../conf/") {
    $this->_path = $path;
    include_once("IDBAccess.php");
    $this->_dbAccess = new IDBAccess($path);
    $this->_dbAccess->openConnection('r');
  }

	# select species,count(orfid) as nOrfs from orfgene group by species

	function computeStats() {

		$aSpecies = [];
		$q = "select distinct species from orfgene";
		if ($this->_dbAccess->query("sp", $q)) {	
			while ($row = $this->_dbAccess->nextObject("sp")){
				$aSpecies[] = addslashes($row['species']);
			}
		}

		foreach ($aSpecies as $s) {
			print "\n-------------------------------\n$s\n";
			
			$q = "select count(distinct orfid) as num from orfgene where species='$s'";
			print "# ORFs: ".$this->_dbAccess->getObject("num", $q)."\n";

			$q = "select count(distinct R.tfid) as tfs from regulation as R, orfgene as O where R.tfid=O.orfid and O.species='$s'";
			print "# TFs with regulations: ".$this->_dbAccess->getObject("tfs", $q)."\n";

			$q = "select count(distinct C.tfid) as tfs from tfconsensus as C, orfgene as O where C.tfid=O.orfid and O.species='$s'";
			print "# TFs with consensus: ".$this->_dbAccess->getObject("tfs", $q)."\n";

			$q = "select count(distinct C.consensusid) as c from tfconsensus as C, orfgene as O where C.tfid=O.orfid and C.source='BSRG curated' and O.species='$s'";
			print "# distinct consensus: ".$this->_dbAccess->getObject("c", $q)."\n";

			$q = "select count(GO.goid) as num from orfgene as O, goorflist as GO where O.species='$s' and O.orfid=GO.orfid";
			print "# GO terms: ".$this->_dbAccess->getObject("num", $q)."\n";

			// Regulations
			$q = "select count(distinct R.tfid,R.targetid) as regs from regulation as R, orfgene as O where R.tfid=O.orfid and O.species='$s'";
			print "Total # regulations: " . $this->_dbAccess->getObject("regs", $q) ."\n";

			foreach (array("Direct","Indirect") as $code) {
				$q  = "select count(distinct R.regulationid) as regs from regulation as R, orfgene as O, ";
				$q .= "regulationdata as RD, evidencecodeBSRG as E where R.targetid=O.orfid and ";
				$q .= "O.species='$s' and R.regulationid=RD.regulationid and ";
				$q .= "RD.evidencecodeid=E.evidencecodeid and E.code='$code'";
				print " - with $code evidence: " . $this->_dbAccess->getObject("regs", $q) . "\n";
			}

			$q = "select count(distinct RD.envconditionid) as num from regulationdata as RD where RD.regulationid in ";
			$q .= "(select R.regulationid from regulation as R, orfgene as O where R.tfid=O.orfid and O.species='$s')";
			print "# EnvConds: ".$this->_dbAccess->getObject("num", $q)."\n";

			$q  = "select count(distinct pubmedid) as num from regulationdata where ";
			$q .= "regulationid in (select distinct regulationid from regulation ";
			$q .= "where tfid in (select orfid from orfgene where species='$s'))";
			print "# PubmedIDs: ".$this->_dbAccess->getObject("num", $q)."\n";



			/*# -----------------------------------------
			#continue;
			print "Regulation Evidence\n";
			$q = "select BSRG.code as CODE, count(BSRG.code) as count from evidencecodeBSRG as BSRG where BSRG.evidencecodeid in ";
			$q .= "(select RD.evidencecodeid from regulationdata as RD where RD.regulationid in ";
			$q .= "(select R.regulationid from regulation as R, orfgene as O where R.tfid=O.orfid and O.species='$s')) group by BSRG.code";
			if ($this->_dbAccess->query("evidence", $q)) {
				while($evi = $this->_dbAccess->nextObject("evidence")){
					print "# ".$evi['CODE']." evidence: ".$evi['count']."\n";
				}
			}

			print "Consensus Evidence\n";
			$q = "select BSRG.code as CODE, count(BSRG.code) as count from evidencecodeBSRG as BSRG where BSRG.evidencecodeid in (select CD.evidencecodeid from consensusdata as CD where CD.tfconsensusid in (select TFC.tfconsensusid from tfconsensus as TFC where TFC.tfid in (select O.orfid from orfgene as O where O.species=$s'))) group by BSRG.code";
			if ($this->_dbAccess->query("evidence", $q)) {
				while($evi = $this->_dbAccess->nextObject("evidence")){
					print "# ".$evi['CODE']." evidence: ".$evi['count']."\n";
				}
			}
			 */
			// potential homo regulations
			print "Potential Homologous regulations\n";
                        $q = "select count(*) as num from homopotreg where tfspecies='$s'";
                        if ($this->_dbAccess->query("homopotreg", $q)) {
                                while($row = $this->_dbAccess->nextObject("homopotreg")){
                                        print "# potential homo regulations as tf: ".$row['num']."\n";
                                }
			}
                        $q = "select count(*) as num from homopotreg where homospecies='$s'";
                        if ($this->_dbAccess->query("homopotreg", $q)) {
                                while($row = $this->_dbAccess->nextObject("homopotreg")){
                                        print "# potential homo regulations as target: ".$row['num']."\n";
                                }
                        }

		}

		print "-------------------------\n";
		print "Total statistics:\n";
		$sList = "'".implode("','",$aSpecies)."'";

		$q  = "select count(distinct R.tfid,R.targetid) as regs from regulation as R, ";
		$q .= "orfgene as O where R.tfid=O.orfid and O.species in ($sList)";

		print "Total # regulations: " . $this->_dbAccess->getObject("regs", $q) ."\n";

		$q  = "select count(distinct R.tfid) as tfs from regulation as R, orfgene as O ";
		$q .= "where R.tfid=O.orfid and O.species in ($sList)";
		print "# TFs with regulations: ".$this->_dbAccess->getObject("tfs", $q)."\n";

		$q  = "select count(distinct C.tfid) as tfs from tfconsensus as C, orfgene as O ";
		$q .= "where C.tfid=O.orfid and O.species in ($sList)";
		print "# TFs with consensus: ".$this->_dbAccess->getObject("tfs", $q)."\n";

		$q  = "select count(distinct C.consensusid) as c from tfconsensus as C, orfgene as O ";
		$q .= "where C.tfid=O.orfid and O.species in ($sList)";
		print "# distinct consensus: ".$this->_dbAccess->getObject("c", $q)."\n";

		$q  = "select count(GO.goid) as num from orfgene as O, goorflist as GO where ";
		$q .= "O.species in ($sList) and O.orfid=GO.orfid";
		print "# GO terms: ".$this->_dbAccess->getObject("num", $q)."\n";

		$q = "select count(distinct RD.envconditionid) as num from regulationdata as RD where RD.regulationid in ";
                $q .= "(select R.regulationid from regulation as R, orfgene as O where R.tfid=O.orfid and O.species in ($sList))";
                print "# EnvConds: ".$this->_dbAccess->getObject("num", $q)."\n";

	}
}

$stats = new Stats();
$stats->computeStats();
