<?php
class UpdatePubmedInfo {
	private $_dbAccess;
  private $_path;

	function __construct($path = "../conf/") {
    $this->_path = $path;
    include_once("IDBAccess.php");
    $this->_dbAccess = new IDBAccess($path);
    $this->_dbAccess->openConnection('w');
  }

	private function dbclean($str) {
    return addslashes(trim($str));
  }

	function update() {
		$q = "select * from pubmed";
		if ($this->_dbAccess->query("pubmed", $q)) {
			while ($row = $this->_dbAccess->nextObject("pubmed")) {
				$pmid = $row['pubmedid'];
				if ($pmid=="0") continue; // this is the default N/A PMID
				$title   = $row['title'];
                                $journal = $row['journal'];
                                $volume  = $row['volume'];
                                $issue   = $row['issue'];
                                $date    = $row['date'];
                                $pages   = $row['pages'];
				$authors = $row['authors'];
				if (empty($title)||empty($journal)||empty($volume)||empty($issue)||empty($date)||empty($pages)||empty($authors)){
					$s = 1;
					do {
						print "\rTrying to fetch PMID: $pmid\n";
						$url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pubmed&id=$pmid&retmode=json";
						$data    = file_get_contents($url);
						$json    = json_decode($data,TRUE);
						$title   = $json["result"][$pmid]['title'];
						$journal = $json["result"][$pmid]['source'];
						$volume  = $json["result"][$pmid]['volume'];
						$issue   = $json["result"][$pmid]['issue'];
						$date    = $json["result"][$pmid]['pubdate'];
						$pages   = $json["result"][$pmid]['pages'];
						$aAuthors = [];
						foreach (array_keys($json["result"][$pmid]['authors']) as $k){
							$aAuthors[] = $json["result"][$pmid]['authors'][$k]['name'];
						}
						$authors = join(", ",$aAuthors);
						if (empty($title)) { sleep($s); $s = $s*2; }
					} while (empty($title));
					$this->insertRef($pmid, $authors, $title,$journal, $date, $volume, $issue, $pages);
				}
			}
		}
	}

	private function insertRef($pubmedid, $authors, $title, $journal, 
		$date, $volume, $issue, $pages) {
		$q  = "update pubmed set authors='$authors', title='$title', ";
		$q .= "journal='$journal', date='$date', volume='$volume', ";
		$q .= "issue='$issue', pages='$pages' where pubmedid='$pubmedid'";
		print "\nINSERT:\t$q\n";
		$this->_dbAccess->insertUpdate($q);
	}

}

$cod = new UpdatePubmedInfo();
$cod->update();
?>
