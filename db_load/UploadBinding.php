<?php
class UploadConsensus {
	private $_dbAccess;
	private $_path;

	function __construct($path = "../conf/") {
		$this->_path = $path;
		include_once("IDBAccess.php");
		$this->_dbAccess = new IDBAccess($path);
		$this->_dbAccess->openConnection('w');
	}

	private function dbclean($str) {
		return addslashes(trim($str));
	}

	private function getORFid($species, $orfgene) {
                $dborfgene = $this->dbclean($orfgene);
                if (empty($dborfgene)) return false;
                $dbspecies = $this->dbclean($species);
                if (empty($dbspecies)) return false;

                $protein = $dborfgene;

                // some protein names include a 'p' in the end. This remove it
                if (substr($dborfgene, -1) == 'p') {
                       $dborfgene = substr($dborfgene, 0, strlen($dborfgene) - 1);
                }

                $q = "select orfid from orfgene where species='$dbspecies' and ";
                $q .= "(orf='$dborfgene' or gene='$dborfgene' or gdid='$dborfgene')";
                $orfid = $this->_dbAccess->getObject("orfid",$q);
                if (!$orfid) {
                        // not an orf or a gene... maybe an alias!?
                        $q = "select O.orfid from orfgene as O, alias as A where (A.alias like'";
                        $q .= "$dborfgene, %' or A.alias like '%, $dborfgene%') and O.orfid=A.orfid and O.species='$dbspecies'";
                        $orfid = $this->_dbAccess->getObject("orfid", $q);
                        if (!$orfid) {
                                // or... maybe a protein!?
                                $orfid = $this->getTFid($dbspecies,$protein);
                        }
                }
                return $orfid;
        }

        private function getTFid($species, $protein) {
                $q = "select orfid from orfgene where species='$species' and ";
                $q .= "orfid in (select tfid from protein where protein='$protein')";
                $tfid = $this->_dbAccess->getObject("orfid",$q);
                return $tfid;
        }

	// Equivalence functions, can be customized to transform common input into a standardized string
        private function envcondEquiv($str) {
                if ($str == "Not described") return "N/A";
                if ($str == "-") return "N/A";
                if ($str == "stress") return "Stress";
                if ($str == "") return "N/A"; // the description can be empty, in that case it is N/A
                return $str;
        }
        private function evicodeEquiv($str) {
                if ($str == "DNA Binding") return "Direct";
                if ($str == "Expression") return "Indirect";
                if ($str == "") return "N/A";
                return $str;
        }
        // verify if group exists, if not, create it
        private function insertGroup($group,$sub,$desc){
                $group = $this->dbclean($group);
                $sub = $this->dbclean($sub);
                if (!empty($sub)) {
                        $q = "select groupid from envconditiongroups where subgroup='$sub'";
                        $groupid = $this->_dbAccess->getObject("groupid",$q);
                        if (!$groupid) {
                                if (!empty($group)){
                                        $q = "insert into envconditiongroups (biggroup,subgroup) values ('$group','$sub')";
                                        $this->_dbAccess->insertUpdate($q);
                                }
                                else {
                                        print "[ERROR]\tCould not insert line $line: missing env condition group";
                                        return false;
                                }
                                $q = "select groupid from envconditiongroups where subgroup='$sub'";
                                $groupid = $this->_dbAccess->getObject("groupid",$q);
                                if (!$groupid) {
                                        print "[ERROR]\tCould not insert line $line: could not insert env condition group";
                                        return false;
                                }
                        }
                }
                else {
                        print "[ERROR]\tCould not insert line $line: missing env condition subgroup";
                        return false;
                }
                // env cond group done, now creating env condition itself
                $q = "insert into envcondition (envconditiondesc,groupid) values ('$desc',$groupid)";
                $this->_dbAccess->insertUpdate($q);
                $q = "select envconditionid from envcondition where envconditiondesc='$desc'";
                $envcondid = $this->_dbAccess->getObject("envconditionid",$q);
                return $envcondid;
        }

	function parseFile($filename,$binomial_name) {
		if (!file_exists($filename)) {
			return false;
		} 
		$lines = file($filename);
		$i = 0;
		foreach ($lines as $line) {
			$line = trim($line);
			$fields = explode("\t", $line);

			$strain = $this->dbclean($fields[2]);
			$species = $binomial_name." ".$strain;

			// INITIAL VALIDATION
			foreach ($fields as $field){
				$col = trim($field);
				if (empty($col)) { print "\n[ERROR]\tCould not insert line $line: please fill all the columns.\n"; continue 2; }
			}

			// TF
			$tf = trim($fields[0]);
			if (substr($tf, -1) == 'p') {
				$tf = substr($tf, 0, strlen($tf) - 1);
			}
			$tfid = $this->getORFid($species, $tf);
			if (!$tfid) {
				print "\n[ERROR]\tCould not insert line $line: TF not found in database";
				continue;
			}

			// Evidence Code
			if (isset($fields[4]) && isset($fields[5])) {
				$fields[4] = $this->dbclean($fields[4]);
				$fields[5] = $this->dbclean($fields[5]);
				$q = "insert into evidencecode (code,experiment) values ";  
				$q .= "('{$fields[4]}','{$fields[5]}')";
				$this->_dbAccess->insertUpdate($q);
				$q = "select evidencecodeid from evidencecode where code='{$fields[4]}' and experiment='{$fields[5]}'";
				$evcode = $this->_dbAccess->getObject("evidencecodeid",$q);
			}
			else {
				print "\n[ERROR]\tCould not insert line $line: Evidence code and/or experiment missing";
                                continue;
			}
			if (!isset($evcode)) {
				print "\n[ERROR]\tCould not insert line $line: Could not get Evidence code ID";
				continue;
			}

			// Environmental condition
			$ecdesc = $this->envcondEquiv($this->dbclean($fields[6]));
			if (!empty($ecdesc)) {
				$q = "select envconditionid from envcondition where envconditiondesc='$ecdesc'";
				$envcondid = $this->_dbAccess->getObject("envconditionid",$q);
				if (!$envcondid) {
					$envcondid = $this->insertGroup($fields[7],$fields[7],$ecdesc);
                                        if (!$envcondid) {
                                                print "[ERROR]\tCould not insert line $line: could not insert environmental condition";
                                                continue;
                                        }
				}
			}
			else {
                                print "\n[ERROR]\tCould not insert line $line: Envi condition missing";
                                        continue;
                        }

                        // Pubmed
                        if (isset($fields[3])){
                                $pmid = $this->dbclean($fields[3]);
                                $q = "insert into pubmed (pubmedid,journal,title,authors,date) ";
                                $q .= "values ('$pmid','','','','')";
                                $this->_dbAccess->insertUpdate($q);
                        }
                        else {
                                print "\n[ERROR]\tCould not insert line $line: PMID missing";
                                continue;
                        }

			// Consensus
                        if (isset($fields[1])){
                                $consensus = strtoupper($this->dbclean($fields[1]));
                                $q = "insert into consensus (IUPACseq) values ('$consensus')";
                                $this->_dbAccess->insertUpdate($q);
                                $q = "select consensusid from consensus where IUPACseq='$consensus'";
                                $consensusid = $this->_dbAccess->getObject("consensusid",$q);
                                if (!$consensusid) {
                                        print "\n[ERROR]\tCould not insert line $line: sequence not in database and not able to insert it";
                                        continue;
                                }
                        }
                        else {
                                print "\n[ERROR]\tCould not insert line $line: Consensus missing";
                                        continue;
                        }

                        // TFconsensus
                        $q = "insert into tfconsensus (tfid,consensusid) values ('$tfid','$consensusid')";
                        $this->_dbAccess->insertUpdate($q);
                        $q = "select tfconsensusid from tfconsensus where tfid='$tfid' and consensusid='$consensusid'";
                        $tfconsensusid = $this->_dbAccess->getObject("tfconsensusid",$q);
                        if (!$tfconsensusid) {
                                print "[TFconsensusid] $line";
                                continue;
                        }


			// Consensus data
			$q  = "insert into consensusdata (strain,tfconsensusid,evidencecodeid,pubmedid,";
			$q .= "envconditionid) values ('$strain','$tfconsensusid','$evcode','$pmid','$envcondid')";
			if (!$this->_dbAccess->insertUpdate($q)) {
				print "[ConsData] $line";
				print "Query: $q\n";
				print $this->_dbAccess->error()."\n";
				continue;
			}
			$i++;
		}
		print $i."/".count($lines);
		return true;
	}

}

if (count($argv)!=3) {
        die("\nUsage: php {$argv[0]} <bindings_file.tsv> <species> \n\n");
}
else {
        $cp = new UploadConsensus();
        print "Uploading binding information from file $argv[1] for $argv[2] ...\n";
}

$filename = $argv[1];
$species = $argv[2];

if ($cp->parseFile($filename,$species)) {
        print "Done!\n";
} else {
        print "Could not parse file [$filename]!\n";
}

?>
