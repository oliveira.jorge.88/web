<?php
class GO {
	private $_dbAccess;
	private $_path;

	function __construct($path = "../conf/") {
		$this->_path = $path;
		include_once("IDBAccess.php");
		$this->_dbAccess = new IDBAccess($path);
		$this->_dbAccess->openConnection('w');
	}

	private function dbclean($str) {
		return addslashes(trim($str));
	}

	private function getORFid($species, $orfgene) {
		$dborfgene = $this->dbclean($orfgene);
		if (empty($dborfgene)) return false;
		$dbspecies = $this->dbclean($species);
		if (empty($dbspecies)) return false;
		$q = "select orfid from orfgene where species='$dbspecies' and ";
		$q .= "(orf='$dborfgene' or gene='$dborfgene' or gdid='$dborfgene')";
		$orfid = $this->_dbAccess->getObject("orfid",$q);
		if (!$orfid) {
			// not an orf or a gene... maybe an alias!?
			$q = "select O.orfid from orfgene as O, alias as A where A.alias='";
			$q .= "$dborfgene' and O.orfid=A.orfid and O.species='$dbspecies'";
			$orfid = $this->_dbAccess->getObject("orfid", $q);
		}
		return $orfid;
	}

	private function getGOid($term) {
                $dbterm = $this->dbclean($term);
                if (empty($dbterm)) return false;
                $q = "select goid from geneontology where term='$dbterm'";
		$goid = $this->_dbAccess->getObject("goid",$q);
                return $goid;
        }

	function parseFile($species,$filename) {

		if (!file_exists($filename)) {
			return false;
		} 

		$dbspecies = $this->dbclean($species);
		
		$lines = file($filename);
		$i = 0;
		foreach ($lines as $line) {

			$fields = explode("\t", $line);

			$term = $fields[0];

			$goid = $this->getGOid($term);
			if (!$goid) print "\nCould not insert line '$line' GO term not found ";

			$orf = $fields[1];
			
			$orfid = $this->getORFid($species,$orf);
			if (!$orfid) print "\nCould not insert line '$line' ORF/Gene not found for species $species";

			if ($orfid!=NULL && $orfid!='' && $goid!=NULL && $goid!=''){

				$ins = "INSERT INTO goorflist (goid,orfid) VALUES ('$goid',$orfid)";
				print "\n".$ins;
						
				if(!$this->_dbAccess->insertUpdate($ins)){

					print_r($this->_dbAccess->error());

				}	
				else { 
					$i++;
					print "\n$ins";
				}
			}
		}

		print "\nInserted ".$i."/".count($lines);
		return true;

	}

}

if (count($argv)!=4) {
        die("\nUsage: php {$argv[0]} <GO_ORF_list.tsv> <species> <strain> \n The file is consisted of a pair of GO terms and Gene/ORF per line, divided by a tab\n\n");
}
else {
	$species = $argv[2]." ".$argv[3];
	$filename = $argv[1];
        $cp = new GO();
        print "\nUploading into Gene Ontologies ORF list from file $filename for $species...\n";
}

if ($cp->parseFile($species,$filename)) {
        print "\nDone!\n";
} else {
        print "\nCould not parse file [$filename]!\n";
}

?>
