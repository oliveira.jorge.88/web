<?php
class UpHomoRegs {
	private $_dbAccess;
	private $_path;

	function __construct($path = "../conf/") {
		$this->_path = $path;
		include_once("IDBAccess.php");
		$this->_dbAccess = new IDBAccess($path);
		$this->_dbAccess->openConnection('w');
	}

	private function dbclean($str) {
		return addslashes(trim($str));
	}

	private function insertDB($tfid,$tgid,$tfspecies,$homospecies) {
		$q = "INSERT INTO homopotreg VALUES ($tfid,$tgid,'$tfspecies','$homospecies')"; 
		$this->_dbAccess->insertUpdate($q);
	}

	function upload($path) {

		$dh = opendir($path) or die("Unable to open dir $path!");
		$files = glob($path.'*', GLOB_BRACE);
		foreach ($files as $file){
			$fh = fopen($file, "r") or die("Unable to open file $file!");
			while (!feof($fh)){
				$line = trim(fgets($fh));
				if (substr($line,0,1)==='#') { continue; }
				$aux = explode("\t",$line);
				if (!isset($aux[0])||!isset($aux[1])||!isset($aux[2])||!isset($aux[3])) { continue; }
				$this->insertDB($aux[0],$aux[1],$this->dbclean($aux[2]),$this->dbclean($aux[3]));
			}
		}
		return true;
	}

}

$cod = new UpHomoRegs();

$path = 'homo_pot_regs/';

if ($cod->upload($path)) {
	print "\nDone uploadind homologous potential regulations into the database!\n";
} else {
	print "\nCould not process!\n";
}
 
?>
