<?php
class GeneOntology {
	private $_dbAccess;
	public $_terms;
	private $_path;

	function __construct($path = "../conf/") {
		$this->_terms = array();
		include_once("IDBAccess.php");
		$this->_dbAccess = new IDBAccess($path);
		$this->_dbAccess->openConnection('w');
	}

	function parseFile($file) {
		if (!file_exists($file)) {
			return false;
		}

		$lines = file($file);
		$goid = "";
		$term = "";
		$namespace = "";
		$parents = array();
		$is_obsolete = false;
		foreach ($lines as $line) {
			if (substr($line, 0, 5)=="[Term") {
				if (!empty($namespace) && !empty($term) && !$is_obsolete) {
					$this->_terms[$goid]['term'] = $term;
					$this->_terms[$goid]['onto'] = $namespace;
					$this->_terms[$goid]['is_a'] = $parents;
				}
				$goid = "";
				$term = "";
				$namespace = "";
				$parents = array();
				$is_obsolete = false;
			} elseif (substr($line, 0, 4)=="id: ") {
				$goid = trim(substr($line, 4));
			} elseif (substr($line, 0, 6)=="name: ") {
				$term = trim(substr($line, 6));
			} elseif (substr($line, 0, 11)=="namespace: ") {
				$t = trim(substr($line, 11));
				if     ($t=="biological_process") { $namespace = "process";   }
				elseif ($t=="molecular_function") { $namespace = "function";  }
				elseif ($t=="cellular_component") { $namespace = "component"; }
			} elseif (substr($line, 0, 6)=="is_a: ") {
				$parents[substr($line, 6, 10)] = substr($line, 6, 10);
			} elseif (substr($line, 0, 22)=="relationship: part_of ") {
				$parents[substr($line, 22, 10)] = substr($line, 22, 10);
			} elseif (substr($line, 0, 25)=="intersection_of: part_of ") {
				$parents[substr($line, 25, 10)] = substr($line, 25, 10);
			} elseif (substr($line, 0, 11)=="is_obsolete") {
				$is_obsolete = true;
			}
		}
		return true;
	}

	function hasConsistentParents() {
		return !empty($this->_terms)
			&& $this->consistent($this->_terms);
	}

	private function consistent($terms) {
		foreach (array_keys($this->_terms) as $goid) {
			if (!empty($terms[$goid]['is_a'])) {
				foreach ($terms[$goid]['is_a'] as $parent) {
					if (!array_key_exists($parent, $terms)) {
						print "$goid -> $parent\n";
						return false;
					}
				}
			}
		}
		return true;
	}

	# This was modified to be faster - using inserts with multiple rows
	function sendToDB($path = "") {
		# First insert terms
		$q = ""; $i = 0;
		foreach (array_keys($this->_terms) as $goid) {
			if (!empty($q)) $q .= ", ";
			$q .= "('".$goid."','".addslashes($this->_terms[$goid]['term'])."','";
			$q .= $this->_terms[$goid]['onto']."')";
			$i++;
			if ($i >= 100) {
				$q = "insert into geneontology (goid,term,onto) values $q";
				$this->_dbAccess->insertUpdate($q);
				$q = ""; $i = 0;
			}
		}
		if (!empty($q)) {
			$q = "insert into geneontology (goid,term,onto) values $q";
			$this->_dbAccess->insertUpdate($q);
		}

		# Now insert relationships
		$q = ""; $i = 0;
		foreach (array_keys($this->_terms) as $goid) {
			foreach ($this->_terms[$goid]['is_a'] as $parent) {
				if (!empty($q)) $q .= ", ";
				$q .= "('".$parent."','".$goid."')";
				$i++;
			}
			if ($i >= 100) {
				$q = "insert into goparents (goid,goidson) values $q";
				$this->_dbAccess->insertUpdate($q);
				$q = ""; $i = 0;
			}
		}
		if (!empty($q)) {
			$q = "insert into goparents (goid,goidson) values $q";
			$this->_dbAccess->insertUpdate($q);
		}
	}

	function computeSonsDepth($goid, $depth) {
		# Update depth to parent $goid
		$q = "update geneontology set depth='$depth' where goid='$goid'";
		$this->_dbAccess->insertUpdate($q);
		# compute depth of $goid sons
		$q = "select goidson from goparents where goid='$goid'";
		$tmpname = rand();
		if ($this->_dbAccess->query($tmpname, $q)) {
			while ($row = $this->_dbAccess->nextObject($tmpname)) {
				$q2 = "select depth from geneontology where goid='".$row['goidson']."'";
				$sonDepth = $this->_dbAccess->getObject("depth",$q2);
				if (!$sonDepth || ($depth+1) < $sonDepth) {
					$this->computeSonsDepth($row['goidson'], $depth+1);
				}
			}
		}
		$this->_dbAccess->freeResult($tmpname);
	}

	function checkDepthConsistency() {
		$q = "select count(goid) as num from geneontology where depth='0'";
		$n = $this->_dbAccess->getObject("num", $q);
		print " . Found $n GO terms at depth=0!\n";
	}
	function deleteGOtables() {
		$q = "delete from goorflist";
		$this->_dbAccess->insertUpdate($q);
		$q = "delete from goparents";
		$this->_dbAccess->insertUpdate($q);
		$q = "delete from geneontology";
		$this->_dbAccess->insertUpdate($q);
	}
}

if (count($argv)!=2) {
        die("\nUsage: php {$argv[0]} <gene_ontology_file.obo> \n\n");
}
else {
        $go = new GeneOntology();
        print "Uploading into Gene Ontologies from file $argv[1]...\n";
}

$filename = $argv[1];

if ($go->parseFile($filename)) {
	print "Done!\n";
	if ($go->hasConsistentParents()) {
		print "Deleting old GO tables...\n";
		$go->deleteGOtables();
		print "Done!\n";

		print "Inserting ".count($go->_terms)." into DB... ";
		$go->sendToDB();
		print "Done!\n";

		print "Computing GO term depths...\n";
		print " . biological_process\n"; $go->computeSonsDepth("GO:0008150",1);
		print " . molecular_function\n"; $go->computeSonsDepth("GO:0003674",1);
		print " . cellular_component\n"; $go->computeSonsDepth("GO:0005575",1);
		print "Done!\n";

		print "Checking GO term depth consistency...\n";
		$go->checkDepthConsistency();
	} else {
		print "Could not find parents of some GO terms!\n";
	}
} else {
	print "Could not parse file!\n";
}
?>
