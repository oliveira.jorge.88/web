#!/usr/bin/bash

THREADS="4"
RAW="genomes"

command -v blastp >/dev/null 2>&1 || { echo >&2 "BLAST must be previously installed!"; exit 1; }
command -v makeblastdb >/dev/null 2>&1 || { echo >&2 "BLAST must be previously installed!"; exit 1; }

if [[ -d "$RAW" ]] && [[ ! -z `ls -A "$RAW"` ]]; then
	echo "Genome folder exists and is not empty."
else 
	echo "Genome folder does not exist or is empty. Please place your genome(s) in the folder to compute the homologs."
fi

BASE="homology"
if [ -d "$BASE" ]; then
	echo "Warning: ${BASE} folder already exists. "
else 
	mkdir $BASE
fi

BLASTDB="${BASE}/blast_databases"
if [ -d "$BLASTDB" ]; then
	echo "${BLASTDB} folder already exists"
else
	mkdir $BLASTDB
fi

for genome in `ls $RAW`
do
	makeblastdb -in $RAW/$genome -dbtype prot -title $genome-blastdb -out $BLASTDB/$genome-blastdb 2> $BLASTDB/$genome-makedb.error
done
echo "BLAST DATABASES BUILD!"

BLASTERROR="${BASE}/blast_error"
if [ -d "$BLASTERROR" ]; then
	echo "Warning: ${BLASTERROR} folder already exists"
else
	mkdir $BLASTERROR
fi

BLASTOUT="${BASE}/blast_results"
if [ -d "$BLASTOUT" ]; then
        echo "Warning: ${BLASTOUT} folder already exists. "
else
	mkdir $BLASTOUT
fi

for genome in `ls $RAW`
do
	for database in `ls $RAW`
	do
		if [ -f "$BLASTOUT/$genome-vs-$database.blastp" ]; then
			echo "$BLASTOUT/$genome-vs-$database.blastp already exists, skipping..."
		else 
			if [ "$genome" != "$database" ]
				then
					blastp -db $BLASTDB/$database-blastdb -query $RAW/$genome -out $BLASTOUT/$genome-vs-$database.blastp -evalue 1e-5 -outfmt 6 -num_threads $THREADS 2> $BLASTERROR/$genome-vs-$database.blasterror
			fi
		fi
	done
done
echo "BLAST ALIGNMENTS COMPLETED!"

BESTHITS="${BASE}/blast_besthits"
if [ -d "$BESTHITS" ]; then
        echo "Warning: ${BESTHITS} folder already exists. "
else
	mkdir $BESTHITS
fi

for blast in `ls $BLASTOUT`
do
	cat $BLASTOUT/$blast | php scripts/blast_besthit.php > $BESTHITS/$blast.bbh 2> $BLASTERROR/$blast.bbh.error
done
echo "BESTHITS COMPUTED!"

perl scripts/cross_blasts.pl $BESTHITS/ . > $BASE/blast_orthologies.tab
echo "ALL DONE!"
