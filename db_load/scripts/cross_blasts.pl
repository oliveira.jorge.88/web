#!/usr/bin/perl -w

use strict;

die "Usage: $0 <dir to process> <dir to save> \n" unless @ARGV == 2;

my $dir_to_process = shift;
my $save_dir = shift;
opendir my $dh, $dir_to_process or die "Cannot open $dir_to_process: $!";
my @files = readdir $dh;
@files = grep {/bbh$/} @files;
my @processed;

FILE: foreach my $filename(@files) {

next FILE if (grep( /^$filename$/, @processed ) );

my $a2dfname = $filename;

$filename =~ s/.blastp.bbh$//;
my @aux = split /-vs-/, $filename;

my $aname = $aux[0];
my $dname = $aux[1];

my $d2afname = $dname."-vs-".$aname.".blastp.bbh";

push @processed, $d2afname;
push @processed, $a2dfname;

$aname =~ s/_protseq.fasta.orf$//;
$dname =~ s/_protseq.fasta.orf$//;

my($a2dfile, $d2afile);

open($a2dfile, $dir_to_process.$a2dfname) or die "Cannot open $a2dfname : $!\n";
open($d2afile, $dir_to_process.$d2afname) or die "Cannot open $d2afname : $!\n";

my %a2d = ();
my %d2a = ();

while(<$a2dfile>){
	chomp;
	my @f = split /\s+/;
	$a2d{$f[0]}{$f[1]} = 1;
}

while(<$d2afile>){
	chomp;
	my @f = split /\s+/;
	$d2a{$f[0]}{$f[1]} = 1;
}

#$open(my $fh, '>', "$save_dir.$aname-vs-$dname-crossed.tab") or die "cannot open output file: $save_dir.$aname-vs-$dname-crossed.tab";

foreach my $ida (keys %a2d){
	foreach my $idb (keys %{$a2d{$ida}}){
		unless (exists $d2a{$idb}{$ida}){
			delete $a2d{$ida}{$idb};
			next;
		}
		print "$aname\t$ida\t$dname\t$idb\n"; 
	}
}
#close $fh;
}
