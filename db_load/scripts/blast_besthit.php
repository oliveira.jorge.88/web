<?php
function stdin_stream() {
	while ($line = fgets(STDIN)) {
		yield $line;
	}
}

$delta = 0.1;
$lastid = "";
$lastscores = [];
foreach (stdin_stream() as $line) {
	$line = trim($line);
	if ($line[0]=="#") { continue; }
	$f = explode("\t", $line);
	$currscore = end($f);
	$currtarget = $f[1];
	if ($f[0] == $lastid) {
		$lastscores[$currscore][$currtarget] = $line;
		continue;
	} else {
		if (!empty($lastscores)){
		       	$m = max(array_keys($lastscores));
			foreach (array_keys($lastscores) as $k) {
				$flexscore = $k*(1+$delta);
				if ($k*(1+$delta) >= $m) {
					foreach ($lastscores[$k] as $sco=>$goodline){
			                        print("$goodline\n");
                			}
				}
			}
			$lastscores = [];
		}
		$lastid = $f[0];
		$lastscores[$currscore][$currtarget] = $line;
	}
}
if (!empty($lastscores)) $m = max(array_keys($lastscores));
foreach (array_keys($lastscores) as $k) {
	$flexscore = $k*(1+$delta);
	if ($k*(1+$delta) >= $m) {
		foreach ($lastscores[$k] as $sco=>$goodline){
			print("$goodline\n");
		}
	}
}

?>
