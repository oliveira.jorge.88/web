#!/usr/bin/perl -w

use strict;

# Receives the tabular result of a Blastn query and keeps only the best hits for each query_id

my $delta = 1.1;
my $lastid = '';
my $lastscore = 1000_000;
while(<>){
	chomp;
	next if /^#/;
	my @f = split /\s+/;
	next if ( ($f[0] eq $lastid) and (($f[-1]*$delta) < $lastscore) ); 
	$lastid = $f[0];
	$lastscore = $f[-1];
	print join("\t",@f)."\n";
}
	
