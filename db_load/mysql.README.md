# DATABASE Setup 

It assumes that the user already installed the necessary packages (see [../README.md](../README.md)), in particular the installation of the MySQL/MariaDB relational database.

## MySQL/MariaDB

Once the relational database is installed and running, the machine administrator should log in as `root` (with the chosen password) and create a `communityyeast` database:

    [user@server ~]$ mysql -uroot -p
    MariaDB [(none)]> create database communityyeast;

## Configure differentiated users and permissions

Then the administrator should create two users:
- `userreader`, which is used for the webserver access, and therefore should have limited permissions;
- `userwriter`, which is used for DB administration either through the DB command line or through the loading scripts.

The user is free to choose other usernames and passwords. They should coincide with the ones specified in `conf/dbaccess.properties.php`.

    MariaDB [(none)]> create user 'userreader'@'localhost' identified by 'somepassword';
    MariaDB [(none)]> create user 'userwriter'@'localhost' identified by 'somepassword';

Finally, the administrator should configure the user's permissions:

    MariaDB [(none)]> grant select on communityyeast.* to 'userreader'@'localhost';
    MariaDB [(none)]> grant select,references,drop,alter,create,insert,update,delete,lock tables on communityyeast.* to 'userwriter'@'localhost';

## Load the database structure and data

To load the database with the relational table structure and the associated data, we provide three .sql files, each representing an increasing level of detail:
- `communityyeast.sql`, which creates only the table structure without any data;
- `communityyeast-Sc-data.sql`, which creates the table structure and populates the tables with the existing information on *S. cerevisiae*;
- `communityyeast-Sc-Rt-data.sql`, which creates the table structure and populates the tables with the existing information on *S. cerevisiae* and *R. toruloides*.

Each of these files can be loaded using the above defined database usernames and passwords:

    [user@server ~]$ mysql -uuserwriter -Dcommunityyeast -p < file.sql

If you which to load a specific organism, please check the instructions on `db_load/README.md`.

## Dump data

Whenever you change the data (e.g. loading a new organism), if you wish to backup all the data inserted into your local Community Yeastract, you can dump the data:

    [user@server ~]$ mysqldump -uuserwriter -p --single-transaction --quick communityyeast > communityyeast-data.sql

Finally, at this point the database preparation is done.
