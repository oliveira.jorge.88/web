<?php
class Groups {
	private $_dbAccess;
	private $_path;

	function __construct($path = "../conf/") {
		$this->_path = $path;
		include_once("IDBAccess.php");
		$this->_dbAccess = new IDBAccess($path);
		$this->_dbAccess->openConnection('w');
	}

	private function dbclean($str) {
		return addslashes(trim($str));
	}

	function parseFile($filename) {
		if (!file_exists($filename)) {
			return false;
		} 
		$lines = file($filename);
		$i = 0;
		foreach ($lines as $line) {
			$fields = explode("\t", $line);
			if (empty($fields[0])) continue;
			if (empty($fields[1])) continue;
			$biggroup = $this->dbclean($fields[0]);
			$subgroup = $this->dbclean($fields[1]);

			// envconditiongroups
			$q = "insert into envconditiongroups (biggroup,subgroup) values ('$biggroup','$subgroup')";
			$this->_dbAccess->insertUpdate($q);
			$i++;
		}
		print $i."/".count($lines)." ";
		return true;
	}

}

if (count($argv)!=2) {
        die("\nUsage: php {$argv[0]} <groups_file.tsv> \nThe groups file is consisted of a pair of big-group and sub-group per line, divided by a tab\n\n");
}
else {
        $cp = new Groups();
        print "Uploading Environmental condition groups from file $argv[1]...\n";
}

$filename = $argv[1];

if ($cp->parseFile($filename)) {
        print "Done!\n";
} else {
        print "Could not parse file [$filename]!\n";
}

?>
