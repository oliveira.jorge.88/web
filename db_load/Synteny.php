<?php
class Synteny {
	private $_dbAccess;
	private $_path;

	function __construct($path = "../conf/") {
		$this->_path = $path;
		include_once("IDBAccess.php");
		$this->_dbAccess = new IDBAccess($path);
		$this->_dbAccess->openConnection('w');
	}

	function makeDBmap() {

		$q1 = "select species, coords, orf from orfgene ";

		$aMap = [];
		$revMap = [];
		if ($this->_dbAccess->query("synt", $q1)) {
			while ($row = $this->_dbAccess->nextObject("synt")) {
				$sp       = preg_replace("/'/","",$row['species']);
				$orf      = $row['orf'];
				$dbcoords = $row['coords'];
				if ($dbcoords == 'NULL' or $dbcoords=='') {
					$pos = 'NA';
					$chr = 'NA';
				}
				else {
					if (strpos($dbcoords, 'from') !== false) {
						$aux   = explode(" from ",$dbcoords);
						$chr   = $aux[0];
						$coords = $aux[1];
						if (strpos($coords, '..') !== false) {
							$aux2 = explode("..",$coords);
						}
						elseif (strpos($coords, '-') !== false) {
							$aux2 = explode("-",$coords);
						}
					}
					else if (strpos($dbcoords, ':') !== false) {
						$aux   = explode(":",$dbcoords);
						$chr   = explode("_",$aux[0])[0];
						$coords = $aux[1];
						if (strpos($coords, '-') !== false) {
							$aux2 = explode("-",$coords);
						}
					}
					$start = preg_replace("/[^0-9]/", "", $aux2[0]);
					$stop  = preg_replace("/[^0-9]/", "", end($aux2));
					$pos = ($start<$stop)?$start:$stop;
				}
				$aMap[$sp][$chr][] = array('orf' => $orf, 'pos' => $pos, 'xrefs'=> array());
				$index=max(array_keys($aMap[$sp][$chr]));
				$revMap[$orf] = array('sp' => $sp, 'chr' => $chr, 'pos' => $pos, 'index'=> $index);
			}
		}
		return [$aMap, $revMap];
	}

	function makeBLASTmap($aMap, $revMap, $blastfile) {

		$mapfile = fopen('orgmap.tab', "r") or die("Unable to open orgmap file!");
		$orgMap = [];
		while (!feof($mapfile)) {
			$line = trim(fgets($mapfile));
			if ($line=='') continue;
			$aux = explode("\t",$line);
			$orgMap[$aux[0]] = $aux[1];
		}
		fclose($mapfile);

		$file = fopen($blastfile, "r") or die("Unable to open file!");

		while (!feof($file)) {
			$line = trim(fgets($file));
			if ($line == '') continue;
			$aux1 = explode("\t",$line);
			if (!isset($orgMap[$aux1[0]]) || !isset($orgMap[$aux1[2]])) continue;
			$org1 = $orgMap[$aux1[0]];
			$orf1 = $aux1[1];
			$orf2 = $aux1[3];
			if (!isset($revMap[$orf1]['sp']) || !isset($revMap[$orf2]['sp'])){ continue; }
			$chr1 = $revMap[$orf1]['chr'];
			$pos1 = $revMap[$orf1]['index'];
			$org2 = $orgMap[$aux1[2]];
                        $chr2 = $revMap[$orf2]['chr'];
			$pos2 = $revMap[$orf2]['index'];
			$aMap[$org1][$chr1][$pos1]['xrefs'][] = $orf2;
			$aMap[$org2][$chr2][$pos2]['xrefs'][] = $orf1;
		}

		fclose($file);

		return [$aMap,$orgMap];
	}

	function filterFalsePositives($aMap,$revMap,$window,$min_pairs){
		$total=0;
		$filtered=0;
		// SP => CHR => POS => 
		foreach(array_keys($aMap) as $sp){
			foreach(array_keys($aMap[$sp]) as $chr){
				foreach(array_keys($aMap[$sp][$chr]) as $index){
					$orf = $aMap[$sp][$chr][$index]['orf'];
					$aHomo = $aMap[$sp][$chr][$index]['xrefs'];
					foreach ($aHomo as $key=>$homolog){
						$homoIndex = $revMap[$homolog]['index'];
						$homoPos   = $revMap[$homolog]['pos'];
						$homoSp    = $revMap[$homolog]['sp'];
						$homoChr   = $revMap[$homolog]['chr'];
						$w_start = (($index-$window)<0)?0:($index-$window);
						$w_end = (($index+$window)>max(array_keys($aMap[$sp][$chr])))?max(array_keys($aMap[$sp][$chr])):($index+$window);
						$hw_start = (($homoIndex-$window)<0)?0:($homoIndex-$window);
                                                $hw_end = (($homoIndex+$window)>max(array_keys($aMap[$homoSp][$homoChr])))?max(array_keys($aMap[$homoSp][$homoChr])):($homoIndex+$window);
						$neighbors=0;
						if ($homoChr!='NA' && $chr!='NA') {
							for ($i=$w_start ; $i<$w_end+1 ; $i++ ) {
								$localNeighbor = $aMap[$sp][$chr][$i]['orf'];
								for ($j=$hw_start ; $j<$hw_end+1 ; $j++) {
									$aHomoNeighbors = $aMap[$homoSp][$homoChr][$j]['xrefs'];
									if (isset($aHomoNeighbors) && in_array($localNeighbor,$aHomoNeighbors)) $neighbors++;
								}
							}
						}
						if ($neighbors<$min_pairs) { 
							unset($aMap[$sp][$chr][$index]['xrefs'][$key]);
							$filtered++;
						}
						$total++;
						
					}
				}
			}
		}
		print "Parameters: window=$window, mininum neighbors=$min_pairs\nFiltered $filtered / $total \n";
		return $aMap;
	}

	# filter_lonely -> filters orfs without orthlogs
	 function speciesPillars($aMap,$revMap,$orgMap,$filter_lonely){ 
		 
		 foreach($orgMap as $short=>$sp){
			$pillars[$sp] = [];
			foreach (array_keys($aMap[$sp]) as $chr){
				foreach (array_keys($aMap[$sp][$chr]) as $index){
					$orf = $aMap[$sp][$chr][$index]['orf'];
					if ($filter_lonely && empty($aMap[$sp][$chr][$index]['xrefs'])) continue;
					$pillars[$sp][$orf][$sp][] = $orf;
                                        foreach ($aMap[$sp][$chr][$index]['xrefs'] as $key=>$homoORF){
                                                $homoSp    = $revMap[$homoORF]['sp'];
                                                $homoChr   = $revMap[$homoORF]['chr'];
                                                $homoIndex = $revMap[$homoORF]['index'];
                                                if (!isset($pillars[$sp][$orf][$homoSp]) || !in_array($homoORF,$pillars[$sp][$orf][$homoSp])) $pillars[$sp][$orf][$homoSp][] = $homoORF;
                                                #foreach($aMap[$homoSp][$homoChr][$homoIndex]['xrefs'] as $j=>$refORF){
                                                 #       $refSp    = $revMap[$refORF]['sp'];
                                                  #      if (!isset($pillars[$sp][$orf][$refSp]) || !in_array($refORF,$pillars[$sp][$orf][$refSp]) ) $pillars[$sp][$orf][$refSp][] = $refORF;
                                                #}
                                        }
                                }
			}
		}
		$head = join("\t",array_keys($orgMap));
		foreach($orgMap as $short => $sp){
			$filename = $short."-pillars.tab";
			$fh = fopen($filename, "w") or die("\nUnable to open file  $filename !");			
			fwrite($fh, $head);
               		foreach(array_keys($pillars[$sp]) as $orf){
	                       	fwrite($fh,"\n");
	                        foreach($orgMap as $short=>$org){
        	                        $p = (isset($pillars[$sp][$orf][$org]))?join(",",$pillars[$sp][$orf][$org]):"---";
               		                fwrite($fh,"$p\t");
                       		}
			}
			fclose($fh);
		}
		return;
	 }

}

$blastfile = "homology/blast_orthologies.tab";

$cod = new Synteny();
list($aMap,$revMap) = $cod->makeDBmap();
if ($aMap) {
	list($aMap,$orgMap) = $cod->makeBLASTmap($aMap,$revMap,$blastfile);
	$window=15;  // considering 15 neighboors in each direction
	mkdir("synteny");
	chdir("synteny");
	$cwd = getcwd();
	for ($pairs=1;$pairs<=4;$pairs++){ // calculated minimum pairs from 0 to 3
		$folder = "w".$window."p".($pairs-1);
		mkdir($folder);
                chdir($folder);
		$aMapFiltered = $cod->filterFalsePositives($aMap,$revMap,$window,$pairs); 
		$cod->speciesPillars($aMapFiltered,$revMap,$orgMap,true);
		chdir($cwd);
	}
	print "\nDone!\n";
} else {
	print "Could not compute synteny!\n";
}
 
?>
