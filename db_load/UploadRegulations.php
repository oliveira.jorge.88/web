<?php
class Regulations {
	private $_dbAccess;
	private $_path;

	function __construct($path = "../conf/") {
		$this->_path = $path;
		include_once("IDBAccess.php");
		$this->_dbAccess = new IDBAccess($path);
		$this->_dbAccess->openConnection('w');
	}

	private function dbclean($str) {
		return addslashes(trim($str));
	}

	private function getORFid($species, $orfgene) {
                $dborfgene = $this->dbclean($orfgene);
                if (empty($dborfgene)) return false;
                $dbspecies = $this->dbclean($species);
                if (empty($dbspecies)) return false;

                $protein = $dborfgene;

                // some protein names include a 'p' in the end. This remove it
                if (substr($dborfgene, -1) == 'p') {
                       $dborfgene = substr($dborfgene, 0, strlen($dborfgene) - 1);
                }

                $q = "select orfid from orfgene where species='$dbspecies' and ";
                $q .= "(orf='$dborfgene' or gene='$dborfgene' or gdid='$dborfgene')";
                $orfid = $this->_dbAccess->getObject("orfid",$q);
                if (!$orfid) {
                        // not an orf or a gene... maybe an alias!?
                        $q = "select O.orfid from orfgene as O, alias as A where (A.alias like'";
                        $q .= "$dborfgene, %' or A.alias like '%, $dborfgene%') and O.orfid=A.orfid and O.species='$dbspecies'";
                        $orfid = $this->_dbAccess->getObject("orfid", $q);
                        if (!$orfid) {
                                // or... maybe a protein!?
                                $orfid = $this->getTFid($dbspecies,$protein);
                        }
                }
                return $orfid;
        }

        private function getTFid($species, $protein) {
                $q = "select orfid from orfgene where species='$species' and ";
                $q .= "orfid in (select tfid from protein where protein='$protein')";
                $tfid = $this->_dbAccess->getObject("orfid",$q);
                return $tfid;
        }

	// Equivalence functions, can be customized to transform common input into a standardized string
	private function envcondEquiv($str) {
		if ($str == "Not described") return "N/A";
		if ($str == "-") return "N/A";
		if ($str == "stress") return "Stress";
		if ($str == "") return "N/A"; // the description can be empty, in that case it is N/A
		return $str;
	}
	private function associationEquiv($str) {
		if (strtolower($str) == "positive" || $str == "+")
			return "Positive";
		if (strtolower($str) == "negative" || $str == "-")
			return "Negative";
		if ($str == "+-" || $str == "+/-" || strtolower($str) == "positive/negative")
			return "Positive/Negative";
		return "N/A";
	}
	private function evicodeEquiv($str) {
                if ($str == "DNA Binding") return "Direct";
		if ($str == "Expression") return "Indirect";
		if ($str == "") return "N/A";
                return $str;
        }
	// verify if group exists, if not, create it
	private function insertGroup($group,$sub,$desc){
		$group = $this->dbclean($group);
		$sub = $this->dbclean($sub);
		if (!empty($sub)) {
			$q = "select groupid from envconditiongroups where subgroup='$sub'";
                        $groupid = $this->_dbAccess->getObject("groupid",$q);
			if (!$groupid) {
				if (!empty($group)){
					$q = "insert into envconditiongroups (biggroup,subgroup) values ('$group','$sub')";
					$this->_dbAccess->insertUpdate($q);
				}
				else { 
                                        print "[ERROR]\tCould not insert line $line: missing env condition group";
                                        return false;
				}
				$q = "select groupid from envconditiongroups where subgroup='$sub'";
	                        $groupid = $this->_dbAccess->getObject("groupid",$q);
				if (!$groupid) {
					print "[ERROR]\tCould not insert line $line: could not insert env condition group";
                                        return false;
				}
			}
		}
		else {
			print "[ERROR]\tCould not insert line $line: missing env condition subgroup";
                        return false;
		}
		// env cond group done, now creating env condition itself
		$q = "insert into envcondition (envconditiondesc,groupid) values ('$desc',$groupid)";
		$this->_dbAccess->insertUpdate($q);
		$q = "select envconditionid from envcondition where envconditiondesc='$desc'";
		$envcondid = $this->_dbAccess->getObject("envconditionid",$q);
		return $envcondid;
	}
	
	function parseFile($binomial_name,$filename) {
		if (!file_exists($filename)) {
			return false;
		} 
		$lines = file($filename);
		$i = 0;
		foreach ($lines as $line) {
			$line = trim($line);
			$fields = explode("\t", $line);

                        // INITIAL VALIDATION
                        foreach ($fields as $field){
                                $col = trim($field);
                                if (empty($col)) { print "\n[ERROR]\tCould not insert line $line: please fill all the columns.\n"; continue 2; }
                        }

			$strain = $this->dbclean($fields[2]);
                        if (empty($strain)) {
                                print "\n[ERROR]\tCould not insert line $line: missing strain\n";
                                continue;
			}
			$species = $binomial_name." ".$strain;

			// TF
			$tf = $this->dbclean($fields[0]);
			// some protein names include a 'p' in the end. This remove it
			if (substr($tf, -1) == 'p') {
				$tf = substr($tf, 0, strlen($tf) - 1);
			}
			$tfid = $this->getORFid($species, $tf);
			if (!$tfid) {
				print "\n[ERROR]\tCould not insert line $line: no matching TF found in database\n";
				continue;
			}

			// Target gene
                        $gene =$this->dbclean($fields[1]);

                        $tgid = $this->getORFid($species, $gene);
                        if (!$tgid) {
                                print "[TG]\tCould not insert line $line: could not find gene $gene in the database";
                                continue;
                        }

			// Pubmed
			$pmid = $this->dbclean($fields[3]);
			if ((!isset($pmid)) or (empty($pmid))){
				print "\n[ERROR]\tCould not insert line $line: PUBMED id empty\n";
				continue;
			}

			// is never empty because of the equiv function
			if (!isset($fields[5])){
				print "[ERROR]\tCould not insert line $line: missing association type\n";
                                 continue;
			}
			else{
				$assoctype = $this->associationEquiv($this->dbclean($fields[5]));
			}

			if (!isset($fields[4]) || !isset($fields[6])) {
				print "[ERROR]\tCould not insert line $line: missing evidence code and/or description\n";
				 continue;
			}

			// Environmental condition
                        $ecdesc = $this->envcondEquiv($this->dbclean($fields[7]));
                        if (!empty($ecdesc)) {
                                $q = "select envconditionid from envcondition where envconditiondesc='$ecdesc'";
                                $envcondid = $this->_dbAccess->getObject("envconditionid",$q);
				if (!$envcondid) {
                                        $envcondid = $this->insertGroup($fields[8],$fields[9],$ecdesc);
                                        if (!$envcondid) {
                                                print "[ERROR]\tCould not insert line $line: could not insert environmental condition";
                                                continue;
                                        }
				}
                        }

			// Evidence Code
			if (isset($fields[4]) && isset($fields[6])) {
				$fields[4] = $this->evicodeEquiv($this->dbclean($fields[4]));
				$fields[6] = $this->dbclean($fields[6]);
				$q = "insert into evidencecode (code,experiment) values "; 
				$q .= "('{$fields[4]}','{$fields[6]}')";
				$this->_dbAccess->insertUpdate($q);
				$q = "select evidencecodeid from evidencecode where code='{$fields[4]}' and experiment='{$fields[6]}'";
				$evcode = $this->_dbAccess->getObject("evidencecodeid",$q);
			}
			if (!isset($evcode)) {
				print "[ERROR]\tCould not insert line $line: could not find evidence code\n";
				continue;
			}
			else { print "\nEVCODE is $evcode"; }

			// Regulation TF->TG
			$q = "insert into regulation (tfid,targetid) values ('$tfid','$tgid')";
			$this->_dbAccess->insertUpdate($q);
			$q = "select regulationid from regulation where tfid='$tfid' and targetid='$tgid'";
			$regid = $this->_dbAccess->getObject("regulationid",$q);
			if (!$regid) {
				print "[RegID]\tCould not insert line $line: could not get regulation id";
				continue;
			}

                        // do this at the end if everything is okay
                        $q = "insert into pubmed (pubmedid,journal,title,authors,date) ";
                        $q .= "values ($pmid,'','','',";
                        $q .= "'')";
                        $this->_dbAccess->insertUpdate($q);

			// Regulation data
			$q  = "insert into regulationdata (strain,regulationid,pubmedid,association,evidencecodeid";
			$q .= ",envconditionid) values ('$strain','$regid','$pmid','$assoctype','$evcode','$envcondid')";
			if (!$this->_dbAccess->insertUpdate($q)) {
				print "\n$q";
				print "[RegData]\tCould not insert line: $line, regulation data info missing";
				print $this->_dbAccess->error() . "\n";
				continue;
			}
			$i++;
			print "\nINSERTED LINE $line SUCCESSFULLY";
		}
		print "\n".$i."/".count($lines)."\n";
		return true;
	}
}


if (count($argv)!=3) {
        die("\nUsage: php {$argv[0]} <regulations_file.tsv> <species> \n\n");
}
else {
	$cod = new Regulations();
        print "Uploading regulations from file $argv[1] for $argv[2] ...\n";
}

$filename = $argv[1];
$species = $argv[2];

if ($cod->parseFile($species,$filename)) {
	print "Done!\n";
} else {
	print "Could not parse file [$filename]!\n";
}

?>
