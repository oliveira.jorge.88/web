<?php
class View {

    /*
     * regulation
     */
    static function regulation($reg, $site) {
    if (count($reg)==0) {
?>
<span class="error">No regulatory association found!</span>
<?php
      return;
    }
?>
<span class="error">The <?=$site?> team, is actively curating each regulation evidence code, association type and environmental condition.</span>

<table border="1" summary="main content">
  <tr>
    <th>Transcription<br/>Factor</th>
    <th>Target<br/>ORF/Genes</th>
    <th>References</th>
    <th>Evidence<br/>Code</th>
    <th>Evidence<br/>Experiment</th>
    <th>Association<br/>Type</th>
    <th>Strain</th>
    <th>Environmental<br/>Condition</th>
  </tr>
<?php
#print "<hr/>Reg: <pre>";print_r($reg);print "</pre>";
# $reg[$tf][$orf][$id]
    foreach (array_keys($reg) as $tf) {
      $rowtf = 0;
      foreach (array_keys($reg[$tf]) as $orf) {
        foreach (array_keys($reg[$tf][$orf]) as $id) {
          foreach (array_keys($reg[$tf][$orf][$id]['envcond']) as $env ) {
            $rowtf += 1;
          }
        }
      }
?>
  <tr>
    <td class="align" rowspan="<?=$rowtf?>">
      <a href="view.php?existing=protein&amp;proteinname=<?=$tf?>"><?=$tf?></a>
    </td>
<?php
      $roworf = 0;
      foreach (array_keys($reg[$tf]) as $orf) {
      #  if ($orf == "YBL030C") print_r($reg[$tf][$orf]);
        $roworf = 0;
        foreach (array_keys($reg[$tf][$orf]) as $id) {
          foreach (array_keys($reg[$tf][$orf][$id]['envcond']) as $env) {
            $roworf += 1;
          }
        }
?>
    <td rowspan="<?=$roworf?>" class="align">
      <a href="view.php?existing=locus&amp;orfname=<?=$orf?>"><?=$orf?></a>
    </td>
<?php
        $ipm=0;
        foreach (array_keys($reg[$tf][$orf]) as $id) {
          $rowenv=0;
          foreach (array_keys($reg[$tf][$orf][$id]['envcond']) as $env) {
            $rowenv += 1;
          }
          if ($ipm>0) {print "<tr>";}  
          # URL errata:
          $tooltip = "";
          if ($id=="30237501") {
            $tooltip = "data-toggle=\"tooltip\" data-placement=\"auto\" title=\"";
            $tooltip .= "Concerning Tables 1 and 2 of the article, the raw headers of ";
            $tooltip .= "the 3rd and 4th columns should read (mRNA Wt)Stress / ";
            $tooltip .= "(mRNA Wt)Control and (mRNA Zbhaa1Δ)Stress / (mRNA Wt)Stress, ";
            $tooltip .= "respectively, where 'Stress' refers to acetic acid (AA) ";
            $tooltip .= "in Table 1 and copper (Cu) in Table 2.";
            $tooltip .= "\"";
          }
?>
    <td rowspan="<?=$rowenv?>" class="align">
    <a onclick="window.open(this.href); return false;" href="https://www.ncbi.nlm.nih.gov/pubmed/<?=$id?>" <?=$tooltip?>>PubMed&nbsp;<img src="images/external.png" alt="external link" /></a>
      <?=View::printReference($reg[$tf][$orf][$id]);?>
    </td>
    <td rowspan="<?=$rowenv?>" class="align"><?=$reg[$tf][$orf][$id]['code']?></td>
    <td rowspan="<?=$rowenv?>" class="align"><?=$reg[$tf][$orf][$id]['experiment']?></td>
    <td rowspan="<?=$rowenv?>" class="align"><?=$reg[$tf][$orf][$id]['association']?></td>
    <td rowspan="<?=$rowenv?>" class="align"><?=$reg[$tf][$orf][$id]['strain']?></td>
<?php
          $iec = 0;
          foreach (array_keys($reg[$tf][$orf][$id]['envcond']) as $env) {
            if ($iec > 0) print "<tr>";
?>
    <td class="align"><?=$reg[$tf][$orf][$id]['envcond'][$env]?></td></tr>
<?php
            $iec++;
          }
          $ipm++;
        }
      }
    }
?>
</table>
<?php
  }
    
    
    /*
     * consensus
     */
    static function consensus($reg, $site) {
    if (count($reg)==0) {
?>
<span class="error">No transcription factor binding site found!</span>
<?php
      return;
    }
?>
<span class="error">The <?=$site?> team, is actively curating each transcriptor factor binding site evidence code and environmental condition.</span>

<table border="1" summary="main content">
  <tr>
    <th>Transcription<br/>Factor</th>
    <th>Binding Site</th>
    <th>References</th>
    <th>Evidence<br/>Code</th>
    <th>Evidence<br/>Experiment</th>
    <th>Environmental<br/>Condition</th>
  </tr>
<?php
    foreach (array_keys($reg) as $tf) {
      $i = count($reg[$tf]);
?>
  <tr>
    <td class="align" rowspan="<?=$i?>"><a href="view.php?existing=protein&amp;proteinname=<?=$tf?>"><?=$tf?></a></td>
<?php
      print $b = false;
      foreach (array_keys($reg[$tf]) as $c) {
          if ($b) print "<tr>";
?>
    <td class="align"><?=$c?></td>
    <td class="align">
<?php
          foreach (array_keys($reg[$tf][$c]) as $id) {
            $abbrev = View::printReference($reg[$tf][$c][$id]);
?>
<a onclick="window.open(this.href); return false;" href="https://www.ncbi.nlm.nih.gov/pubmed/<?=$id?>">
PubMed&nbsp;<img src="images/external.png" alt="external link" /></a>&nbsp;<?=$abbrev?><br/>
<?php
        }
?>
    </td>
    <td><?=$reg[$tf][$c][$id]['code']?></td>
    <td><?=$reg[$tf][$c][$id]['experiment']?></td>
    <td><?=$reg[$tf][$c][$id]['envcond']?></td>
</tr>
<?php
        $b = true;
      }
    }
?>
</table>
<?php
    }

    /*
     * printReference
     */
    static function printReference($refs) {
      $authors = explode(", ", $refs['authors']);
      $abbrev = (count($authors)>1)? $authors[0] . " et al.": $authors[0];
      $abbrev .= ", " . $refs['journal'];
      $abbrev .= ", " . $refs['date'];
      $abbrev .= ";" . $refs['volume'] . "(" . $refs['issue'] . ")";
      $abbrev .= ":" . $refs['pages'];
      $abbrev .= "<span style=\"display:none\">$abbrev</span>";
      return $abbrev;
    }

    static function orthologs($orfgene, $web){
?>
<table border="0" summary="main content">   
  <tr><th align="left">Standard Name</th>
    <td class="bgalign"><?=$orfgene['genename']?></td></tr>
  <tr><th align="left">Systematic Name</th>
    <td class="bgalign"><?=$orfgene['orfname']?></td></tr>
<?php  if (isset($orfgene['altname'])) { ?>
  <tr><th align="left">Alias</th>
    <td class="bgalign"><?=join(", ", $orfgene['altname'])?></td></tr>
<?php } ?>
<?php if (isset($orfgene['orthologs'])) { ?>
  <tr>
		<th rowspan="2">
      <a href="help_orthology.php" title="Help">
        <img style="border:0" src="images/information.gif" alt="help icon">
      </a>
		</th>
    <th class="center" colspan="4">BLAST Best-Score</th>
  </tr>
  <tr>
    <th>Only Homology</th>
    <th>+1 neighbor synteny</th>
    <th>+2 neighbors synteny</th>
    <th>+3 neighbors synteny</th>
  </tr>
<?php 
      foreach (array_keys($orfgene['orthologs']) as $s ) {
        print "<tr><th>$s</th>";
        for ($syn=0;$syn<4;$syn++){
          print "<td>";
          if (isset($orfgene['orthologs'][$s][$syn])) {
            $sGenes = "";
            foreach($orfgene['orthologs'][$s][$syn] as $v){
              $o = $v['orf'];
              $og = ($o==$v['gene'] || $v['gene']=="Uncharacterized")?
                $o : "$o/".$v['gene'];
              $dburl = $web->getDBurl($s);
              if (!empty($sGenes)) $sGenes .= "<br/>";
              $sGenes .= "<a href=\"".$dburl."view.php?existing=locus&amp;orfname=$o\">$og</a>";
            }
            print $sGenes;
          }
          print "</td>";
        }
        print "</tr>";
      }
    }
?>
</table>
<?php
    }
    
    /*
     * orfgene
     */
    static function orfgene($orfgene, $web, $bGeneSeq = false) {
      $extname = $web->getProp("db.$web->_dbname.ext.name");
      $exturl  = $web->getProp("db.$web->_dbname.ext.url");
      $s0 = "";
      $s1 = ($bGeneSeq)? "": "&amp;withsequence=true";
      $s2 = ($bGeneSeq)? "Hide Seq": "View Seq";
      $s3 = ($bGeneSeq)? "<pre>".$orfgene['genesequence']."</pre>": "<i>Sequence hidden</i>";
?>
<table border="0" summary="main content">
  <tr><th align="left">Standard Name</th>
    <td class="bgalign"><?=$orfgene['genename']?></td></tr>
  <tr><th align="left">Systematic Name</th>
    <td class="bgalign"><?=$orfgene['orfname']?></td></tr>
<?php  if (isset($orfgene['altname'])) { ?>
  <tr><th align="left">Alias</th>
    <td class="bgalign"><?=join(", ", $orfgene['altname'])?></td></tr>
<?php } ?>
  <tr><th align="left">Strain</th>
    <td class="bgalign"><?=$orfgene['strain']?></td></tr>
  <tr><th align="left">Description</th>
    <td class="bgalign"><?=$orfgene['description']?></td></tr>
<?php if (isset($exturl)) { ?>
  <tr><th align="left">External source</th>
    <td class="bgalign">
    <a onclick="window.open(this.href); return false;" href="<?=$exturl?><?=$orfgene['ext']?>">Open in <?=$extname?>
    <img alt="external link" src="images/external.png"/></a></td></tr>
<?php } ?>
  <tr><td colspan="2"></td></tr>
  <tr><th align="left">Gene&nbsp;Sequence<br/><small>
    Download <a href="download.php?type=gene&amp;id=<?=$orfgene['orfname']?>">FASTA</a><br>
    <a href="view.php?existing=locus&amp;orfname=<?=$orfgene['orfname']?><?=$s0.$s1?>"><?=$s2?></a></small></th>
    <td class="bgalign"><?=$s3?></td></tr>
  <tr><th align="left">Promoter&nbsp;Sequence<br/><small>
    Download <a href="download.php?type=promoter&amp;id=<?=$orfgene['orfname']?>">FASTA</a></small>
<?php
  if (isset($orfgene['fwrpos'])) {
        print View::printPositions($orfgene, "F");
  }
?>
    </th>
    <td class="bgalign">
<?php  if (isset($orfgene['fwrpos'])) {

        print View::printSeqWithPos($orfgene['promotersequence'], $orfgene['fwrpos'], "F");
    } else { ?>
    <pre><?=$orfgene['promotersequence']?></pre>
<?php  } ?>
    </td></tr>
<?php  if (isset($orfgene['revpos'])) { ?>
  <tr><th align="left">Promoter&nbsp;Sequence<br/><small>Complementary&nbsp;Strand</small>
<?php    print View::printPositions($orfgene, "R"); ?>
    </th>
    <td class="bgalign">
<?php    print View::printSeqWithPos($orfgene['promotersequence'], $orfgene['revpos'], "R"); ?>
    </td>
  </tr>
<?php } ?>
  <tr><th align="left">Chr coordinates</th>
    <td class="bgalign"><?=$orfgene['coords']?></td></tr>
<?php  if (isset($orfgene['has_regdoc'])) { ?>
  <tr><td class="bgalign" colspan="2">
    <a href="findregulators.php?type=doc&amp;genes=<?=$orfgene['orfname']?>&amp;t_pos=true&amp;t_neg=true&amp;evidence=plus">
    See documented transcription factors</a></td></tr>
<?php  } if (isset($orfgene['has_regpot'])) { ?>
  <tr><td class="bgalign" colspan="2">
    <a href="findregulators.php?type=pot&amp;genes=<?=$orfgene['orfname']?>">
    See potential transcription factors</a></td></tr>
<?php  } ?>
</table>
<?php
    }
    
    
    /*
     * printPositions
     */
    static private function printPositions($orfgene, $strand = "F") {
    $out = "<br/><br/><small>";
    $i = 1;
    if (isset($orfgene['reg_protein'])) {
        $out .= "<a href=\"view.php?existing=protein&amp;proteinname=";
        $out .= $orfgene['reg_protein'] . "\">";
        $out .= $orfgene['reg_protein'] . "</a>";
    }
    if (isset($orfgene['reg_consensus'])) {
        $out .= "&nbsp;-&nbsp;<a href=\"view.php?existing=consensus&amp;consensus=";
        $out .= $orfgene['reg_consensus'] . "&amp;proteinname=";
        $out .= $orfgene['reg_protein'] . "\">";
        $out .= $orfgene['reg_consensus'] . "</a>";
    }
    
    $fwr = ($strand == "F")? true: false;
    if ($fwr) {
        $positions = $orfgene['fwrpos'];
    } else {
        $positions = $orfgene['revpos'];
    }
    foreach ($positions as $pos => $len) {
        if ($strand == "F") $add = 0;
      else $add = $len;
      
      $out .= "<br/>$i. pos: ".(($pos + $add) - 1000)."  size: ".$len;
      $i++;
      }
      $out .= "</small>";
      return $out;
  }
    
  
    /*
     * printSeqWithPos
     */
    static function printSeqWithPos($sequence, $fwrpos, $strand = "F") {
        $inc = 0;
        $seqarray = explode("\n",$sequence);
        if ($strand == "F") {
            $inicio = "<pre>".array_shift($seqarray)."<br/>";
        } else {
            array_shift($seqarray);
      $inicio = "<pre> Complementary Strand<br/>";
      include_once('service/IUPAC.php');
      for ($i = count($seqarray)-1; $i >= 0; $i--) {
        $seqarray[$i] = IUPAC::complement($seqarray[$i]);
      }
            $inc = 0;
        }
          $linelen = strlen($seqarray[0]);
    $plainseq = join("<br/>", $seqarray);
    
    $FONTcount = 0;
    $BRsize = strlen("<br/>");
    $font = "<span style=\"background-color:#00FF00\">";
    $ffont = "</span>";
    $FONTsize = strlen("$font$ffont");

    foreach ($fwrpos as $p => $l) {
      $nlinesI = (int)($p/$linelen);
      $nlinesF = (int)(($p+$l)/$linelen);
      $pos = $p + $BRsize*$nlinesI + $FONTsize*$FONTcount + $inc;

      $newseq  = substr($plainseq, 0, $pos).$font; # antes FONT
      if ($nlinesI == $nlinesF) {
        $newseq .= substr($plainseq, $pos, $l); # entre FONT
        $newseq .= $ffont.substr($plainseq, $pos+$l); # depois FONT
      } else {
        $newseq .= substr($plainseq, $pos, $l+$BRsize);# entre FONT
        $newseq .= $ffont.substr($plainseq, $pos+$l+$BRsize); # depois FONT
      }
      $FONTcount++;
      $plainseq = $newseq;
    }
    return $inicio.$plainseq."</pre>";
    }
    
    /*
     * viewMenu
     */
    static function viewMenu($name, $o, $p) {
?>
   <div class="tab">
     <button class="tab<?=($name=="locus")?" sel":""?>" tabindex="0" onclick="location.href='view.php?existing=locus&amp;orfname=<?=$o?>';">Locus Info</button>
   </div>
   <div class="tab">
     <button class="tab<?=($name=="protein")?" sel":""?>" tabindex="0" onclick="location.href='view.php?existing=protein&amp;proteinname=<?=$p?>';">Protein Info</button>
   </div>
   <div class="tab">
     <button class="tab<?=($name=="go")?" sel":""?>" tabindex="0" onclick="location.href='view.php?existing=go&amp;orfname=<?=$o?>';">GO Info</button>
   </div>
    <div class="tab">
     <button class="tab<?=($name=="orthologs")?" sel":""?>" tabindex="0" onclick="location.href='view.php?existing=orthologs&amp;orfname=<?=$p?>';">Orthologs</button>
   </div>
</td></tr>
<tr><td class="align">
<?php
    }
    
    /*
     * protein
     */
    static function protein($protein, $extName, $extUrl) {
?>
<table border="0" summary="main content">
  <tr><th align="left">Protein Name</th>
    <td class="bgalign"><?=$protein['proteinname']?></td></tr>
  <tr><th align="left">Description</th>
    <td class="bgalign"><?=$protein['description']?></td></tr>
    <tr><th align="left">Aminoacid&nbsp;Sequence<br/><small>
      Download <a href="download.php?type=aminoacid&amp;id=<?=$protein['proteinname']?>">FASTA</a>
      </small>
  </th>
<!--    <a href="view.php?existing=protein&amp;proteinname=< ?=$protein['proteinname']?>"></a> -->
    <td class="bgalign"><pre><?=$protein['aminoacidsequence']?>&nbsp;</pre></td></tr>
<?php  if (isset($protein['consensus'])) { ?>
  <tr><th align="left">Transcription&nbsp;Factor<br/>Binding site(s)</th>
    <td class="bgalign">
<?php    foreach ($protein['consensus'] as $c) { ?>
    <a href="view.php?existing=consensus&amp;proteinname=<?=$protein['proteinname']?>&amp;consensus=<?=$c?>">
    <?=$c?>
    </a><br/>
<?php    } ?>
    </td></tr>
<?php  }
    if (isset($protein['has_regdoc'])) { ?>
  <tr><th class="left">Regulon</th>
    <td class="bgalign">
    <a href="vennregulon.php?proteinname=<?=$protein['proteinname']?>">
      See Venn diagram regulon</a> &nbsp;&nbsp;<img src="images/new.gif"/></td></tr>
  <tr><th class="left">Documented targets</th>
    <td class="bgalign">
    <a href="findregulated.php?type=doc&amp;tfs=<?=$protein['proteinname']?>&amp;t_pos=true&amp;t_neg=true&amp;evidence=plus">
    See documented regulated genes</a></td></tr>
<?php  } 
if ($protein['has_regpot']) { ?>
<tr><th class="left">Potential targets</th>
                <td class="bgalign">
<a href="findregulated.php?type=pot&amp;tfs=<?=$protein['proteinname']?>">
    See potential regulated genes</a>
    <small><span class="error">(Attention! Can return hundreds of genes)</span></small></td></tr>
<?php  } if (isset($extUrl)) { ?>
  <tr><th align="left">External source</th>
    <td class="bgalign">
    <a onclick="window.open(this.href); return false;" href="<?=$extUrl?><?=$protein['ext']?>">Open in <?=$extName?>
    <img alt="external link" src="images/external.png"/></a></td></tr>
<?php } ?>
</table>
<?php
    }

    static function getExtLinkGO($goid) {
      return "<a href=\"http://amigo.geneontology.org/amigo/term/$goid\""
        . "onclick=\"window.open(this.href); return false;\">"
        . "<img src=\"images/external.png\" alt=\"external link\" /></a>";
    }

    static function GOterm($go) {
      $onto = ($go['onto']=="function"?"Molecular function" :
        ($go['onto']=="process"?"Biological process":
        "Cellular component"));
?>
<table border="1" summary="main content">
  <tr><th align="left">Ontology</th>
    <td class="align"><?=$onto?></td></tr>
  <tr><th align="left">GO ID</th>
    <td class="align"><?=$go['goid']?></td></tr>
  <tr><th align="left">GO term</th>
    <td class="align">level <?=$go['depth']?>: <?=$go['term']?>
    <?=View::getExtLinkGO($go['goid'])?>
    </td></tr>
  <tr><td></td><td></td></tr>
  <tr><th align="left">Parent terms</th>
    <td class="align">
<?php
      print View::printGOTerms($go['parents']);
?>
    </td></tr>
  <tr><th align="left">Children terms</th>
    <td class="align">
<?php
      print View::printGOTerms($go['children']);
?>
    </td></tr>
  <tr><th aligh="left">Associated ORF/genes</th>
    <td class="align">
<?php
      $txt = "";
      foreach ($go['genes'] as $orf => $gene) {
        $elem = $orf;
        if ($orf!=$gene) {
          $elem .= "/$gene";
        }
        $txt .= "<a href=\"view.php?existing=go&orfname=$orf\">$elem</a><br/>";
      }
      print $txt;
?>
    </td></tr>
</table>
<?php
    }

    /*
     * GO
     */
    static function GO($go, $extName, $extUrl) {
?>
<table border="0" summary="main content">
  <tbody resource="view.php?existing=protein&amp;proteinname=<?=$go['protein']?>">
  <tr><th align="left">Protein Name</th>
    <td class="bgalign"><?=$go['protein']?></td></tr>
  <tr><th align="left">Description</th>
    <td class="bgalign"><?=$go['description']?></td></tr>
  </tbody>
<?php   if (isset($go['process'])) { # &rarr; ?>
  <tr><th align="left">GO Terms:<br/>Biological&nbsp;Process</th>
    <td class="bgalign">
<?php
      print View::printGOTerms($go['process']);
?>
    </td></tr>
<?php
  } if (isset($go['function'])) { # &rarr; ?>
  <tr><th align="left">GO Terms:<br/>Molecular&nbsp;Function</th>
    <td class="bgalign">
<?php
      print View::printGOTerms($go['function']);
?>
    </td></tr>
<?php } if (isset($go['component'])) { # &rarr; ?>
  <tr><th align="left">GO Terms:<br/>Cellular&nbsp;Component</th>
    <td class="bgalign">
<?php
      print View::printGOTerms($go['component']);
?>
    </td></tr>
<?php } if (isset($extUrl)) { ?>
  <tr><th class="left">External source</th>
    <td class="bgalign">
    <a onclick="window.open(this.href); return false;" href="<?=$extUrl?><?=$go['ext']?>">Open in <?=$extName?>
    <img alt="external link" src="images/external.png"/></a></td></tr>
<?php } ?>
</table>
<?php
  }


  static function printGOTerms($goTerms, $level = true) {
    $first = true;
    $toPrint = "";
    $linkView = "<a href=\"view.php?existing=goterm&amp;goid=";
    $linkGO1  = "<a href=\"http://amigo.geneontology.org/amigo/term/";
    $linkGO2  = "onclick=\"window.open(this.href); return false;\"><img src=\"images/external.png\" alt=\"external link\" /></a>";
    foreach ($goTerms as $id => $goObj) {
      if (!$first) $toPrint .= "<br/>";
      if ($level) {
        $toPrint .= "level {$goObj['depth']}: $linkView$id\">{$goObj['term']}</a> $linkGO1$id\" title=\"$id\" $linkGO2";
      } else {
        $toPrint .= "<a href=\"view.php?existing=goterm&amp;goid=$id\">$goObj</a>";
      }
      $first = false;
    }
    return $toPrint;
  }
}
?>
