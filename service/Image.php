<?php
class Image {

	function __construct() {
	}
	function __destruct() {

	}

	function bindingSites($matches, $motifs, $seqSize, $local, $tf2motif, $docs, $aORFids=NULL) {

		$linelen = 1800; $linethick = 30; $linei = 150;
		
                $base = 80; $space = 110;
                $colorspace = 1750 / count($tf2motif); $actualColor = 0;

		$seq_names = array_keys($matches);

		$im = imagecreate($linelen + 2*$linei,$space*count($seq_names));

		$bgcolor   = imagecolorallocate($im, 255, 255, 255);
		$rectcolor = imagecolorallocate($im, 150, 150, 255);
		$txtcolor  = imagecolorallocate($im, 0, 0, 0);
		$aColors = array();

		$i = 0;

		$maxSize = 0;
		foreach ($seq_names as $name) {
			if ($seqSize[$name] > $maxSize)
				$maxSize = $seqSize[$name];
		}

		// INITIALIZES THE RECTANGLE FOR EACH ORF / GENE SEQUENCE

		foreach ($seq_names as $name) { 
			
			$base_x = $linelen * (1 - $seqSize[$name] / $maxSize);
			$x = $linelen;
			$y = $base + $space*$i;
			/* Promoter */
			imagefilledrectangle($im, $base_x, $y - $linethick, $linelen, $y + $linethick, $rectcolor);
			/* Arrow tip */
			$values = array(
				0 => 10 + $x, 1 => $y - $linethick,
				2 => 200 + $x, 3 => $y - $linethick,
				4 => 225 + $x, 5 => $y,
				6 => 200 + $x, 7 => $y + $linethick,
				8 => 10 + $x, 9 => $y + $linethick);

			imagefilledpolygon($im, $values, 5, $rectcolor);
			/* Gene name */
				
			if (!isset($aORFids)) { $label = $name;}
			else {	
				$label = (isset($aORFids[$name]['gene'])&&($aORFids[$name]['gene']!='Uncharacterized')&&($aORFids[$name]['gene']!=$aORFids[$name]['orf']))?
					$aORFids[$name]['gene']."/".$aORFids[$name]['orf']:$aORFids[$name]['orf']; 
			}
			imagestring($im, 8, $x + 12, $y-13, $label, $txtcolor);
			/* Promoter length */
			if (isset($aORFids[$name]['species'])){ imagestring($im, 6, $x + 12, $y-50, $aORFids[$name]['species'] , $txtcolor); 	}
			imagestring($im, 6, $x + 12, $y+10, $seqSize[$name] . " bp" , $txtcolor); 
     

			/* TFs */

			foreach (array_keys($tf2motif) as $tf) {

				if ((isset($_POST['redone']) && !isset($_POST[$tf])) or (($docs) && !in_array($tf, $docs)) ) {
                                        $aColors[$tf]['isset'] = false;
                                } else { $aColors[$tf]['isset'] = true; }

			foreach ($tf2motif[$tf] as $key => $motif) {
				
				if (isset($matches[$name][$motif]['fwr'])) foreach ($matches[$name][$motif]['fwr'] as $p) {

						/* position */
				    		$pos = $base_x + (int)$p * $linelen / $maxSize;
				    		/* color */
				    		if (!isset($aColors[$tf]['color'])) {
							list($r, $g, $b) = $this->distributeColors($actualColor);
							$actualColor += $colorspace;
							$tfColor = imagecolorallocate($im, $r, $g, $b);
                                                	$aColors[$tf]['color'] = $tfColor;
                                                	$aColors[$tf]['r'] = $r;
                                                	$aColors[$tf]['g'] = $g;
                                                	$aColors[$tf]['b'] = $b;

						} else {
							$tfColor = $aColors[$tf]['color'];
			    			}
			    			
			    			if ($aColors[$tf]['isset']) {
				    			imagefilledrectangle($im, $pos, $y - $linethick, $pos+16, $y + $linethick, $tfColor);
				    			imagestringup($im, 6, $pos, $y-$linethick-2, (int)$p - $seqSize[$name], $txtcolor);
				    			imagestringup($im, 6, $pos, $y+$linethick-1, $tf, $bgcolor);
			    			}
					}

				if (isset($matches[$name][$motif]['rev'])) foreach ($matches[$name][$motif]['rev'] as $p) {


						
						/* position */	
						#  $p = $p + $l;
						
						$pos = $base_x + (int)$p * $linelen / $maxSize;
				    		/* color */
				    		if (!isset($aColors[$tf]['color'])) {
							list($r, $g, $b) = $this->distributeColors($actualColor);
							$actualColor += $colorspace;
							$tfColor = imagecolorallocate($im, $r, $g, $b);
							$aColors[$tf]['color'] = $tfColor;
                                                	$aColors[$tf]['r'] = $r;
                                                	$aColors[$tf]['g'] = $g;
                                                	$aColors[$tf]['b'] = $b;
				    		} else {
							$tfColor = $aColors[$tf]['color'];
				    		}

				    		if ($aColors[$tf]['isset']) {
				    			imagefilledrectangle($im, $pos, $y - $linethick, $pos+16, $y + $linethick, $tfColor);
				    			imagestringup($im, 6, $pos, $y-$linethick-4, (int)$p - $seqSize[$name], $txtcolor);
				    			imagestringup($im, 6, $pos, $y+$linethick-2, $tf, $bgcolor);
				    		}
			    		}
			}
			}
			$i++;
		}

		$file = rand() . ".png";
                imagePNG($im, $local . $file);
                imagedestroy($im);

		foreach (array_keys($aColors) as $tf) {
                        $imgtmp = imagecreate(50,13);
                        $r = $aColors[$tf]['r'];
                        $g = $aColors[$tf]['g'];
                        $b = $aColors[$tf]['b'];
                        $bg = imagecolorallocate($imgtmp, $r, $g, $b);
                        imagefilledrectangle($imgtmp, 0, 0, 50, 13, $bg);
                        $txt = imagecolorallocate($imgtmp, 255-$r, 255-$g, 255-$b);
                        imagestring($imgtmp, 2, 1, -1, $tf, $txt);
                        $f = rand() . ".png";
                        imagePNG($imgtmp, $local . $f);
                        imagedestroy($imgtmp);
			$aColors[$tf]['file'] = $f;
                }
			
		return array($file,$aColors);
    }
    
    
	function promoterBindingTFs($aORFGenes, $reg, $fwr, $rev, $local, $docs) {



        $linelen = 700; $linethick = 15; $linei = 40;
		$base = 40; $space = 60;
		$colorspace = 50; $actualColor = 0;

		$im = imagecreate($linelen + 2*$linei,$space*count($reg));

		$bgcolor   = imagecolorallocate($im, 255, 255, 255);
		$rectcolor = imagecolorallocate($im, 150, 150, 255);
		$txtcolor  = imagecolorallocate($im, 0, 0, 0);
		$aColors = array();
		
		$i = 0;

		foreach ($aORFGenes as $orf => $gene) {
			$x = $linelen;
			$y = $base + $space*$i;
			/* Promoter */
			imagefilledrectangle($im, 0, $y - $linethick, $linelen, $y + $linethick, $rectcolor);
			/* Arrow tip */
			$values = array(
				0 => 10 + $x, 1 => $y - $linethick,
				2 => 40 + $x, 3 => $y - $linethick,
				4 => 60 + $x, 5 => $y,
				6 => 40 + $x, 7 => $y + $linethick,
				8 => 10 + $x, 9 => $y + $linethick);
			imagefilledpolygon($im, $values, 5, $rectcolor);
			/* Gene name */
			imagestring($im, 3, $x + 12, $y - 12, "Gene", $txtcolor);
			imagestring($im, 2, $x + 12, $y, $gene, $txtcolor);

			/* TFs */
			foreach (array_keys($reg[$orf]) as $tf) {

				if ( isset($_POST['redone']) && ( (!isset($_POST[$tf])) or (in_array($tf, $docs)) ) ) {
				    $aColors[$tf]['isset'] = false;
				} else $aColors[$tf]['isset'] = true;			    
			    
			    foreach (array_keys($reg[$orf][$tf]) as $id) {

					if (isset($fwr[$id]))
					foreach ($fwr[$id] as $p => $l) {
			            /* position */
					    $pos = (int)$p * $linelen / 1000;
					    /* color */
					    if (!isset($aColors[$tf]['color'])) {
							list($r, $g, $b) = $this->distributeColors($actualColor);
							$actualColor += $colorspace;
							$tfColor = imagecolorallocate($im, $r, $g, $b);
							$aColors[$tf]['color'] = $tfColor;
							$aColors[$tf]['r'] = $r;
							$aColors[$tf]['g'] = $g;
							$aColors[$tf]['b'] = $b;
					    } else {
							$tfColor = $aColors[$tf]['color'];
					    }
		        
					    if ($aColors[$tf]['isset']) {
					    imagefilledrectangle($im, $pos, $y - $linethick, $pos+8, $y + $linethick, $tfColor);
					    imagestringup($im, 1, $pos, $y-$linethick-2, (int)$p - 1000, $txtcolor);
					    imagestringup($im, 1, $pos, $y+$linethick-1, $tf, $bgcolor);
					    }
			        }
			        if (isset($rev[$id]))
			        foreach ($rev[$id] as $p => $l) {
			            /* position */
			            $p = $p + $l;
					    $pos = (int)$p * $linelen / 1000;
					    /* color */
					    if (!isset($aColors[$tf]['color'])) {
							list($r, $g, $b) = $this->distributeColors($actualColor);
							$actualColor += $colorspace;
							$tfColor = imagecolorallocate($im, $r, $g, $b);
							$aColors[$tf]['color'] = $tfColor;
							$aColors[$tf]['r'] = $r;
							$aColors[$tf]['g'] = $g;
							$aColors[$tf]['b'] = $b;							
					    } else {
							$tfColor = $aColors[$tf]['color'];
					    }
		        
					    if ($aColors[$tf]['isset']) {
					    imagefilledrectangle($im, $pos, $y - $linethick, $pos+8, $y + $linethick, $tfColor);
					    imagestringup($im, 1, $pos, $y-$linethick-2, (int)$p - 1000, $txtcolor);
					    imagestringup($im, 1, $pos, $y+$linethick-1, $tf, $bgcolor);
					    }
			        }
			    }
			}
			$i++;
		}
		$file = rand() . ".png";
		imagePNG($im, $local . $file);
		imagedestroy($im);


		foreach (array_keys($aColors) as $tf) {
			$imgtmp = imagecreate(50,13);
			$r = $aColors[$tf]['r'];
			$g = $aColors[$tf]['g'];
			$b = $aColors[$tf]['b'];
			$bg = imagecolorallocate($imgtmp, $r, $g, $b);
			imagefilledrectangle($imgtmp, 0, 0, 50, 13, $bg);
			$txt = imagecolorallocate($imgtmp, 255-$r, 255-$g, 255-$b);
			imagestring($imgtmp, 2, 1, -1, $tf, $txt);
			$f = rand() . ".png";
			imagePNG($imgtmp, $local . $f);
			imagedestroy($imgtmp);
			$aColors[$tf]['file'] = $f;
		}

		return array($file, $aColors);
    }


    /*
     * distributeColors
     */
	private function distributeColors($num) {
		$r=0; $g=0; $b=0;
		
		if ($num < 255) {
			$r += $num;
		} elseif ($num < (255*2)) {
			$r = 255;
			$g = $num - 255;
		} elseif ($num < (255*3)) {
			$g = 255;
			$r = (255*3) - $num;
		} elseif ($num < (255*4)) {
			$g = 255;
			$b = $num - (255*3);
		} elseif ($num < (255*5)) {
			$b = 255;
			$g = (255*5) - $num;
		} elseif ($num < (255*6)) {
			$b = 255;
			$r = $num - (255*5);
		} elseif ($num - (255*7)) {
			$b = 255;
			$r = 255;
			$g = $num - (255*6);
		}
		return array($r, $g, $b);
	}

	
	/*
	 * generateDotGraph
	 */
	function generateDotGraph($reg, $web, $ext = "svg", $aTFs, $aORFGenes) {
		$withimage = false;
		$uniqueTFs = array();
		$uniqueOrfs = array();
		$i = 0;
		$it = 0;
		$c = array("red", "blue", "green", "yellow", "magenta", "pink", 
			"indigo", "blueviolet", "chocolate", "forestgreen");
		$l = count($c);

		$graph = "digraph test {\n";
		$graph .= "\t node [shape=box];\n";
		foreach (array_keys($reg) as $tfid) {
			$tf = $aTFs[$tfid]['prot'];
			$it++;
			$color = $c[$it%$l];
			switch($tf{strlen($tf)-1}) {
			case 'p':
			case 'P':
				$t = substr($tf,0,strlen($tf) - 1);
				break;
			default:
				$t = $tf;
			}
			//$t = substr($tf,0,strlen($tf) - 1);
			$graph .= "\t\"$t\" [style=filled];\n";
			foreach ($reg[$tfid] as $key => $orfid) {
				$gene = $aORFGenes[$orfid]['gene'];
				$g = strtolower($gene);
				$g{0} = strtoupper($g{0});
				$graph .= "\t\"$t\" -> \"$g\" [color=$color]\n";
				$i++;
			}
		}
		$graph .= "}";
		# concentrators=true
		# size=x,y

		$tmp = rand();
		$namedot = "image_$tmp.dot";
		$nameext = "image_$tmp.$ext";
		$name    = $namedot;
		
		/* Save the dot file */
		$path = $web->getProp("path.tmp.local");
		if (file_put_contents($path.$namedot, $graph) === false) {
			throw new Exception("Error writing graph description to disk!");
		}
		$dotpath = $web->getProp("prog.dot.path");
		$dotbin  = $web->getProp("prog.dot.bin");
		if ($i < 10000) {
			/* Generate the output file */
			system($dotpath."$dotbin -T$ext $path"."$namedot -o $path".$nameext, $retval);
			if ($retval != 0) {
				throw new Exception("Error generating the graph image!");
			}
			$name = $nameext;
			$withimage = true;
		}

		$url = $web->getProp("path.tmp.url");
		return array($url, $name, $withimage);
	}

	function generateD3jsGraph($regs) {
		$json = "";
		$aSingletons = array();
		foreach (array_keys($regs) as $tf) {
			$t = substr($tf,0,strlen($tf) - 1);
			if (!empty($json)) $json .= ",";
			$json .= "\n{\"name\":\"com.yeastract.proteinname.";
			$json .= $tf."\",\"imports\":[";
			$genes = "";
			foreach ($regs[$tf] as $o => $gene) {
				if (!empty($genes)) $genes .= ",";
				$g = str_replace(",","",strtolower($gene));
				$g{0} = strtoupper($g{0});
				if (array_key_exists($g."p", $regs)) {
					$genes .= "\"com.yeastract.proteinname.{$g}p\"";
				} else {
					$genes .= "\"com.yeastract.genename.$g\"";
					if (!in_array($g,$aSingletons)) {
						array_push($aSingletons,$g);
					}
				}
			}
			$json .= "$genes]}";
		}
		foreach ($aSingletons as $g) {
			$json .= ",\n{\"name\":\"com.yeastract.genename.$g\",\"imports\":[]}";
		}
		$json = "[$json\n]";
		return $json;
	}
		
}
?>
