<?php
class GOterm {
	private $_id;
	private $_term;
	private $_depth;
	private $_aGenes;

	function __construct($id, $term, $depth) {
		$this->_id = $id;
		$this->_term = $term;
		$this->_depth = $depth;
		$this->_aGenes = array();
	}
	function __destruct() {
	}

	function getId()    { return $this->_id;    }
	function getTerm()  { return $this->_term;  }
	function getDepth() { return $this->_depth; }
	function getGenes() { return $this->_aGenes;  }

	function setid($id)       { $this->_id = $id;        }
	function setterm($term)   { $this->_term = $term;    }
	function setdepth($depth) { $this->_depth = $depth;  }
	function addGene($g)      { $this->_aGenes[$g] = $g; }
	function addGenes($genes) {
		foreach ($genes as $g) {
			$this->_aGenes[$g] = $g;
		}
	}
}
?>
