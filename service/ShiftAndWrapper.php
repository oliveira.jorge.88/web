<?php
class ShiftAndWrap {

	public $_boxes;
	public $_matches;

	function __construct($motif,$sequences,$subst,$simple=false) {

		include_once('ShiftAnd.php');

		// this array is always ODD number -> object, gaps, object, etc	

		$this->_matches = $this->getMatches($this->recursiveSplit($motif),$sequences,$subst,$simple);

	}

	private function getMatches($aBoxes,$sequences,$subst,$simple){ // simple gives only obj[tf][orf] = true

		if (!function_exists('array_key_first')) {
                  
			function array_key_first(array $arr) {
				foreach($arr as $key => $unused) {
					return $key;
				}
				return NULL;
			}
		}


		$retObj = [];
		$tf2seq = [];

		include_once('service/IUPAC.php');

		foreach($sequences as $name => $seq){

			$matches = [];

			if ( ($simple) && (isset($tf2seq[$name])) ) continue;

			for ($i=0 ; $i<count($aBoxes); $i++) {
				// align first box
				if ($i==0){
					$matches['F'][$i] = $aBoxes[$i]->find($seq,$subst);
					$matches['R'][$i] = $aBoxes[$i]->find(strrev(IUPAC::complement($seq)),$subst);
					if (empty($matches['F'][$i]) && empty($matches['R'][$i])) break;
				}
				elseif ( $i & 1 ) {
					//odd, do nothing
				}
				else {
					// even, verify previous matches
					if (!empty($matches['F'][$i-2])) {
						$start_f = array_key_first($matches['F'][$i-2]);
						$box_len_f = $matches['F'][$i-2][$start_f];
						$min_gap = $aBoxes[$i-1][0];
						$max_gap = $aBoxes[$i-1][1];
						$offset_f = $start_f + $box_len_f + $min_gap;
						$matches['F'][$i] = $aBoxes[$i]->find(substr($seq,$offset_f),$subst); // only try to match from the last match + size + gap
						// fix offset and validate matches based on previous matches
						foreach ($matches['F'][$i] as $oldkey => $len){
							$newkey = $oldkey+$offset_f;
							$keep=false;
							foreach($matches['F'][$i-2] as $pos=>$size){
								$gap = $newkey-$size-$pos;
								if (($gap>=$min_gap)&&($gap<=$max_gap)){
									$keep=true;
									$newkey = $pos;
									$newlen = $size + $gap + $len;
									break; // as long as one box supports this match we are happy
								}
							}
							if ($keep) $matches['F'][$i][$newkey] = $newlen;
							unset($matches['F'][$i][$oldkey]);
						}
						unset($matches['F'][$i-2]);
					}
					// do the same for reverse strand 
					if (!empty($matches['R'][$i-2])) {
						$start_r = array_key_first($matches['R'][$i-2]);
						$box_len_r = $matches['R'][$i-2][$start_r];
						$min_gap = $aBoxes[$i-1][0];
                                                $max_gap = $aBoxes[$i-1][1];
					#	$gap = $aBoxes[$i-1][0];
						$offset_r = $start_r+$box_len_r+$min_gap;
						$matches['R'][$i] = $aBoxes[$i]->find(substr(strrev(IUPAC::complement($seq)),$offset_r),$subst);
						foreach ($matches['R'][$i] as $oldkey => $len){
							$newkey = $oldkey+$offset_r;
							$keep=false;
							foreach($matches['R'][$i-2] as $pos=>$size){
								$gap = $newkey-$size-$pos;
								if (($gap>=$min_gap)&&($gap<=$max_gap)){
									$keep=true;
									$newkey = $pos;
									$newlen = $size + $gap + $len;
									break; // as long as one box supports this match we are happy
								}
							}
							if ($keep) $matches['R'][$i][$newkey] = $newlen;
							unset($matches['R'][$i][$oldkey]);
						}
						unset($matches['R'][$i-2]);
					}

				}
				// when we get to the last box, create the retObj
				if ($i==(count($aBoxes)-1)) {
					if (!empty($matches['F'][$i])) {
						$retObj[$name]['fwr'] = $matches['F'][$i];
					}
					if (!empty($matches['R'][$i])) {
						$retObj[$name]['rev'] = $matches['R'][$i];
					}
					if (!empty($matches['F'][$i]) || !empty($matches['R'][$i])){
						if ($simple) $tf2seq[$name] = true;
					}
				}
			}
		}
	
	if ($simple) return $tf2seq;
	return $retObj;

	}

	private function recursiveSplit($motif) { 
		$retObj = [];
		$gap = [];

		// if it is simple, just initialize SA and send it 
                if (strpos($motif, "{") === false) { $subSA = new ShiftAnd($motif) ; return [$subSA] ; }

		// if it is complex, divide it 
                if (strpos($motif, ",") !== false) {
			$aMotifs = preg_split('/[N]{(\d*),(\d*)}/', $motif);
			preg_match_all('!\d+,\d+!', $motif, $gap);
		}
		// if it is simple, but requires expansion
                else {
			$retStr = "";

                        for ($i=0, $last=0; $i<strlen($motif); $i++) {
                                if ($motif{$i} == '{') {
                                        $i++;
                                        $n = "";
                                        while ($motif{$i} != '}') {
                                                $n = $n . $motif{$i};
                                                $i++;
                                        }
                                        for ($j=1; $j < $n; $j++) {
                                                $retStr .= $motif{$last};
                                        }
                                } else {
                                        $retStr .= $motif{$i};
                                        $last = $i;
                                }
			}
                        $aMotifs = [$retStr];

		}
		$i= 0;
		// Analyze all the created boxes
		foreach ($aMotifs as $box){
			if (strpos($box, "{") === false) {
				// create the shiftAnd object
				$subSA = new ShiftAnd($box);
				$retObj[] = $subSA;
				if (!empty($gap[0][$i])) $retObj[] = preg_split('/,/', $gap[0][$i]);
			}
			else {
				// if it still has complexities, call it again
			       $aux= $this->recursiveSplit($box);
			       foreach ($aux as $subaux){
                                       $retObj[] = $subaux;
			       }
			       if (!empty($gap[0][$i])) $retObj[] = preg_split('/,/', $gap[0][$i]);
			}
			$i++;
                }

                return $retObj;
        }

}
?>
