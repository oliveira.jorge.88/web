<?php
class ShiftAnd {

    private $_pattern;
    private $_p_size;
    private $_text;
    private $_t_size;
    
    private $_nboxes;
    private $_boxEndBits;
    private $_boxBeginBits;
    private $_bit2box;
    private $_gaps;
    private $_vbit;
    private $_win;
    private $_sizebox;
    
    private $_bbit;
    private $_ws;
    private $_mask;
    private $_s;
    
    private function left($n) { return ($n & (-$n));    }
    private function lpop($n) { return ($n & ($n - 1)); }
    private function fastlog2($n) {
	    $log = -1;
	    while($n != 0) {
	        $log++;
	        $n = $n >> 1;
	    }
	    return $log;
	}	
    private function bin2string($bin) {
        $res = "";
        for ($p = 31; $p >= 0; $p--) {
            $res .= ($bin & (1 << $p))? "1" : "0";
        }
        return $res . " <- $bin\n";
    }
    
    function __construct($pattern) {

	    # all sequences must be uppercase
	    $this->_pattern = strtoupper(trim($pattern));
            $this->_p_size  = strlen($this->_pattern);
            $this->_nboxes  = 1;
                $b = ($this->_p_size - 1);
                $p = (1 /*sizeof(uint)*/ * 8);
                $this->_boxEndBits = 0x01 << ($b % $p);
                $this->_boxBeginBits = ($this->_boxEndBits << 1) | 1;

		/* Sigma preprocess */
		$this->_s = array(
			'A' => 0, 'C' => 0, 'T' => 0, 'G' => 0,
			'R' => 0, 'Y' => 0, 'S' => 0, 'W' => 0, 'K' => 0, 'M' => 0,
			'B' => 0, 'D' => 0, 'H' => 0, 'V' => 0,
			'N' => 0, 'X' => 0
		);
		$this->_mask = 1;
		for ($i=0; $i < $this->_p_size; $i++) {
			$this->_s[$this->_pattern{$i}] |= $this->_mask;
			$this->_mask = ($this->_mask << 1);
		}
		$this->_s['A'] |= $this->_s['R'] | $this->_s['W'] | $this->_s['M'] | $this->_s['D'] | $this->_s['H'] | $this->_s['V'] | $this->_s['N'] | $this->_s['X'] ;
		$this->_s['C'] |= $this->_s['Y'] | $this->_s['S'] | $this->_s['M'] | $this->_s['B'] | $this->_s['H'] | $this->_s['V'] | $this->_s['N'] | $this->_s['X'] ;
		$this->_s['T'] |= $this->_s['Y'] | $this->_s['W'] | $this->_s['K'] | $this->_s['B'] | $this->_s['D'] | $this->_s['H'] | $this->_s['N'] | $this->_s['X'] ;
		$this->_s['G'] |= $this->_s['R'] | $this->_s['S'] | $this->_s['K'] | $this->_s['B'] | $this->_s['D'] | $this->_s['V'] | $this->_s['N'] | $this->_s['X'] ;
		
		$this->_s['R'] = $this->_s['A'] | $this->_s['G'];
		$this->_s['Y'] = $this->_s['C'] | $this->_s['T'];
		$this->_s['S'] = $this->_s['C'] | $this->_s['G'];
		$this->_s['W'] = $this->_s['A'] | $this->_s['T'];
		$this->_s['K'] = $this->_s['G'] | $this->_s['T'];
		$this->_s['M'] = $this->_s['A'] | $this->_s['C'];
		
		$this->_s['B'] = $this->_s['C'] | $this->_s['T'] | $this->_s['G'];
		$this->_s['D'] = $this->_s['A'] | $this->_s['G'] | $this->_s['T'];
		$this->_s['H'] = $this->_s['C'] | $this->_s['T'] | $this->_s['A'];
		$this->_s['V'] = $this->_s['C'] | $this->_s['A'] | $this->_s['G'];
		
		$this->_s['N'] = $this->_s['A'] | $this->_s['C'] | $this->_s['T'] | $this->_s['G'];
		$this->_s['X'] = $this->_s['A'] | $this->_s['C'] | $this->_s['T'] | $this->_s['G'];

    }
    
    function find($text = "", $subst = 0) {
	    
	    $retObj = array();

        $this->_text = trim($text);
        $this->_t_size = strlen($this->_text);

		$S = array();
		for ($i=0; $i<=$subst; $i++) { $S[$i] = 0; }
		$i = 0;
		$mask = ($this->_mask >> 1);

		while($i < $this->_t_size) {
#print $this->_text{$i} ." ";
		    $V = ($S[0] << 1) | $this->_boxBeginBits;
		    $S[0] = (($S[0] << 1) | 1 ) & $this->_s[$this->_text{$i}];
		    $j = 1;
		    while($j <= $subst) {
			    $W = ($S[$j] << 1) | $this->_boxBeginBits;
				$S[$j] = (($S[$j] << 1) & $this->_s[$this->_text{$i}]) | $V;
				$V = $W;
				$j++;
			}

			$j = 0;
			while($j <= $subst) {
				if($S[$j] & $mask) {
				    $res = $i - $this->_p_size + 1;
				    $retObj[$res] = $this->_p_size;
					$j = $subst;
#print "[$i-".$this->_p_size."+1=$res] ";
				}
				$j++;
			}
			$i++;
		}
		return $retObj;
	}

}
?>
