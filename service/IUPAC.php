<?php
class IUPAC {
    
	static function complement($sequence) {
		for ($i = 0; $i < strlen($sequence); $i++) {
			switch($sequence{$i}) {
				case 'A': $sequence{$i} = 'T'; break;
				case 'T': $sequence{$i} = 'A'; break;
				case 'G': $sequence{$i} = 'C'; break;
				case 'C': $sequence{$i} = 'G'; break;
				# Stay the same :)
				#case 'W': $sequence{$i} = 'W'; break;
				#case 'S': $sequence{$i} = 'S'; break;
				case 'R': $sequence{$i} = 'Y'; break;
				case 'Y': $sequence{$i} = 'R'; break;
				case 'M': $sequence{$i} = 'K'; break;
				case 'K': $sequence{$i} = 'M'; break;
				
				case 'D': $sequence{$i} = 'H'; break;
				case 'H': $sequence{$i} = 'D'; break;
				case 'V': $sequence{$i} = 'B'; break;
				case 'B': $sequence{$i} = 'V'; break;
			}
		}
		return $sequence;
	}

	static function expandIUPAC($iupac) {
		$iupac = str_replace("W", "[AT]", $iupac);
		$iupac = str_replace("S", "[CG]", $iupac);
		$iupac = str_replace("R", "[AG]", $iupac);
		$iupac = str_replace("Y", "[TC]", $iupac);
		$iupac = str_replace("M", "[AC]", $iupac);
		$iupac = str_replace("K", "[TG]", $iupac);

		$iupac = str_replace("D", "[ATG]", $iupac);
		$iupac = str_replace("H", "[ATC]", $iupac);
		$iupac = str_replace("V", "[ACG]", $iupac);
		$iupac = str_replace("B", "[TGC]", $iupac);

		$iupac = str_replace("N", "[ATGC]", $iupac);

		return $iupac;
	}
}
?>