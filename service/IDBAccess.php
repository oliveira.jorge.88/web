<?php
/* Generic Interface for a DB Access
 * calls the specific class that represents the
 * desired connection
 */
class IDBAccess {
	private $_db;
	private $_path;
   
    function __construct($path = "", $type = "mysql")
    {
			$this->_path = $path;
        $dbclass = "DB$type";
        $this->_db = new $dbclass;
    }
    
    function __destruct() {
    }

		function openDB($access = 'r') {
			try {
				include($this->_path . 'conf/dbaccess.properties.php');
				$host   = $dbaccess['Host'];
				$dbname = $dbaccess['DBName'];
				$user   = $dbaccess['ReaderUser'];
				$pass   = $dbaccess['ReaderPass'];
				if ($access == 'w') {
					$user = $dbaccess['WriterUser'];
					$pass = $dbaccess['WriterPass'];
				}
				$this->_db->open($host, $dbname, $user, $pass);
			} catch (Exception $e) {
				print "<hr/>" . $e->getMessage() . "<hr/>";
			}
		}

    function getScriptAccess() {
        include('dbaccess.properties.php');
       	$dbname = $dbaccess['DBName'];
       	$user = $dbaccess['AdminUser'];
        $pass = $dbaccess['AdminPass'];
        return "$dbname $user $pass";
    }

    function insertUpdate($query)
    {
        return $this->_db->insertUpdate($query);
    }

    function query($name,$query)
    {
        return $this->_db->query($name,$query);
    }

    function num_rows($name)
    {
        return $this->_db->num_rows($name);
    }

    function nextObject($name)
    {
        return $this->_db->nextObject($name);
    }

    function getObject($name,$query)
    {
        return $this->_db->getObject($name,$query);
    }

    function freeResult($name)
    {
        $this->_db->freeResult($name);
    }

    function dataSeek($name,$rowNumber)
    {
        return $this->_db->dataSeek($name,$rowNumber);
    }

}


/* Specific SQL connection - MySQL Functions
 * Handle multiple results simultaneous
 *  - using $hash{'result_name'}
 */
class DBmysql {
    private $_link;
    private $_result;
    
    function __construct() {
    }
    
    function __destruct() {
		mysqli_close($this->_link);
        unset($this->_link);
    }
    
  	/*************************************************************************/
    
    function open($host, $dbname, $user, $pass)
    {
        /* Open a mysql connection */
			$this->_link = mysqli_connect($host, $user, $pass, $dbname);
			if (mysqli_connect_error()) {
				throw new Exception("Could not connect " . $this->error());
			}
    }

		function getObject($name, $query) {
			$field = false;
			if ($result = $this->_link->query($query)) {
				$obj = $result->fetch_object();
				if ($obj) {
					$field = $obj->$name;
				}
				$result->close();
			}
			return $field;
    }

    function insertUpdate($query)
    {
        return $this->_link->query($query);
    }

    function error()
    {
        return "(" . mysqli_connect_errno() . ") " . mysqli_connect_error();
    }

    function query($name, $query) {
			if (isset($this->_result{$name})) {
				unset($this->_result{$name});
			}
			$this->_result{$name} = $this->_link->query($query);
			if (!$this->_result{$name}) {
				unset($this->_result{$name});
				throw new Exception("IDBAccess::query: " + $this->_link->error);
			}
			if (mysqli_num_rows($this->_result{$name}) == 0) {
				$this->_result{$name}->close();
				unset($this->_result{$name});
				return false;
			}
      return true;
    }

    function num_rows($name)
    {
        if (isset($this->_result{$name})) {
            return mysqli_num_rows($this->_result{$name});
        }
        else return 0;
    }

    /* returns an array with the next obj fields
     * return false otherwise
     * $name - result name (handling multiple results)
     */
		function nextObject($name) {
			if (!isset($this->_result{$name}))
				return false;
			$obj = $this->_result{$name}->fetch_array(MYSQLI_ASSOC);
			return $obj;
		}

		function freeResult($name) {
			if (isset($this->_result{$name})) {
				$this->_result{$name}->close();
			}
			unset($this->_result{$name});
		}

    function dataSeek($name,$rowNumber)
    {
        return mysql_data_seek($this->_result{$name},$rowNumber);
    }
}


/* Specific SQL connection - postgresSQL Functions
 * Handle multiple results simultaneous
 *  - using $hash{'result_name'}
 */
class DBpostgres {
    private $_link;
    private $_result;
    
    function open($host, $dbname, $user, $pass)
    {
        $connectionString  = "host=$host ";
        $connectionString .= "user=$user ";
        $connectionString .= "password=$pass ";
        $connectionString .= "dbname=$dbname";

        /* Open a postgres connection */
        $this->_link = pg_connect($connectionString)
        or die("Could not connect: ".pg_last_error());
    }
    
    function close()
    {
        pg_close($this->_link);
    }

    function getObject($name,$query)
    {
        $field = false;
        $result = pg_query($this->_link, $query);
        if (@pg_num_rows($result) > 0) {
            $obj = pg_fetch_object($result, 0);
            $field = $obj->$name;
            pg_free_result($result);
        }
        return $field;
    }

    function insertUpdate($query)
    {
        return pg_query($this->_link, $query);
    }

    function error()
    {
        return pg_last_error($this->_link);
    }

    function query($name,$query) 
    {
        if (isset($this->_result{$name}))
            unset($this->_result{$name});
        $this->_result{$name} = pg_query($this->_link, $query);
        if ($this->_result{$name} &&
            (pg_num_rows($this->_result{$name}) > 0)) {
            return true;
        } else {
            unset($this->_result{$name});
            return false;
        }
    }

    function num_rows($name)
    {
        if (isset($this->_result{$name})) {
            return pg_num_rows($this->_result{$name});
        }
        else return 0;
    }

    /* returns an array with the next obj fields
     * return false otherwise
     * $name - result name (handling multiple results)
     */
    function nextObject($name)
    {
        if (!isset($this->_result{$name}))
            return false;
        $obj = _fetch_array($this->_result{$name},MYSQL_ASSOC);
        return $obj;
    }

    function freeResult($name)
    {
        if (isset($this->_result{$name}))
            pg_free_result($this->_result{$name});
        unset($this->_result{$name});
    }

    function dataSeek($name,$rowNumber)
    {
        return pg_result_seek($this->_result{$name}, $rowNumber);
    }
}
?>
