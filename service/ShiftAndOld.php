<?php
class ShiftAnd {
    private $_pattern;
    private $_p_size;
    private $_text;
    private $_t_size;
    
    private $_nboxes;
    private $_boxEndBits;
    private $_boxBeginBits;
    private $_bit2box;
    private $_gaps;
    private $_vbit;
    private $_win;
    private $_sizebox;
    
    private $_bbit;
    private $_ws;
    private $_mask;
    private $_s;
    
    private function left($n) { return ($n & (-$n));    }
    private function lpop($n) { return ($n & ($n - 1)); }
    private function fastlog2($n) {
	    $log = -1;
	    while($n != 0) {
	        $log++;
	        $n = $n >> 1;
	    }
	    return $log;
	}	
    private function bin2string($bin) {
        $res = "";
        for ($p = 31; $p >= 0; $p--) {
            $res .= ($bin & (1 << $p))? "1" : "0";
        }
        return $res . " <- $bin\n";
    }
    
    function __construct($pattern) {
        if (strpos($pattern, "{") === false) {
            $this->construct_1Box($pattern);
        } else {
            $this->construct_nBox($pattern);
        }
        
		/* Sigma preprocess */
		$this->_s = array(
			'A' => 0, 'C' => 0, 'T' => 0, 'G' => 0,
			'R' => 0, 'Y' => 0, 'S' => 0, 'W' => 0, 'K' => 0, 'M' => 0,
			'B' => 0, 'D' => 0, 'H' => 0, 'V' => 0,
			'N' => 0
		);
		$this->_mask = 1;
		for ($i=0; $i < $this->_p_size; $i++) {
			$this->_s[$this->_pattern{$i}] |= $this->_mask;
			$this->_mask = ($this->_mask << 1);
		}
		$this->_s['A'] |= $this->_s['R'] | $this->_s['W'] | $this->_s['M'] | $this->_s['D'] | $this->_s['H'] | $this->_s['V'] | $this->_s['N'];
		$this->_s['C'] |= $this->_s['Y'] | $this->_s['S'] | $this->_s['M'] | $this->_s['B'] | $this->_s['H'] | $this->_s['V'] | $this->_s['N'];
		$this->_s['T'] |= $this->_s['Y'] | $this->_s['W'] | $this->_s['K'] | $this->_s['B'] | $this->_s['D'] | $this->_s['H'] | $this->_s['N'];
		$this->_s['G'] |= $this->_s['R'] | $this->_s['S'] | $this->_s['K'] | $this->_s['B'] | $this->_s['D'] | $this->_s['V'] | $this->_s['N'];
		
		$this->_s['R'] = $this->_s['A'] | $this->_s['G'];
		$this->_s['Y'] = $this->_s['C'] | $this->_s['T'];
		$this->_s['S'] = $this->_s['C'] | $this->_s['G'];
		$this->_s['W'] = $this->_s['A'] | $this->_s['T'];
		$this->_s['K'] = $this->_s['G'] | $this->_s['T'];
		$this->_s['M'] = $this->_s['A'] | $this->_s['C'];
		
		$this->_s['B'] = $this->_s['C'] | $this->_s['T'] | $this->_s['G'];
		$this->_s['D'] = $this->_s['A'] | $this->_s['G'] | $this->_s['T'];
		$this->_s['H'] = $this->_s['C'] | $this->_s['T'] | $this->_s['A'];
		$this->_s['V'] = $this->_s['C'] | $this->_s['A'] | $this->_s['G'];
		
		$this->_s['N'] = $this->_s['A'] | $this->_s['C'] | $this->_s['T'] | $this->_s['G'];
    }
    
	private function construct_1Box($pattern) {
        $this->_pattern = trim($pattern);
        $this->_p_size  = strlen($this->_pattern);
        $this->_nboxes  = 1;
    }
    private function construct_nBox($pattern) {
        $this->_gaps = array();
        $this->_sizebox = array();

        $this->_boxEndBits = 0;
        $this->_nboxes = 1;
        
        $this->_pattern = "";
        $prev = "";
        $g = 0;
        for ($i=0, $end=1; $i < strlen($pattern); $i++) {
            switch ($pattern{$i}) {
                case '{':
                    $this->_sizebox[$this->_nboxes-1]--;
                    $end = $end >> 1;
                    $this->_boxEndBits += ($end >> 1);
					$this->_p_size--;
                    $this->_nboxes++;
                    $prev = "";
                    $mgap = 0; $vgap = 0;
                    do {
                        $mgap = $mgap*10 + $pattern{$i};
                        $i++;
                    } while ($pattern{$i}!=',' && $pattern{$i}!='}');
                    if ($pattern{$i}==',') {
                        $i++;
                        if ($pattern{$i}!='}') {
                            do {
	                            $vgap = $vgap*10 + $pattern{$i};
    	                        $i++;
        	                } while ($pattern{$i}!='}');
                        	$vgap -= $mgap;
                        }
                    }
                    $this->_gaps[$g]['mgap'] = $mgap;
                    $this->_gaps[$g]['vgap'] = $vgap;
                    $g++;
                    break;
                default:
                    $this->_p_size++;
                    $this->_pattern .= $prev;
                    $prev = $pattern{$i};
                    $end = $end << 1; // x2
                    if (!isset($this->_sizebox[$this->_nboxes - 1])) {
                        $this->_sizebox[$this->_nboxes - 1] = 0;
                    }
                    $this->_sizebox[$this->_nboxes - 1]++;
            }
        }
        $this->_pattern .= $prev;
        $this->_boxEndBits += ($end >> 1);
        
        // To reverse the sizebox
        $this->_sizebox = array_reverse($this->_sizebox);

print "<br>pattern[".$this->_pattern."]<br>";
print "boxEnd=[".$this->bin2string($this->_boxEndBits)."]<br>";

        // To reverse the pattern
        $this->_pattern = strrev($this->_pattern);
        
        // To reverse the BoxEndBits
		$v = $this->_boxEndBits;
		$v = (($v >> 1) & 0x55555555) | (($v & 0x55555555) << 1); // swap odd and even bits
		$v = (($v >> 2) & 0x33333333) | (($v & 0x33333333) << 2); // swap consecutive pairs
		$v = (($v >> 4) & 0x0F0F0F0F) | (($v & 0x0F0F0F0F) << 4); // swap nibbles ... 
		$v = (($v >> 8) & 0x00FF00FF) | (($v & 0x00FF00FF) << 8); // swap bytes
		$v = ( $v >> 16             ) | ( $v               << 16);// swap 2-byte long pairs
		$v >>= (32 - $this->_p_size + 1);
		$h = $this->_boxEndBits;
		while ($this->_boxEndBits != 0) {
		    $h = $this->_boxEndBits;;
		    $this->_boxEndBits &= ($this->_boxEndBits - 1);
		}
		$this->_boxEndBits = $v | $h;
		$this->_boxBeginBits = ($this->_boxEndBits << 1) | 1;
		
		
        $this->_vbit = array();
        $this->_bbit = 0;
		$this->_mask = 1;
		$this->_ws   = 0;
		
		for ($i=0; $i < $g; $i++) {
		    $this->_mask = $this->_mask << ($this->_gaps[$i]['vgap'] + 1); // x 2^(vgap+1)
		    $this->_vbit[$i] = $this->_mask >> 1; // x ½
		    $this->_bbit = $this->_bbit | $this->_vbit[$i];
		    $this->_ws += $this->_gaps[$i]['mgap'] + $this->_gaps[$i]['vgap'];
		    print "<br>ws=".$this->_ws;
		}
		if ($g > 0) {
		    $this->_ws -= $this->_gaps[0]['vgap'];
		    print "<br>ws=".$this->_ws . " psize=".$this->_p_size;
		}
		print "<br>F=".$this->bin2string($this->_boxEndBits);
		print "<br>LEFT(F)=".$this->left($this->bin2string($this->_boxEndBits));
		print "<br>fastlog2(LEFT(F))=".$this->fastlog2($this->left($this->bin2string($this->_boxEndBits)));
		$this->_ws += $this->_p_size - $this->fastlog2($this->left($this->_boxEndBits));
		print "<br>ws=".$this->_ws;
		$this->_bbit = ~$this->_bbit;

		$this->_bit2box = array();
		$this->_win = array();
		
		/* bit2box preprocess */
		$this->_mask = 1;
		for ($i=0, $j=0; $i < 32; $i++) {
		    $this->_bit2box[$i] = $j;
		    if ($this->_boxEndBits & $this->_mask) {
		        $j++;
		    }
		    $this->_mask = ($this->_mask << 1);
		}		
		
print "<br>pattern[".$this->_pattern."]<br>";
print "p_size=[".$this->_p_size."]<br>";
print "boxEnd=[".$this->bin2string($this->_boxEndBits)."]<br>";
print "boxBegin=[".$this->bin2string($this->_boxBeginBits)."]<br>";
print "bbit=[".$this->bin2string($this->_bbit)."]<br>";
print "mask=[".$this->bin2string($this->_mask)."]<br>";
print "ws=[".$this->bin2string($this->_ws)."]<br>";
print_r($this->_vbit);print "<br>";
print_r($this->_gaps);print "<br>";
print_r($this->_sizebox);print "<br>";
print_r($this->_bit2box);print "<br>";
    }
    
    function find($text = "", $subst = 0) {
        return ($this->_nboxes == 1)? $this->find_1Box($text, $subst): $this->find_nBox($text, $subst);
    }
    private function find_1Box($text, $subst) {
        $retObj = array();
        
        $this->_text = trim($text);
        $this->_t_size = strlen($this->_text);
        
		$this->_mask = ($this->_mask >> 1);
		$S = array();
		for ($i=0; $i<=$subst; $i++) { $S[$i] = 0; }
		$i = 0;

		while($i < $this->_t_size) {
		    $V = $S[0];
		    $S[0] = (($S[0] << 1) | 1 ) & $this->_s[$this->_text{$i}];
		    $j = 1;
		    while($j <= $subst) {
		        $W = $S[$j];
				$S[$j] = (($S[$j] << 1) & $this->_s[$this->_text{$i}]) | ($V << 1);
				$V = $W;
				$j++;
			}

			$j = 0;
			while($j <= $subst) {
				if($S[$j] & $this->_mask) {
				    $res = $i - $this->_p_size + 1;
				    $retObj[$res] = $res;
					$j = $subst;
				}
				$j++;
			}
			$i++;
		}
		return $retObj;
	}

	
	private function find_nBox($text, $subst) {
        $retObj = array();

        $this->_text = trim($text);
        $this->_t_size = strlen($this->_text);
        
        $S = array();
        for ($i=0; $i<=$subst; $i++) { $S[$i] = 0; }
        for ($i=0; $i<=($this->_ws*($subst+1)); $i++) { $this->_win[$i] = 0; }
		$i = 0;		
		while($i < $this->_t_size) {
		    $V = 0;
		    $this->_mask = $this->_boxBeginBits;
		    $j = 0;
		    do {
		        $W = $S[$j];
				$S[$j] = ((($S[$j] << 1) | $this->_mask) & $this->_s[$this->_text{$this->_t_size - $i - 1}]) | ($V << 1);
				$V = $W;
				$this->_mask = 0;
				$y = (($i-1)%$this->_ws)*($subst+1)+($j);
				$y = ($y < 0)? 0 : $y;
				$this->_win[(($i)%$this->_ws)*($subst+1)+($j)] = 
					($this->_win[$y] >> 1) & $this->_bbit;
				$j++;
			} while($j <= $subst);

			$j = 0;
			do {
			    $h = $S[$j] & $this->_boxEndBits;
			    while ($h != 0) {
			        $box = $this->_bit2box[$this->fastlog2($this->left($h))];
			        if ($box == 0) {
			            $this->_win[(($i)%$this->_ws)*($subst+1)+($j)] |= $this->_vbit[$box];
			        } else {
			            $e = 0;
			            while (($j+$e) <= $subst) {
			                $a = ($i - $this->_gaps[$box-1]['mgap'] - $this->_sizebox[$box]);
			                $b = ($a%$this->_ws)*($subst+1)+($e);
			                $b = ($b<0)? 0 : $b; // PHP does not support unsigned integers
			                if ((($this->_bbit + ($this->_vbit[$box-1]>>($this->_gaps[$box-1]['vgap']))) ^ $this->_bbit)
			                	& $this->_win[$b]) {
		                	    if($box == ($this->_nboxes - 1)) {
		                	        $res = 1000 - $i;
		                	        //array_push($retObj, $res);
		                	        // To eliminate repeated positions due to gap variation
		                	        $retObj[$res] = $res;
		                	    } else {
		                	        $this->_win[(($i)%$this->_ws)*($subst+1)+($j + $e)] |= $this->_vbit[$box];
		                	    }
		                	}
		                	$e++;
			            }
			        }
			        $h = $this->lpop($h);
			    }
			    $j++;
			} while ($j <= $subst);
			$i++;
		}
		return $retObj;
	}
}
?>