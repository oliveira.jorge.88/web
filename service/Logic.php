<?php
class Logic {
	private $_dbAccess;
	private $_species;
	private $_strains;
	public  $_properties;
	
	function __construct($species, $strains, $path = "") {
		include_once($path . "service/IDBAccess.php");
		$this->_dbAccess = new IDBAccess($path);
		$this->_dbAccess->openDB(); 
		include_once($path."conf/Properties.php");

		$this->_properties = new Properties($path);
		$this->_species = $species;
		# conf file to DB
		$this->_strains = explode("\t", $strains);
	}	
	
	function __destruct() {
	}
	
	private function dbSpecies() { // To use in DB queries
		$sp = addslashes($this->_species);
		if (!empty($this->_strains) && count($this->_strains)>0) {
			$s = "";
			foreach ($this->_strains as $strain) {
				if (!empty($s)) $s .= ",";
				$s .= "'$sp $strain'";
			}
			return "species in ($s)";
		} else {
			return "species='$sp'";
		}
	}

	private function dbclean($str) {
		return addslashes(trim($str));
	}

	function aSpecies() { // To use in forms for the user
		$sp = [];
		if (!empty($this->_strains) && count($this->_strains)>0) {
			foreach ($this->_strains as $strain) {
				$sp[] = $this->_species . " $strain";
			}
		} else {
			$sp[] = $this->_species;
		}
		return $sp;
	}

	function nameReducer($bigA) { // To create an abreviated name for species
		$aShort = array();
		foreach($bigA as $s){
			$bin = explode(" ",$s);
                        $fam = array_shift($bin);
			$shortFam = $fam[0];
			$sp = array_shift($bin);
			$shortSp = substr($sp, 0, 3);
                        $strain = implode(" ",$bin);
			$new = $shortFam.". ".$shortSp." ".$strain;
			$aShort[] = $new;
		}
		return $aShort;
	}

	function getHomologSpecies() { // to get an array of all homologous species
		$live = $_SERVER['SERVER_NAME'] != 'localhost';
		$homolog = array();
		$species = $this->dbSpecies();
		$q  = "select distinct species from orfgene where orfid in (select ";
		$q .= "distinct orfiddest from orthologs where orfidsrc in (select ";
		$q .= "distinct orfid from orfgene where $species))";
		if ($this->_dbAccess->query("homologs", $q)) {
			while ($row = $this->_dbAccess->nextObject("homologs")) {
				if (strpos($species,addslashes($row['species']))!==false) continue;
				$homolog[] = $row['species'];
			}
		}
		$this->_dbAccess->freeResult("homologs");
		return $homolog;
	}

	// WARNING: THIS EXCLUDES ORFS WITH NO PROTEIN ASSOCIATED
	function orfIDs2Info($aORFids) { // To get info from an orf ID
		$retObj = [];
		if (empty($aORFids)) return $retObj; 
		$q  = "select O.orfid,O.orf,O.gene,O.species,P.protein from orfgene as O, ";
		$q .= "protein as P where P.tfid=O.orfid and O.orfid in (";
		$q .= join(",", $aORFids) . ")";
		if ($this->_dbAccess->query("ids2info", $q)) {
			while ($row = $this->_dbAccess->nextObject("ids2info")) {
				$retObj[$row['orfid']]['orf']     = $row['orf'];
				$retObj[$row['orfid']]['gene']    = $row['gene'];
				$retObj[$row['orfid']]['prot']    = $row['protein'];
				$retObj[$row['orfid']]['species'] = $row['species'];
			}
		}
		$this->_dbAccess->freeResult("ids2info");
		return $retObj;
	}

	function user2Info($sGenes='', $aSpecies = [], $description = false) { // to get info from a string of orfs/genes from the user, if empty returns all orfs of current species 

		$retObj = [];
		$notgeneslist = [];

		$f = function($v) { return $this->dbclean($v); };
		$species = empty($aSpecies)? "":
			" and O.species in ('".join("','", array_map($f, $aSpecies))."')";

		if ($sGenes==''){
			$q  = "select O.orfid,O.orf,O.gene,O.species,O.gdid,P.protein from orfgene as O, ";
                        $q .= "protein as P where P.tfid=O.orfid ";
                        $q .= "$species";
                        if ($this->_dbAccess->query("infoorfgene", $q)) {
                                while ($row = $this->_dbAccess->nextObject("infoorfgene")) {
                                        $retObj[$row['orfid']]['orf']     = $row['orf'];
                                        $retObj[$row['orfid']]['gene']    =  (strcasecmp($row['gene'], "Uncharacterized")!=0)?$row['gene']: $row['orf'];
                                        $retObj[$row['orfid']]['prot']    = $row['protein'];
					$retObj[$row['orfid']]['species'] = $row['species'];
					$pos = strpos($row['gdid'],":");
					$retObj[$row['orfid']]['ext'] = ($pos === false)?$row['gdid']:substr($row['gdid'], $pos+1);
					if ($description) $retObj[$row['orfid']]['desc'] = $this->_dbAccess->getObject("description",
                                                        "select D.description from description as D, orfgene as O where " .
                                                        "O.descriptionid=D.descriptionid and O.orfid={$row['orfid']}");
				}
			}
		} else {
			$aGenes = $this->formString2Array($sGenes);
			foreach ($aGenes as $orfgene){
				$dborfgene = $this->dbclean($orfgene);
				// ORF or GENE
				$q  = "select O.orfid,O.orf,O.gene,O.species,O.gdid,P.protein from orfgene as O, ";
				$q .= "protein as P where P.tfid=O.orfid and (";
				$q .= "O.orf='$dborfgene' or O.gene='$dborfgene') $species";
				if ($this->_dbAccess->query("infoorfgene", $q)) {
					while ($row = $this->_dbAccess->nextObject("infoorfgene")) {
						$retObj[$row['orfid']]['user']    = $orfgene;
						$retObj[$row['orfid']]['orf']     = $row['orf'];
						$retObj[$row['orfid']]['gene']    =  (strcasecmp($row['gene'], "Uncharacterized")!=0)?$row['gene']: $row['orf'];
						$retObj[$row['orfid']]['prot']    = $row['protein'];
						$retObj[$row['orfid']]['species'] = $row['species'];
						$pos = strpos($row['gdid'],":");
	                                        $retObj[$row['orfid']]['ext'] = ($pos === false)?$row['gdid']:substr($row['gdid'], $pos+1);
						 if ($description) $retObj[$row['orfid']]['desc'] = $this->_dbAccess->getObject("description",
                                                        "select D.description from description as D, orfgene as O where " .
                                                        "O.descriptionid=D.descriptionid and O.orfid={$row['orfid']}");

					}
				} else {
					// ALIAS
					$q  = "select O.orfid,O.orf,O.gene,O.species,O.gdid,P.protein from orfgene as O, ";
					$q .= "alias as A, protein as P where A.orfid=O.orfid and P.tfid=O.orfid and ";
					$q .= "A.alias='$dborfgene' $species";
					if ($this->_dbAccess->query("infoalias", $q)) {
						while ($row = $this->_dbAccess->nextObject("infoalias")) {
							$retObj[$row['orfid']]['user']    = $orfgene;
							$retObj[$row['orfid']]['orf']     = $row['orf'];
							$retObj[$row['orfid']]['gene']    = (strcasecmp($row['gene'], "Uncharacterized")!=0)?$row['gene']: $row['orf'];
							$retObj[$row['orfid']]['prot']    = $row['protein'];
							$retObj[$row['orfid']]['species'] = $row['species'];
							$pos = strpos($row['gdid'],":");
		                                        $retObj[$row['orfid']]['ext'] = ($pos === false)?$row['gdid']:substr($row['gdid'], $pos+1);
							if ($description) $retObj[$row['orfid']]['desc'] = $this->_dbAccess->getObject("description",
                                                        "select D.description from description as D, orfgene as O where " .
                                                        "O.descriptionid=D.descriptionid and O.orfid={$row['orfid']}");

						}
					} else {
						// PROTEIN
						$q  = "select O.orfid,O.orf,O.gene,O.species,O.gdid,P.protein from orfgene as O, ";
						$q .= "protein as P where P.tfid=O.orfid and P.protein='$dborfgene' $species";
						if ($this->_dbAccess->query("infoprotein", $q)) {
							while ($row = $this->_dbAccess->nextObject("infoprotein")) {
								$retObj[$row['orfid']]['user']    = $orfgene;
								$retObj[$row['orfid']]['orf']     = $row['orf'];
								$retObj[$row['orfid']]['gene']    =  (strcasecmp($row['gene'], "Uncharacterized")!=0)?$row['gene']: $row['orf'];
								$retObj[$row['orfid']]['prot']    = $row['protein'];
								$retObj[$row['orfid']]['species'] = $row['species'];
								$pos = strpos($row['gdid'],":");
			                                        $retObj[$row['orfid']]['ext'] = ($pos === false)?$row['gdid']:substr($row['gdid'], $pos+1);
								 if ($description) $retObj[$row['orfid']]['desc'] = $this->_dbAccess->getObject("description",
                                                        "select D.description from description as D, orfgene as O where " .
                                                        "O.descriptionid=D.descriptionid and O.orfid={$row['orfid']}");

							}
						} else {
							$notgeneslist[] = $orfgene;
						}
						$this->_dbAccess->freeResult("infoprotein");
					}
					$this->_dbAccess->freeResult("infoalias");
				}
				$this->_dbAccess->freeResult("infoorfgene");
			}
		}
		return array($retObj, $notgeneslist);
	}

	// THIS IS NOT CURRENTLY USED BUT MIGHT BE USEFUL // 
	function orf2id($sGenes, $species=false) { // transform a string of orfs/genes from the user into orf ids

	 	$dbspecies = ($species)? "species in ('".$this->dbclean($species)."')": $this->dbSpecies();
		$aGenes = $this->formString2Array($sGenes);

		$retObj = array();
		$notgeneslist = array();

		foreach ($aGenes as $orfgene){
			$orfid="";
			$dborfgene = $this->dbclean($orfgene);
	                $q = "select orfid,orf from orfgene where $dbspecies and ";
        	        $q .= "(orf='$dborfgene' or gene='$dborfgene')";
	                $orfid = $this->_dbAccess->getObject("orfid",$q);
        	        if ($orfid=="") {
                	        // not an orf or a gene... maybe an alias!?
                        	$q = "select O.orfid from orfgene as O, alias as A where A.alias='";
	                        $q .= "$dborfgene' and O.orfid=A.orfid and O.$dbspecies";
        	                $orfid = $this->_dbAccess->getObject("orfid", $q);
			}
			if ($orfid=="") { $notgeneslist[] = $orfgene;}
			else {	$retObj[$orfid] = $orfgene; }
		}
                return array($retObj, $notgeneslist);
        }

        function getHomoORFInfo($aORFids, $toSpecies, $synteny=0, $reverse = false) { // get all homologs of given ids, for the specified synteny

                $species = ($reverse)?$this->dbSpecies():"species in ('".addslashes($toSpecies)."')";
                $orflist = implode(",",$aORFids);

                $retObj = array();
                $q  = "select O.orfid,O.orf,O.gene,P.protein, H.orfiddest, ";
                $q .= "Homo.orf as homoorf,Homo.gene as homogene from orfgene as O, ";
                $q .= "protein as P, orthologs as H left join orfgene as Homo on ";
                $q .= "H.orfiddest=Homo.orfid where O.orfid in ($orflist) ";
                $q .= "and P.tfid=O.orfid ";
                $q .= " and H.classif=$synteny ";
                $q .= "and O.orfid=H.orfidsrc and H.orfiddest in (select distinct ";
                $q .= "orfid from orfgene where $species )";
                if ($this->_dbAccess->query("homologous", $q)) {
                        while ($row = $this->_dbAccess->nextObject("homologous")) {
                                $retObj[$row['orf']]['gene']            = $row['gene'];
                                $retObj[$row['orf']]['protein']         = $row['protein'];
                                $retObj[$row['orf']]['orfid']           = $row['orfid'];
                                $retObj[$row['orf']]['homo']['orfid'][] = $row['orfiddest'];
                                $retObj[$row['orf']]['homo']['orf'][]   = $row['homoorf'];
                                $retObj[$row['orf']]['homo']['gene'][]  = $row['homogene'];
                        }
                }
                $this->_dbAccess->freeResult("homologous");
                return $retObj;
        }

	function upstreamSeq($aORFids,$sp=NULL,$from=-1000,$to=-1) {  // to get promoters of each given orf id // when sp is defined it outputs all upstream sequences of given species
      
		$aPromoters = []; 
		if (empty($aORFids) && $sp==NULL) return $aPromoters;

		$size = ($to - $from)+1;
	        $from = $from + 1000;

		$q  = "select orfid,promoterseq from orfgene where ";
		if (isset($sp)) { $q .= "species in ('".addslashes($sp)."')"; }
		elseif (!empty($aORFids)) { $q .= "orfid in (".join(",", $aORFids) . ")";}
		if ($this->_dbAccess->query("prom", $q)) {
			while ($row = $this->_dbAccess->nextObject("prom")) {
		        	$lines = explode("\n", $row['promoterseq']);
	        		array_shift($lines);
				$seq = join($lines);
				$seq = substr($seq, $from, $size);
        			$aPromoters[$row['orfid']] = $seq;
			}   
		}   
	    	return $aPromoters;
 	}

	function gene($name) { // format a name to gene
		$newname = trim($name);
		if (strlen(trim($name)) > 0) {
			# If it is a protein name, remove the ending 'p'
			switch ($newname{strlen($newname) - 1}) {
				case 'p':
				case 'P':
				$newname = substr($newname,0,strlen($newname) - 1);
			}
		}
		return $newname;
	}

	function protein($name) { // format a name to protein
		$newname = trim($name);
		# If it is a gene name, add the ending 'p'
		switch ($newname{strlen($newname) - 1}) {
			case 'p':break;
			case 'P':break;
			default:
				$newname = $newname."p";
		}
		return $newname;
	}

	/*
	 * formString2Array
	 */
	function formString2Array($sInputData) { // to use in forms where the user inputs several orf/genes
		$aData = array();
		$initdata = preg_split("/[\s;|]+/", trim($sInputData));

		foreach ($initdata as $data) {
			if (strlen(trim($data)) == 0) continue;
			$aData[] = trim($data," \t\n\r\0\x0B,;");
		}	
		return $aData;
	}
	
	/*
	 * motifIsComplex
	 */
	function motifIsComplex($motif) { // identify if a motif is complex
	    if (strpos($motif, "{") === false &&
	    	strpos($motif, "}") === false) {
	        return false;
	    }
	    return true;
	}
	
	/*
	 * motifIUPACCompress
	 */
	function motifIUPACCompress($motif) { // compress a motif, all the motifs in the DB are compressed
	    $good = true;
	    $motif = strtoupper($motif);

	    if (strpos($motif, "/") !== false) {
                $motif = str_replace("/", "", $motif);
	    	$motif = str_replace("(","[", $motif);
		$motif = str_replace(")","]", $motif);
	    }
	    
	    if (strpos($motif, "[") !== false || strpos($motif, "]") !== false) {
    	    	$motif = str_replace(array("[AT]","[TA]"), "W", $motif);
    		$motif = str_replace(array("[CG]","[GC]"), "S", $motif);
    		$motif = str_replace(array("[AG]","[GA]"), "R", $motif);
    		$motif = str_replace(array("[TC]","[CT]"), "Y", $motif);
    		$motif = str_replace(array("[AC]","[CA]"), "M", $motif);
    		$motif = str_replace(array("[TG]","[GT]"), "K", $motif);
    		
    		$motif = str_replace(array("[ATG]","[AGT]","[GAT]","[GTA]","[TGA]","[TAG]"), "D", $motif);
    		$motif = str_replace(array("[ATC]","[ACT]","[CAT]","[CTA]","[TCA]","[TAC]"), "H", $motif);
    		$motif = str_replace(array("[ACG]","[AGC]","[GAC]","[GCA]","[CGA]","[CAG]"), "V", $motif);
    		$motif = str_replace(array("[TGC]","[TCG]","[GTC]","[GCT]","[CTG]","[CGT]"), "B", $motif);
    		$n = array("[ATGC]","[ATCG]","[AGTC]","[AGCT]","[ACTG]","[ACGT]",
    				   "[TGCA]","[TGAC]","[TCGA]","[TCAG]","[TAGC]","[TACG]",
    				   "[GATC]","[GACT]","[GTAC]","[GTCA]","[GCAT]","[GCTA]",
    				   "[CATG]","[CAGT]","[CTAG]","[CTGA]","[CGAT]","[CGTA]");
    		$motif = str_replace($n, "N", $motif);
		}
		if (strpos($motif, "[") !== false || strpos($motif, "]") !== false) {
			$good = false;
		}
		
		return array($motif,$good);
	}
	
	/*
	 * motifComplement
	 */
	function motifComplement($motif) {
	    $comp = $motif;
	    $n = strlen($motif) - 1;
	    for ($i = 0; $i < ($n+1); $i++) {
	        switch($motif{$i}) {
	            case 'A': case 'a': $comp{$n-$i} = 'T'; break;
	            case 'T': case 't': $comp{$n-$i} = 'A'; break;
	            case 'G': case 'g': $comp{$n-$i} = 'C'; break;
	            case 'C': case 'c': $comp{$n-$i} = 'G'; break;
	            
	            case 'R': case 'r': $comp{$n-$i} = 'Y'; break;
	            case 'Y': case 'y': $comp{$n-$i} = 'R'; break;
	            case 'M': case 'm': $comp{$n-$i} = 'K'; break;
	            case 'K': case 'k': $comp{$n-$i} = 'M'; break;
	
	            case 'D': case 'd': $comp{$n-$i} = 'H'; break;
	            case 'H': case 'h': $comp{$n-$i} = 'D'; break;
	            case 'V': case 'v': $comp{$n-$i} = 'B'; break;
	            case 'B': case 'b': $comp{$n-$i} = 'V'; break;
	            
	            # N, W, S
	            default: $comp{$n-$i} = $motif{$i};
	        }
	    }
	    return $comp;
	}	

        /*
         * expandRegExp
         */	
	function expandRegExp($seq) { // to expand motifs to be used in sequence alignments
		$retObj = [];

		if (strpos($seq, ",") !== false) {
			$expanded = [];
			preg_match_all('/{(\d*),(\d*)}/', $seq, $coord);
			$seqs = preg_split('/[N]{(\d*),(\d*)}/', $seq);
			$num_seqs = ($coord[2][0]-$coord[1][0]+1);
			$n = str_repeat("N",$coord[1][0]);
			for ($j=0; $j<$num_seqs;$j++){
				array_push($expanded,$seqs[0].$n.$seqs[1]);
				$n .= "N";
			}

			foreach($expanded as $seq){
				$retStr = "";
				for ($i=0, $last=0; $i<strlen($seq); $i++) {
					if ($seq{$i} == '{') {
						$i++;
						$n = "";
						while ($seq{$i} != '}') {
							$n = $n . $seq{$i};
							$i++;
						}
						for ($j=1; $j < $n; $j++) {
							$retStr .= $seq{$last};
						}
					}
					else {     
						$retStr .= $seq{$i};
						$last = $i;   
					}
				}
				array_push($retObj, $retStr);
			}
		}
		else {

			$retStr = "";

			for ($i=0, $last=0; $i<strlen($seq); $i++) {
				if ($seq{$i} == '{') {
					$i++;
					$n = "";
					while ($seq{$i} != '}') {
						$n = $n . $seq{$i};
						$i++;
					}
					for ($j=1; $j < $n; $j++) {
						$retStr .= $seq{$last};
					}
				} else {
					$retStr .= $seq{$i};
					$last = $i;
				}    
			}
			array_push($retObj, $retStr);
		}
		return $retObj;
	}

	 function ids2orthologs($aIDs,$synteny=0,$diff=false) { // to get the ids of orthologs from an input of ids
                $live = $_SERVER['SERVER_NAME'] != 'localhost';
                $curr2homo = array();
                $homo2curr = array();

                $q  = "select orfidsrc, orfiddest ";
                $q .= "from orthologs where ";
                $q .= "orfidsrc in (".join(",",$aIDs).")";
                $q .= " and classif=$synteny";
                if (($diff) && ($synteny<3)) {
                        $q .= " and orfiddest not in (select OD.orfid ";
                        $q .= "from orthologs as H, orfgene as O, orfgene as OD where ";
                        $q .= "H.orfidsrc=O.orfid and O.orfid in (".join(",",$aIDs).") and ";
                        $q .= "H.orfiddest=OD.orfid";
                        $q .= " and H.classif>$synteny)";
		}
                if ($this->_dbAccess->query("orthologs", $q)) {
			while ($row = $this->_dbAccess->nextObject("orthologs")) {
				$curr = $row['orfidsrc'];
				$homo = $row['orfiddest'];
				if (!isset($curr2homo[$curr])) $curr2homo[$curr]=[];
				$curr2homo[$curr][] = $homo;
				if (!isset($homo2curr[$homo])) $homo2curr[$homo]=[];
				$homo2curr[$homo][] = $curr;
                        }
                }
                $this->_dbAccess->freeResult("orthologs");
                return array($curr2homo, $homo2curr);
        }

	/*
	 * allSameLength
	 */
	function allSameLength($sequences) { // used in iupac generation, where all sequences must be same len
		$len = strlen($sequences[0]);

	    	foreach ($sequences as $seq) {
        		if (strlen($seq) != $len) return false;
    		}

	    	return true;
	}
	
	/*
	 * sequenceDB2File
	 */
	function sequenceDB2File($type, $id) { // to download sequences
		$sequence = false;

		list($aORFGenes, $aGUnknown) = $this->user2Info($id,$this->aSpecies());

		if (empty($aORFGenes)) return $sequence;
		$aORFs = array_keys($aORFGenes);
		$orfid = array_pop($aORFs);

		switch($type) {
	    	case "aminoacid":
				$q  = "select P.{$type}seq from protein as P ";
				$q .= "where P.tfid=$orfid";
    		break;
   	 	case "gene":
    		case "promoter":
    		$q  = "select ".$type."seq from ";
    		$q .= "orfgene where orfid='$orfid' ";
    		break;
		}

		if ($this->_dbAccess->query("sequence", $q)) {
			$row = $this->_dbAccess->nextObject("sequence");
			$sequence = $row[$type."seq"]; 
		}
		$this->_dbAccess->freeResult("sequence");

		if ($sequence == false) return $sequence;

		$filename  = $type . "_fasta_" . rand() . ".txt";
		$localpath = $this->_properties->get("path.tmp.local");

		if (!$handle = fopen($localpath . $filename, 'w')) return false;
		if (fwrite($handle, $sequence) === FALSE) return false;
		fclose($handle);

		$webpath = $this->_properties->get("path.tmp.url");
		return $webpath . $filename;
	}
	
	 function simpleConsensus2meme($consensus) {
		//               A     C     G     T
		$iupac['A'] = "1.000 0.000 0.000 0.000";
		$iupac['C'] = "0.000 1.000 0.000 0.000";
		$iupac['G'] = "0.000 0.000 1.000 0.000";
		$iupac['T'] = "0.000 0.000 0.000 1.000";
		
		$iupac['R'] = "0.500 0.000 0.500 0.000";
		$iupac['Y'] = "0.000 0.500 0.000 0.500";
		$iupac['M'] = "0.500 0.500 0.000 0.000";
		$iupac['K'] = "0.000 0.000 0.500 0.500";
		$iupac['S'] = "0.000 0.500 0.500 0.000";
		$iupac['W'] = "0.500 0.000 0.000 0.500";
		
		$iupac['H'] = "0.333 0.333 0.000 0.333";
		$iupac['B'] = "0.000 0.333 0.333 0.333";
		$iupac['V'] = "0.333 0.333 0.333 0.000";
		$iupac['D'] = "0.333 0.000 0.333 0.333";
		
		$iupac['N'] = "0.250 0.250 0.250 0.250";
		$iupac['x'] = "0.250 0.250 0.250 0.250";

		$iupac['a'] = "0.500 0.166 0.166 0.166";
		$iupac['c'] = "0.166 0.500 0.166 0.166";
		$iupac['g'] = "0.166 0.166 0.500 0.166";
		$iupac['t'] = "0.166 0.166 0.166 0.500";


		$retObj = "";
		for ($i = 0; $i < strlen($consensus); $i++) {
			$retObj .= $iupac[$consensus[$i]] . "\n";
		}
		return $retObj;
	 }

	 function simpleConsensus2transfac($consensus) {
		//                A     C     G     T
		$iupac['A'] = "1.000 0.000 0.000 0.000 A";
		$iupac['C'] = "0.000 1.000 0.000 0.000 C";
		$iupac['G'] = "0.000 0.000 1.000 0.000 G";
		$iupac['T'] = "0.000 0.000 0.000 1.000 T";
		
		$iupac['R'] = "0.500 0.000 0.500 0.000 R";
		$iupac['Y'] = "0.000 0.500 0.000 0.500 Y";
		$iupac['M'] = "0.500 0.500 0.000 0.000 M";
		$iupac['K'] = "0.000 0.000 0.500 0.500 K";
		$iupac['S'] = "0.000 0.500 0.500 0.000 S";
		$iupac['W'] = "0.500 0.000 0.000 0.500 W";
		
		$iupac['H'] = "0.333 0.333 0.000 0.333 H";
		$iupac['B'] = "0.000 0.333 0.333 0.333 B";
		$iupac['V'] = "0.333 0.333 0.333 0.000 V";
		$iupac['D'] = "0.333 0.000 0.333 0.333 D";
		
		$iupac['N'] = "0.250 0.250 0.250 0.250 N";
		$iupac['x'] = "0.250 0.250 0.250 0.250 N";

		$iupac['a'] = "0.500 0.166 0.166 0.166 a";
		$iupac['c'] = "0.166 0.500 0.166 0.166 c";
		$iupac['g'] = "0.166 0.166 0.500 0.166 g";
		$iupac['t'] = "0.166 0.166 0.166 0.500 t";

		$retObj = "";
		for ($i = 0; $i < strlen($consensus); $i++) {
			$p = $i + 1;
			$retObj .= ($p>9)?"$p  ":"0$p  ";
			$retObj .= $iupac[$consensus[$i]] . "\n";
		}
		return $retObj;
	}
	
	/*
	 * consensus2matrix
	 * Documentation: http://www.gene-regulation.com/pub/databases/transfac/doc/matrix1.html
	 */
	function consensus2matrix($type, $tf, $consensus) {
		$parts = array();
		if ($this->motifIsComplex($consensus)) {
		    $parts = preg_split("/N{\d+(,\d+)?}/", $consensus);
		} else {
			$parts[] = $consensus;
		}
		
		$retObj = "";
		if ($type == "transfac") {
    		date_default_timezone_set('UTC');
    		$date = date(DATE_RFC850);
    
    		$retObj  = "DT  $date\n"; 
    		$retObj .= "XX\n";
    		$retObj .= "NA  $tf\n";
    		$retObj .= "XX\n";
    		$retObj .= "DE  $consensus\n";
    		$retObj .= "XX\n";
    		
    		for ($i=0; $i < count($parts); $i++) {
    			$retObj .= "P$i  A     C     G     T\n";
    			$retObj .= $this->simpleConsensus2transfac($parts[$i]);
    			$retObj .= "XX\n";
    		}
    		
		} else { // MEME
			$retObj .= "\nMEME version 4\n\nALPHABET= ACGT\n";
			$retObj .= "********************************************************************************\n";
			$retObj .= "MOTIF {$consensus}\n";
			$retObj .= "********************************************************************************\n";
			for ($i=0; $i < count($parts); $i++) {
			$retObj .= "--------------------------------------------------------------------------------\n";
			$retObj .= "Motif {$parts[$i]} position-specific probability matrix\n";
			$retObj .= "--------------------------------------------------------------------------------\n";
			$retObj .= "letter-probability matrix: alength= 4 w= " . strlen($parts[$i]) . " nsites= 20 E= 0\n";
			$retObj .= $this->simpleConsensus2meme($parts[$i]);
			}
		}
		return $retObj;
	}

        function getTFMotifs($species = false, $aTFids = false) { // generic call to db to get motifs

                $retObj = [];

                $org = ($species)?
                        "species='".addslashes($species)."'":
                        $this->dbSpecies();

                $q  = "select distinct P.tfid, P.protein, C.IUPACseq from protein as P, ";
                $q .= "consensus as C, tfconsensus as TFC, orfgene as O where ";
                $q .= "C.consensusid=TFC.consensusid and TFC.tfid=P.tfid and ";
                $q .= "P.tfid=O.orfid and O.$org";
                if ($aTFids) {
                        $tflist = join(",", array_keys($aTFids));
                        $q .= " and P.tfid in ($tflist)";
                }
                if ($this->_dbAccess->query("consensus", $q)) {
                        while ($row = $this->_dbAccess->nextObject("consensus")) {
                                $tfid = $row['tfid'];
                                $c = $row['IUPACseq'];
                                $retObj[$tfid][] = $c;
                        }
                }
                $this->_dbAccess->freeResult("consensus");
                return $retObj;
        }

	  /* matchTFOnSequences  */
         function matchTFOnSequences($aTFs, $aSequences, $subst=0, $simple=false) { // simple gives only obj[tf][orf] = true

                $retObj = [];

                include_once('ShiftAndWrapper.php');

                foreach (array_keys($aTFs) as $tfid) {

                        foreach ($aTFs[$tfid] as $motif){

                                $SA = new ShiftAndWrap($motif,$aSequences,$subst,$simple);

                                $matches = $SA->_matches;

                                if ($simple){
                                        if (isset($retObj[$tfid])){ $retObj[$tfid] = $retObj[$tfid] + $matches;}
                                        else { $retObj[$tfid] =$matches; }
                                }
                                else {
                                        foreach(array_keys($matches) as $orfid){
                                                $retObj[$orfid][$motif] = $matches[$orfid];
                                        }
                                }
                        }
                }
                return $retObj;
         }

	function getTFs($withHomo = false) { // GET ALL proteins with regulations and their info
		$retObj = [];

		$q  = "select O.orfid,O.orf,O.gene,P.protein from orfgene as O, protein ";
		$q .= "as P where O.{$this->dbSpecies()} and O.orfid=P.tfid and (O.orfid ";
		$q .= "in (select distinct tfid from regulation)";
		if ($withHomo) {
			$q .= " or O.orfid in (select distinct orfiddest from orthologs where orfidsrc in (select distinct tfid from regulation))";
		}
		$q .= ") order by P.protein asc";
		if ($this->_dbAccess->query("tfs", $q)) {
			while ($row = $this->_dbAccess->nextObject("tfs")) {
				$retObj[$row['orfid']]['orf']  = $row['orf'];
				$retObj[$row['orfid']]['gene'] = $row['gene'];
				$retObj[$row['orfid']]['prot'] = $row['protein'];
			}
		}
		$this->_dbAccess->freeResult("tfs");
		
		return $retObj;
	}


	function getGO($aOrfGenes, $goDB, $hyperN) { // get gene ontology info

		$hypern = count($aOrfGenes);

		$assocs = array();
		
		if ($goDB != "process" && $goDB != "function" && $goDB != "component") {
			$goDB = "process";
		}
		$q  = "select G.goid, orfid from goorflist as T, geneontology as G ";
		$q .= "where orfid in (".join(",",array_keys($aOrfGenes)).") ";
		$q .= " and G.goid=T.goid and G.onto='$goDB'";
		$this->_dbAccess->query("goids", $q);
		while ($row = $this->_dbAccess->nextObject("goids")) {
			$assocs[$row['goid']][] = $row['orfid'];
		}
		$this->_dbAccess->freeResult("goids");

		$terms = array();
		foreach (array_keys($assocs) as $id) {
			$terms[$id]['setNum'] = count($assocs[$id]);
			$terms[$id]['setPer'] = $terms[$id]['setNum'] / $hypern;

			$q  = "select count(distinct orfid) as assoc from goorflist where ";
			$q .= "goid='$id' and orfid in (select distinct orfid from orfgene ";
			$q .= "where {$this->dbSpecies()})";
			$q .= " and goid in (select distinct goid from geneontology where onto='$goDB')";
			$terms[$id]['dbNum'] = $this->_dbAccess->getObject("assoc", $q);
			$terms[$id]['dbPer'] = $terms[$id]['dbNum'] / $hyperN;
			
			#$terms[$id]['setNum'] / $terms[$id]['dbNum'];
			
			$q = "select * from geneontology where goid='$id' and onto='$goDB'";
			$this->_dbAccess->query("idinfo", $q);
			if ($row = $this->_dbAccess->nextObject("idinfo")) {
				$terms[$id]['term'] = $row['term'];
				$terms[$id]['depth'] = $row['depth'];
			}
			$this->_dbAccess->freeResult("idinfo");
		}

		return array($assocs, $terms);
	}

	function getNumPromoters($aSpecies = []){ // get number of promoters for an array of species
                $retObj = [];

		$q = "select species, count(promoterseq) as num from orfgene where promoterseq!='' ";
                $f = function($value) { return $this->dbclean($value); };
                $q .= empty($aSpecies)? "":
			" and species in ('".join("','", array_map($f, $aSpecies))."')";
		$q .= " group by species";
		if ($this->_dbAccess->query("proms", $q)) {
			$total=0;
			while ($row = $this->_dbAccess->nextObject("proms")) {
				$retObj[$row['species']]=$row['num'];
				$total += $row['num'];
                        }
                }
                $this->_dbAccess->freeResult("proms");
                return array($retObj,$total);
	}

	
	function getTotalNumDBGenes() { // get total number of genes for the current species
		$q = "select count(orf) as gTotal from orfgene where {$this->dbSpecies()}";
		return $this->_dbAccess->getObject("gTotal", $q);
	}


	  function getCombinationOrths($sp,$synteny=0) { // get all the species combinations and their orthologs

		  $combs = explode(",",$sp);  
		  $aOrths = array();
		  $sps = "(";
		  $main='';

		  foreach($combs as $species){
			  if ($main==''){ $main = $species; }
			  else {
				  $dbspecies = $this->dbclean($species);
				  $sps .= "'$dbspecies'".",";
			  }
		  }

		  $sps = rtrim($sps, ",");
		  $sps .= ")";

		  $q  = "SELECT distinct O1.orf as orf1, O1.gene as gene1, O1.species as species1, O2.orf as orf2, O2.gene as gene2, O2.species as species2 ";
		  $q .= "from orfgene as O1, orfgene as O2, orthologs as H where O1.orfid=H.orfidsrc and O2.orfid=H.orfiddest and O1.species='$main' and O2.species in $sps and H.classif=$synteny";

		  if ($this->_dbAccess->query("orths", $q)) {
	
			  while ($row = $this->_dbAccess->nextObject("orths")) {
				  $orf = $row['orf1'];
				  $sp = $this->dbclean($row['species2']);
				  $orths[$orf][$sp] = $row;
			  }

			  foreach (array_keys($orths) as $orf) {
				  foreach($combs as $species) {
					  $dbspecies = $this->dbclean($species);
					  if ($species!=$main && !isset($orths[$orf][$dbspecies])) {
						  unset($orths[$orf]);
						  continue 2;
					  }
				  }
			  }
		  }

		  $this->_dbAccess->freeResult("orths");

		  foreach (array_keys($orths) as $orf) {
			  foreach(array_keys($orths[$orf]) as $species) {
	
				  $sp1 = $orths[$orf][$species]['species1'];
				  $sp2 = $orths[$orf][$species]['species2'];

				  $orf1 = $orths[$orf][$species]['orf1'];
				  $orf2 = $orths[$orf][$species]['orf2'];

				  $gene1 = $orths[$orf][$species]['gene2'];
				  $gene2 = $orths[$orf][$species]['gene2'];
			  
				  $aOrths[$sp1][$orf1] = $gene1;
				  $aOrths[$sp2][$orf2] = $gene2;
			  }
		  }
		  return $aOrths;
	  }

	 function getCombinationCounts($allComb,$synteny=0) { // gives ortholog counts and labels for each combination of species

                $aCounts = array();
                $aLabels = array();
		$orths=[];

                foreach ($allComb as $key => $combs){

                        $shortComb = $this->nameReducer($combs);

			$aLabels[$key] = join(" & ",$shortComb);

			if (count($combs)==1) { 
				$dbspecies = $this->dbclean($combs['0']);
				$q = "select distinct O1.orf as orf1 from orfgene as O1 where O1.species='$dbspecies'"; 
                        	if ( $this->_dbAccess->query("orths", $q)) { $aCounts[$key] = $this->_dbAccess->num_rows("orths"); }
				else { $aCounts[$key] = 0; }
				$this->_dbAccess->freeResult("orths");
			}
			else {

				$sps = "(";
				$main='';
				foreach($combs as $species){
					$dbspecies = $this->dbclean($species);
					if ($main==''){ $main = $dbspecies; }
					else {
						$sps .= "'$dbspecies'".",";
					}
				}

				$sps = rtrim($sps, ",");
				$sps .= ")";

				$q  = "SELECT distinct O1.orf as orf1, O1.gene as gene1, O1.species as species1, O2.orf as orf2, O2.gene as gene2, O2.species as species2 ";
				$q .= "from orfgene as O1, orfgene as O2, orthologs as H where O1.orfid=H.orfidsrc and O2.orfid=H.orfiddest and O1.species='$main' and O2.species in $sps and H.classif=$synteny";

				if ($this->_dbAccess->query("orths", $q)) {
					
					while ($row = $this->_dbAccess->nextObject("orths")) {
						$orf = $row['orf1'];
						$sp = $this->dbclean($row['species2']);
						$orths[$orf][$sp] = 1;
					}
					foreach (array_keys($orths) as $orf) { 
						foreach($combs as $species) {
							$dbspecies = $this->dbclean($species);
							if ($species!=$main && !isset($orths[$orf][$dbspecies])) { 
									unset($orths[$orf]);
									continue 2;
							}
					
						}
					}
					$aCounts[$key] = count($orths);
				} else {				
					$aCounts[$key] = 0;
				}
				$this->_dbAccess->freeResult("orths");
			}	
		}

                return array($aCounts,$aLabels);

          }

	function get_array_combination($arr) { // gives all the possible combinations with the elements of the given array
	    $results = array(array( ));

	    foreach ($arr as $values)
        	foreach ($results as $combination)
                	array_push($results, array_merge(array($values),$combination)); // Get new values and merge to your previous combination. And push it to your results array
	    return $results;
	}

	/******************************* getRegulon *******************************/
	function getRegulon($tf) { // service for venn regulon, gets documented and calculated potencial regs

		  $regs = [];
		  $regData = [];
		  $aTGs = [];      
		  $pottargets = [];

		  list($aTFs, $unknownTFs) = $this->user2info($tf,$this->aSpecies());
		  if (empty($aTFs)) return array($regs, $regData, $aTGs);

		  $tfid = array_keys($aTFs)[0];
		  $sp = $aTFs[$tfid]['species'];

		  $aRegIDs = [];
	       	  // Get Documented Regulations
		
		  $regs1 = $this->getRegulations(array_keys($aTFs), [], '', '', '', true, false, false) ;

		  if (!empty($regs1)){
			  $aTGids= [];
			  $regs1 = current($regs1);
			  foreach($regs1 as $regid => $tgid){
				  if (!in_array($tgid,$aTGids))   $aTGids[] = $tgid;
				  if (!in_array($regid,$aRegIDs)) $aRegIDs[$regid]= $regid;
				  $regs[$tgid] = $regid;
			  }
		  } else {
			  return array($regs, $regData, $aTGs);
		  }

		  // Get Regulation Data
		  $regData = $this->getRegData($aRegIDs,false);
	  
		  $aPromoters = $this->upstreamSeq('',$sp);
	  
		  $aMotifs = $this->getMotifsFromDB(false, $aTFs); 
		  $inputmotifs = [];
		  foreach(array_keys($aMotifs) as $tfid) {
			  foreach (array_keys($aMotifs[$tfid]) as $tf) {
				  foreach($aMotifs[$tfid][$tf] as $motif) {
					  if (!isset($inputmotifs[$motif][$motif])) $inputmotifs[$motif][$motif] = [];
					  $inputmotifs[$motif][$motif][] = $tfid;
				  }
			  }	  
		  }
	  
		  list($matches,$motif2tf) = $this->tfBindingSites($aPromoters, $inputmotifs);

		  foreach (array_keys($matches) as $tgid){
			  $aTGids[] = $tgid;	  
		  }	
	  
		  $aTGs = $this->orfIDs2Info($aTGids);

		  $potregcount = [];
		  foreach (array_keys($matches) as $tgid){
			  if (!isset($aTGs[$tgid])) continue;
			  $orf = $aTGs[$tgid]['orf'];
			  $pottargets[$orf] = $orf;
			  foreach (array_keys($matches[$tgid]) as $motif){
				  if (isset($potregcount[$motif])){ $potregcount[$motif]++;}
				  else { $potregcount[$motif] = 1 ;}
			  }
		  }
		  return array($regs, $regData, $aTGs, $potregcount, $pottargets);
	}

        /******************************* buildJSON *******************************/
	function buildJSONComp($aHomoTFids,$aHomoTGids,$aInfo,$homoTFs,$homoTGs,$aCurrSp,$homologSpecies){ // Builds JSON file for Cross Species Network comparison

		$currentSpecies = array_shift($aCurrSp); 

		$homoID2curr = [];
		$currID2homo = [];
		foreach (array_keys($homoTFs) as $ORF){
			print "<pre>";
			print_r($homoTFs[$ORF]);
			print "</pre>";
			foreach ($homoTFs[$ORF]['homo']['orfid'] as $k=>$homoTFid){
				$orfgene = ($homoTFs[$ORF]['gene']=='Uncharacterized')?$ORF:$homoTFs[$ORF]['gene'];
				$homoID2curr[$homoTFid][] = $orfgene;
				$orfgene2 = ($aInfo[$homoTFid]['gene']=='Uncharacterized')?
					$aInfo[$homoTFid]['orf']:$aInfo[$homoTFid]['gene'];
			       	$currID2homo[$homoTFs[$ORF]['orfid']][] = $orfgene2; 
        		}
		}

		foreach (array_keys($homoTGs) as $TG){
			foreach ($homoTGs[$TG]['homo']['orfid'] as $k=>$homoTGid){
				$orfgene = ($homoTGs[$TG]['gene']=='Uncharacterized')?$TG:$homoTGs[$TG]['gene'];
				$homoID2curr[$homoTGid][] = $orfgene;
				$orfgene2 = ($aInfo[$homoTGid]['gene']=='Uncharacterized')?
					$aInfo[$homoTGid]['orf']:$aInfo[$homoTGid]['gene'];
				$currID2homo[$homoTGs[$TG]['orfid']][] = $orfgene2;
        		}
		}

		$homoTGs = $homoTGs + $homoTFs;
		$homoTFs = $homoTGs;

		$json = "";
		list($regs, $regData) =	$this->getJSONregs($aHomoTFids, $aHomoTGids);

		$regsAllLoc   = [];
		$regsUniqLoc  = [];
		$regsCommon   = [];
		$regsUniqHomo = [];
		$regsAllHomo  = [];

		// check all current species regulations
		foreach (array_keys($regs) as $tfid) {
			$species = $aInfo[$tfid]['species'];
			$tf = ($aInfo[$tfid]['gene']=='Uncharacterized')?
				$aInfo[$tfid]['orf']:$aInfo[$tfid]['gene'];
                        $tforf = $aInfo[$tfid]['orf'];
			$tfName = $tf;
			if (isset($currID2homo[$tfid])){ $tfName .= " (".join(", ",$currID2homo[$tfid]).")";}
			elseif (isset($homoID2curr[$tfid])){ $tfName .= " (".join(", ",$homoID2curr[$tfid]).")";}
			foreach (array_keys($regs[$tfid]) as $tgid) {
				$tgorf = $aInfo[$tgid]['orf'];
				$tg = ($aInfo[$tgid]['gene']!='Uncharacterized')?
					$aInfo[$tgid]['gene']:$aInfo[$tgid]['orf'];
				$tgName = $tg;
				if ($species==$currentSpecies){
					if (isset($currID2homo[$tgid])){ $tgName .= " (".join(", ",$currID2homo[$tgid]).")";}
					$regid = $regs[$tfid][$tgid]['regid'];
					$regsAllLoc[$tfName][$tgName] = $regData[$regid];
					$regsAllLoc[$tfName][$tgName]['join'] = $regs[$tfid][$tgid]['join'];

					// Unique doesn't exist in homoSpecies
					if (($regs[$tfid][$tgid]['join']!='Y') && (!isset($currID2homo[$tfid]) || !isset($currID2homo[$tgid]))) {
						$regsUniqLoc[$tfName][$tgName] = $regData[$regid];
						$regsUniqLoc[$tfName][$tgName]['join'] = $regs[$tfid][$tgid]['join'];
					} else {
						$hasReg = false;
						if (isset($homoTFs[$tforf]) && isset($homoTGs[$tgorf])) { // if it does not exist in homolog species then it is unique
							foreach ($homoTFs[$tforf]['homo']['orfid'] as $k=>$homoTF) {
								$aTGs = [];
								$regTmp = [];
								foreach ($homoTGs[$tgorf]['homo']['orfid'] as $j=>$homoTG) {
									if (isset($regs[$homoTF][$homoTG])) {
										//$hasReg = true;
										$homoregid = $regs[$homoTF][$homoTG]['regid'];
										$big = array_intersect($regData[$regid]['biggroup'], $regData[$homoregid]['biggroup']);
										$sub = array_intersect($regData[$regid]['subgroup'], $regData[$homoregid]['subgroup']);
										if (!empty($big) || !empty($sub)) {
											$g = ($aInfo[$homoTG]['gene']=='Uncharacterized')?$aInfo[$homoTG]['orf']:$aInfo[$homoTG]['gene'];
											$aTGs[] = $g;
											if (!isset($regTmp['biggroup'])) {
												$regTmp['biggroup'] = $big;
											} else {
												$regTmp['biggroup'] = array_merge($regData[$regid]['biggroup'], $big);
											}
											if (!isset($regTmp['subgroup'])) {
												$regTmp['subgroup'] = $sub;
											} else {
												$regTmp['subgroup'] = array_merge($regData[$regid]['subgroup'], $sub);
											}
										}
										$regTmp['evd'] = $regData[$regid]['evd'];
										$regTmp['type'] = $regData[$regid]['type'];
									}
								}
								if (!empty($aTGs) && !empty($regTmp['biggroup'])) {
									if (!isset($regTmp['subgroup'])) $regTmp['subgroup'] = [];
									$regTmp['biggroup'] = array_values($regTmp['biggroup']);
									$regTmp['subgroup'] = array_values($regTmp['subgroup']);
									$newtgName = "$tg (" . join(", ", $aTGs) . ")";
									$regsCommon[$tfName][$newtgName] = $regTmp;
									$regsCommon[$tfName][$newtgName]['join'] = $regs[$tfid][$tgid]['join'];
									$hasReg=true;  // ITS UNIQUE ONLY IF NOT COMMON
								}
							}
						}
						if (!$hasReg) {
							$regsUniqLoc[$tfName][$tgName] = $regData[$regid];
							$regsUniqLoc[$tfName][$tgName]['join'] = $regs[$tfid][$tgid]['join'];

						}
					}
				}
				elseif ($species==$homologSpecies){
					if (isset($homoID2curr[$tgid])) $tgName = "$tgName (".join(", ",$homoID2curr[$tgid]).")";	
					$homoregid = $regs[$tfid][$tgid]['regid'];
					$regsAllHomo[$tfName][$tgName] = $regData[$homoregid];
					$regsAllHomo[$tfName][$tgName]['join'] = $regs[$tfid][$tgid]['join'];
	
					// Unique doesn't exist in local Species DOES THIS EVER HAPPENS ?!?!
					if (!isset($homoID2curr[$tfid]) || !isset($homoID2curr[$tgid])) {
						$regsUniqHomo[$tfName][$tgName] = $regData[$homoregid];
						$regsUniqHomo[$tfName][$tgName]['join'] = $regs[$tfid][$tgid]['join'];
					} else {
						$hasReg = false;
						foreach (array_keys($homoTFs) as $tforf) {
							if (!in_array($homoTFs[$tforf]['gene'],$homoID2curr[$tfid])) continue;
							$aTGs = [];
							$regTmp = [];
							foreach (array_keys($homoTGs) as $tgorf) {
								if (!in_array($homoTGs[$tgorf]['gene'],$homoID2curr[$tgid])) continue;
								if (isset($regs[$homoTFs[$tforf]['orfid']][$homoTGs[$tgorf]['orfid']])) {
									$hasReg = true;
									$regid = $regs[$tfid][$tgid]['regid'];
									$big = array_diff($regData[$homoregid]['biggroup'], $regData[$regid]['biggroup']); 
									$sub = array_diff($regData[$homoregid]['subgroup'], $regData[$regid]['subgroup']); 
									if (!empty($big) || !empty($sub)) {
										$aTGs[] = $tg;
										if (!isset($regTmp['biggroup'])) { $regTmp['biggroup'] = $big;	} 
										else {	$regTmp['biggroup'] = array_merge($regData[$homoregid]['biggroup'], $big);	}
										if (!isset($regTmp['subgroup'])) { $regTmp['subgroup'] = $sub;	} 
										else {	$regTmp['subgroup'] = array_merge($regData[$homoregid]['subgroup'], $sub);	}
									}
									$regTmp['evd'] = $regData[$homoregid]['evd'];
									$regTmp['type'] = $regData[$homoregid]['type'];
								}
							}
							if (!empty($aTGs) && !empty($regTmp['biggroup'])) {
								if (!isset($regTmp['subgroup'])) $regTmp['subgroup'] = [];
							}
						}
						if (!$hasReg) {
							$regsUniqHomo[$tfName][$tgName] = $regData[$homoregid];
							$regsUniqHomo[$tfName][$tgName]['join'] = $regs[$tfid][$tgid]['join'];
						}
					}
				}
			}
		}
	
		$json  = $this->buildSubJSON($regsAllLoc,   "all$currentSpecies");
		if (!empty($json)) $json .= ",\n";
		$json .= $this->buildSubJSON($regsUniqLoc,  "unq$currentSpecies");
		if (!empty($json)) $json .= ",\n";
		$json .= $this->buildSubJSON($regsCommon,   "common");
		if (!empty($json)) $json .= ",\n";
		$json .= $this->buildSubJSON($regsUniqHomo, "unq$homologSpecies");
		if (!empty($json)) $json .= ",\n";
		$json .= $this->buildSubJSON($regsAllHomo,  "all$homologSpecies");
		if (!empty($json)) $json .= "\n";
		$json = "[\n$json]";

		list($url,$name) = $this->saveTmpFile($json, "RegulatoryNetwork", "json");
		return array($url, $name);
	}

	function buildSubJSON($regs, $species) {
		$json = "";

		foreach (array_keys($regs) as $source) {
			foreach (array_keys($regs[$source]) as $target) {
				if (!empty($json)) $json .= ",\n";
				$json .= "\t{ source: {name: \"$source\"}, target: {name: \"$target\"}, ";
				$json .= "species: \"$species\", join: \"".$regs[$source][$target]['join']."\", ";
				$json .= "biggroup: " . json_encode($regs[$source][$target]['biggroup']) . ", ";
				$json .= "subgroup: " . json_encode($regs[$source][$target]['subgroup']) . ", ";
				$json .= "evd: \"" . $regs[$source][$target]['evd'] . "\", ";
				$json .= "type: \"" . $regs[$source][$target]['type'] . "\"}";
			}
		}
		return $json;
	}
	
	private function getJSONregs($aTFids,$aTGids) { // Get the regulations without and with join for all ids
		$regs = [];
		$regData = [];

		$aRegIDs = [];
		// Get regulations -- No JOIN
		$regs1 = $this->getRegulations($aTFids, $aTGids, '', '', '', true, false, false) ;
		
		foreach (array_keys($regs1) as $tfid) {	
			foreach($regs1[$tfid] as $regid => $tgid){
				if (!in_array($regid,$aRegIDs)) $aRegIDs[$regid]= $regid;
				$regs[$tfid][$tgid]['regid'] = $regid;
				$regs[$tfid][$tgid]['join'] = 'N';
			}
		}

		// Get regulations -- With JOIN
		$regs2 = $this->getRegulations($aTFids, $aTGids, '', '', '', true, false, true) ;

		foreach (array_keys($regs2) as $tfid) {
			foreach($regs2[$tfid] as $regid => $tgid){
				if (!in_array($regid,$aRegIDs)) $aRegIDs[$regid]= $regid;
				if (isset($regs[$tfid][$tgid])) continue;
                                $regs[$tfid][$tgid]['regid'] = $regid;
                                $regs[$tfid][$tgid]['join'] = 'Y';
                        }
		}

		if (!empty($aRegIDs)) $regData = $this->getRegData($aRegIDs);	

		return array($regs, $regData);
	}

	function buildJSONViz($aTFInfo,$aTGInfo,$curr2homo,$homo2curr,$currSp){ // build JSON for Cross species Homologous network analysis

		$aInfo = $aTFInfo+$aTGInfo;

		$json = "";

		list($regs, $regData) =	$this->getJSONregs(array_keys($aTFInfo),array_keys($aTGInfo));

		$aHomoSp = [];

		$json = "";
		foreach(array_keys($regs) as $tfid){
			foreach (array_keys($regs[$tfid]) as $tgid) {
				$species = $aInfo[$tfid]['species'];
				if (!in_array($species,$aHomoSp)) $aHomoSp[]=$species;
				if (!empty($json)) { $json .= ","; }
				$target = [];
				$source = [];
				if (in_array($species,$currSp)){
					if (empty($source) && isset($curr2homo[$tfid])){
						foreach ($curr2homo[$tfid] as $htfid){
							$htf = ($aInfo[$htfid]['gene']=='Uncharacterized')?
								$aInfo[$htfid]['orf']:$aInfo[$htfid]['gene'];
							if (!in_array($htf, $source)) $source[] = $htf;
						}
					}
					if (empty($target) && isset($curr2homo[$tgid])){
						foreach ($curr2homo[$tgid] as $htgid){
							$htg = ($aInfo[$htgid]['gene']=='Uncharacterized')?
                                                                $aInfo[$htgid]['orf']:$aInfo[$htgid]['gene'];
							if (!in_array($htg, $target)) $target[] = $htg;
						}
					}
				} else {
					if (empty($source) && isset($homo2curr[$tfid])){
						foreach ($homo2curr[$tfid] as $htfid){
							$htf = ($aInfo[$htfid]['gene']=='Uncharacterized')?
                                                                $aInfo[$htfid]['orf']:$aInfo[$htfid]['gene'];
							if (!in_array($htf, $source)) $source[] = $htf;
						}
					}
					if (empty($target) && isset($homo2curr[$tgid])){
						foreach ($homo2curr[$tgid] as $htgid){
							$htg = ($aInfo[$htgid]['gene']=='Uncharacterized')?
                                                                $aInfo[$htgid]['orf']:$aInfo[$htgid]['gene'];
							if (!in_array($htg, $target)) $target[] = $htg;
						}
					}
				}
				if (!empty($source)){
					$tflist = "";
					foreach ($source as $src){
						if ($tflist == "") {$tflist = $src;}
						else $tflist .= ", $src";
					}
				}
				$tf = ($aInfo[$tfid]['gene']=='Uncharacterized')?
                                                                $aInfo[$tfid]['orf']:$aInfo[$tfid]['gene'];
				if (!in_array($species,$currSp)) {
					if (!empty($source)) {
						$json .= "\n{source: {name: \"$tf ($tflist)\"}, ";
					} else {
						$json .= "\n{source: {name: \"$tf\"}, ";
					}
				} else {
					$json .= "\n{source: {name: \"$tf\"}, ";
				}
				if (!empty($target)) {
					$tglist = "";
					foreach ($target as $tgt){
						if ($tglist == "") {$tglist = $tgt;}
						else $tglist .= ", $tgt";
					}
				}
				$tg = ($aInfo[$tgid]['gene']=='Uncharacterized')?
                                                                $aInfo[$tgid]['orf']:$aInfo[$tgid]['gene'];
				if (!in_array($species,$currSp)) {
					if (!empty($target)) {
						$json .= "target: {name: \"$tg ($tglist)\"}, ";
					} else {
						$json .= "target: {name: \"$tg\"}, ";
					}
				} else {
					$json .= "target: {name: \"$tg\"}, ";
				}
				$json .= "species: \"$species\", ";

				$join = $regs[$tfid][$tgid]['join'];
				$json .= "join: \"". $join ."\", ";

				$regid = $regs[$tfid][$tgid]['regid'];
				if (isset($regData[$regid])) {
					$json .= "biggroup: ".json_encode($regData[$regid]['biggroup']).", ";
					$json .= "subgroup: ".json_encode($regData[$regid]['subgroup']).", ";
					$json .= "evd: \"".$regData[$regid]['evd']."\", ";
					$json .= "type: \"".$regData[$regid]['type']."\"}";
				} else {
					$json .= "}";
				}
		}
	}

	$json = "[".$json."\n]";
	list($url,$name) = $this->saveTmpFile($json, "RegulatoryNetwork", "json");
	return array($url,$name,$aHomoSp);
	}


  private function reduceRegDataEvd($old, $evd) { // translate evidence codes
    if (empty($old)) {
      $old = "unspec";
    }
		// Ignoring "Prediction"
    switch ($old) {
    case "unspec":
      if ("Direct"==$evd) {
        return "bindonly";
      } elseif ("Indirect"==$evd) {
        return "expronly";
			}
      return "unspec";
    case "bindonly":
      if ("Indirect"==$evd) {
        return "bindexpr";
      }
      return "bindonly";
    case "bindexpr":
      return "bindexpr";
    case "expronly":
      if ("Direct"==$evd) {
        return "bindexpr";
      }
      return "expronly";
    }
    return "unspec";
  }
	private function reduceRegDataType($old, $type) { // translate association codes
    if (empty($old)) {
      $old = "unspec";
    }
    switch ($old) {
    case "unspec":
      if ($type == "Positive") {
        return "pos";
      } elseif ($type == "Negative") {
        return "neg";
      } elseif ($type == "Positive/Negative") {
        return "dual";
      }
      return "unspec";
    case "pos":
      if ($type == "Negative") {
        return "dual";
      } elseif ($type == "Positive/Negative") {
        return "dual";
      }
      return "pos";
    case "neg":
      if ($type == "Positive") {
        return "dual";
      } elseif ($type == "Positive/Negative") {
        return "dual";
      }
      return "neg";
    case "dual":
      return "dual";
    }
    return "unspec";
  }

	# IS THIS GOING TO BE USED SOMEWHERE ?!?!
	#
  function getAllPaths($srcNode, $dstNode, $maxLen) {
    list($srcOrfGene, $unknownSrc) = $this->user2Info($srcNode,$this->aSpecies()); // CONFIRM IF MAKES SENSE WERE
    list($dstOrfGene, $unknownDst) = $this->user2Info($dstNode,$this->aSpecies());
    if (empty($srcOrfGene) || empty($dstOrfGene)) {
      if (empty($srcOrfGene))
        $unknown[] = $srcNode;
      if (empty($dstOrfGene))
        $unknown[] = $dstNode;
      return array(array(), array(), $unknown);
    }

    $a = microtime(true);
    require_once('DirectedGraph.php');
    $graph = new DirectedGraph();

    $q  = "select T.orfname as src, R.orfname as dest from regulation as R, translation as T ";
    $q .= "where T.proteinname=R.proteinname";
    if ($this->_dbAccess->query("allRegs", $q)) {
      while ($row = $this->_dbAccess->nextObject("allRegs"))
        $graph->addEdge($row['src'], $row['dest']);
			// strtoupper removed for speedup
    }
    $this->_dbAccess->freeResult("allRegs");
    $b = microtime(true);
    print "Get whole DB regulatory graph: " . ($b - $a) . " s<br/>";

    $a = microtime(true);
    // Get all paths from graph
    list($pathset, $orfset) = $graph->getAllPaths(array_keys($srcOrfGene)[0], array_keys($dstOrfGene)[0], $maxLen);
    list($orf2gene, $unknown) = $this->user2Info(join(" ", $orfset),$this->aSpecies());
    $b = microtime(true);
    print "Compute relevant paths: " . ($b - $a) . " s<br/>";

    $a = microtime(true);
    // Classification of edges
    $edgeSet = array();
    foreach ($pathset as $path) {
      $q  = "select sum(case when RD.evidencecode like 'Direct: %' ";
      $q .= "then 1 else 0 end) as dir, sum(case when (RD.";
      $q .= "evidencecode like 'Indirect: %' and RD.type like ";
      $q .= "'Positive%') then 1 else 0 end) as undirPos, sum(case ";
      $q .= "when (RD.evidencecode like 'Indirect: %' and RD.type ";
      $q .= "like '%Negative') then 1 else 0 end) as undirNeg from ";
      $q .= "regulationdata as RD, regulation as R where R.";
      $q .= "regulationID=RD.regulationID and R.proteinname='";
      $q .= addslashes($orf2gene[$path[0]])."p' and R.orfname='{$path[1]}'";
      if ($this->_dbAccess->query("regType", $q)) {
        $row = $this->_dbAccess->nextObject("regType");
        if ($row['dir'] > 0) {
          $edgeSet[] = array($path[0], $path[1], "direct");
        } elseif ($row['undirPos'] > 0 && $row['undirNeg'] == 0) {
          $edgeSet[] = array($path[0], $path[1], "undirPos");
        } elseif ($row['undirPos'] == 0 && $row['undirNeg'] > 0) {
          $edgeSet[] = array($path[0], $path[1], "undirNeg");
        } else {
          $edgeSet[] = array($path[0], $path[1], "undirDual");
        }
      } else {
        $edgeSet[] = array($path[0], $path[1], "unknown");
      }
      $this->_dbAccess->freeResult("regType");
      $b = microtime(true);
    }
    print "Edge classification: " . ($b - $a) . " s<br/>";


    return array($edgeSet, $orf2gene, $unknown);

  }

	function mapRegulations($homoDocReg,$homoORFGenes,$homoTFs) {  // maps regulations in current species from regulations in homologous species

		$mapping = [];
		$docReg = array();

		foreach (array_keys($homoDocReg) as $homotfid) {
			foreach(array_keys($homoTFs) as $tf) {
				$tfid = $homoTFs[$tf]['orfid'];
				foreach($homoTFs[$tf]['homo']['orfid'] as $j=>$homoid) {
					if ($homoid==$homotfid){
						$tfid = $homoTFs[$tf]['orfid'];
						$mapping[$tfid]['gene'] = $homoTFs[$tf]['homo']['gene'][$j];
						$mapping[$tfid]['orf'] = $homoTFs[$tf]['homo']['orf'][$j];
						foreach (array_values($homoDocReg[$homotfid]) as $homotargetid) {
							foreach(array_keys($homoORFGenes) as $orf){
								foreach($homoORFGenes[$orf]['homo']['orfid'] as $i=>$homoorfid) {
									if ($homoorfid==$homotargetid){
										$orfid = $homoORFGenes[$orf]['orfid'];
										if (isset($docReg[$tfid])&&in_array($orfid,$docReg[$tfid])) continue;
										$docReg[$tfid][] = $orfid;
										$mapping[$orfid]['gene'] = $homoORFGenes[$orf]['homo']['gene'][$i];
										$mapping[$orfid]['orf'] = $homoORFGenes[$orf]['homo']['orf'][$i];
									}
								}
							}
						}
					}
				}
			}
		}

		return array($docReg,$mapping);
	}

	function writeCSV($func, $content) {
		$pathtmp = $this->_properties->get("path.tmp.local");
		$fname = $func."_".rand() . ".csv";
		file_put_contents($pathtmp . $fname, $content);
		return $this->_properties->get("path.tmp.url") . $fname;
	}

	/*
	 * generic call to db regulations 
	 */
	function getRegulations($aTFs, $aORFGenes, $ecgroup, $eviCode, $assocType, $simple=false, $filters=true, $join=false) {

		$regulations = array();

		if ($join) { 
	                $aTFs = array_merge($aTFs,$aORFGenes);
        	        $aORFGenes = $aTFs;
		}

		$q  = "select distinct R.* from regulation as R where ";
		if (!empty($aORFGenes)){ $orflist = join(",",$aORFGenes); $q .= " R.targetid in ($orflist)"; }
		if (!empty($aORFGenes) && !empty($aTFs)) $q .= " and ";
		if (!empty($aTFs)){ $tflist = join(",",$aTFs); $q .= " R.tfid in ($tflist) "; }
		if (empty($aORFGenes) && empty($aTFs)) $q .= "R.tfid in (select orfid from orfgene where {$this->dbSpecies()}) ";
		if ($filters){
			$q .= $this->getMysqlEviCode($eviCode, $assocType);
			$q .= $this->getMysqlEnvCond($ecgroup);
		}
		if (!$simple) {
			 // tgs and tfs that have regulations
	                $aTGids = [];
        	        $aTFids = [];
		}
		if ($this->_dbAccess->query("reg", $q)) {
			while ($row = $this->_dbAccess->nextObject("reg")) {
				$regulations[$row['tfid']][$row['regulationid']] = $row['targetid'];
				if (!$simple) { 
					if (!in_array($row['tfid'],$aTFids)) $aTFids[$row['tfid']] = $row['tfid'];
				        if (!in_array($row['targetid'],$aTGids)) $aTGids[$row['targetid']] = $row['targetid'];	
				}
			}
		}
		$this->_dbAccess->freeResult("reg");

		if ($simple) return $regulations;
		return array($regulations,$aTFids,$aTGids);
	}

	function getRegData($aRegIDs,$condgroups=true) { // get regulation data from reg ids
                $regData = [];
                $sRegIDs = join(",", $aRegIDs);
                // Get Regulation Data
                $q  = "select distinct R.regulationid, R.association, E.code, ";
                $q .= "E.experiment from regulationdata as R, evidencecode as E ";
                $q .= "where R.regulationid in ($sRegIDs) and R.evidencecodeid=E.evidencecodeid";
                if ($this->_dbAccess->query("regdata", $q)) {
                        while ($row = $this->_dbAccess->nextObject("regdata")) {
                                $regid = $row['regulationid'];
                                // Evd
                                if (!isset($regData[$regid]['evd'])) $regData[$regid]['evd'] = "";
                                $regData[$regid]['evd'] = $this->reduceRegDataEvd($regData[$regid]['evd'], $row['code']);
                                // Type
                                if (!isset($regData[$regid]['type'])) $regData[$regid]['type'] = "";
                                $regData[$regid]['type'] = $this->reduceRegDataType($regData[$regid]['type'], $row['association']);
                                // Groups
                                $regData[$regid]['biggroup'] = [];
                                $regData[$regid]['subgroup'] = [];
                        }
                }
                $this->_dbAccess->freeResult("regdata");

		if ($condgroups) {
			// Get Environmental Condition groups
			$q  = "select distinct R.regulationid, EG.* from regulationdata as R, ";
			$q .= "envcondition as E, envconditiongroups as EG where R.regulationid ";
			$q .= "in ($sRegIDs) and R.envconditionid=E.envconditionid and ";    
			$q .= "E.groupid=EG.groupid";
			if ($this->_dbAccess->query("envcond", $q)) {
				while ($row = $this->_dbAccess->nextObject("envcond")) {
					$regid = $row['regulationid'];
					array_push($regData[$regid]['biggroup'], $row['biggroup']);
					array_push($regData[$regid]['subgroup'], $row['subgroup']);
				}
			}
			$this->_dbAccess->freeResult("envcond");
		}
		else {
			return $regData;
		}

                return $regData;
        }

	function regulations2stats($regulations,$hypern){

		$regCount = array();

		foreach (array_keys($regulations) as $tfid) {
			$regCount[$tfid]['setNum'] = count($regulations[$tfid]);
			# % reg in Set
			$regCount[$tfid]['setPer'] = $regCount[$tfid]['setNum'] / $hypern;
		}

		$q2  = "select count(distinct R.targetid) as numGenes from ";
		$q2 .= "regulation as R where R.tfid='";
	
		foreach (array_keys($regCount) as $tfid) {
			$tmp = $q2 . $tfid."'";
			$regCount[$tfid]['dbNum'] = $this->_dbAccess->getObject("numGenes", $tmp);
			$regCount[$tfid]['dbPer'] = $regCount[$tfid]['setNum'] / $regCount[$tfid]['dbNum']; 
		}
		return $regCount;
	}

	# input regCount[id][SetNum, SetPer, DBNum, DBPer]
	function callRhyper($regCount,$hyperN,$hypern){

		/*		
			# Hypergeometric call to R
			# N - number of global elements (p.e. # of genes in the database wth promoter)
			# R - number of elements from N regulated by transcription factors
			# n - number of inserted IDs, applied to orthologIDs, and filtered by species (count user ids)
			# r - number of actual regulations (array regs)
		 */
		
		$content = "";
		foreach (array_keys($regCount) as $tfid) {
			$content .= "print(phyper(".$regCount[$tfid]['setNum'].",".$regCount[$tfid]['dbNum'].",".
				($hyperN - $regCount[$tfid]['dbNum']) . ",".$hypern."), digits=22)\n";
		}
		$Rfile = $this->_properties->get("path.tmp.local")."pvalue_".rand().".R";
		if (file_put_contents($Rfile, $content) === false) {
			throw new Exception("Error writing R hypergeometric functions to disk!");
		}
		$Rpath = $this->_properties->get("prog.R.path");
		$Rbin  = $this->_properties->get("prog.R.bin");
		exec($Rpath . $Rbin . " $Rfile", $aLines);
		$i = 0;
		foreach (array_keys($regCount) as $tfid) {
			$val = substr($aLines[$i],4);
			$regCount[$tfid]['pvalue'] = 1 - substr($aLines[$i],4);
			$i++;
		}
		return $regCount;
	}

	/*
	 * iupacGeneration
	 */
	function iupacGeneration($sequences) {
	    $path = $this->_properties->get("prog.espresso.path");
	    $bin  = $this->_properties->get("prog.espresso.bin");
	    
		$espresso_bin = array("A" => "1000", 
				      "C" => "0100", 
				      "G" => "0010", 
				      "T" => "0001", 
				      "R" => "1010",
				      "Y" => "0101",
				      "S" => "0110",
				      "W" => "1001",
				      "K" => "0011",
				      "M" => "1100",
				      "B" => "0111",
				      "D" => "1011",
				      "H" => "1101",
				      "V" => "1110",
				      "N" => "1111",
				      "." => "0000",
				      "-" => "0000");
		$espresso_iupac = array("1000" => "A", 
				        "0100" => "C", 
  				        "0010" => "G", 
				        "0001" => "T", 
				        "1010" => "R",
				        "0101" => "Y",
				        "0110" => "S",
				        "1001" => "W",
				        "0011" => "K",
				        "1100" => "M",
				        "0111" => "B",
				        "1011" => "D",
				        "1101" => "H",
				        "1110" => "V",
				        "1111" => "N",
				        "0000" => ".",
				        "0000" => "-");

		$nseq = count($sequences);
		$size_seq = strlen($sequences[0]);

		$espresso_in = ".mv ". ($size_seq+1) . " 0 ";
		for($i = 0; $i < $size_seq;$i++)
			$espresso_in .= "4 ";
		$espresso_in .= "1\n";
		foreach($sequences as $seq){
			for ($i = 0 ; $i  < $size_seq ; $i++){
				$espresso_in .= @$espresso_bin[$seq{$i}];
				$espresso_in .= " ";
			}
			$espresso_in .= "1\n";
		}
		
		$descriptorspec = array(
			0 => array("pipe", "r"),
			1 => array("pipe", "w"),
			2 => array("pipe", "w")
		);
		$process = proc_open($path . $bin, $descriptorspec, $pipes);
		fwrite($pipes[0], $espresso_in);
		fclose($pipes[0]);

		$espresso_out = "";
		 while (!feof($pipes[1])) {
		       $espresso_out .= fgets($pipes[1]);
		}
		fclose($pipes[1]);
		fclose($pipes[2]);/*error could be handled before pipe closing...*/

		$return_value = proc_close($process);

		$iupac = array();
		
		$lines = explode("\n", trim($espresso_out));
		$a1stLine = explode(" ", $lines[1]);
		$iupac_size = array_pop($a1stLine);

		for ($i = 2 ; $i < $iupac_size+2; $i++){
			$iupac_seq = "";
			$codes = explode(" ", trim($lines[$i]));
			array_pop($codes); /*throw away last number (1)*/
			foreach ($codes as $code) {
				$iupac_seq .= $espresso_iupac[$code];
			}
			array_push($iupac, $iupac_seq);
		}
		
		return $iupac;
	}
	

	function findHomoRegulated($homoTFInfo,$toSpecies,$synteny) {

		$tfidlist = [];
		foreach (array_keys($homoTFInfo) as $tf) {
			foreach ($homoTFInfo[$tf]['homo']['orfid'] as $key => $homoTFid){ 
				$tfidlist[$homoTFid]= $homoTFid;
			}
		}

		// find regulations with the homologous ids
		list($regs,$aHomoTFids,$aHomoTGids) = $this->getRegulations($tfidlist, [], '', '', '', false, false, false);

		$homoDocReg = [];
		foreach(array_keys($regs) as $tfid){
			foreach($regs[$tfid] as $regid => $tgid){
                                  $homoDocReg[$tfid][] = $tgid;
                        }
		}
		
		if (!empty($homoDocReg)){
			$TGInfo = $this->getHomoORFInfo(array_keys($aHomoTGids), $toSpecies, $synteny, true);
			$aTGids = [];
			foreach(array_keys($TGInfo) as $homotargets){
				foreach($TGInfo[$homotargets]['homo']['orfid'] as $tgid){
					$aTGids[$tgid] = $tgid;	
				}
			}
			// Mapping back to the original species
			$homoTGInfo = $this->getHomoORFInfo(array_keys($aTGids), $toSpecies, $synteny);

			list($docReg,$mapping) = $this->mapRegulations($homoDocReg,$homoTGInfo,$homoTFInfo);
		}
		else {
			$docReg = [];
			$mapping = [];
			return array($docReg,$mapping);
		}

		return array($docReg,$mapping);
	}

 	function getMysqlEnvCond($ecgroup) { // to include in regulation queries to the DB
		if (!$ecgroup['big']) return "";
		$q  = " and R.regulationid in (select distinct regulationid from regulationdata ";
		$q .= "where envconditionid in (select envconditionid from envcondition where ";
		$q .= "groupid in (select groupid from envconditiongroups where ";
		$q .= "biggroup='{$ecgroup['big']}'";
		if ($ecgroup['sub']) $q .= " and subgroup='{$ecgroup['sub']}'";
	 	return "$q)))";
 	}

	function getMysqlEviCode($eviCode, $assocType) { // to include in regulation queries to the DB
		$q = " and R.regulationid in (select distinct regulationid from regulationdata where evidencecodeid in (select evidencecodeid from evidencecode where ";
		if ($eviCode['dir'])	$q .= "code='Direct'";
		if ($eviCode['indir'])	$q .= "code='Indirect'" . $this->getMysqlAssocType($assocType);
		if ($eviCode['and'])	return $q . "code='Direct')) and R.regulationid in (select distinct regulationid from regulationdata where evidencecodeid in (select evidencecodeid from evidencecode where code='Indirect')" . $this->getMysqlAssocType($assocType). ")";
		if ($eviCode['plus'])	return $q . "code='Direct' or code='Indirect')" . $this->getMysqlAssocType($assocType).")";
		return "$q))";
	}

	function getMysqlAssocType($assocType) { // to include in regulation queries to the DB
		if ( $assocType['pos'] && !$assocType['neg'] && !$assocType['NA']) $q = " and association like 'Positive%'";
		if ( $assocType['pos'] && !$assocType['neg'] &&  $assocType['NA']) $q = " and (association like 'Positive%' or association like 'N/A')";
		if ( $assocType['pos'] &&  $assocType['neg'] && !$assocType['NA']) $q = " and (association like 'Positive%' or association like '%Negative')";
		if ( $assocType['pos'] &&  $assocType['neg'] &&  $assocType['NA']) $q = " and (association like 'Positive%' or association like '%Negative' or association like 'N/A')";
		if (!$assocType['pos'] &&  $assocType['neg'] && !$assocType['NA']) $q = " and association like '%Negative'";
		if (!$assocType['pos'] &&  $assocType['neg'] &&  $assocType['NA']) $q = " and (association like '%Negative' or association like 'N/A')";
		return $q;
	}

	function findHomoRegulators($homoTGInfo,$toSpecies,$synteny) {

		$mapping1 = [];
		$orfidlist = [];
		foreach (array_keys($homoTGInfo) as $orf) {
			foreach ($homoTGInfo[$orf]['homo']['orfid'] as $key => $orfid){
				$orfidlist[$orfid]= $orfid;
				$mapping1[$orf]['orf'][]  = $homoTGInfo[$orf]['homo']['orf'][$key];
                                $mapping1[$orf]['gene'][] = $homoTGInfo[$orf]['homo']['gene'][$key];
			}
		}

		$docReg = array();
		$docRegTo = array();
		$homoTFs = array();

                // find regulations with the homologous ids
                list($regs,$aHomoTFids,$aHomoTGids) = $this->getRegulations([],$orfidlist, '', '', '', false, false, false);

                if (!empty($regs)){

	                $homoDocReg = [];
        	        foreach(array_keys($regs) as $tfid){
                	        foreach($regs[$tfid] as $regid => $tgid){
                        	          $homoDocReg[$tfid][] = $tgid;
	                        }
        	        }

			// Mapping back to the original species
			$homoRevORFs = $this->getHomoORFInfo(array_keys($aHomoTFids), $toSpecies, $synteny, true);
				
			// Get ORF names from the IDs
			$aTFids = [];
			foreach (array_keys($homoRevORFs) as $homoORF) {
				foreach($homoRevORFs[$homoORF]['homo']['orfid'] as $key => $revid){
					$aTFids[$revid]= $revid;
				}
			}

			$homoTFInfo = $this->getHomoORFInfo(array_keys($aTFids), $toSpecies, $synteny, false);

			list($docReg,$mapping2) = $this->mapRegulations($homoDocReg,$homoTGInfo,$homoTFInfo);
			$mapping = $mapping1 + $mapping2;
			return array($docReg, $mapping);
                }
                else {
                        $docReg = [];
                        $mapping = [];
                        return array($docReg,$mapping);
                }
	}

	/*
	 * getRegAssociations
	 */
	function getRegAssociations($aTFs, $aORFGenes, $allToOne = false, $ecgroup, $eviCode, $assocType) {

		$regdoc = array();
			
		// Documented
		if ($allToOne) {
			$q  = "select R.targetid, count(R.tfid) as tfnumber ";
			$q .= "from regulation as R ";
			$q .= " where R.targetid in (".join(",",array_keys($aORFGenes)).")";
		        $q .= " and R.tfid in (".join(",",array_keys($aTFs)).") ";	
			$q .= $this->getMysqlEviCode($eviCode, $assocType);
			$q .= $this->getMysqlEnvCond($ecgroup);
			$q .= " group by R.targetid";
			if ($this->_dbAccess->query("docreg", $q)) {
				while ($row = $this->_dbAccess->nextObject("docreg")) {
                                	$o = $aORFGenes[$row['targetid']]['orf'];
                                	$g = $aORFGenes[$row['targetid']]['gene'];
					$regdoc[$g]['orf'] = $o;
					$regdoc[$g]['tfcount'] = $row['tfnumber'];
				}
				ksort($regdoc);
			}
		} else {
			// One-to-one
			$q = "select R.targetid, R.tfid from regulation as R";
                        $q .= " where R.targetid in (".join(",",array_keys($aORFGenes)).")";
                        $q .= " and R.tfid in (".join(",",array_keys($aTFs)).") ";
			$q .= $this->getMysqlEviCode($eviCode, $assocType);
			$q .= $this->getMysqlEnvCond($ecgroup);
			if ($this->_dbAccess->query("regdoc", $q)) {
				while ($row = $this->_dbAccess->nextObject("regdoc")) {
					$o = $aORFGenes[$row['targetid']]['orf'];
					$g = $aORFGenes[$row['targetid']]['gene'];
					$p = $aTFs[$row['tfid']]['prot'];
					$regdoc[$p][$o] = $g;
				}
			}
		}

		// Potential
		$aPromoters  =      $this->upstreamSeq(array_keys($aORFGenes));

		foreach ($aPromoters as $orf => $fasta) {
                	$seq = preg_replace('/^>.+\n/', '', $fasta);
               		$seq = preg_replace('/\n/','',$seq);
			$aPromoters[$orf] = $seq;
		}

		$aMotifs = $this->getMotifsFromDB(false,$aTFs); 
		list($fwd, $rev) =  $this->matchMotifOnSequence($aMotifs, $aPromoters);

		$potReg = [];
		// FORWARD MATCHES
                foreach (array_keys($fwd) as $orf){
                       	foreach (array_keys($fwd[$orf]) as $motif) {
                               	foreach (array_keys($aMotifs[$motif]) as $tf) {
	                                foreach (array_keys($fwd[$orf][$motif]) as $pos){
       	                                        if (!isset($potReg[$tf][$orf])) {
               	                                        $potReg[$tf][$orf] = $pos;
                       	                        }
                               	                else {
                                       	                $potReg[$tf][$orf] .= ",".$pos;
                                               	}
                                        }
       	                        }
               	        }
		}
		// REV MATCHES
        	foreach (array_keys($rev) as $orf){
       	                foreach (array_keys($rev[$orf]) as $motif) {
               	                foreach (array_keys($aMotifs[$motif]) as $tf) {
                               	        foreach (array_keys($rev[$orf][$motif]) as $pos){
                                       	        if (!isset($potReg[$tf][$orf])) {
                                               	        $potReg[$tf][$orf] = $pos;
                                                }
       	                                        else {
               	                                        $potReg[$tf][$orf] .= ",".$pos;
                      	                        }
                               	        }
                                }
       	                }
		}

		if ($allToOne){
			$regpot = [];
			$count = [];
			foreach (array_keys($potReg) as $tf){
				foreach (array_keys($potReg[$tf]) as $orfid){
					if (!isset($count[$orfid])){ $count[$orfid]=1;}
					else { $count[$orfid]++; }
				}
			}
			
			foreach ($count as $orfid => $tfcount){
				$gene = $aORFGenes[$orfid]['gene'];
				$regpot[$gene]['orf'] = $orfid;
				$regpot[$gene]['tfcount'] = $tfcount;
			}
			return array($regdoc, $regpot,$aMotifs);
		}
		return array($regdoc, $potReg,$aMotifs);
	}
	
	/*
	 * getRegMatrix
	 */
	function getRegMatrix($aTFs, $aORFGenes, $regs, $nBinds = 1, $ecgroup, $eviCode, $assocType) {
		$retObj = array();
		$retObjDoc = array();
		$retObjPot = array();

		$tflist = join(",",array_keys($aTFs));
		$orflist = join(",",array_keys($aORFGenes));

		if ($regs['doc'] || $regs['plus'] || $regs['and']) {
			$q  = "select distinct R.tfid, R.targetid from regulation as R where R.targetid in ($orflist) and R.tfid in ($tflist)  ";
			$q .= $this->getMysqlEviCode($eviCode, $assocType);
			$q .= $this->getMysqlEnvCond($ecgroup);
			if ($this->_dbAccess->query("regs", $q)) {
				while ($row = $this->_dbAccess->nextObject("regs")) {
					$orfid = $row['targetid'];
					$tfid = $row['tfid'];
					if (isset($aORFGenes[$orfid])) {
							$retObjDoc[$tfid][] = $orfid;
					}
				}
			}
			$this->_dbAccess->freeResult("regs");
		}

		if ($regs['pot'] || $regs['plus'] || $regs['and']) { // POTENCIAL REGULATIONS, we only care to know if there is a regulation

			$aPromoters  =   $this->upstreamSeq(array_keys($aORFGenes));
        		foreach ($aPromoters as $orf => $fasta) {
	        	        $seq = preg_replace('/^>.+\n/', '', $fasta);
        	        	$seq = preg_replace('/\n/','',$seq);
	                	$aPromoters[$orf] = $seq;
			}

			$aMotifs = $this->getMotifsFromDB(false,$aTFs);

		    	$matches =  $this->motifOnSequencesHasMatch($aMotifs, $aPromoters,0,true);

	                foreach (array_keys($matches) as $orfid){
        	                foreach (array_keys($matches[$orfid]) as $tfid) {
                        		if (!isset($retObjPot[$tfid]) || !in_array($orfid,$retObjPot[$tfid])) $retObjPot[$tfid][] = $orfid;
	                        }
        	        }

		}

		if     ($regs['plus']) { 
			foreach (array_keys($retObjDoc) as $key){
                                if (isset($retObjPot[$key])){
                                        $retObj[$key] = array_merge($retObjDoc[$key],$retObjPot[$key]);
                                }
                        }
		}
		else if ($regs['and'])  { 
			foreach (array_keys($retObjDoc) as $key){
				if (isset($retObjPot[$key])){
					$retObj[$key] = array_intersect($retObjDoc[$key],$retObjPot[$key]);
				}
			}
		}
		elseif ($regs['doc'])  { 
			$retObj = $retObjDoc ; 
		}
		elseif ($regs['pot'])  { $retObj = $retObjPot ; }

		return $retObj;
	}
	
	
	/*
	 * viewRegulation
	 */
	function viewRegulation($sTFs, $sORFGenes) { // gets all info on the regulation between tfs and orfs
		$retObj = array();

		list($aTFs, $aPUnknown) = $this->user2Info($sTFs,$this->aSpecies());	
		list($aORFGenes, $aGUnknown) = $this->user2Info($sORFGenes,$this->aSpecies());
		
		if (empty($aTFs) || empty($aORFGenes)) return $retObj;

		$q  = "select RD.regulationid, P.protein, O.orf,O.gene, RD.association, ";
		$q .= "RD.strain, EV.code,EV.experiment, Pub.* ";
		$q .= "from pubmed as Pub, regulation as R, orfgene as O, ";
		$q .= "protein as P, regulationdata as RD, evidencecode as EV ";
		$q .= "where P.tfid=R.tfid and R.targetid=O.orfid ";
		$q .= "and O.orfid in (".join(",",array_keys($aORFGenes)).") ";
		$q .= "and P.tfid in (".join(",",array_keys($aTFs)).") ";
		$q .= "and RD.evidencecodeid=EV.evidencecodeid ";
		$q .= "and R.regulationid=RD.regulationid ";
		$q .= "and RD.pubmedid=Pub.pubmedid";
		if ($this->_dbAccess->query("regs", $q)) {
			while ($row = $this->_dbAccess->nextObject("regs")) {
				$p  = $row['protein'];
				$o  = $row['orf'];
				$id = $row['pubmedid'];
				$retObj[$p][$o][$id]['authors'] = $row['authors'];
				$retObj[$p][$o][$id]['journal'] = $row['journal'];
				$retObj[$p][$o][$id]['date']    = $row['date'];
				$retObj[$p][$o][$id]['volume']  = $row['volume'];
				$retObj[$p][$o][$id]['issue']   = $row['issue'];
				$retObj[$p][$o][$id]['pages']   = $row['pages'];
				$retObj[$p][$o][$id]['code']    = $row['code'];
				$retObj[$p][$o][$id]['strain']  = $row['strain'];
				$retObj[$p][$o][$id]['experiment']  = $row['experiment'];
				$retObj[$p][$o][$id]['association'] = $row['association'];
				$retObj[$p][$o][$id]['envcond'] = [];
				$q  = "select EV.envconditiondesc from envcondition as EV, ";
				$q .= "regulationdata as RD where EV.envconditionid=RD.envconditionid ";
				$q .= "and RD.regulationid={$row['regulationid']} and RD.pubmedid=";
				$q .= $row['pubmedid'];
				if ($this->_dbAccess->query("envcond", $q)) {
					while ($row = $this->_dbAccess->nextObject("envcond")) {
						$retObj[$p][$o][$id]['envcond'][] = $row['envconditiondesc'];
					}
				}
				$this->_dbAccess->freeResult("envcond");
			}
		}
		$this->_dbAccess->freeResult("regs");

		return $retObj;
	}
	
	/*
	 * viewConsensus
	 */
	function viewConsensus($sTFs, $sConsensus) { 
		$retObj = array();

		list($aTFs, $aPUnknown) = $this->user2info($sTFs,$this->aSpecies());
		
		$clist = "''";	
		$aConsensus = $this->formString2Array($sConsensus);
		foreach ($aConsensus as $c) {
			$clist .= ", '$c'";
		}

		$q  = "select TFC.tfconsensusid, P.protein, C.IUPACseq, EC.code, ";
		$q .= "EC.experiment, Pub.* from pubmed as Pub, tfconsensus as TFC, ";
		$q .= "consensus as C, protein as P, consensusdata as CD, ";
		$q .= "evidencecode as EC where P.tfid in (".join(",",array_keys($aTFs)).") and ";
		$q .= "C.IUPACseq in ($clist) and C.consensusid=TFC.consensusid and ";
		$q .= "TFC.tfid=P.tfid and TFC.tfconsensusid=CD.tfconsensusid and ";
		$q .= "CD.evidencecodeid=EC.evidencecodeid ";
		$q .= "and Pub.pubmedid=CD.pubmedid order by P.protein asc";
		if ($this->_dbAccess->query("consensus", $q)) {
		    while ($row = $this->_dbAccess->nextObject("consensus")) {
				$retObj[$row['protein']][$row['IUPACseq']][$row['pubmedid']]['code'] = $row['code'];
				$retObj[$row['protein']][$row['IUPACseq']][$row['pubmedid']]['experiment'] = $row['experiment'];
				$retObj[$row['protein']][$row['IUPACseq']][$row['pubmedid']]['authors'] = $row['authors'];
				$retObj[$row['protein']][$row['IUPACseq']][$row['pubmedid']]['journal'] = $row['journal'];
				$retObj[$row['protein']][$row['IUPACseq']][$row['pubmedid']]['date']    = $row['date'];
				$retObj[$row['protein']][$row['IUPACseq']][$row['pubmedid']]['volume']  = $row['volume'];
				$retObj[$row['protein']][$row['IUPACseq']][$row['pubmedid']]['issue']   = $row['issue'];
				$retObj[$row['protein']][$row['IUPACseq']][$row['pubmedid']]['pages']   = $row['pages'];
				$q  = "select EV.envconditiondesc from envcondition as EV, ";
				$q .= "consensusdata as CD where EV.envconditionid=CD.envconditionid ";
				$q .= "and CD.tfconsensusid={$row['tfconsensusid']} and CD.pubmedid=";
				$q .= $row['pubmedid'];
				$retObj[$row['protein']][$row['IUPACseq']][$row['pubmedid']]['envcond'] =
					$this->_dbAccess->getObject("envconditiondesc", $q);
				if (empty($retObj[$row['protein']][$row['IUPACseq']][$row['pubmedid']]['envcond'])) $retObj[$row['protein']][$row['IUPACseq']][$row['pubmedid']]['envcond'] = "N/A";
		    }
		}
		$this->_dbAccess->freeResult("consensus");

		return $retObj;
	}
	
	/*
	 * viewOrfgene
	 */
	function viewOrfgene($orfgene) {
		
		if (strlen($orfgene)==0) throw new Exception("No ORF/Gene was inserted!");

		list($aORFGenes, $aGUnknown) = $this->user2Info($orfgene,$this->aSpecies());

		if (empty($aORFGenes)) throw new Exception("The ORF '$orfgene' does not exist, in species '{$this->_species}'!"); 

		$aORFs = array_keys($aORFGenes);
		$orfid = array_pop($aORFs);
		
		$retObj = array();
		$retObj['orfid'] = $orfid;
		$q  = "select * from orfgene where orfid=$orfid ";
		
		if ($this->_dbAccess->query("orfgene", $q)) {
			while ($row = $this->_dbAccess->nextObject("orfgene")) {
				$retObj['orfname'] = $row['orf'];
				$retObj['genename'] = $row['gene'];
				$retObj['promotersequence'] = $row['promoterseq'];
				$retObj['genesequence'] = $row['geneseq'];
				$retObj['coords'] = $row['coords'];
				$pos = strpos($row['gdid'],":");
				$retObj['ext'] = ($pos === false)?
					$row['gdid'] :
					substr($row['gdid'], $pos+1);

				// species
				$retObj['strain'] = substr($row['species'], strlen($this->_species)+1);

				$qq = "select protein from protein where tfid='".$row['orfid']."'";
				if ($this->_dbAccess->query("prot", $qq)) {
					$r = $this->_dbAccess->nextObject("prot");
					$retObj['proteinname'] = $r['protein'];
				}
				$this->_dbAccess->freeResult("prot");

				$retObj['description'] = ""; // Default value: may not exist
				$qq  = "select D.description from orfgene as O, description as D where ";
				$qq .= "D.descriptionid=O.descriptionid and O.orfid='".$row['orfid']."'";
				if ($this->_dbAccess->query("desc", $qq)) {
					$r = $this->_dbAccess->nextObject("desc");
					$retObj['description'] = $r['description'];
				}
				$this->_dbAccess->freeResult("desc");

				$qq = "select alias from alias where orfid='".$row['orfid']."'";
				if ($this->_dbAccess->query("alias", $qq)) {
					while ($r = $this->_dbAccess->nextObject("alias")) {
						$retObj['altname'][] = $r['alias'];
					}
				}
				$this->_dbAccess->freeResult("alias");
			}
		}
		$this->_dbAccess->freeResult("orfgene");

		return $retObj;
	}

	function viewOrthologs($orfgene) {
		
		list($aInfo,$aGUnknown) = $this->user2Info($orfgene,$this->aSpecies());

                if (empty($aInfo)) {
                        if (strlen($orfgene)>0) {
                                throw new Exception("The ORF '$orfgene' does not exist, in species '{$this->_species}'!");
                        } else {
                                throw new Exception("No ORF/Gene was inserted!");
                        }
		}

		$orfid = array_keys($aInfo)[0];
		$retObj = array('orfname'=> $aInfo[$orfid]['orf'] ,'genename'=> $aInfo[$orfid]['gene'], 'proteinname'=> $aInfo[$orfid]['prot'] );

		$aHomoIDs = [];
		$curr2homo=[];
		
		for ($i=0;$i<4;$i++){
			list($curr2homo[$i], $homo2curr) = $this->ids2orthologs(array_keys($aInfo),$i,true);
			foreach( array_keys($homo2curr) as $id) { $aHomoIDs[] = $id; }
		}

		$aInfo = $aInfo + $this->orfIDs2Info($aHomoIDs);	

		foreach(array_keys($curr2homo) as $syn){
			foreach(array_keys($curr2homo[$syn]) as $orfid){
				foreach($curr2homo[$syn][$orfid] as $k=>$homoorfid){
					$horf  = $aInfo[$homoorfid]['orf'];
					$hgene = $aInfo[$homoorfid]['gene'];
					$hsp   = $aInfo[$homoorfid]['species'];
					$retObj['orthologs'][$hsp][$syn][] = array( 'orf' => $horf, 'gene' => $hgene);
				}
			}
		}

                return $retObj;
	}	
	
	/*
	 * viewProtein
	 */
	function viewProtein($tfInfo) { // GDID ?? 
    
		$retObj = array();
    		$tfid = array_keys($tfInfo)[0];

		$retObj['proteinname'] = $tfInfo[$tfid]['prot'];
		$retObj['orfname'] = $tfInfo[$tfid]['orf'];

		$q  = "select P.*,O.orfid,O.orf,O.gdid from protein as P, orfgene ";
		$q .= "as O where P.tfid=O.orfid and P.tfid=$tfid ";

		if ($this->_dbAccess->query("protein", $q)) {
		    while ($row = $this->_dbAccess->nextObject("protein")) {
		        $retObj['aminoacidsequence'] = $row['aminoacidseq'];
			$pos = strpos($row['gdid'],":");
			$retObj['ext'] = ($pos === false)?
				$row['gdid'] :
				substr($row['gdid'], $pos+1);
			$qq  = "select D.description from description as D, ";
			$qq .= "orfgene as O where O.descriptionid=D.descriptionid ";
			$qq .= "and O.orfid='".$row['orfid']."'";
			$retObj['description'] = 
				$this->_dbAccess->getObject("description", $qq);
		    }
		}
		$this->_dbAccess->freeResult("protein");

		$q  = "select C.IUPACseq from tfconsensus as T, consensus as C, ";
		$q .= "protein as P, orfgene as O where P.tfid=T.tfid and ";
		$q .= "T.consensusid=C.consensusid and P.tfid=O.orfid and ";
		$q .= "P.tfid=$tfid ";
		if ($this->_dbAccess->query("consensus", $q)) {
		    while ($row = $this->_dbAccess->nextObject("consensus")) {
		        $retObj['consensus'][] = $row['IUPACseq'];
		    }
		}
		$this->_dbAccess->freeResult("consensus");
		
		$q = "select distinct R.tfid from regulation as R, protein as P, ";
		$q .= "orfgene as O where P.tfid=R.tfid and P.tfid=$tfid and ";
		$q .= "P.tfid=O.orfid ";
		if ($this->_dbAccess->getObject("tfid", $q)) {
		    $retObj['has_regdoc'] = true;
		}

		$aPromoters = $this->upstreamSeq('',$tfInfo[$tfid]['species']);

		$aMotifs = $this->getMotifsFromDB(false, $tfInfo); 
		$retObj['has_regpot'] = $this->matchMotifOnSequence($aMotifs, $aPromoters ,0, true); // true means boolean version of the function

		return $retObj;
	}

	private function getGOIDterm($table, $goid) {
		$q = "select term from $table where goid='$goid'";
		return $this->_dbAccess->getObject("term", $q);
	}

	function viewGOterm($goid) {
		// info on goid
		$q = "select * from geneontology where goid='".addslashes($goid)."'";
		if ($this->_dbAccess->query("go", $q)) {
			$row = $this->_dbAccess->nextObject("go");
			$retObj['goid']  = $row['goid'];
			$retObj['onto']  = $row['onto'];
			$retObj['term']  = $row['term'];
			$retObj['depth'] = $row['depth'];
		}
		$this->_dbAccess->freeResult("go");
		if (!isset($retObj)) return false;

		// info on parents
		$retObj['parents'] = [];
		$q  = "select G.* from geneontology as G, goparents as P where ";
		$q .= "P.goidson='{$retObj['goid']}' and P.goid=G.goid";
		if ($this->_dbAccess->query("goparents", $q)) {
			while ($row = $this->_dbAccess->nextObject("goparents")) {
				$retObj['parents'][$row['goid']]['depth'] = $row['depth'];
				$retObj['parents'][$row['goid']]['term']  = $row['term'];
				$retObj['parents'][$row['goid']]['onto']  = $row['onto'];
			}
		}
		$this->_dbAccess->freeResult("goparents");

		# Children
		$retObj['children'] = [];
		$q  = "select G.* from geneontology as G, goparents as P where ";
		$q .= "P.goid='{$retObj['goid']}' and P.goidson=G.goid";
		if ($this->_dbAccess->query("gochildren", $q)) {
			while ($row = $this->_dbAccess->nextObject("gochildren")) {
				$retObj['children'][$row['goid']]['depth'] = $row['depth'];
				$retObj['children'][$row['goid']]['term']  = $row['term'];
				$retObj['children'][$row['goid']]['onto']  = $row['onto'];
			}
		}
		$this->_dbAccess->freeResult("gochildren");

		# Associated genes
		$retObj['genes'] = [];
		$q  = "select O.orf,O.gene,O.species from orfgene as O, goorflist ";
		$q .= "as GL where GL.goid='{$retObj['goid']}' and GL.orfid=O.orfid ";
		$q .= "and O.{$this->dbSpecies()}";
		if ($this->_dbAccess->query("gogenes", $q)) {
			while ($row = $this->_dbAccess->nextObject("gogenes")) {
				$retObj['genes'][$row['orf']] = $row['gene'];
			}
		}
		$this->_dbAccess->freeResult("gogenes");
		
		return $retObj;
	}

	private function getORFGOterms($orfid) {
		// Default values: terms from some ontologies may not exist
		$retObj = array('process' => [], 'function' => [], 'component' => []);
		$q  = "select GO.goid,GO.term,GO.depth,GO.onto from goorflist as OL, ";
		$q .= "geneontology as GO where OL.orfid='{$orfid}";
		$q .= "' and OL.goid=GO.goid order by GO.depth";
		if ($this->_dbAccess->query("goorflist", $q)) {
		    while ($row = $this->_dbAccess->nextObject("goorflist")) {
					$retObj[$row['onto']][$row['goid']]['depth'] = $row['depth'];
					$retObj[$row['onto']][$row['goid']]['term']  = $row['term'];
		    }
		}
		$this->_dbAccess->freeResult("goorflist");
		return array($retObj['process'], $retObj['function'], $retObj['component']);
	}

	/*
	 * viewGO
	 */
	function viewGO($orfname) {
      
		list($aInfo, $aPUnknown) = $this->user2Info($orfname,$this->aSpecies(),true);
      
		$retObj = array();
		$orfid = array_keys($aInfo)[0];
		$aOrf = array_pop($aInfo);
		$retObj['orf']         = $aOrf['orf'];
		$retObj['gene']        = $aOrf['gene'];
		$retObj['protein']     = $aOrf['prot'];
		$retObj['description'] = $aOrf['desc'];
		$retObj['ext']         = $aOrf['ext'];

		list($retObj['process'], $retObj['function'], $retObj['component']) =
			$this->getORFGOterms($orfid);

		return $retObj;
	}
	
	/*
	 * checkFastA
	 */
	 function checkFastA($input) {
		$lines = explode("\n", $input);

		if (!$lines && count($lines) == 1) return false;
		if (!preg_match("/^>\s*(\S+)/", $lines[0], $matches)) return false;

		$s = "";
		for ($i=1; $i<count($lines); $i++) {
			$s .= trim($lines[$i]);
		}
		if (strlen($s)==0) return;
		return array($matches[1], $s);
	}

	/*
	 * splitFastASequence
	 */
	function splitFastASequence($input) {
		$retObj = array();
		$seqs = explode(">", $input);
		if (empty($seqs[0])) {
			array_shift($seqs);
		} else {
			$seqs[0] = "userSeq\n".$seqs[0];
		}
		foreach ($seqs as $seq) {
			$tmp = $this->checkFastA(">$seq");
			if ($tmp) {
				list($name, $s) = $tmp;
				$retObj[$name] = $s;
			}
		}
		return $retObj;
	}
	
	/*
	 * matchMotifOnSequence
	 */
	function matchMotifOnSequence($aMotifs, $aSequences, $nSubst = 0, $bool=false) { // boolean version stops and returns true if a single match is found
	    include_once('service/ShiftAnd.php');
	    include_once('service/IUPAC.php');
	    $fwr = array();
	    $rev = array();

	    foreach(array_keys($aMotifs) as $tfid){
		    foreach (array_keys($aMotifs[$tfid]) as $protein) {
			    foreach($aMotifs[$tfid][$protein] as $key=>$m){
			    	$aMotif_exp = $this->expandRegExp($m);
				    foreach ($aMotif_exp as $m_exp) {
					    $m_exp = str_ireplace("X","N",$m_exp);
					    $shiftAndFwr = new ShiftAnd($m_exp);
					    $shiftAndRev = new ShiftAnd(strrev(IUPAC::complement($m_exp)));
		    	
					    foreach ($aSequences as $orf => $s) {
			    
					    	// Forward Strand
					    	$pos = $shiftAndFwr->find($s, $nSubst);
						if (count($pos)>0) {
							if ($bool) return true;
							$fwr[$orf][$tfid] = $pos;
						}
						// Reverse Strand
						$pos = $shiftAndRev->find($s, $nSubst);
						if (count($pos)>0) {
							if ($bool) return true;
		            				$rev[$orf][$tfid] = $pos;
				        	}
			             	     }
			    	     }
			    }
		    }
	    }
	    if ($bool) return false;
	    return array($fwr, $rev);
	}

	function motifOnSequencesHasMatch($aMotifs, $aSequences, $nSubst = 0) { 
		include_once('service/ShiftAnd.php');
		include_once('service/IUPAC.php');
		$matches = [];

		foreach ($aSequences as $orf => $s) {
			foreach(array_keys($aMotifs) as $tfid){
				if (isset($matches[$orf][$tfid]))       continue;
				foreach (array_keys($aMotifs[$tfid]) as $protein) {
					foreach($aMotifs[$tfid][$protein] as $key=>$m){
						$aMotif_exp = $this->expandRegExp($m);
						foreach ($aMotif_exp as $m_exp) {
							$m_exp = str_ireplace("X","N",$m_exp);
				
							$shiftAndFwr = new ShiftAnd($m_exp);
				                        $shiftAndRev = new ShiftAnd(strrev(IUPAC::complement($m_exp)));

							// Forward Strand
							$pos = $shiftAndFwr->find($s, $nSubst);
							if (count($pos)>0) {
								$matches[$orf][$tfid] = true;
								continue 4;
							}
							// Reverse Strand
							$pos = $shiftAndRev->find($s, $nSubst);
							if (count($pos)>0) {
								$matches[$orf][$tfid] = true;
								continue 4;
							}
						}
					}
				}
			}
		}
		return $matches;
	}

	/*
	 * saveTmpFile
	 */
	function saveTmpFile($content, $name, $ext) {
		date_default_timezone_set('UTC');
		$now = getdate();
		$date = $now['year'].$now['mon'].$now['mday']."_";
		$date .= $now['hours'].$now['minutes'];

		$tmpname = $name . "_$date" . "_" . rand() . "." . $ext;
    
		$abspath = $this->_properties->get("path.tmp.local");    
		$urlpath = $this->_properties->get("path.tmp.url");

		if (!$fh = @fopen($abspath.$tmpname, 'w')) {
			throw new Exception("Could not open file '$urlpath"."$tmpname'");
		}
		if (empty($content)) $content=" ";
		if (!@fwrite($fh, $content)) {
			throw new Exception("Could not write file '$urlpath"."$tmpname'");
		}
    
		fclose($fh);
	        
		return array($urlpath, $tmpname);
	}
	
	
	function getEnvironCondsGroupsFromDB() { // to use in frontend forms
		$retObj = array();
		$biggroup = array();

		$q  = "select distinct biggroup,subgroup from envconditiongroups";
		if ($this->_dbAccess->query("envcond", $q)) {
			while ($row = $this->_dbAccess->nextObject("envcond")) {
				$biggroup[$row['biggroup']] = $row['biggroup'];
				if (!array_key_exists($row['biggroup'], $retObj)) {
					$retObj[$row['biggroup']] = array();
				}
				array_push($retObj[$row['biggroup']], $row['subgroup']);
			}
		}
		$this->_dbAccess->freeResult("envcond");
		return array($biggroup, $retObj);
	}
	
	function getMotifsFromDB($species = false, $aTFids = false) { // generic call to db to get motifs
		$retObj = [];

		$org = ($species)?
			"species='".addslashes($species)."'":
			$this->dbSpecies();
		
		$q  = "select distinct P.tfid, P.protein, C.IUPACseq from protein as P, ";
		$q .= "consensus as C, tfconsensus as TFC, orfgene as O where ";
		$q .= "C.consensusid=TFC.consensusid and TFC.tfid=P.tfid and ";
		$q .= "P.tfid=O.orfid and O.$org";
		if ($aTFids) {
			$tflist = join(",", array_keys($aTFids));
			$q .= " and P.tfid in ($tflist)";
		}
		if ($this->_dbAccess->query("consensus", $q)) {
			while ($row = $this->_dbAccess->nextObject("consensus")) {
				$tfid = $row['tfid'];
				$p = $row['protein'];
				$c = $row['IUPACseq'];
				$retObj[$tfid][$p][] = $c;
			}
		}
		$this->_dbAccess->freeResult("consensus");
		return $retObj;
	}

	function getDocumentedFromDB() { // get all the protein involved in regulations for the current species

		$retObj = array();
		
		$q = "select distinct P.protein from regulation as R, ";
		$q .= "orfgene as O, protein as P where O.{$this->dbSpecies()} and ";
		$q .= "	P.tfid=R.tfid and O.orfid=R.targetid ";
		
                if ($this->_dbAccess->query("protein", $q)) {
                        while ($row = $this->_dbAccess->nextObject("protein")) {
                                $p = $row['protein'];
                                $retObj[] = $p;
                        }
                }
                $this->_dbAccess->freeResult("tf");
                return $retObj;
        }

	/*
	 * searchMotifOnDB
	 */
	function searchMotifOnDB($mUser, $subst, $species=false) { // searches for inserted motifs in the DB in global and local matches
	        $retObj  = array();
	        $retObj2 = array();
		$motifsDB = array();
		
		list($mcUser, $good) = $this->motifIUPACCompress($mUser);
		if (!$good) {
		    return array($retObj, $retObj2, $motifsDB, array($mUser));
		}
		
		$ORIGINAL   = 0;
		$COMPRESSED = 1;
		$LENGTH     = 2;
		$INVERTED   = 3;
		$TF         = 4;

		$dbCons = $this->getMotifsFromDB($species);

		foreach (array_keys($dbCons) as $tfid) {
			foreach (array_keys($dbCons[$tfid]) as $tf){
			       foreach($dbCons[$tfid][$tf] as $i=> $mDB) {
					if ($this->motifIsComplex($mDB)) continue;
					if (isset($motifsDB[$mDB])) {
						$motifsDB[$mDB][$TF][$tf] = $tf;
					} else {
						$mDB = str_ireplace('X','N',$mDB); // to prevent errors in shiftAnd caused by non iupac X character
	    					$motif = array();
    						$motif[$ORIGINAL]   = $mDB;
	    					$motif[$COMPRESSED] = $mDB; // all motifs in DB are compressed
    						$motif[$LENGTH]     = strlen($mDB);
    						$motif[$INVERTED]   = $this->motifComplement($mDB);
	    					$motif[$TF][$tf]    = $tf;
    						$motifsDB[$mDB]     = $motif;
					}
				}
		
			}
		}
		
		include_once('service/ShiftAnd.php');
		$safwr = new ShiftAnd($mcUser);
		$sarev = new ShiftAnd($this->motifComplement($mcUser));
		
		foreach ($motifsDB as $m => $seq) {
		    	$matches = $safwr->find($seq[$COMPRESSED], $subst);
			if (count($matches)>0) {
				$retObj[$m]['fwr'] = $matches;
			}
			$matches = $sarev->find($seq[$COMPRESSED], $subst);				
			if (count($matches)>0) {
				$retObj[$m]['rev'] = $matches;
			}
			$motifxfwr = new ShiftAnd($seq[$COMPRESSED]);
			$matches = $motifxfwr->find($mcUser, $subst);
			if (count($matches)>0) {
				$retObj2[$m]['fwr'] = $matches;
			}
			unset($motifxfwr);
			$motifxrev = new ShiftAnd($this->motifComplement($seq[$COMPRESSED]));
			$matches = $motifxrev->find($mcUser, $subst);
			if (count($matches)>0) {
				$retObj2[$m]['rev'] = $matches;
			}
			unset($motifxrev);
		}
		unset($safwr);
		unset($sarev);
		
   		return array($retObj, $retObj2, $motifsDB);
	}
	
        /*
         * Identify user inserted Motifs on DB
         */
        function identifyMotifs($aMotifs,$docs=[],$species=false) {

		$motifs = [];

		$tmp = $this->getMotifsFromDB($species);

		foreach (array_keys($tmp) as $tfid){
			foreach(array_keys($tmp[$tfid]) as $tf){
				foreach ($tmp[$tfid][$tf] as $ref => $mo){
	                        	$aM = $this->expandRegExp($mo);
		                        foreach ($aM as $m){
        		                        $motifsFromDB[$tf][$m] = $mo;
                		        }
				}
                	}
        	}

		$aMotifs = explode("\n", trim($aMotifs));
		$i = 1;
		// EXPAND MOTIFS BEFORE TRYING TO REGOGNIZE THEM
		foreach ($aMotifs as $dadmo) {
			$dadmo = trim($dadmo);
			$aM = ($this->motifIsComplex($dadmo))? $this->expandRegExp($dadmo):[$dadmo];
			foreach ($aM as $m){
				$m = trim($m);
				if (empty($m)) continue;
				list($mot, $ok) = $this->motifIUPACCompress($m);
				if (!$ok) continue;
				// FOR EACH USER MOTIF, VERIFY IF THE MOTIF EXISTS IN THE DB, IF SO, NAME IT
				$exists[$i]=false;
				foreach (array_keys($motifsFromDB) as $tf) {
					if (!empty($docs) && in_array($tf, $docs)) continue; // FILTER DOCUMENTED REGULATIONS
					if (array_key_exists($m,$motifsFromDB[$tf])) {
						$motifs[$dadmo][$m][] = $tf;
						$exists[$i]=true;
						continue 1; // a Motif can be shared by more than one TF!!!
					}
				}
				if (!$exists[$i]) $motifs[$dadmo][$m][] = "user$i";
				$i++;
			}
		}
		return $motifs;
	}

	/* tfBindingSites  */
	 function tfBindingSites($sequences, $motifs, $subst=0) { // motif vs sequence match service that retains information regarding the compressed motif sequence whose expanded matches

		$retObj = [];
		$motif2tf = [];

		include_once('ShiftAnd.php');

		foreach (array_keys($motifs) as $dadMotif) {
			$motif2tf[$dadMotif] = [];
			foreach(array_keys($motifs[$dadMotif]) as $sonMotif){
				$clean_m = str_ireplace("X","N",$sonMotif);
				if (strstr($clean_m,"/")) list($clean_m, $good) = $this->motifIUPACCompress($clean_m);
				$shiftAnd = new ShiftAnd($clean_m);
				foreach ($sequences as $name => $seq) {
					$matches = @$shiftAnd->find($seq,$subst); // FIX THIS ERRORS
					if (count($matches)>0) { $retObj[$name][$dadMotif]['fwr'] = $matches;	}
					$matches = @$shiftAnd->find($this->motifComplement($seq),$subst);
					if (count($matches)>0) { $retObj[$name][$dadMotif]['rev'] = $matches;	}
				}
				foreach ($motifs[$dadMotif][$sonMotif] as $tfname){ if (!in_array($tfname,$motif2tf[$dadMotif]))  	$motif2tf[$dadMotif][] = $tfname; }
			}
		}

		return array($retObj,$motif2tf);
	 }	
	
	/*
	 * searchTerm
	 */
	function quickSearchTerm($type, $term) {
	  $tmp = "";
	  $retObj = array();
		if ($type == "description") {
			$q = "select distinct O.orfid,O.orf,O.gene,D.description from orfgene as O, description as D";
			$q .= " where D.descriptionID=O.descriptionID and ";
			$q .= "O.{$this->dbSpecies()} and D.description like '%$term%'";

			if ($this->_dbAccess->query("orfname", $q)) {
			    while ($row = $this->_dbAccess->nextObject("orfname")) {
			        $elem = array();
			        $elem['orfname']     = $row['orf'];
			        $elem['genename']    = $row['gene'];
			        $elem['description'] = $row['description'];
							$qalias = "select alias from alias where orfid=" . $row['orfid'];
							if ($this->_dbAccess->query("alias", $qalias)) {
								$aAlias = array();
								while ($ralias = $this->_dbAccess->nextObject("alias")) {
									$aAlias[] = $ralias['alias'];
								}
								$elem['alias'] = $aAlias;
							}
							$this->_dbAccess->freeResult("alias");
							array_push($retObj, $elem);
			    }
			}
			$this->_dbAccess->freeResult("orfname");

		} elseif ($type == "protein") {
			$q  = "select P.protein,D.description from protein as P, orfgene as O, description as D where ";
			$q .= "P.tfid=O.orfid and O.descriptionid=D.descriptionid and P.protein like '%$term%' ";
			$q .= "and O.{$this->dbSpecies()}";
			if ($this->_dbAccess->query("proteinname", $q)) {
				while ($row = $this->_dbAccess->nextObject("proteinname")) {
					$elem = [];
					$elem['protein'] = $row['protein'];
					$elem['description'] = $row['description'];
					array_push($retObj, $elem);
				}
			}
			$this->_dbAccess->freeResult("proteinname");

		} elseif ($type == "orfname" || $type == "genename" || $type == "alias") {
			$q = "select distinct O.orfid,O.orf,O.gene,O.descriptionid from orfgene as O";
			if ($type == "alias") $q .= ", alias as A";
			$q .= " where O.{$this->dbSpecies()} and ";

			if ($type == "orfname") $q .= "O.orf ";
			elseif ($type == "genename") $q .= "O.gene";
			elseif ($type == "alias") $q .= "O.orfid=A.orfid and A.alias";
			$q .= " like '%$term%'";
			if ($this->_dbAccess->query("orfname", $q)) {
			    while ($row = $this->_dbAccess->nextObject("orfname")) {
			        $elem = array();
			        $elem['orfname']     = $row['orf'];
			        $elem['genename']    = $row['gene'];
							// Description
							if ($row['descriptionid']) {
								$qDesc  = "select description from description where descriptionid=";
								$qDesc .= $row['descriptionid'];
								$elem['description'] = $this->_dbAccess->getObject("description", $qDesc);
							} else {
								$elem['description'] = ""; // may be empty
							}
							// Alias
							$qalias = "select alias from alias where orfid=" . $row['orfid'];
							if ($this->_dbAccess->query("alias", $qalias)) {
								$aAlias = array();
								while ($ralias = $this->_dbAccess->nextObject("alias")) {
									$aAlias[] = $ralias['alias'];
								}
								$elem['alias'] = $aAlias;
							}
							$this->_dbAccess->freeResult("alias");
							array_push($retObj, $elem);
			    }
			}
			$this->_dbAccess->freeResult("orfname");

		} elseif ($type == "pubmedid") {
			$q = "select * from pubmed where pubmedid like '%$term%'";
			if ($this->_dbAccess->query("pubmedid", $q)) {
				while ($r = $this->_dbAccess->nextObject("pubmedid")) {
					$tmp = array(
						'authors' => $r['authors'],
						'journal' => $r['journal'],
						'date'    => $r['date'],
						'volume'  => $r['volume'],
						'issue'   => $r['issue'],
						'pages'   => $r['pages']);
					$retObj[$r['pubmedid']] = $tmp;
				}
			}
			$this->_dbAccess->freeResult("pubmedid");

		} elseif ($type == "authors") {
			$q  = "select * from pubmed where authors like '%$term%'";
			if ($this->_dbAccess->query("authors", $q)) {
				while ($r = $this->_dbAccess->nextObject("authors")) {
					$tmp = array(
						'authors' => $r['authors'],
						'journal' => $r['journal'],
						'date'    => $r['date'],
						'volume'  => $r['volume'],
						'issue'   => $r['issue'],
						'pages'   => $r['pages']);
					$retObj[$r['pubmedid']] = $tmp;
				}
			}	    			
			$this->_dbAccess->freeResult("authors");

		} elseif ($type == "process" || $type == "function" || $type == "component") {
			$q  = "select * from geneontology where onto='$type' and term like '%$term%'";
			if ($this->_dbAccess->query("goterm", $q)) {
				while ($row = $this->_dbAccess->nextObject("goterm")) {
					$retObj[$row['goid']] = $row['term'];
				}
			}
			$this->_dbAccess->freeResult("goterm");
		}

		return $retObj;
	}
	
	/*
	 * quickSearch
	 */
	function quickSearch($term) {
		$retObj = array();
		$orfgene = $this->gene($term);

		/* When it's exactly a orfname */
		$q = "select orf from orfgene where orf='$orfgene' and {$this->dbSpecies()}";
		$orfname = $this->_dbAccess->getObject("orf", $q);
		if (!$orfname) {
			/* genename */
			$q = "select orf from orfgene where gene='$orfgene' and {$this->dbSpecies()}";
			$orfname = $this->_dbAccess->getObject("orf", $q);
			if (!$orfname) {
				/* alternativename */
				$q = "select O.orf from orfgene as O, alias as A where A.alias='$orfgene' and A.orfid=O.orfid and O.{$this->dbSpecies()}";
				$orfname = $this->_dbAccess->getObject("orf", $q);
				if (!$orfname) {
					/* proteinname */
					$q = "select O.orf from orfgene as O, protein as P where P.protein='$term' and O.{$this->dbSpecies()}";
					$orfname = $this->_dbAccess->getObject("orf", $q);
				}
			}
		}
		if ($orfname) {		    
			$retObj['orfname'] = $orfname;
			return $retObj;
		}

		/* orfname */
		$q = "select count(orf) as num from orfgene where orf like '%$term%' and {$this->dbSpecies()}";
		$numorfs = $this->_dbAccess->getObject("num", $q);
		$retObj['ORFs']['orfname'] = $numorfs;

		/* genename */
		$q = "select count(gene) as num from orfgene where gene like '%$term%' and {$this->dbSpecies()}";
		$numgenes = $this->_dbAccess->getObject("num", $q);
		$retObj['genes']['genename'] = $numgenes;

		/* alternativename */
		$q = "select count(orf) as num from orfgene as O, alias as A where A.alias like '%$term%' and A.orfid=O.orfid and {$this->dbSpecies()}";
		$numalts = $this->_dbAccess->getObject("num", $q);
		$retObj['alternative gene names']['alias'] = $numalts;

		/* protein */
		$q = "select count(P.protein) as num from protein as P, orfgene as O where P.protein like '%$term%' and P.tfid=O.orfid and {$this->dbSpecies()}";
		$numprots = $this->_dbAccess->getObject("num", $q);
		$retObj['protein names']['protein'] = $numprots;

		/* description */
		$q = "select distinct(count(D.description)) as num from description as D, orfgene as O where D.description like '%$term%' and D.descriptionid=O.descriptionid and O.{$this->dbSpecies()}";
		$numdesc = $this->_dbAccess->getObject("num", $q);
		$retObj['gene descriptions']['description'] = $numdesc;

		/* authors */
// TODO
		/* function */
		$q = "select count(distinct term) as num from geneontology where onto='function' and term like '%$term%'";
		$num = $this->_dbAccess->getObject("num", $q);
		$retObj['terms from Molecular Function Gene Ontology']['function'] = $num;
		/* process */
		$q = "select count(distinct term) as num from geneontology where onto='process' and term like '%$term%'";
		$num = $this->_dbAccess->getObject("num", $q);
		$retObj['terms from Biological Process Gene Ontology']['process'] = $num;
		/* component */
		$q = "select count(distinct term) as num from geneontology where onto='component' and term like '%$term%'";
		$num = $this->_dbAccess->getObject("num", $q);
		$retObj['terms from Cellular Component Gene Ontology']['component'] = $num;

		return $retObj;
	}

}

?>
