<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter();
$web->printHeader("Home", "index.php",
	"Help", "help_index.php",
	"Rank by GO", "");
$web->printBarMenu("Help - Rank by GO");
?>

<table border="0" class="center" style="width:75%" summary="main content">
<tr><td>

<p>
This utility ranks a given set of genes according to the GO terms assigned to them by the Candida Genome Database.
</p>

<p>
The input required is a list of genes and/or ORF names.
</p>

<p>
Ranking can be done by the three gene ontologies: Biological Process, Molecular Function or Cellular Component.
This query returns a table organized by GO terms, ordered by the percentage of the genes (from the input list) associated with each GO term, or by the enrichment of the users dataset in that specific GO term, when compared to the genome (evaluated by p-value).
For each GO term the table contains the names of the genes associated with it.
</p>

</td></tr>
</table>

<?php
$web->printFooter();
$web->closePage();
?>
