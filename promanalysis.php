<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter("no");
$web->printHeader("Home", "index.php",
"Cross Strain Comparison", "formpromanalysis.php",
"Result", "");
$web->printBarMenu("Cross Strain Comparison");

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

$strains = $web->getProp("db.$web->_dbname.dbstrains");
$aStrains = preg_split('/\s+/',$strains);
$numStrains = count($aStrains);
if ($numStrains>1){
	$binomial_name = $web->getProp("db.$web->_dbname.dbspecies");
}

$tfSpecies   = (isset($_POST) && isset($_POST['tf_species'])) ? $_POST['tf_species']: false;
$homolog = (isset($_POST) && isset($_POST['homospecies'])) ? $_POST['homospecies']: false;
$synteny = (isset($_POST) && isset($_POST['synteny'])) ? $_POST['synteny']: 0;
$userGenes   = (isset($_POST) && isset($_POST['genes']))? $_POST['genes']: false;
$currSpecies = (isset($_POST) && isset($_POST['currspecies'])) ? $_POST['currspecies']: false;
$currSp = $currSpecies;

foreach($homolog as $homo){ 
	if (isset($homoGET)) {
		$homoGET .= "|".$homo; 
	} else {
		$homoGET = $homo;
	}	
}

$allSpecies = array_merge($homolog, $currSpecies);


// OBTER UM ARRAY $[MOTIFS][TF] PARA O ORGANISMO tf_species
$aMotifs = $service->getTFMotifs($tfSpecies);

$aTFInfo = $service->orfIDs2Info(array_keys($aMotifs));

list($aInfo, $aUnknown) = $service->user2info($userGenes,$currSpecies); 

include_once('inc/head.inc.php');
Head::printUnknown($aUnknown);

if (!isset($aMotifs) || empty($aMotifs)) {
?>
	<p>
		<b><i><?=$tfSpecies?></i></b> does not have any associated TF binding sites.
	</p>
<?php
} elseif (!isset($aInfo) || empty($aInfo)) {
?>
	<p>
		No valid ORF/genes inserted for
		<b><i><?=$web->getProp("db.$web->_dbname.dbspecies")?></i></b>
	</p>
<?php
} else {

	// CREATE AN ARRAY CONTAINING ALL THE UPSTREAM SEQS 
	$homoORFs   = [];
	$aPromoters = [];
	$aHomoORFids = [];
	$homo2orf = [];
	$curr2homo = [];
	foreach ($allSpecies as $toSpecies){ 
		$homoORFs[$toSpecies] = $service->getHomoORFInfo(array_keys($aInfo), $toSpecies, $synteny);
		foreach (array_keys($homoORFs[$toSpecies]) as $orf){ 
			foreach ($homoORFs[$toSpecies][$orf]['homo']['orfid'] as $k => $homoorfid) {
				$aHomoORFids[] = $homoorfid;
				$curr2homo[$orf][] = $homoorfid;
				$homo2orf[$homoorfid][] = $orf;
			}
		}
	}

	$homoInfo = $service->orfIDs2Info($aHomoORFids);

	$aORFids = array_merge($aHomoORFids,array_keys($aInfo));

	$aPromoters = $service->upstreamSeq($aORFids);

	$matches = $service->matchTFOnSequences($aMotifs,$aPromoters,0,true);

	$homoSpecies = array_merge($homolog, $currSpecies);

	// CREATE ARRAY OF TFS ORGANIZED BY ORF, SPECIES AND HOMOORF AND MERGING FWD AND REV
        $tfs = [];
	// FORWARD MATCHES
	
	foreach (array_keys($matches) as $tfid) {
		$tf = $aTFInfo[$tfid]['prot'];
		foreach (array_keys($matches[$tfid]) as $homoorfid){
                	$aORFs = [];
	                if (isset($aInfo[$homoorfid]['orf'])) $aORFs[] = $aInfo[$homoorfid]['orf']; // this is essencial for the current strain matches to be computed
        	        if (isset($homo2orf[$homoorfid])) foreach ($homo2orf[$homoorfid] as $homoorf) { // this is essencial when there is an homoorf which is homolog for several input orfs
                        	$aORFs[] = $homoorf;
			}
			$sp  = (isset($homoInfo[$homoorfid]))?$homoInfo[$homoorfid]['species']:$aInfo[$homoorfid]['species'];
                        foreach ($aORFs as $orf){
                        	if (!isset($tfs[$orf][$sp][$homoorfid])||!in_array($tf,$tfs[$orf][$sp][$homoorfid])) $tfs[$orf][$sp][$homoorfid][] = $tf;
                        }
                }
        }

	// CALCULATE INTERSECTIONS AND UNIQUE
	$uniq = [];
	$inter = [];
	if ($numStrains>1) $bin_inter = []; // binominal intersection (multistrain databases)
	foreach(array_keys($tfs) as $orf) {
		foreach (array_keys($tfs[$orf]) as $org) {
			foreach (array_keys($tfs[$orf][$org]) as $homoorf) {
				// if exists homolog but there are no tfs, intersection is null
				if (!isset($tfs[$orf][$org][$homoorf])) { 
					if (($numStrains>1) && (strpos($org, $binomial_name) !== false)) $bin_inter[$orf] = []; 
					$inter[$orf] = [];
					continue;
				}
				if (!isset($inter[$orf]) ) {
					$inter[$orf] = $tfs[$orf][$org][$homoorf]; 
				}
				else {
					$inter[$orf] = array_intersect($inter[$orf],$tfs[$orf][$org][$homoorf]);
				}
				if ($numStrains>1 && strpos($org, $binomial_name) !== false){
					if (!isset($bin_inter[$orf])){ $bin_inter[$orf]=$tfs[$orf][$org][$homoorf]; }
					else { $bin_inter[$orf] = array_intersect($bin_inter[$orf],$tfs[$orf][$org][$homoorf]);}
				}
				$uniq[$orf][$org][$homoorf] = $tfs[$orf][$org][$homoorf];
				$aOther = array_diff($homoSpecies, [$org]);
				foreach ($aOther as $key => $other) {
					if (isset($tfs[$orf][$other])){
						foreach (array_keys($tfs[$orf][$other]) as $horf) {
							$uniq[$orf][$org][$homoorf] = array_diff($uniq[$orf][$org][$homoorf],$tfs[$orf][$other][$horf]);
						}
					}
				}
			}
		}
	}

	// JOIN THE INTERSECT OF EACH ORF
	$intersect = array();
	if ($numStrains>1) $bin_intersect = array();
	foreach (array_keys($inter) as $orf){
		if ($numStrains>1) $bin_intersect[$orf] = join(" ", $bin_inter[$orf]);
		$intersect[$orf] = join(" ", $inter[$orf]);
	}	

	// JOIN THE UNIQUE TFS OF EACH ORF-ORG
	$uniq_tf = array();
	foreach (array_keys($uniq) as $orf){
		foreach (array_keys($uniq[$orf]) as $org){
			foreach (array_keys($uniq[$orf][$org]) as $homoorf) {
				$uniq_tf[$orf][$org][$homoorf] = join(" ", $uniq[$orf][$org][$homoorf]);
			}
		}
	}

	$cols = count($homoSpecies);
	$spcols = ($numStrains>1)?($cols+2):($cols+1);
	$currSpecies = join(" and ", $currSpecies);
	?>

<table class="center" border="0" width="90%">
<tr>
<td>
<p>
	Searching for potential regulations using TFs from <i><?=$tfSpecies?></i>
	on orthologs of <i><?=$currSpecies?></i>.
</p>
<?php
$web->printSyntenyWarn($synteny);
?>
<p>
	<font style="background-color:#79b23e;">Green</font> in the species columns indicates that there is at least one TF that uniquely regulates the 
	corresponding gene or one of its orthologs.<br/>
In the last column it means that there is at least one TF that regulates all the genes and orthologs.
</p>
<p>
<font style="background-color:yellow;">Yellow</font> indicates that there is no ortholog for that gene in that species.
</p>

<table class="center" border="1" summary="main content">
	<tr>
		<th align="center" align="center" rowspan="3"><strong>Gene/ORF</strong></th>
		<th align="center" colspan="<?=$spcols?>"><strong>TFs binding to the promoter</strong></th>
		<th align="center" rowspan="3"><strong>Find <br/>Binding Sites</strong></th> 
	</tr>
	<tr>
		<th align="center" colspan="<?=$cols?>"><strong>Uniquely in species</strong></th>
<?php
if ($numStrains>1){ print "<th align=\"center\" rowspan=\"2\"><strong>On all<br/>$binomial_name</strong></th>"; }
?>
		<th align="center" rowspan="2"><strong>On all<br/>orthologs</strong></th>
	</tr>
	<tr>
<?php
foreach ($homoSpecies as $org){
?>
		<th align="center"><em><?=$org?></em></th>
<?php } ?>
	</tr>
<?php

	$ncols = [];
	$maxcols = [];

	foreach (array_keys($aInfo) as $orfid ){
		$orf = $aInfo[$orfid]['orf'];
		$gene = $aInfo[$orfid]['gene'];
		$maxcols[$orf] = 1;
		foreach (array_keys($homoORFs) as $sp) {
			$numcols = 0;
			if (isset($homoORFs[$sp][$orf])){
				foreach (array_keys($homoORFs[$sp][$orf]['homo']['orfid']) as $cls) {
					$numcols++;	
				}
			}
			$ncols[$orf][$sp] = $numcols;
			if ($ncols[$orf][$sp]>$maxcols[$orf]){$maxcols[$orf]=$ncols[$orf][$sp];}
		}
		$link = "view.php?existing=locus&amp;orfname=$orf";
		$find = "findregulators.php?tf_species=$tfSpecies&homo=$homoGET&orfname=$orf&synteny=$synteny";
		$orfg = $web->Gene($gene,$orf,true);
?>
	<tr>
		<td><a href="<?=$link?>"><?=$orfg?></a></td>
<?php

		foreach ($allSpecies as $homo){
			if ($aInfo[$orfid]['species'] == $homo){
                                print "<td bgcolor=\"#79b23e\">";
                                if (isset($uniq_tf[$orf][$homo])){
                                        $curr_uniq = current($uniq_tf[$orf][$homo]);
                                        if (!empty($curr_uniq)) {
                                                print "<strong>Unique TFs:</strong> $curr_uniq";
                                        }
                                        else {
                                                print "<font style=\"color:red;\">";
                                                print "No unique TFs</font>";
                                        }
                                }
                                else {
                                        print "<font style=\"color:red;\">";
                                        print "No unique TFs</font>";
                                }
                                print "</td>";
                        }
                        else {
                                if ( !isset($homoORFs[$homo][$orf])){
                                        print "<td bgcolor=\"#f1e887\"> No ortholog</td>";
                                }
                                else {
                                        print "<td bgcolor=\"#79b23e\">";
                                        $first = true;
                                        foreach(array_keys($uniq_tf[$orf][$homo]) as $homoorf){
                                                $hg = $homoInfo[$homoorf]['gene'];
                                                $ho = $homoInfo[$homoorf]['orf'];
                                                if (!$first) print "<hr/>";
                                                print "<strong>Homolog: </strong>";
                                                $web->printGene($hg,$ho,true,$homo);
                                                print "<hr/>";
                                                if (isset($uniq_tf[$orf][$homo][$homoorf]) && !empty($uniq_tf[$orf][$homo][$homoorf])){
                                                        print "<strong>Unique TFs:</strong> {$uniq_tf[$orf][$homo][$homoorf]}";
                                                }
                                                else {
                                                        print "<font style=\"color:red;\">";
                                                        print "No unique TFs</font><br>";
                                                }
                                                $first = false;
                                        }
                                        print "</td>";
                                }
                        }
                }

		
		// ALL Strains Column
	if ($numStrains>1){
		if ( isset($bin_intersect[$orf]) && !empty($bin_intersect[$orf])){
                	print "<td bgcolor=\"#79b23e\">{$bin_intersect[$orf]}</td>";
		}
		else {
                	print "<td bgcolor=\"#ae5a41\"><font style=\"color:white;\">No TFs in all strains</font></td>";
		}
	}
	// ALL SPECIES COLUMN
	if ( isset($intersect[$orf]) && !empty($intersect[$orf])){
		print "<td bgcolor=\"#79b23e\">{$intersect[$orf]}</td>";
	}
	else {
		print "<td bgcolor=\"#ae5a41\"><font style=\"color:white;\">No TFs in all species</font></td>";
	}
?>
<td align="center"><a href="<?=$find?>">Promoter</a></td></tr>
<?php
}
?>
</table>
<input type="reset" value="Go Back" onclick="javascript:history.back()"/>
</td></tr>
</table>
<?php } 
?>
<br/>
<?php
$web->printFooter();
$web->closePage();
?>
