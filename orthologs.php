<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter("no");
$web->printHeader("Home", "index.php",
"ORF/Gene Orthologs List", "javascript:history.back()",
"Result", "");
$web->printBarMenu("ORF/Gene Orthologs List");

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

$currentSpecies = $service->aSpecies();
$synteny = isset($_POST['synteny'])?$_POST['synteny']:"0";

$species = false;

if (isset($_POST['set'])){
	$aOrths = $service->getCombinationOrths($_POST['set'],$synteny);

	$userORFs = "";
	foreach($currentSpecies as $key=>$sp){
		if (!isset($aOrths[$sp])) { unset($currentSpecies[$key]);}
		else { $userORFs .= " ".join(" ",array_keys($aOrths[$sp])); }
	}
}

list($aORFGenes, $unknown) = isset($_POST['orfs'])?$service->user2info($_POST['orfs'],$currentSpecies):$service->user2info($userORFs,$currentSpecies);

list($curr2homo,$homo2curr) = $service->ids2orthologs(array_keys($aORFGenes),$synteny);

$homoInfo = $service->orfIDs2Info(array_keys($homo2curr));

$orthologs=[];
$species = [];
foreach(array_keys($curr2homo) as $orfid){
	$orf = $aORFGenes[$orfid]['orf'];
	foreach($curr2homo[$orfid] as $k=>$homoorfid){
		$sp = $homoInfo[$homoorfid]['species'];
		if (!in_array($sp,$species)) $species[] = $sp;
		$orthologs[$orf][$sp][] = $homoorfid;
	}
}

if (isset($_POST['set'])) $species = array_diff(explode(",",$_POST['set']),$currentSpecies);

?>
<table border="0" class="center" width="70%" summary="main content">
	<tr><td class="align">
<?php
include_once('inc/head.inc.php');
Head::printUnknown($unknown);

$web->printSyntenyWarn($synteny);

$csv = "";
if (count($aORFGenes) > 0) {
	$csv .= current($currentSpecies);
	foreach ($species as $s) {
		$csv .= "\t$s";
	}
	$csv .= "\n";
    $link = "view.php?existing=locus&amp;orfname=";
?>
    <table border="1" summary="ORF/Gene">
			<tr>
				<th align="center" colspan="2"><?=implode("<br/>",$currentSpecies)?></th>
<?php foreach ($species as $s) { ?>
				<th align="center"><?=$s?></th>
<?php } ?>
			</tr>
			<tr>
				<th>Inserted List</th>
				<th class="center">ORF/Gene&nbsp;name</th>
<?php foreach ($species as $s) { ?>
				<th class="center">ORF/Gene&nbsp;name</th>
<?php } ?>
      </tr>
<?php 
	foreach (array_keys($aORFGenes) as $i) {
		$o = $aORFGenes[$i]['orf'];
		$g = $aORFGenes[$i]['gene'];
		$csv .= $web->Gene($g,$o);
		$og = $web->Gene($g,$o, true);
?>
	  <tr>
	    <td class="align"><?=$aORFGenes[$i]['user']?></td>
			<td class="align"><a href="<?=$link.$o?>"><?=$og?></a></td>
<?php		foreach ($species as $s) {
					$txt = "";
					$csv .= "\t";
					if (isset($orthologs[$o]) && isset($orthologs[$o][$s])) {
						foreach ($orthologs[$o][$s] as $k=>$z) {
							$hg = $homoInfo[$z]['gene'];
							$ho = $homoInfo[$z]['orf'];
							$txt2 = $web->linkGene($hg,$ho,true,$s);
							if ($txt!=""){ 
								$txt .= "<br/> $txt2";
								$csv .= " ";
							} else {
								$txt = $txt2;
							}
							$csv .= $web->Gene($hg,$ho);
						}
					}
?>
			<td class="align"><?=$txt?></td>
<?php   }
$csv .= "\n";
?>
	  </tr>
<?php   
} ?>
	</table>
<?php
} else {
	print "<p><span class=\"error\">There wasn't any valid orf name on the list!</span></p>";
}
?>
	</td></tr>
	<tr><td class="align">
	<input type="reset" value="Go Back" onclick="javascript:history.back()" />
<?php
if (count($aORFGenes) > 0) {
		$link = $service->writeCSV("orthologs", $csv);
		print "<small>Download as <a href=\"$link\">csv</a> file.</small>";
	}
?>
	</td></tr>
	</table>
<?php
$web->printFooter();
$web->closePage();
?>
