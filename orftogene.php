<?php
include_once('inc/HeaderFooter.php');
$web = new HeaderFooter("no");
$web->printHeader("Home", "index.php",
"ORF List <-> Gene List", "javascript:history.back()",
"Result", "");
$web->printBarMenu("ORF List &hArr; Gene List");

include_once('service/Logic.php');
$service = new Logic(
  $web->getProp("db.$web->_dbname.dbspecies"),
  $web->getProp("db.$web->_dbname.dbstrains"));

$localSpecies = $service->aSpecies();
list($aORFGenes, $unknown) = $service->user2Info($_POST['orfs'],$localSpecies,true);

?>
<table border="0" class="center" width="70%" summary="main content">
	<tr><td class="align">
<?php
include_once('inc/head.inc.php');
Head::printUnknown($unknown);

if (count($aORFGenes) > 0) {
	$csv = "";
	$input = "";
    $orfs  = "";
    $genes = "";
    $link = "<a href=\"view.php?existing=locus&amp;orfname=";
    foreach (array_keys($aORFGenes) as $i) {
	    $input .= "<tr>
		    <td class=\"align\">". $aORFGenes[$i]['user']."</td>
		    <td class=\"align\">". $link . $aORFGenes[$i]['orf'] . "\">".$aORFGenes[$i]['orf']."</a></td>
				<td class=\"align\">".$link . $aORFGenes[$i]['orf'] . "\">".$aORFGenes[$i]['gene']."</a></td>
				<td class=\"align\">" . $aORFGenes[$i]['desc'] . "</td></tr>";
	    $aTGs[] = $aORFGenes[$i]['gene'];
//	    $orfs  .= "<td class=\"align\">".$link . $aORFGenes[$i]['orf'] . "\">".$aORFGenes[$i]['orf']."</a><td/>";
//	    $genes .= "<td class=\"align\">".$link . $aORFGenes[$i]['orf'] . "\">".$aORFGenes[$i]['gene']."</a></td></tr>";
	    $csv .= $aORFGenes[$i]['user']."\t".$aORFGenes[$i]['orf']."\t".$aORFGenes[$i]['gene']."\t".$aORFGenes[$i]['desc']."\n";
    }
?>
    <table border="1" summary="ORF/Gene">
      <tr>
	<th>Inserted List</th>
	<th>ORF Name</th>
	<th>Gene Name</th>
	<th>Description</th>
      </tr>
	    <?=$input?>
</td></tr>
	</table>
<?php
$tmppath = $web->getProp("path.tmp.local");
$fname = "orftogene_".rand() . ".csv";
file_put_contents($tmppath . $fname, $csv);
$lcsv = $web->getProp("path.tmp.url") . $fname;
?>
        </td></tr>
        <tr><td class="align">
        <input type="reset" value="Go Back" onclick="javascript:history.back()" />
<small>Download as <a href="<?=$lcsv?>">csv</a> file.</small>
</td></tr>
        </table>

<?php
} else {
	print "<p><span class=\"error\">There wasn't any valid orf name on the list!</span></p>
	<tr><td class=\"align\">
	<input type=\"reset\" value=\"Go Back\" onclick=\"javascript:history.back()\" /></td></tr>
        </table>";
}
$web->printFooter();
$web->closePage();
?>
